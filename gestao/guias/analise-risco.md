---
title: Análise de Riscos
description: Recomendações para Análise de Risco
date: 13/04/2022
keywords: analise, risco, serviços, apps, componente, atividade
---

A **Análise de Riscos** deve fazer parte da rotina de planejamento estratégico de todo produto. Por meio dessa prática, é possível evitar diversos tipos de problemas.

## A importância da Análise de Riscos

Qualquer ação humana está sujeita a riscos. Se você sai na rua, está sujeito ao risco de sofrer um acidente. Porém, sabendo disso, você toma medidas para reduzir essas ameaças, como andar com mais atenção, fechar o vidro do carro nos semáforos e não carregar muito dinheiro na carteira.

Além disso, você provavelmente tem um plano de emergência, caso essas medidas não funcionem: um parente para o qual você pode pedir ajuda ou o número da polícia na discagem rápida do telefone.

No contexto do Design System, a realidade é muito parecida, pois qualquer decisão ou atividade traz algum tipo de risco.

{{% notice info %}}
Por isso, antes de agir, é preciso analisar quais são as ameaças prováveis e traçar estratégias para reduzir a possibilidade de que elas ocorram — além de determinar medidas para lidar com o pior cenário.
{{% /notice %}}

## Como fazer uma Análise de Riscos

A seguir, apresentamos os quatro passos básicos de uma análise de riscos. Por meio dessas etapas, é possível identificar os riscos e traçar estratégias para lidar com eles da melhor maneira.

### 1. Determine a situação a ser analisada

Uma análise de riscos é sempre feita a respeito de uma situação específica. Evite fazer uma única análise para várias decisões ou para atividades diferentes.

Imagine, por exemplo, um componente complexo que utiliza vários subcomponentes. Incontáveis mudanças estão em jogo aqui — cada uma com suas próprias consequências. Assim, é necessário desmembrar a demanda nas diferentes ações que o compõem a fim de avaliar os riscos de cada uma.

### 2. Defina os possíveis resultados negativos da situação elencada

Nessa etapa, você deve se perguntar: o que pode dar errado?

Liste todas as possibilidades negativas que a demanda pode causar.

### 3. Estipule o grau de cada risco

Agora é hora de determinar o grau de cada risco.

Existe uma fórmula matemática para isso: calcule a probabilidade de ocorrência vezes a gravidade do resultado. Cada um desses fatores é representado por um número, dentro de uma escala (por exemplo, 1 – 3 – 5).

Essa técnica permite priorizar as ações para riscos de **alto grau** — aqueles que têm alta probabilidade de ocorrer e, ao mesmo tempo, geram resultados mais graves. Enquanto isso, os riscos que dificilmente vão acontecer, e aqueles que geram resultados de **baixa gravidade**, podem ser analisados posteriormente.

### 4. Defina suas estratégias

Defina o que você vai fazer a respeito do risco identificado. Tradicionalmente, são reconhecidos cinco tipos de estratégias: **aceitar**, **evitar**, **transferir**, **mitigar** e **explorar**.

Veja cada uma delas em detalhes.

#### Aceitar riscos

Significa que você só vai fazer alguma coisa se — e quando — o risco se concretizar. Normalmente, essa estratégia é usada para os riscos de menor grau. Afinal, a chance de acontecer e o impacto são pequenos. Portanto, não vale a pena gastar tempo — e dinheiro — com essas ameças menores.

#### Evitar riscos

É exatamente o oposto da estratégia anterior. Significa mudar totalmente o plano de ação para não ter nenhuma chance de encarar a ameaça em questão. É uma alternativa mais adequada para riscos de alto grau.

#### Transferir riscos

A ideia por trás dessa estratégia é transferir o risco para uma outra demanda — algo que pode ser feito quando você trabalha em um projeto que envolve diferentes partes. É uma prática mais comum do que parece. Essencialmente, todo processo de terceirização implica em transferir os riscos da atividade para o terceiro que a executa. Isto é, outra parte fica responsável por erros, atrasos, falhas de qualidade e acidentes.

#### Mitigar riscos

Basicamente, envolve tentativas de reduzir o grau do risco. Não é uma estratégia com garantia de sucesso, como no caso de conseguir evitar completamente os riscos, mas é menos drástica. Portanto, é uma boa alternativa quando a mudança de planos representa uma perda muito grande ou, simplesmente, não é possível.

#### Explorar riscos

Essa estratégia é usada quando temos um risco de **impacto positivo** (isto é, a possibilidade de que algo bom aconteça). Por isso, é focada em aumentar o grau do risco — para aproveitar todas as oportunidades que podem surgir.

Normalmente, não trabalhamos apenas com uma dessas estratégias, mas com um conjunto. Dessa maneira, é possível blindar a decisão ou a atividade contra as ameaças, aumentando as chances de sucesso. A garantia nunca é total, mas a meta deve ser atingir o maior patamar de segurança possível, a fim de preservar a qualidade do produto.
