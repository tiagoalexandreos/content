---
title: Critérios de Análise Qualitativa dos Componentes
description: Critérios de Análise Qualitativa dos Componentes
date: 14/12/2021
keywords: critérios, análise, qualitativa, componente, documentação, componentes, regras, acessibilidade, implementados, forma, padrão, página, construção, código, design
---

## Arquitetura

1. O componente foi construído seguindo as regras de semântica indicadas pela w3c?
1. O componente foi construído de forma otimizada, separação de responsabilidades?
1. O uso dos mixins estão corretos?
1. As regras de estilos seguem o padrão definido no Design System na página construção de componentes?
1. Os tokens forma consumidos de forma correta e otimizada?
1. JavaScript segue a arquitetura/padrões definidos na página construção de componentes?
1. O código pug está seguindo o padrão definido na página construção de componentes?

## Acessibilidade

1. O componente implementa as regras de acessibilidade indicadas na documentação de Design?
1. O componente segue as regras de acessibilidade quanto ao uso do wai-aria?
1. A validação de acessibilidade automatizada passou com sucesso?
1. Foi possível utilizar o componente apenas com o teclado?
1. É possível utilizar o componente em dispositivos móveis?

## Usabilidade

1. Os exemplos foram implementados seguindo todas os aspectos de usabilidade definidos pelo Designer?

## Layout

1. A anatomia do componente segue exatamente o que está previsto na diretriz de Design?
1. O layout do componente foi implementado conforme anatomia definida?
1. Os fundamentos visuais e padrões foram aplicados corretamente?

## Documentação

1. A documentação do componente segue o [padrão estabelecido]({{% relref "desenvolvimento/guias/documentacao-de-desenvolvedor.md" %}})?
2. A documentação dev está atualizada e de acordo com o exemplo  implementado?

## Funcionalidades

1. Os tipos, variações e comportamentos definidos na documentação de Design foram todos implementados? Caso não tenham sido implementados quais exemplos ainda faltam serem implementados?

## Qualidade

1. O componente Possui teste funcional?
1. O componente possui testes de regressão visual?
1. O componente funciona bem nas múltiplas resoluções?
1. O componente funciona bem quando o conteúdo interno é maior que o previsto?

## Documentação Código

1. O código(SASS, JS, PUG) está documentando?
2. A documentação está fácil de ser entendida?
