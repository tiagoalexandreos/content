---
title: Critérios de Classificação
description: Critérios de Classificação dos Componentes
date: 26/03/2021
keywords: critérios, análise, classificação, componente, tipos, componentes, complexidade, esforço, acessibilidade, comportamentos, responsividade, dependências, criticidade, impacto
---

## Complexidade

Esforço para mater o componente.

- Anatomia: quantidade de elementos internos do componente
- Tipos: quantidades de tipos do componente
- Funcionalidades

   1. Estados
   1. Validações
   1. Acessibilidade
   1. Dark mode
   1. Comportamentos

- Responsividade
- Dependências

   1. Interna
   2. Externa

## Criticidade

Impacto que o componente causa no Design System.

- Baixo: **até 1 componente**
- Médio: **de 2 a 3 componentes** ou **templates**
- Alta: **acima de 3 componentes**
