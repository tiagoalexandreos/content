---
title: Magic Button
description: Critérios de Analise e Classificação do Componente Magic Button
date: 25/03/2021
keywords: critérios, análise, classificação, superfície, magic button, complexidade, anatomia, tipos, comportamentos, posicionamento, densidade, estados,dependências, criticidade, impactos, componentes
---

### Complexidade

- Anatomia:

   1. Superfície de apoio (?)
   2. Superfície
   3. Label
   4. Ícone

- Tipos:

   1. Pílula
   2. Circular

- Comportamentos:

   1. [Posicionamento](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/docs/components/magicbutton/magicbutton-dsg.md#3-posicionamento)
   1. [Densidade](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/docs/components/magicbutton/magicbutton-dsg.md#4-densidade)
   1. [Estados](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/docs/components/magicbutton/magicbutton-dsg.md#5-estados)

- Dependências:

   1. [Button](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/src/components/magicbutton/magicbutton.pug#L9)

### Criticidade **Baixa**

Impactos: **0 componentes**
