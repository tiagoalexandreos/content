---
title: Avatar
description: Critérios de Analise e Classificação do Componente Avatar
date: 25/03/2021
keywords: critérios, análise, classificação, componentes, avatar, anatomia, criticidade, complexidade, densidade
---

### Complexidade

- Anatomia:

   1. [Superfície circular](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/docs/components/avatar/avatar-dsg.md#1-superf%C3%ADcie-circular)
   2. [Representação do usuário](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/docs/components/avatar/avatar-dsg.md#2-representa%C3%A7%C3%A3o-do-usu%C3%A1rio)

- Tipos

   1. [Icônico](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/docs/components/avatar/avatar-dsg.md#1-avatar-ic%C3%B4nico)
   2. [Fotográfico](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/docs/components/avatar/avatar-dsg.md#2-avatar-fotogr%C3%A1fico)
   3. [Letra](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/docs/components/avatar/avatar-dsg.md#3-avatar-letra)

- Comportamentos:

   1. [Densidades](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/docs/components/avatar/avatar-dsg.md#1-densidades)

### Criticidade **Média**

Impactos: **2 componentes**

#### Card

- PUG: <https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/src/components/card/card.pug#L79>

#### Header

- PUG: <https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/src/components/header/header.pug#L124>
