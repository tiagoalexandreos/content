---
title: Upload
description: Critérios de Analise e Classificação do Componente Upload
date: 25/03/2021
keywords: critérios, análise, classificação, upload, complexidade, anatomia, elementos, comportamentos, aplicação, estados, interactive, hover, disabled, focus, pressed, error, success, adição, arrastar, feedbacks, mensagens, sistema, truncamento, texto, resolução, colunas, criticidade, impactos, componentes
---

### Complexidade

- Anatomia: [8 elementos](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/docs/components/upload/upload-dsg.md#anatomia)
- Comportamentos:

   1. Aplicação dos estados [(interactive, hover, disabled, focus. pressed, error, success)](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/docs/components/upload/upload-dsg.md#2-outros-estados)
   1. [Adição de Arquivos](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/docs/components/upload/upload-dsg.md#1-adi%C3%A7%C3%A3o-de-arquivos)
   1. [Arrastar Arquivos](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/docs/components/upload/upload-dsg.md#3-selecionar-ou-arrastar-arquivos)
   1. [Feedbacks de mensagens do sistema](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/docs/components/upload/upload-dsg.md#4-mensagem-de-erro)
   1. [Truncamento de texto](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/docs/components/upload/upload-dsg.md#5-truncamento-de-texto)
   1. [Envio de 1 arquivo](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/docs/components/upload/upload-dsg.md#6-envio-de-1-arquivo)
   1. [Resolução de 4 colunas](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/docs/components/upload/upload-dsg.md#7-resolu%C3%A7%C3%A3o-de-4-colunas)

- Dependências:

   1. [Button](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/src/components/upload/_upload.scss#L6)
   1. [Item](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/src/components/upload/_upload.scss#L95)
   1. [Loading](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/src/components/upload/upload.js#L186)
   1. [Message](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/src/components/upload/upload.js#L107)
   1. [Tooltip](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/src/components/upload/upload.js#L214)

### Criticidade **Baixa**

Impactos: **0 componentes**
