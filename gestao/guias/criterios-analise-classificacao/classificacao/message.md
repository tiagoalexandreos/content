---
title: Message
description: Critérios de Analise e Classificação do Componente Message
date: 25/03/2021
keywords: critérios, análise, classificação, message, complexidade, anatomia, elementos, contextual, comportamentos, responsividade, superfície, posicionamento, estados, informativo, dependências, criticidade, impactos, componentes
---

### Complexidade

- Anatomia: [5 elementos](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/docs/components/message/message-dsg.md#anatomia)
- Tipos

   1. [Padrão](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/docs/components/message/message-dsg.md#1-mensagem-tipo-padr%C3%A3o)
   1. [Contextual](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/docs/components/message/message-dsg.md#2-mensagem-tipo-contextual)

- Comportamentos:

   1. [Responsividade](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/docs/components/message/message-dsg.md#1-responsividade)
   1. [Superfície do Message x Quebra de Linha](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/docs/components/message/message-dsg.md#2-superf%C3%ADcie-do-message-x-quebra-de-linha)
   1. [Posicionamento](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/docs/components/message/message-dsg.md#3-posicionamento)
   1. Estados
       - [Informativo](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/docs/components/message/message-dsg.md#estado-informativo)
       - [Sucesso](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/docs/components/message/message-dsg.md#estado-sucesso)
       - [Alerta](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/docs/components/message/message-dsg.md#estado-alerta)
       - [Erro](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/docs/components/message/message-dsg.md#estado-erro)
   1. Dependências:
       - [Button](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/tree/main/src/components/button)

### Criticidade **Alta**

Impactos: **6 componentes**

#### Checkbox

- PUG: <https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/src/components/checkbox/checkbox.pug#L47>

#### Input

- PUG: <https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/src/components/input/input.pug#L47>

#### Radio

- PUG: <https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/src/components/radio/radio.pug#L35>

#### Select

- PUG: <https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/src/components/select/select.pug#L35>

#### Upload

- PUG: <https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/src/components/upload/upload.pug#L36>
- JS: <https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/src/components/upload/upload.js#L71>
