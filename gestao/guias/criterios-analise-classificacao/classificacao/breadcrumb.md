---
title: Breadcrumb
description: Critérios de Analise e Classificação do Componente Breadcrumb
date: 25/03/2021
keywords: critérios, análise, classificação, componentes, breadcrumb, anatomia, comportamento, criticidade, complexidade, densidade
---

### Complexidade

- Anatomia:

   1. Botão Terciário Home
   2. Separador
   3. Link
   4. Título da página atual

- Comportamentos:

   1. [Truncamento](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/docs/components/breadcrumb/breadcrumb-dsg.md#truncamento)
   1. [Especial](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/docs/components/breadcrumb/breadcrumb-dsg.md#2-especial)
   1. [Abrir Tooltip](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/docs/components/breadcrumb/breadcrumb-dsg.md#2-truncamento-nos-hiperlinks)

- Dependências:

   1. [Button](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/src/components/breadcrumb/breadcrumb.pug#L6)
   1. [Card](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/src/components/breadcrumb/breadcrumb.js#L16)
   1. [Tooltip](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/src/components/breadcrumb/breadcrumb.pug#L37)

### Criticidade **Baixa**

Impactos: **0 componentes**
