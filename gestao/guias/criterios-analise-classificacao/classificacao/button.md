---
title: Button
description: Critérios de Analise e Classificação do Componente Button
date: 26/03/2021
keywords: critérios, análise, classificação, complexidade, anatomia, elementos, tipos, padrão, comportamentos, ênfases, densidades, estados, criticidade,impactos, componentes
---

### Complexidade

- Anatomia: [4 elementos](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/docs/components/button/button-dsg.md#anatomia)
- Tipos:

   1. [Padrão](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/docs/components/button/button-dsg.md#1-bot%C3%A3o-padr%C3%A3o)
   2. [Circular](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/docs/components/button/button-dsg.md#2-bot%C3%A3o-circular)

- Comportamentos:

   1. [Ênfases](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/docs/components/button/button-dsg.md#%C3%AAnfases)
   2. [Loading](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/docs/components/button/button-dsg.md#4-componente-loading)
   3. [Bloco](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/docs/components/button/button-dsg.md#1-bloco)
   4. [Densidades](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/docs/components/button/button-dsg.md#2-densidade)
   5. [Toggle](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/docs/components/button/button-dsg.md#4-comportamento-toggle)
   6. [Estados](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/docs/components/button/button-dsg.md#6-estados)
   7. [Modo para fundos escuros](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/docs/components/button/button-dsg.md#%C3%AAnfases)

### Criticidade **Alta**

Impactos: **21 componentes**

#### Breadcrumb

- PUG: <https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/src/components/breadcrumb/breadcrumb.pug#L6>
- JS: <https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/src/components/breadcrumb/breadcrumb.js#L71>

#### Card

- PUG: <https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/src/components/card/card.pug#L79>
- SCSS: <https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/src/components/card/_card.scss#L131>
- JS: <https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/src/components/card/card.js#L17>

#### Cookiebar

- PUG: <https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/src/components/cookiebar/cookiebar.pug#L13>
- SCSS: <https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/src/components/cookiebar/_cookiebar.scss#L11>
- JS: <https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/src/components/cookiebar/cookiebar.js#L53>

#### Datetimepicker

- PUG: <https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/src/components/datetimepicker/datetimepicker.pug#L4>
- JS: <https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/src/components/datetimepicker/datetimepicker.js#L41>

#### Header

- PUG: <https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/src/components/header/header.pug#L56>
- SCSS: <https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/src/components/header/_header.scss#L132>

#### Input

- PUG: <https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/src/components/input/input.pug#L20>
- SCSS: <https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/src/components/input/_input.scss#L9>

#### Item

- PUG: <https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/src/components/item/item.pug#L19>
- SCSS: <https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/src/components/item/_item.scss#L103>

#### List

- PUG: <https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/src/components/list/list.pug#L255>
- SCSS: <https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/src/components/list/_list.scss#L33>

#### Loading

- PUG: <https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/src/components/loading/loading.pug#L2>

#### Magic Button

- PUG: <https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/src/components/magicbutton/magicbutton.pug#L7>
- SCSS: <https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/src/components/magicbutton/_magicbutton.scss#L32>

#### Menu

- PUG: <https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/src/components/menu/menu.pug#L60>
- SCSS: <https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/src/components/menu/_menu.scss#L325>

#### Message

- PUG: <https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/src/components/message/message.pug#L1>

#### Modal

- PUG: <https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/src/components/modal/modal.pug#L9>

#### Pagination

- PUG: <https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/src/components/pagination/pagination.pug#L7>
- SCSS: <https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/src/components/pagination/_pagination.scss#L136>

#### Select

- PUG: <https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/src/components/input/input.pug#L23>
- SCSS: <https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/src/components/select/_select.scss#L7>
- JS: <https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/src/components/select/select.js#L53>

#### Signin

- PUG: <https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/src/components/signin/signin.pug#L1>
- SCSS: <https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/src/components/signin/_signin.scss#L1>

#### Table

- PUG: <https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/src/components/table/table.pug#L13>
- SCSS: <https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/src/components/table/_table.scss#L251>

#### Tag

- PUG: <https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/src/components/tag/examples.pug#L10>
- SCSS: <https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/src/components/tag/_tag.scss#L65>
- JS: <https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/src/components/tag/tag.js#L52>

#### Tooltip

- PUG: <https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/src/components/tooltip/tooltip.pug#L61>

#### Upload

- SCSS: <https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/src/components/upload/_upload.scss#L6>
- JS: <https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/src/components/upload/upload.js#L47>

#### Wizard

- PUG: <https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/src/components/wizard/wizard.pug#L31>
- SCSS: <https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/src/components/wizard/_wizard.scss#L263>
