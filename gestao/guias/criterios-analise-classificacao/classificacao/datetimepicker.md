---
title: DateTimePicker
description: Critérios de Analise e Classificação do Componente DateTimePicker
date: 25/03/2021
keywords: critérios, análise, classificação, datetimepicker, datas, complexidade, anatomia, elementos, tipos,comportamentos, navegação, dependências, criticidade, impactos, componentes
---

### Complexidade

- Anatomia: [8 elementos](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/docs/components/datetimepicker/datetimepicker-dsg.md#anatomia)
- Tipos:

   1. [DateTimePicker](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/docs/components/datetimepicker/datetimepicker-dsg.md#1-datetimepicker)
   2. [DatePicker](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/docs/components/datetimepicker/datetimepicker-dsg.md#2-datepicker)
   3. [TimePicker](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/docs/components/datetimepicker/datetimepicker-dsg.md#3-timepicker)

- Comportamentos:

   1. [Navegação entre as datas](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/docs/components/datetimepicker/datetimepicker-dsg.md#1-navega%C3%A7%C3%A3o-entre-as-datas)
   1. [Intervalo de datas](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/docs/components/datetimepicker/datetimepicker-dsg.md#2-intervalo-de-datas)
   1. [Estados](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/docs/components/datetimepicker/datetimepicker-dsg.md#3-estados)

- Dependências:

   1. [Button](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/src/components/datetimepicker/datetimepicker.pug#L4)
   1. [Input](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/src/components/datetimepicker/datetimepicker.pug#L3)
   1. [flatpickr](https://flatpickr.js.org/) (**Dependência externa**)

### Criticidade **Baixa**

Impactos: **0 componentes**
