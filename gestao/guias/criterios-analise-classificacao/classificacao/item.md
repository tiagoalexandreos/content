---
title: Item
description: Critérios de Analise e Classificação do Componente Item
date: 25/03/2021
keywords: critérios, análise, classificação, item, complexidade, anatomia, área, conteúdo, comportamentos, estados,rotulagem, criticidade, impactos, componentes
---

### Complexidade

- Anatomia

   1. [Área de conteúdo](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/docs/components/item/item-dsg.md#1-superf%C3%ADcie)
   2. Divider (Opcional)

- Comportamentos:

   1. [Direção](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/docs/components/item/item-dsg.md#2-alinhamento-e-espa%C3%A7amento)
   2. [Estados](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/docs/components/item/item-dsg.md#estados)
   3. [Expansão](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/docs/components/item/item-dsg.md#4-expans%C3%A3o)
   4. [Rotulagem](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/docs/components/item/item-dsg.md#5-rotulagem)

### Criticidade **Alta**

Impactos: **8 componentes**

#### Card

- PUG: <https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/src/components/card/card.pug#L51>
- SCSS: <https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/src/components/card/_card.scss#L126>

#### Footer

- PUG: <https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/src/components/footer/footer.pug#L12>
- SCSS: <https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/src/components/footer/_footer.scss#L9>

#### Header

- PUG: <https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/src/components/header/header.pug#L83>
- SCSS: <https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/src/components/header/_header.scss#L156>

#### List

- PUG: <https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/src/components/list/list.pug#L26>
- SCSS: <https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/src/components/list/_list.scss#L14>
- JS: <https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/src/components/list/list.js#L9>

#### Modal

- PUG: <https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/src/components/modal/modal.pug#L112>

#### Notification

- PUG: <https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/src/components/notification/notification.pug#L38>
- SCSS: <https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/src/components/notification/_notification.scss#L44>
- JS: <https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/src/components/notification/notification.js#L41>

#### Pagination

- PUG: <https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/src/components/pagination/pagination.pug#L49>
- SCSS: <https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/src/components/pagination/_pagination.scss#L131>

#### Table

- PUG: <https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/src/components/table/table.pug#L17>
