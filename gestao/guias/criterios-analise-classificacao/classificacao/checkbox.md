---
title: Checkbox
description: Critérios de Analise e Classificação do Componente Checkbox
date: 24/03/2021
keywords: critérios, análise, classificação, checkbox, complexidade, anatomia, elementos, comportamentos, aplicação, estados, disposição, horizontal, vertical, dependências, criticidade, impactos, componentes
---

### Complexidade

- Anatomia: [5 elementos](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/docs/components/checkbox/checkbox-dsg.md#anatomia)
- Comportamentos:

   1. Aplicação dos estados

- [default](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/docs/components/checkbox/checkbox-dsg.md#1-n%C3%A3o-selecionado-estado-padr%C3%A3o)
- [hover](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/docs/components/checkbox/checkbox-dsg.md#2-hover)
- [checked](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/docs/components/checkbox/checkbox-dsg.md#3-selecionado)
- [focus](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/docs/components/checkbox/checkbox-dsg.md#4-foco)
- [invalid](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/docs/components/checkbox/checkbox-dsg.md#5-inv%C3%A1lido)
- [valid](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/docs/components/checkbox/checkbox-dsg.md#6-v%C3%A1lido)
- [disabled](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/docs/components/checkbox/checkbox-dsg.md#7-desabilitado)

   1. Disposição

- [horizontal](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/docs/components/checkbox/checkbox-dsg.md#disposi%C3%A7%C3%A3o-vertical)
- [vertical](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/docs/components/checkbox/checkbox-dsg.md#disposi%C3%A7%C3%A3o-horizontal)

- Dependências:

   1. [Message](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/src/components/checkbox/checkbox.pug#L62)

### Criticidade **Alta**

Impactos: **4 componentes**

#### Item

- PUG: <https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/src/components/item/item.pug#L27>
- SCSS: <https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/src/components/item/_item.scss#L45>
- JS: <https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/src/components/item/item.js#L15>

#### List

- PUG: <https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/src/components/list/list.pug#L51>
- JS: <https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/src/components/list/list.js#L30>

#### Select

- PUG: <https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/src/components/select/select.pug#L16>
- SCSS: <https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/src/components/select/_select.scss#L93>
- JS: <https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/src/components/select/select.js#L28>

#### Table

- PUG: <https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/src/components/table/table.pug#L54>
- SCSS: <https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/src/components/table/_table.scss#L357>
- JS: <https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/src/components/table/table.js#L60>
