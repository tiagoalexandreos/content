---
title: Radio
description: Critérios de Analise e Classificação do Componente Radio
date: 25/03/2021
keywords: critérios, análise, classificação, radio, complexidade, anatomia, elementos, comportamentos, aplicação, estados, default, hover, selected, focused, invalid, valid, disabled, espaçamento, alinhamento, dependências, criticidade, impactos, componentes
---

### Complexidade

- Anatomia: [5 elementos](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/docs/components/radio/radio-dsg.md#anatomia)
- Comportamentos:

   1. [Aplicação dos estados](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/docs/components/radio/radio-dsg.md#estados)

- default
- hover
- delected
- focused
- invalid
- valid
- disabled

   1. [Espaçamento e Alinhamento](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/docs/components/radio/radio-dsg.md#espa%C3%A7amento-e-alinhamento)

- Dependências:

   1. [Message](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/src/components/message)

### Criticidade **Média**

Impactos: **3 componentes**

#### Item

- PUG: <https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/src/components/item/item.pug#L33>
- SCSS: <https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/src/components/item/_item.scss#L47>
- JS: <https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/src/components/item/item.js#L33>

#### Modal

- PUG: <https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/src/components/modal/modal.pug#L78>

#### Select

- PUG: <https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/src/components/select/select.pug#L26>
- SCSS: <https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/src/components/select/_select.scss#L111>
- JS: <https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/src/components/select/select.js#L32>
