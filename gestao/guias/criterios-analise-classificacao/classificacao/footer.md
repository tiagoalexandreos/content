---
title: Footer
description: Critérios de Analise e Classificação do Componente Footer
date: 25/03/2021
keywords: critérios, análise, classificação, footer, complexidade, anatomia, elementos, tipos, comportamentos, dependências, criticidade, impactos, componentes, templates
---

### Complexidade

- Anatomia: [13 elementos](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/docs/components/footer/footer-dsg.md#anatomia)
- Tipos

   1. Escuro
   2. Claro

- Comportamentos:

   1. [Expansão](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/docs/components/footer/footer-dsg.md#comportamento-de-expans%C3%A3o-de-list)

- Dependências:

   1. [Divider](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/src/components/footer/footer.pug#L34)
   1. [Item](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/src/components/footer/footer.pug#L12)
   1. [List](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/src/components/footer/footer.pug#L33)

### Criticidade **Média**

Impactos:

- **0 componentes**
- Usado para criação de templates
