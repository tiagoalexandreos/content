---
title: Wizard
description: Critérios de Analise e Classificação do Componente Wizard
date: 24/03/2021
keywords: critérios, análise, classificação, wizard, horizontal, vertical, slide, complexidade, anatomia, elementos, tipos, comportamentos, aplicação, estados, interactive, hover, pressed, active, disabled, focus, dark-mode, densidade, scroll, dependências, criticidade, impactos,componentes
---

Complexidade:

- Anatomia: [5 elementos](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/docs/components/wizard/wizard-dsg.md#anatomia)
- Tipos:
- [horizontal](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/docs/components/wizard/wizard-dsg.md#slide-tipo-horizontal)
- [vertical](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/docs/components/wizard/wizard-dsg.md#2-tipo-vertical)
- Comportamentos:

   1. Aplicação dos estados

- [interactive](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/docs/components/wizard/wizard-dsg.md#estado-interativo)
- [hover](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/docs/components/wizard/wizard-dsg.md#estado-hover)
- [pressed](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/docs/components/wizard/wizard-dsg.md#estado-pressionado)
- [active](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/docs/components/wizard/wizard-dsg.md#estado-ativo)
- [disabled](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/docs/components/wizard/wizard-dsg.md#estado-desabilitado)
- [focus](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/docs/components/wizard/wizard-dsg.md#estado-foco)

   1. Dark-mode
   1. Etapas representadas por:

- [ícones](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/docs/components/wizard/wizard-dsg.md#6-uso-de-%C3%ADcones)

   1. Densidade:

- [Alta](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/docs/components/wizard/wizard-dsg.md#alta-densidade)
- [Padrão](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/docs/components/wizard/wizard-dsg.md#densidade-padr%C3%A3o)
- [Baixa](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/docs/components/wizard/wizard-dsg.md#baixa-densidade)

   1. Slide

- [Slide Vertical](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/docs/components/wizard/wizard-dsg.md#slide-tipo-vertical)
- [Horizontal](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/docs/components/wizard/wizard-dsg.md#slide-tipo-horizontal)  

   1. [Controle de Exibição do Número de Etapas](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/docs/components/wizard/wizard-dsg.md#3-controle-de-exibi%C3%A7%C3%A3o-do-n%C3%BAmero-de-etapas)
   1. [Scroll para visualizar etapas ocultas](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/docs/components/wizard/wizard-dsg.md#4-scroll)

- Dependências:

   1. [Button](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/src/components/button)

### Criticidade: **Baixa**

Impactos: **0 componentes**
