---
title: Tab
description: Critérios de Analise e Classificação do Componente Tab
date: 24/03/2021
keywords: critérios, análise, classificação, tab, anatomia, elementos, comportamentos, responsividade, navegação, swipe, densidade, estados, ativo, hover, focus, pressionado, criticidade, impactos, componente
---

- Anatomia: [6 elementos](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/docs/components/tab/tab-dsg.md#anatomia)
- Comportamentos:

   1. [Responsividade](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/docs/components/tab/tab-dsg.md#1-responsividade)
   1. [Navegação por Swipe](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/docs/components/tab/tab-dsg.md#2-navega%C3%A7%C3%A3o-por-swipe)
   1. [Densidade](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/docs/components/tab/tab-dsg.md#4-densidade)
   1. Estados

- [Ativo](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/docs/components/tab/tab-dsg.md#estado-ativo)
- [Hover](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/docs/components/tab/tab-dsg.md#estado-hover)
- [Focus](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/docs/components/tab/tab-dsg.md#estado-focus)
- [Pressionado](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/docs/components/tab/tab-dsg.md#estado-pressionado)

### Criticidade **Baixa**

Impactos: **1 componente**

- Notification
- PUG: <https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/src/components/notification/notification.pug#L27>
- SCSS: <https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/src/components/notification/_notification.scss#L38>
