---
title: Select
description: Critérios de Analise e Classificação do Componente Radio
date: 25/03/2021
keywords: critérios, análise, classificação, select, multiselect, complexidade, anatomia, elementos, tipos, comportamentos, aplicação, estados, interativo, foco, hover, selecionado, destacado, abertura, dimensões, dependências, criticidade, impactos, componentes
---

### Complexidade

- Anatomia: [4 elementos](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/docs/components/select/select-dsg.md#anatomia)
- Tipos:
- [Select](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/docs/components/select/select-dsg.md#tipo-select)
- [Multiselect](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/docs/components/select/select-dsg.md#tipo-multiselect)
- Comportamentos:

   1. Aplicação dos estados

- [interativo](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/docs/components/select/select-dsg.md#estado-interativo)
- [foco](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/docs/components/select/select-dsg.md#estado-foco)
- [hover](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/docs/components/select/select-dsg.md#estado-hover)
- [selecionado](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/docs/components/select/select-dsg.md#estado-selecionado)
- [destacado](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/docs/components/select/select-dsg.md#estado-destacado)

   1. Seleção dos Itens

- [Seleção de itens no select](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/docs/components/select/select-dsg.md#sele%C3%A7%C3%A3o-de-itens-no-select)
- [Seleção de itens no Multiselect](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/docs/components/select/select-dsg.md#sele%C3%A7%C3%A3o-de-itens-no-multiselect)
- [Seleção de todos os itens no multiselect](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/docs/components/select/select-dsg.md#sele%C3%A7%C3%A3o-de-todos-os-itens-no-multiselect)
- [Filtro no select e multiselect](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/docs/components/select/select-dsg.md#filtro-no-select-e-multiselect)

   1. [Abertura do select e multiselect](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/docs/components/select/select-dsg.md#2-abertura-do-select-e-multiselect)
   1. [Responsividade](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/docs/components/select/select-dsg.md#3-responsividade)
   1. [Dimensões mínimas e máximas](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/docs/components/select/select-dsg.md#5-dimens%C3%B5es-m%C3%ADnimas-e-m%C3%A1ximas)

- Dependências:

   1. [Input](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/src/components/input)
   2. [Button](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/src/components/button/)
   3. [List](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/src/components/list)
   4. [Checkbox](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/src/components/checkbox)
   5. [Radio](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/src/components/radio)
   6. [Message](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/src/components/message)

### Criticidade **Média**

Impactos: **2 componentes**

#### Modal

- PUG: <https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/src/components/modal/modal.pug#L94>

#### Pagination

- PUG: <https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/src/components/pagination/pagination.pug#L135>
- SCSS: <https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/src/components/pagination/_pagination.scss#L47>
