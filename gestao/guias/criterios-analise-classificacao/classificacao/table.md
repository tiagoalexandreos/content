---
title: Table
description: Critérios de Analise e Classificação do Componente Table
date: 24/03/2021
keywords: critérios, análise, classificação, table, complexidade, anatomia, elementos, tipos, mobile, comportamentos, estados, interativo, hover, selecionado, foco, responsividade, rolagem, vertical, horizontal, redimensionamento, collapse, seleção, ordenação, filtros, densidade,busca, carregando, dependências, criticidade, impactos, componentes
---

### Complexidade

- Anatomia: [7 elementos](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/docs/components/table/table-dsg.md#anatomia)
- Tipos
    1. [Padrão](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/docs/components/table/table-dsg.md#table-tipo-padr%C3%A3o)
    1. [Mobile](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/docs/components/table/table-dsg.md#table-tipo-mobile)
- Comportamentos:

   1. Estados

- [Interativo](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/docs/components/table/table-dsg.md#estado-interativo)
- [Hover](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/docs/components/table/table-dsg.md#estado-hover)
- [Selecionado](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/docs/components/table/table-dsg.md#estado-selecionado)
- [Foco](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/docs/components/table/table-dsg.md#estado-foco)

   1. [Responsividade](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/docs/components/table/table-dsg.md#1-responsividade)
   1. Rolagem

- [Vertical](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/docs/components/table/table-dsg.md#rolagem-vertical)
- [Horizontal](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/docs/components/table/table-dsg.md#rolagem-horizontal-1)

   1. [Coluna Fixa](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/docs/components/table/table-dsg.md#3-coluna-fixa)
   1. [Redimensionamento de Coluna](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/docs/components/table/table-dsg.md#4-redimensionamento-de-coluna)
   1. [Expandir e Collapse](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/docs/components/table/table-dsg.md#5-expandir-e-collapse)
   1. [Seleção de Linhas](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/docs/components/table/table-dsg.md#6-sele%C3%A7%C3%A3o-de-linhas)
   1. [Ordenação](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/docs/components/table/table-dsg.md#7-ordena%C3%A7%C3%A3o)
   1. [Filtros](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/docs/components/table/table-dsg.md#8-filtros)
   1. [Densidade](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/docs/components/table/table-dsg.md#9-densidade)
   1. [Busca](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/docs/components/table/table-dsg.md#10-busca)
   1. [Carregando de Dados](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/docs/components/table/table-dsg.md#11-carregando-de-dados)

- Dependências:

   1. [Button](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/src/components/button)
   1. [List](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/src/components/list)
   1. [Item](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/src/components/item)
   1. [Input](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/src/components/input)
   1. [Divider](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/src/components/divider)
   1. [Checkbox](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/src/components/checkbox)
   1. [Pagination](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/src/components/pagination)

### Criticidade **Baixa**

Impactos: **0 componentes**
