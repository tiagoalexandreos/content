---
title: Pagination
description: Critérios de Analise e Classificação do Componente Notification
date: 24/03/2021
keywords: critérios, análise, classificação, notification, complexidade, anatomia, elementos, comportamentos, responsividade, dependências, criticidade, impactos, componente
---

### Complexidade

- Anatomia: [6 elementos](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/docs/components/pagination/pagination-dsg.md#anatomia)
- Tipos:
- [Padrão](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/docs/components/pagination/pagination-dsg.md#tipo-padr%C3%A3o)
- [Contextual](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/docs/components/pagination/pagination-dsg.md#tipo-contextual)
- Comportamentos:

   1. [Responsividade](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/docs/components/pagination/pagination-dsg.md#1-responsividade)
   1. [Posição e Alinhamento](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/docs/components/pagination/pagination-dsg.md#2-posi%C3%A7%C3%A3o-e-alinhamento)
   1. [Visualização dos Módulos](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/docs/components/pagination/pagination-dsg.md#3-visualiza%C3%A7%C3%A3o-dos-m%C3%B3dulos)
   1. [Número de Páginas Customizável](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/docs/components/pagination/pagination-dsg.md#4-n%C3%BAmero-de-p%C3%A1ginas-customiz%C3%A1vel)
   1. [Uso do Botão Reticências](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/docs/components/pagination/pagination-dsg.md#5-uso-do-bot%C3%A3o-retic%C3%AAncias)
   1. [Número de caracteres](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/docs/components/pagination/pagination-dsg.md#6-n%C3%BAmero-de-caracteres)
   1. [Densidade](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/docs/components/pagination/pagination-dsg.md#7-densidade)
   1. Aplicação dos estados

- [iterativo](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/docs/components/pagination/pagination-dsg.md#estado-interativo)
- [hover](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/docs/components/pagination/pagination-dsg.md#estado-hover)
- [ativo](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/docs/components/pagination/pagination-dsg.md#estado-ativo)
- [pressionado](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/docs/components/pagination/pagination-dsg.md#estado-pressionado)

- Dependências:

   1. [Button](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/src/components/button)
   2. [List](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/src/components/list)
   3. [Item](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/src/components/item)
   4. [Select](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/src/components/select)
   5. [Divider](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/src/components/divider)

### Criticidade **Baixa**

Impactos: **1 componente**

#### Table

- PUG: <https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/src/components/table/table.pug#L54>
- SCSS: <https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/src/components/table/_table.scss#L357>
- JS: <https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/src/components/table/table.js#L60>
