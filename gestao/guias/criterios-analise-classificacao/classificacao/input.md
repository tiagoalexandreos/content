---
title: Input
description: Critérios de Analise e Classificação do Componente Input
date: 25/03/2021
keywords: critérios, análise, classificação, input, complexidade, anatomia, elementos, comportamentos, responsividade,densidade, estados, dependências, criticidade, impactos, componentes
---

### Complexidade

- Anatomia: [6 elementos](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/docs/components/input/input-dsg.md#anatomia)
- Comportamentos:

   1. [Responsividade](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/docs/components/input/input-dsg.md#1-responsividade)
   2. [Densidade](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/docs/components/input/input-dsg.md#2-densidade)
   3. [Estados](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/docs/components/input/input-dsg.md#3-estados)
   4. [Botão de ação](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/docs/components/input/input-dsg.md#3-bot%C3%A3o-circular-opcional)
   5. [Dark mode](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/docs/components/input/input-dsg.md#3-estados)

- Dependências:

   1. [Message](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/src/components/message)

### Criticidade **Alta**

Impactos: **6 componentes**

#### DateTimePicker

- PUG: <https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/src/components/datetimepicker/datetimepicker.pug#L3>
- SCSS: <https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/src/components/datetimepicker/_datetimepicker.scss#L5>

#### Header

- PUG: <https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/src/components/header/header.pug#L49>
- SCSS: <https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/src/components/header/_header.scss#L360>
- JS: <https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/src/components/header/header.js#L7>

#### Modal

- PUG: <https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/src/components/modal/modal.pug#L98>

#### Pagination

- SCSS: <https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/src/components/pagination/_pagination.scss#L48>

#### Select

- PUG: <https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/src/components/select/select.pug#L6>
- SCSS: <https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/src/components/select/_select.scss#L6>
- JS: <https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/src/components/select/select.js#L55>

#### Table

- PUG: <https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/src/components/table/table.pug#L29>
- SCSS: <https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/src/components/table/_table.scss#L245>
