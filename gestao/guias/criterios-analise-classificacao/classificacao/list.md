---
title: List
description: Critérios de Analise e Classificação do Componente List
date: 25/03/2021
keywords: critérios, análise, classificação, list, complexidade, anatomia, elementos, tipos, vertical, horizontal,comportamentos, densidades, alturas, agrupamento, responsividade, expansão, ordenação, dependências, item, criticidade, impactos, componentes
---

### Complexidade

- Anatomia: [5 elementos](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/docs/components/list/list-dsg.md#anatomia)
- [Tipos:](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/docs/components/list/list-dsg.md#tipos)

   1. Vertical
   2. Horizontal

- Comportamentos:

   1. [Densidades e Alturas](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/docs/components/list/list-dsg.md#densidades-e-alturas)
   1. [Agrupamento](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/docs/components/list/list-dsg.md#agrupamento)
   1. [Responsividade](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/docs/components/list/list-dsg.md#responsividade)
   1. [Expansão](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/docs/components/list/list-dsg.md#expans%C3%A3o)
   1. [Ordenação](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/docs/components/list/list-dsg.md#ordena%C3%A7%C3%A3o)

- Dependências:
- [Item](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/tree/main/src/components/item)
- [Divider](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/tree/main/src/components/divider)
- [Checkbox](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/tree/main/src/components/checkbox)
- [Button](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/tree/main/src/components/button)

### Criticidade **Alta**

Impactos: **7 componentes**

#### Card

- PUG: <https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/src/components/card/card.pug#L50>
- SCSS: <https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/src/components/card/_card.scss#L125>

#### Cookiebar

- PUG: <https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/src/components/cookiebar/cookiebar.pug#L12>
- JS: <https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/src/components/cookiebar/cookiebar.js#L260>

#### Footer

- PUG: <https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/src/components/footer/footer.pug#L33>
- SCSS: <https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/src/components/footer/_footer.scss#L81>

#### Header

- PUG: <https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/src/components/header/header.pug#L81>
- SCSS: <https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/src/components/header/_header.scss#L128>

#### Breadcrumb

- SCSS: <https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/src/components/breadcrumb/_breadcrumb.scss#L76>
- JS: <https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/src/components/breadcrumb/breadcrumb.js#L112>

#### Select

- PUG: <https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/src/components/select/select.pug#L12>
- SCSS: <https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/src/components/select/_select.scss#L18>
- JS: <https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/src/components/select/select.js#L30>

#### Table

- PUG: <https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/src/components/table/table.pug#L16>
- SCSS: <https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/src/components/table/_table.scss#L407>
- JS: <https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/src/components/table/table.js#L218>
