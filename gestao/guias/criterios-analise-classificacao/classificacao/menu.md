---
title: Menu
description: Critérios de Analise e Classificação do Componente Menu
date: 25/03/2021
keywords: critérios, análise, classificação, menu, agrupamento, complexidade, anatomia, elementos, comportamentos, estados, interativo, desabilitado, itens, expansão, rótulos, navegação, densidade, dependências, criticidade, impactos, componentes
---

### Complexidade

- Anatomia: [16 elementos](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/docs/components/menu/menu-dsg.md#anatomia)
- Tipos:

   1. [Principal](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/docs/components/menu/menu-dsg.md#menu-principal)
   1. [Auxiliar](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/docs/components/menu/menu-dsg.md#menu-auxiliar)

- Comportamentos:

   1. Estados

- [interativo](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/docs/components/menu/menu-dsg.md#estado-interativo)
- [hover](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/docs/components/menu/menu-dsg.md#estado-hover)
- [foco](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/docs/components/menu/menu-dsg.md#estado-foco)
- [desabilitado](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/docs/components/menu/menu-dsg.md#estado-desabilitado)
- [ativo](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/docs/components/menu/menu-dsg.md#estado-ativo)

   1. [Acionamento/Fechamento](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/docs/components/menu/menu-dsg.md#1-acionamento-fechamento)
   1. [Rolagem](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/docs/components/menu/menu-dsg.md#2-rolagem)
   1. Agrupamento de itens

- [Agrupamento por Expansão](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/docs/components/menu/menu-dsg.md#agrupamento-por-expans%C3%A3o)
- [Agrupamento por Rótulos](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/docs/components/menu/menu-dsg.md#agrupamento-por-r%C3%B3tulos)
- [Agrupamento por Dividers](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/docs/components/menu/menu-dsg.md#agrupamento-por-dividers)

   1. [Navegação em subníveis](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/docs/components/menu/menu-dsg.md#4-navega%C3%A7%C3%A3o-em-subn%C3%ADveis)
   1. [Responsividade](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/docs/components/menu/menu-dsg.md#5-responsividade)
   1. [Densidade](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/docs/components/menu/menu-dsg.md#7-densidade)
   1. Dependências:

- [Button](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/tree/main/src/components/button)
- [Divider](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/tree/main/src/components/divider)

### Criticidade **Média**

Impactos: **2 componentes**

#### Header

- PUG: <https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/src/components/header/header.pug#L60>

#### Template Base

- PUG: <https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/src/partial/pug/base.pug#L14>
- SCSS: <https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/src/partial/scss/templates/_template-base.scss#L19>
