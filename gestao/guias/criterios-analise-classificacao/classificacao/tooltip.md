---
title: Tooltip
description: Critérios de Analise e Classificação do Componente Tooltip
date: 25/03/2021
keywords: critérios, análise, classificação, tooltip, complexidade, anatomia, elementos, comportamentos, estados, informativo, sucesso, alerta, erro, direção, setas, posicionamento, acionamento, foco, fechamento, largura, conteúdo, criticidade, impacto, componentes
---

### Complexidade

- Anatomia: [5 elementos](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/docs/components/tooltip/tooltip-dsg.md#anatomia)
- Comportamentos:

   1. Aplicação dos estados

- [Informativo](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/docs/components/tooltip/tooltip-dsg.md#estado-informativo)
- [Sucesso](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/docs/components/tooltip/tooltip-dsg.md#estado-sucesso)
- [Alerta](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/docs/components/tooltip/tooltip-dsg.md#estado-alerta)
- [Erro](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/docs/components/tooltip/tooltip-dsg.md#estado-erro)

   1. [Direção das setas e posicionamento do tooltip](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/docs/components/tooltip/tooltip-dsg.md#2-dire%C3%A7%C3%A3o-das-setas-e-posicionamento-do-tooltip)
   1. [Acionamento do Tooltip (Mouse Over, Clique, Foco, Automaticamente)](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/docs/components/tooltip/tooltip-dsg.md#3-acionamento-do-tooltip)
   1. [Fechamento do tooltip (Mouse Out, Clique no botão fechar, Por Tempo Determinado)](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/docs/components/tooltip/tooltip-dsg.md#4-fechamento-do-tooltip)
   1. [Largura do tooltip x Conteúdo interno](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/docs/components/tooltip/tooltip-dsg.md#5-largura-do-tooltip-x-conte%C3%BAdo-interno)

### Criticidade **Média**

Impacto: **3 componentes**

#### Breadcrumb

- PUG: <https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/src/components/breadcrumb/breadcrumb.pug#L37>

#### Tag

- PUG: <https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/src/components/tag/examples.pug#L73>

#### Upload

- JS: <https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/src/components/upload/upload.js#L1>
