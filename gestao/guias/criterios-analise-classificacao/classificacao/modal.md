---
title: Modal
description: Critérios de Analise e Classificação do Componente Modal
date: 24/03/2021
keywords: critérios, análise, classificação, modal, complexidade, anatomia, elementos, tipos, entrada, comportamentos, interrupção, posição, scroll, temporizador, dependências, criticidade, impactos, componente
---

### Complexidade

- Anatomia: [6 elementos](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/docs/components/modal/modal-dsg.md#anatomia)
- Tipos:
- [Alerta](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/docs/components/modal/modal-dsg.md#1-modal-de-alerta)
- [Opção](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/docs/components/modal/modal-dsg.md#2-modal-de-op%C3%A7%C3%A3o)
- [Entrada](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/docs/components/modal/modal-dsg.md#3-modal-de-entrada)
- Comportamentos:

   1. [Interrupção](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/docs/components/modal/modal-dsg.md#interrup%C3%A7%C3%A3o)
   1. [Posição](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/docs/components/modal/modal-dsg.md#posi%C3%A7%C3%A3o)
   1. [Scroll bar (Barra de rolagem)](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/docs/components/modal/modal-dsg.md#scroll-bar-barra-de-rolagem)
   1. [Temporizador](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/docs/components/modal/modal-dsg.md#temporizador)

- Dependências:

   1. [Card](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/src/components/card)
   1. [Button](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/src/components/button)
   1. [Radio](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/src/components/radio)
   1. [Select](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/src/components/select)
   1. [Input](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/src/components/input)
   1. [List](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/src/components/list)
   1. [Item](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/src/components/item)
   1. [Divider](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/src/components/divider)

### Criticidade **Baixa**

Impactos: **1 componente**

#### Cookiebar

- PUG: <https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/src/components/cookiebar/cookiebar.pug#L4>
- SCSS: <https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/src/components/cookiebar/_cookiebar.scss#L87>
- JS: <https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/src/components/cookiebar/cookiebar.js#L108>
