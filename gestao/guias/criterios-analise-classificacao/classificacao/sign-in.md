---
title: Sign-in
description: Critérios de Analise e Classificação do Componente Sign-in
date: 25/03/2021
keywords: critérios, análise, classificação, sign-in, complexidade, anatomia, elementos, tipos, comportamentos, densidade, responsividade, dependências, criticidade, impactos, componentes
---

### Complexidade

- Anatomia: [4 elementos](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/docs/components/signin/signin-dsg.md#anatomia)
- [Tipos](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/docs/components/signin/signin-dsg.md#tipos)
- Comportamentos:

   1. [Densidade](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/docs/components/signin/signin-dsg.md#1-densidade)
   1. [Responsividade](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/docs/components/signin/signin-dsg.md#2-responsividade)

- Dependências:

   1. [Button](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/tree/main/src/components/button)

### Criticidade **Baixa**

Impactos: **1 componentes**

#### Header

- PUG: <https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/src/components/header/header.pug#L116>
