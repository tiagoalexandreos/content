---
title: Loading
description: Critérios de Analise e Classificação do Componente Loading
date: 25/03/2021
keywords: critérios, análise, classificação, determinado, componente, complexidade, anatomia, elementos, tipos, comportamentos, movimento, dependências, criticidade, impactos
---

### Complexidade

- Anatomia: [9 elementos](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/docs/components/loading/loading-dsg.md#anatomia)
- Tipos:

   1. Loading Determinado (Tamanho Único)
   1. Loading Indeterminado (Tamanho Padrão)
   1. Loading Indeterminado (Tamanho Pequeno)

- Comportamentos:
    1. Exibição
       - [Dentro ou associado a um componente](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/docs/components/loading/loading-dsg.md#dentro-ou-associado-a-um-componente)
       - [No corpo da página](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/docs/components/loading/loading-dsg.md#no-corpo-da-p%C3%A1gina)
       - [Bloqueando a tela ou parte dela](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/docs/components/loading/loading-dsg.md#bloqueando-a-tela-ou-parte-dela)

    1. [Movimento](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/docs/components/loading/loading-dsg.md#movimento)
        - Loading Indeterminado
        - Loading Determinado

- Dependências:

   1. [Button](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/src/components/button/button.pug)

### Criticidade **Baixa**

Impactos: Nenhum componente usa atualmente o loading
