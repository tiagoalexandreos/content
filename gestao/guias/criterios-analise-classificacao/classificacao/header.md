---
title: Header
description: Critérios de Analise e Classificação do Componente Header
date: 25/03/2021
keywords: critérios, análise, classificação, header, complexidade, anatomia, elementos, tipos, comportamentos, limites, caracteres, responsividade, densidades, dependências, componentes, templates
---

### Complexidade

- Anatomia: [13 elementos](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/docs/components/header/header-dsg.md#anatomia)
- Tipos

   1. [Padrão](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/docs/components/header/header-dsg.md#header-padr%C3%A3o)
   2. [Compacto](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/docs/components/header/header-dsg.md#header-compacto)

- Comportamentos:

   1. [Limites de caracteres](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/docs/components/header/header-dsg.md#quantidade-de-caracteres)
   2. [Dropdown](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/docs/components/header/header-dsg.md#11-lista-dropdown)
   3. [Toggle da busca](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/docs/components/header/header-dsg.md#10-busca)
   4. [Responsividade](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/docs/components/header/header-dsg.md#1-responsividade)
   5. [Densidades](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/docs/components/header/header-dsg.md#2-densidade)
   6. [Sticky](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/docs/components/header/header-dsg.md#6-sticky-header)

- Dependências:

   1. [Avatar](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/src/components/header/header.pug#L126)
   1. [Button](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/src/components/header/header.pug#L24)
   1. [Divider](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/src/components/header/header.pug#L87)
   1. [Input](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/src/components/header/header.pug#L49)
   1. [Item](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/src/components/header/header.pug#L83)
   1. [List](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/src/components/header/header.pug#L81)
   1. [Menu](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/src/components/header/header.pug#L43)
   1. [Notification](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/src/components/header/header.pug#L129)
   1. [Tag](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/src/components/header/header.pug#L133)

### Criticidade **Média**

- **0 componentes**
- Usado para criação de templates
