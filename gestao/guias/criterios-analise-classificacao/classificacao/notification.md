---
title: Notification
description: Critérios de Analise e Classificação do Componente Notification
date: 24/03/2021
keywords: critérios, análise, classificação, notification, complexidade, anatomia, elementos, comportamentos, responsividade, dependências, criticidade, impactos, componente
---

### Complexidade

- Anatomia: [6 elementos](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/docs/components/notification/notification-dsg.md#anatomia)
- Comportamentos:

   1. [Responsividade](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/docs/components/notification/notification-dsg.md#1-responsividade)
   1. [Rolagem Interna](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/docs/components/notification/notification-dsg.md#2-rolagem-interna)

- Dependências:

   1. [Tab](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/tree/main/src/components/tab)
   2. [List](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/tree/main/src/components/list)
   3. [Item](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/tree/main/src/components/item)
   4. [Divider](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/tree/main/src/components/divider)
   5. [Button](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/tree/main/src/components/button)

### Criticidade **Baixa**

Impactos: **1 componente**

#### Header

- PUG: <https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/src/components/header/header.pug#L129>
- SCSS: <https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/src/components/header/_header.scss#L137>
