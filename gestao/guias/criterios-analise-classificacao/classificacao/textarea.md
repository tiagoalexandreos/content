---
title: Textarea
description: Critérios de Analise e Classificação do Componente Textarea
date: 23/03/2021
keywords: critérios, análise, classificação, textarea, densidade, preenchimento, complexidade, anatomia, elementos, comportamentos, estados, interativo, foco, sucesso, erro, informativo, alerta, desabilitado, hover, responsividade, redimensionamento, contagem, caracteres, criticidade, impacto, componentes
---

### Complexidade

- Anatomia: [6 elementos](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/docs/components/textarea/textarea-dsg.md#anatomia)
- Comportamentos:

   1. Aplicação dos estados

- [Interativo](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/docs/components/textarea/textarea-dsg.md#estado-interativo)
- [Foco](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/docs/components/textarea/textarea-dsg.md#estado-foco)
- [Sucesso](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/docs/components/textarea/textarea-dsg.md#estado-sucesso)
- [Erro](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/docs/components/textarea/textarea-dsg.md#estado-erro)
- [Informativo](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/docs/components/textarea/textarea-dsg.md#estado-informativo)
- [Alerta](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/docs/components/textarea/textarea-dsg.md#estado-alerta)
- [Desabilitado](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/docs/components/textarea/textarea-dsg.md#estado-desabilitado)
- [Hover](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/docs/components/textarea/textarea-dsg.md#estado-hover)

   1. [Responsividade](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/docs/components/textarea/textarea-dsg.md#1-responsividade)
   1. [Redimensionamento](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/docs/components/textarea/textarea-dsg.md#2-redimensionamento)
   1. [Barra de Rolagem](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/docs/components/textarea/textarea-dsg.md#3-barra-de-rolagem)
   1. Contagem de Caracteres

- [Preenchimento com limite máximo](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/docs/components/textarea/textarea-dsg.md#1-preenchimento-com-limite-m%C3%A1ximo)
- [Preenchimento sem limite máximo de caractere](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/docs/components/textarea/textarea-dsg.md#2-preenchimento-sem-limite-m%C3%A1ximo-de-caractere)

   1. Densidade

- [Densidade Baixa](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/docs/components/textarea/textarea-dsg.md#a-densidade-baixa)
- [Densidade Média](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/docs/components/textarea/textarea-dsg.md#b-densidade-m%C3%A9dia)
- [Densidade Alta](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/docs/components/textarea/textarea-dsg.md#c-densidade-alta)

### Criticidade **Baixa**

Impacto: **0 componentes**
