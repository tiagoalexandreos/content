---
title: Divider
description: Critérios de Analise e Classificação do Componente Divider
date: 23/03/2021
keywords: critérios, análise, classificação, divider, complexidade, anatomia, elemento, comportamentos, posição, criticidade, impactos, componentes
---

### Complexidade

- Anatomia: 1 elemento
- Comportamentos:

   1. [Posição](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/docs/components/divider/divider-dsg.md#1-posi%C3%A7%C3%A3o)
   1. [Fundo](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/docs/components/divider/divider-dsg.md#2-fundo)

### Criticidade **Alta**

Impactos: **7 componentes**

#### Footer

- PUG: <https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/src/components/footer/footer.pug#L34>
- SCSS: <https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/src/components/footer/_footer.scss#L34>

#### Header

- PUG: <https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/src/components/header/header.pug#L77>

#### List

- PUG: <https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/src/components/list/list.pug#L25>
- SCSS: <https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/src/components/list/_list.scss#L53>

#### Modal

- PUG: <https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/src/components/modal/modal.pug#L106>

#### Notification

- PUG: <https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/src/components/notification/notification.pug#L41>

#### Pagination

- PUG: <https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/src/components/pagination/pagination.pug#L157>
- SCSS: <https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/src/components/pagination/_pagination.scss#L57>

#### Table

- PUG: <https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/src/components/table/table.pug#L45>
