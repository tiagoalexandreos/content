---
title: Switch
description: Critérios de Analise e Classificação do Componente Switch
date: 25/03/2021
keywords: critérios, análise, classificação, switch, complexidade, anatomia, elementos, comportamentos, dimensões, interativa, estados, criticidade, impactos, componente
---

### Complexidade

- Anatomia: [5 elementos](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/docs/components/switch/switch-dsg.md#anatomia)
- Comportamentos:

   1. [Dimensões](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/docs/components/switch/switch-dsg.md#dimens%C3%B5es)
   1. [Área interativa](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/docs/components/switch/switch-dsg.md#%C3%A1rea-interativa)
   1. [Estados](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/docs/components/switch/switch-dsg.md#estados)

### Criticidade **Baixa**

Impactos: **1 componente**

- Cookiebar
- SCSS: <https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/src/components/cookiebar/_cookiebar.scss#L36>
- JS: <https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/src/components/cookiebar/cookiebar.js#L385>
