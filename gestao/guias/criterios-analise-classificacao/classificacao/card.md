---
title: Card
description: Critérios de Analise e Classificação do Componente Card
date: 24/03/2021
keywords: critérios, análise, classificação, card, comportamentos, expansão, complexidade, anatomia, elementos, comportamento, padronização, criticidade, impactos, componentes
---

### Complexidade

- Anatomia: [2 elementos](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/docs/components/card/card-dsg.md#anatomia)
- Tipos
- Comportamentos:

   1. [Expansão](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/docs/components/card/card-dsg.md#1-expans%C3%A3o)
   2. [Estados](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/docs/components/card/card-dsg.md#2-estados)

Atenção! O comportamento de expansão deve ser reescrito para desvincular com o Card segundo a nova padronização de comportamentos

### Criticidade **Média**

Impactos: **3 componentes**

#### Cookiebar

- PUG: <https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/src/components/cookiebar/cookiebar.pug#L4>
- SCSS: <https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/src/components/cookiebar/_cookiebar.scss#L66>
- JS: <https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/src/components/cookiebar/cookiebar.js#L125>

#### Loading

- PUG: <https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/src/components/loading/loading.pug#L6>

#### Modal

- PUG: <https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/src/components/modal/modal.pug#L4>
- SCSS: <https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/src/components/modal/_modal.scss#L44>
