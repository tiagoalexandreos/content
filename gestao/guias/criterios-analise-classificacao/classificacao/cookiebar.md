---
title: Cookiebar
description: Critérios de Analise e Classificação do Componente Cookiebar
date: 25/03/2021
keywords: critérios, análise, classificação, cookiebar, complexidade, anatomia, variável, comportamentos, classes, cookies, dependências, criticidade, impactos, componentes
---

### Complexidade

- Anatomia: [variável](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/docs/components/cookiebar/cookiebar-dsg.md#anatomia)
- Comportamentos:

   1. [Opt In e Opt Out](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/docs/components/cookiebar/cookiebar-dsg.md#1-opt-in-e-opt-out)
   1. [Classes de cookies](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/docs/components/cookiebar/cookiebar-dsg.md#classes)
   1. [Expandir/Retrair](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/docs/components/cookiebar/cookiebar-dsg.md#card)
   1. [Responsividade](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/docs/components/cookiebar/cookiebar-dsg.md#responsividade)

- Dependências:

   1. [Button](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/src/components/cookiebar/cookiebar.pug#L14)
   1. [Card](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/src/components/cookiebar/cookiebar.js#L316)
   1. [Item](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/src/components/cookiebar/cookiebar.js#L410)
   1. [List](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/src/components/cookiebar/cookiebar.pug#L12)
   1. [Message](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/src/components/cookiebar/cookiebar.js#L335)
   1. [Modal](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/src/components/cookiebar/cookiebar.pug#L4)
   1. [Switch](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/src/components/cookiebar/cookiebar.js#L385)

### Criticidade **Baixa**

Impactos: **0 componentes**
