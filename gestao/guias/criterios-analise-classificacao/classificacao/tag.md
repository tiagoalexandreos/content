---
title: Tag
description: Critérios de Analise e Classificação do Componente Tag
date: 25/03/2021
keywords: critérios, análise, classificação, tag, complexidade, anatomia, elementos, tipos, interação, status, comportamentos, estados, densidade, dimensões, distribuição, horizontal, vertical, dependências, criticidade, impactos, componentes
---

### Complexidade

- Anatomia: [5 elementos](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/docs/components/tag/tag-dsg.md#anatomia)
- Tipos
- [interação](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/docs/components/tag/tag-dsg.md#1-tag-de-intera%C3%A7%C3%A3o)
- [texto](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/docs/components/tag/tag-dsg.md#2-tag-de-texto)
- [status](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/docs/components/tag/tag-dsg.md#3-tag-de-status)
- [contagem](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/docs/components/tag/tag-dsg.md#4-tag-de-contagem)
- [ícone](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/docs/components/tag/tag-dsg.md#5-tag-de-%C3%ADcone)
- Comportamentos:

   1. [Aplicação dos estados](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/docs/components/tag/tag-dsg.md#2-estados)
   1. [Densidade](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/docs/components/tag/tag-dsg.md#3-densidade)
   1. [Dimensões](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/docs/components/tag/tag-dsg.md#4-dimens%C3%B5es)
   1. [Distribuição horizontal ou vertical](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/docs/components/tag/tag-dsg.md#5-distribui%C3%A7%C3%A3o)

- Dependências

   1. [Button](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/src/components/tag/examples.pug#L18)
   1. [Tooltip](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/blob/main/src/components/tag/examples.pug#L64)

### Criticidade **Baixa**

Impactos: **0 componentes**
