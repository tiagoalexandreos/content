---
title: "Critérios de Análise e Classificação"
chapter: true
pre: '<i class="far fa-check-circle"></i> '
---

Essa seção é dedicada a conteúdos relacionados aos Critérios de Análise e Classificação do GOVBR-DS.
