---
title: " Fase de Descoberta"
chapter: true
pre: '<i class="fas fa-eye"></i>'
---

Investigamos informações, oportunidades e soluções que serão valiosas para os usuários.

## Fluxo Geral

- [Processo Geral do GOVBR-DS](/gestao/processos)

## Processos relacionados

- [Processo de descoberta de necessidade](/gestao/processos/descoberta/descoberta-necessidade)
