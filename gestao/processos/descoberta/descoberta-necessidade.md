---
title: Descoberta de necessidade
date: 2022-04-29T18:00:27-03:00
draft: false
---

![Processo de descoberta de necessidade](/gestao/processos/imagens/processo-de-descoberta-de-necessidade.png)

## Descrição

O designer e o desenvolvedor responsáveis pela demanda devem juntos propor uma solução que atenda as necessidades solicitadas pela demanda.

## Fase

- [Fase de Descoberta](/gestao/processos/descoberta)

## Processos pai

- [Processo geral do GOVBR-DS](/gestao/processos)

## Tarefas

- [Abrir issue](/gestao/processos/tarefas/abrir-issue)
- [Discovery](/gestao/processos/tarefas/discovery)
- [Propor novo componente](/gestao/processos/tarefas/propor-novo-componente)
- [Relatar problema](/gestao/processos/tarefas/relatar-problema)
- [Sugerir funcionalidade](/gestao/processos/tarefas/sugerir-funcionalidade)
