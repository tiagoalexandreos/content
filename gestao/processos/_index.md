---
title: 'Fluxo de Trabalho'
chapter: true
pre: '<i class="fas fa-project-diagram"></i> '
---

<h1>Fluxo de Trabalho</h1>
<p>O fluxo de trabalho do GOVBR-DS é estruturado em fases. Estão apresentados abaixo as seis fases. Cada um delas pode apresentar um ou vários processos.</p>
<img src="fluxo.png" alt="6 fases do fluxo de trabalho: Descoberta, Análise, Planejamento, Execução, Implantação e Métricas">
<p class="text-up-02 text-weight-semi-bold">Lista de Processos</p>
<div class="row">
  <div class="col-lg-4 mb-lg-3 d-lg-flex">
    <div class="br-card">
      <div class="card-content">
        <div class="bg-red-vivid-60 text-pure-0 text-up-02 text-weight-bold px-3 py-2">
          <i class="fas fa-eye" aria-hidden="true"></i>
          <span class="ml-3">Descoberta</span>
        </div>
        <p class="text-base"><em>Investigamos informações, oportunidades e soluções que serão valiosas para os usuários.</em></p>
        <ul class="text-base">
          <li><a href="/gestao/processos/descoberta/descoberta-necessidade/">Descoberta de necessidade</a></li>
        </ul>
      </div>
    </div>
  </div>
  <div class="col-lg-4 mb-lg-3 d-lg-flex">
    <div class="br-card">
      <div class="card-content">
        <div class="bg-blue-vivid-50 text-pure-0 text-up-02 text-weight-bold px-3 py-2">
          <i class="fas fa-lightbulb" aria-hidden="true"></i>
          <span class="ml-3">Análise</span>
        </div>
        <p class="text-base"><em>Fase de análise da demanda  e definição de riscos, valor, impactos etc.</em></p>
        <ul class="text-base">
          <li><a href="/gestao/processos/analise/analise-demanda/">Análise da demanda</a></li>
        </ul>
      </div>
    </div>
  </div>
  <div class="col-lg-4 mb-lg-3 d-lg-flex">
    <div class="br-card">
      <div class="card-content">
        <div class="bg-green-cool-vivid-50 text-pure-0 text-up-02 text-weight-bold px-3 py-2">
          <i class="fas fa-clipboard-list" aria-hidden="true"></i>
          <span class="ml-3">Planejamento</span>
        </div>
        <p class="text-base"><em>Na fase de planejamento planejamos todos os detalhes das releases e sprints.</em></p>
        <ul class="text-base">
          <li><a href="/gestao/processos/planejamento/formalizacao-demanda/">Formalização da demanda</a></li>
          <li><a href="/gestao/processos/planejamento/planejamento-release/">Planejamento da release</a></li>
          <li><a href="/gestao/processos/planejamento/planejamento-sprint/">Planejamento da sprint</a></li>
        </ul>
      </div>
    </div>
  </div>
  <div class="col-lg-4 mb-lg-3 d-lg-flex">
    <div class="br-card">
      <div class="card-content">
        <div class="bg-orange-vivid-40 text-pure-0 text-up-02 text-weight-bold px-3 py-2">
          <i class="fas fa-cog" aria-hidden="true"></i>
          <span class="ml-3">Execução</span>
        </div>
        <p class="text-base"><em>Durantes as sprints é que criamos, documentamos, e implementamos os artefatos.</em></p>
        <ul class="text-base">
          <li><a href="/gestao/processos/execucao/construcao-produto/">Construção do produto</a></li>
          <li><a href="/gestao/processos/execucao/conducao-release/">Condução da release</a></li>
          <li><a href="/gestao/processos/execucao/conducao-sprint/">Condução da sprint</a></li>
          <li><a href="/gestao/processos/execucao/desenvolvimento-artefatos/">Desenvolvimento dos artefatos</a></li>
        </ul>
      </div>
    </div>
  </div>
  <div class="col-lg-4 mb-lg-3 d-lg-flex">
    <div class="br-card">
      <div class="card-content">
        <div class="bg-magenta-vivid-50 text-pure-0 text-up-02 text-weight-bold px-3 py-2">
          <i class="fas fa-clock" aria-hidden="true"></i>
          <span class="ml-3">Implantação</span>
        </div>
        <p class="text-base"><em>Durante a fase de implantação publicamos os artefatos no site.</em></p>
        <ul class="text-base">
          <li><a href="/gestao/processos/implantacao/implantacao/">Implantação</a></li>
        </ul>
      </div>
    </div>
  </div>
  <div class="col-lg-4 mb-lg-3 d-lg-flex">
    <div class="br-card">
      <div class="card-content">
        <div class="bg-indigo-warm-vivid-70 text-pure-0 text-up-02 text-weight-bold px-3 py-2">
          <i class="fas fa-chart-line" aria-hidden="true"></i>
          <span class="ml-3">Métricas</span>
        </div>
        <p class="text-base"><em>Após implementar, acompanhamos e metrificamos como nossos artefatos estão perfomando.</em></p>
        <ul class="text-base">
          <li><a href="/gestao/processos/metricas/avaliacao-produto/">Avaliação do produto</a></li>
        </ul>
      </div>
    </div>
  </div>
</div>
