---
title: "Fase de Execução"
chapter: true
pre: '<i class="fas fa-cog"></i>'
---

Durante as sprints é que criamos, documentamos e implementamos os artefatos.

## Fluxo Geral

- [Processo Geral do GOVBR-DS](/gestao/processos)

## Processos relacionados

- [Processo de condução da release](/gestao/processos/execucao/conducao-release)
- [Processo de condução da sprint](/gestao/processos/execucao/conducao-sprint)
- [Processo de construção do produto](/gestao/processos/execucao/construcao-produto)
- [Processo de desenvolvimento dos artefatos](/gestao/processos/execucao/desenvolvimento-artefatos)
