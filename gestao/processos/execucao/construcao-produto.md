---
title: Construção do produto
date: 2022-04-29T18:30:59-03:00
draft: false
---

![Processo de construção do produto](/gestao/processos/imagens/processo-de-construção-do-produto.png)

## Descrição

O processo geral para a construção de um produto começa com o entendimento e a análise do problema. O time em conjunto discute as análises realizadas e prioriza quais demandas serão atendidas em uma release.

## Fase

- [Fase de Execução](/gestao/processos/execucao)

## Processos pai

- [Processo geral do GOVBR-DS](/gestao/processos)

## Sub-processos

- [Processo de análise da issue](/gestao/processos/analise/analise-demanda/)
- [Processo de condução da release](/gestao/processos/execucao/conducao-release)

## Tarefas

- [Finalizar issue](/gestao/processos/tarefas/finalizar-issue)
