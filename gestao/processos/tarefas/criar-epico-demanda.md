---
title: Criar épico da demanda
date: 2022-04-29T19:33:26-03:00
draft: false
---

## Descrição

Para maior organização das demandas no GitLab, crie épicos que conterão issues relacionadas.

## Processos relacionados

- [Processo de formalização da demanda](/gestao/processos/planejamento/formalizacao-demanda)

## Links úteis

- [](/)
