---
title: Design review
date: 2022-05-02T10:24:30-03:00
draft: false
---

## Descrição

**Design Review** é a etapa de implementação em que o designer responsável pela tarefa valida o trabalho do desenvolvedor.

## Processos relacionados

- [Processo de desenvolvimento dos artefatos](/gestao/processos/execucao/desenvolvimento-artefatos)

## Links úteis

- [](/)
