---
title: Entendimento com o desenvolvedor
date: 2022-05-02T10:06:55-03:00
draft: false
---

## Descrição

Caso seja necessário, ao se iniciar a fase de implementação, o desenvolvedor deve procurar o designer responsável pela criação da proposta e conjuntamente entender o que deverá ser trabalhado.

## Processos relacionados

- [Processo de desenvolvimento dos artefatos](/gestao/processos/execucao/desenvolvimento-artefatos)

## Links úteis

- [](/)
