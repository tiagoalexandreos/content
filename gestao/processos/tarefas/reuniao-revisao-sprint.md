---
title: Reuniao de revisao da sprint
date: 2022-05-02T09:34:48-03:00
draft: false
---

## Descrição

O objetivo da reunião da **Revisão da Sprint** é de se obter feedback do cliente sobre o incremento do produto gerado na sprint e, com isso, poder frequentemente fazer ajustes de direção, diminuindo os riscos do projeto.

## Processos relacionados

- [Processo de condução da sprint](/gestao/processos/execucao/conducao-sprint)

## Links úteis

- [](/)
