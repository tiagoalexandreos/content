---
title: Definir objetivo da release
date: 2022-04-29T19:23:06-03:00
draft: false
---

## Descrição

Definir o objetivo da release é o primeiro fator a ser considerado durante a etapa de planejamento da release.

## Processos relacionados

- [Processo de planejamento da release](/gestao/processos/planejamento/planejamento-release)

## Links úteis

- [](/)
