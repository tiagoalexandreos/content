---
title: Avaliar o valor da entrega
date: 2022-04-29T19:00:44-03:00
draft: false
---

## Descrição

Avaliação do valor de uma demanda trata-se de um dos quesitos de análise de uma demanda. É de extrema importância avaliar o valor pois essa informação orienta o planejamento da release e da sprint.

## Processos relacionados

- [Processo de análise da issue](/gestao/processos/analise/analise-demanda/)

## Links úteis

- [](/)
