---
title: Validar UIKIT
date: 2022-05-02T10:34:29-03:00
draft: false
---

## Descrição

O designer designado deve validar as alterações no UIKIT considerando se estão de acordo com o que foi proposto nos protótipos e diretrizes.

## Processos relacionados

- [Processo de desenvolvimento dos artefatos](/gestao/processos/execucao/desenvolvimento-artefatos)

## Links úteis

- [](/)
