---
title: Merge request
date: 2022-05-02T10:32:39-03:00
draft: false
---

## Descrição

Após finalizar um trabalho o desenvolvedor deve preparar o Merge Request e atribuir um responsável pela execução do mesmo.

## Processos relacionados

- [Processo de desenvolvimento dos artefatos](/gestao/processos/execucao/desenvolvimento-artefatos)

## Links úteis

- [](/)
