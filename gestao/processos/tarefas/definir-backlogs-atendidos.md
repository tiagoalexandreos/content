---
title: Definir quais backlogs serão atendidos
date: 2022-05-02T09:42:47-03:00
draft: false
---

## Descrição

No evento de planejamento da sprint, a equipe deve definir quais itens de backlog serão atendidos, considerando o planejamento da release.

## Processos relacionados

- [Processo de planejamento da sprint](/gestao/processos/planejamento/planejamento-sprint)

## Links úteis

- [](/)
