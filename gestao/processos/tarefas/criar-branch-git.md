---
title: Criar branch no GIT
date: 2022-05-02T10:35:58-03:00
draft: false
---

## Descrição

Para facilitar a organização do trabalho, crie branches específicas no GitLab relacionados ao assunto da tarefa.

## Processos relacionados

- [Processo de desenvolvimento dos artefatos](/gestao/processos/execucao/desenvolvimento-artefatos)

## Links úteis

- [](/)
