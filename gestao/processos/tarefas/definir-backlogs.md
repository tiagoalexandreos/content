---
title: Definir backlogs
date: 2022-04-29T19:24:20-03:00
draft: false
---

## Descrição

Ao se planejar uma release, defina os itens de backlog que serão trabalhados. Para isso, leve em consideração as análises das demandas criadas.

## Processos relacionados

- [Processo de planejamento da release](/gestao/processos/planejamento/planejamento-release)

## Links úteis

- [](/)
