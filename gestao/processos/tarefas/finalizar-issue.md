---
title: Finalizar issue
date: 2022-04-29T18:45:11-03:00
draft: false
---

## Descrição

Ao se finalizar uma demanda, a issue deve ser também finalizada.

Uma isseu pode ainda ser finalizada quando uma demanda for rejeitada ou cancelada.

## Processos relacionados

- [Processo de construção do produto](/gestao/processos/execucao/construcao-produto)

## Links úteis

- [](/)
