---
title: Criar tag de versão
date: 2022-05-02T13:25:14-03:00
draft: false
---

## Descrição

Tenha em mente que as tags auxiliam na organização das demandas no GitLab. Não esqueça de utilizá-las de forma correta.

## Processos relacionados

- [Processo de implantação](/gestao/processos/implantacao/implantacao)

## Links úteis

- [](/)
