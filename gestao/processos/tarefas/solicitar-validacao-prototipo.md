---
title: Solicitar validação do protótipo
date: 2022-05-02T10:29:21-03:00
draft: false
---

## Descrição

O designer deve após concluir a criação do protótipo, solicitar que o desenvolvedor responsável valide a proposta apresentada.

## Processos relacionados

- [Processo de desenvolvimento dos artefatos](/gestao/processos/execucao/desenvolvimento-artefatos)

## Links úteis

- [](/)
