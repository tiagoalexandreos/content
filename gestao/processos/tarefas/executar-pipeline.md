---
title: Executar pipeline
date: 2022-05-02T11:32:24-03:00
draft: false
---

## Descrição

Após enviar arquivos para o git o pipeline é executado automáticamente executando o build, testes de lint e publicação.

## Processos relacionados

- [Processo de desenvolvimento dos artefatos](/gestao/processos/execucao/desenvolvimento-artefatos)

## Links úteis

- [](/)
