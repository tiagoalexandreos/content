---
title: Idealizar solução
date: 2022-05-02T10:26:34-03:00
draft: false
---

## Descrição

Em conjunto, desenvolvedor e designer responsáveis pela demanda devem idealizar a solução que será prototipada pelo designer e implementada pelo desenvolvedor.

## Processos relacionados

- [Processo de desenvolvimento dos artefatos](/gestao/processos/execucao/desenvolvimento-artefatos)

## Links úteis

- [](/)
