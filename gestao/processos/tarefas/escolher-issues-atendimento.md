---
title: Escolher issues para atendimento
date: 2022-04-29T19:22:05-03:00
draft: false
---

## Descrição

Ao se planejar a release, a equipe deve escolher as demandas previamente analisadas considerando fatores estratégicos como prioridades, riscos, valor, etc.

## Processos relacionados

- [Processo de planejamento da release](/gestao/processos/planejamento/planejamento-release)

## Links úteis

- [](/)
