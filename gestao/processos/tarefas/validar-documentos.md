---
title: Validar documentos
date: 2022-05-02T10:33:18-03:00
draft: false
---

## Descrição

É o processo de validação da documentação visual ou de diretrizes apresentada por um designer.

## Processos relacionados

- [Processo de desenvolvimento dos artefatos](/gestao/processos/execucao/desenvolvimento-artefatos)

## Links úteis

- [](/)
