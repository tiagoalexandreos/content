---
title: Atualizar UIKIT
date: 2022-05-02T10:33:48-03:00
draft: false
---

## Descrição

Todo novo artefato ou alterações de artefatos já existentes no DS devem ser atualizados no UIKIT.

## Processos relacionados

- [Processo de desenvolvimento dos artefatos](/gestao/processos/execucao/desenvolvimento-artefatos)

## Links úteis

- [](/)
