---
title: Gerar ambiente de pré-produção
date: 2022-05-02T11:35:27-03:00
draft: false
---

## Descrição

O desenvolvedor deve gerar o ambiente de pré-produção.

## Processos relacionados

- [Processo de desenvolvimento dos artefatos](/gestao/processos/execucao/desenvolvimento-artefatos)

## Links úteis

- [](/)
