---
title: "Tarefas"
chapter: true
pre: '<i class="fas fa-tasks"></i>'
---

Essa seção é dedicada a conteúdos relacionados às **tarefas** do GOVBR-DS.
