---
title: Coletar feedbacks
date: 2022-04-29T18:49:06-03:00
draft: false
---

## Descrição

A equipe do DS deve sempre estimular os feedbacks internos e externos, assim como considerá-los no momento de análise e planejamento de demandas e, sempre que possível, retornar com mensagem de agradecimento.

## Processos relacionados

- [Processo de avaliação do produto](/gestao/processos/metricas/avaliacao-produto)

## Links úteis

- [](/)
