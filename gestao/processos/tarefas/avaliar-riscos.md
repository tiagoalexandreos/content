---
title: Avaliar riscos
date: 2022-04-29T19:00:34-03:00
draft: false
---

## Descrição

Avaliação de riscos trata-se de um dos quesitos de análise de uma demanda. É de extrema importância avaliar os riscos pois essa informação orienta o planejamento da release e da sprint.

## Processos relacionados

- [Processo de análise da issue](/gestao/processos/analise/analise-demanda/)

## Links úteis

- [](/)
