---
title: Propor solução
date: 2022-04-29T18:49:24-03:00
draft: false
---

## Descrição

Em conjunto, designer e desenvolvedor devem entender e propor a solução que melhor atenda as necessidades da demanda.

## Processos relacionados

- [Processo de avaliação do produto](/gestao/processos/metricas/avaliacao-produto)

## Links úteis

- [](/)
