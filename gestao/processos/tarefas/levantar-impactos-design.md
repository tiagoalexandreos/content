---
title: Levantar impactos de design
date: 2022-04-29T19:00:20-03:00
draft: false
---

## Descrição

Ao realizar a tarefa de análise da demanda é fundamental levantar todos os impactos previstos.

## Processos relacionados

- [Processo de análise da issue](/gestao/processos/analise/analise-demanda/)

## Links úteis

- [](/)
