---
title: "Fase de Implantação"
chapter: true
pre: '<i class="fas fa-clock"></i>'
---

Durante a fase de implantação publicamos os artefatos no site.

## Fluxo Geral

- [Processo Geral do GOVBR-DS](/gestao/processos)

## Processos relacionados

- [Processo de implantação](/gestao/processos/implantacao/implantacao)
