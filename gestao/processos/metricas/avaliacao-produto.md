---
title: Avaliação do produto
date: 2022-04-29T18:31:15-03:00
draft: false
---

![Processo de avaliação do produto](/gestao/processos/imagens/processo-de-avaliacao-do-produto.png)

## Descrição

Todo artefato, após ser implantado, deve ter sua performance acompanhada por meio das métricas que fizerem sentido para o projeto.

## Fase

- [Fase de Métricas](/gestao/processos/metricas)

## Processos pai

- [Processo geral do GOVBR-DS](/gestao/processos)

## Tarefas

- [Abrir issue](/gestao/processos/tarefas/abrir-issue)
- [Analisar resultados](/gestao/processos/tarefas/analisar-resultados)
- [Coletar feedbacks](/gestao/processos/tarefas/coletar-feedbacks)
- [Coletar métricas](/gestao/processos/tarefas/coletar-metricas)
- [Propor solução](/gestao/processos/tarefas/propor-solucao)
- [Reportar resultados](/gestao/processos/tarefas/reportar-resultados)
