---
title: "Fase de Planejamento"
chapter: true
pre: '<i class="fas fa-file-alt"></i>'
---

Na fase de planejamento, planejamos todos os detalhes das releases e sprints.

## Fluxo Geral

- [Processo Geral do GOVBR-DS](/gestao/processos)

## Processos relacionados

- [Processo de formalização da demanda](/gestao/processos/planejamento/formalizacao-demanda)
- [Processo de planejamento da release](/gestao/processos/planejamento/planejamento-release)
- [Processo de planejamento da sprint](/gestao/processos/planejamento/planejamento-sprint)
