---
title: Planejamento da release
date: 2022-04-29T19:18:31-03:00
draft: false
---

![Processo de planejamento da release](/gestao/processos/imagens/processo-de-planejamento-da-release.png)

## Descrição

Todas as etapas da fase de planejamento da release devem acontecer no evento **Planejamento da Release** e contar com a participação de todos os envolvidos no time. Nesta etapa o time em conjunto executa as seguintes tarefas:

### Priorização

Time seleciona as demandas para atendimento.

### Definição dos Objetivos

Time define os objetivos e meta da release.

### Definição do MVP

Time define o MVP (Minimum Viable Product ou produto mínimo viável) das demandas selecionadas.

### Definição dos Backlogs

Time define os itens de backlog da release.

### Estimação de Esforço

Time estima o esforço para o atendimento de cada item do backlog.

### Definição da Quantidade de Sprints

Time define a quantidade suficiente de sprints para o atendimento da release.

### Formalização dos Backlogs

Os itens de backlogs devem ser formalizados no GitLab. O backlog da release é considerado **preparado**.

## Fase

- [Fase de Planejamento](/gestao/processos/planejamento)

## Processos pai

- [Processo de condução da release](/gestao/processos/execucao/conducao-release)

## Sub-processos

- [Processo de formalização da demanda](/gestao/processos/planejamento/formalizacao-demanda)

## Tarefas

- [Escolher issues para atendimento](/gestao/processos/tarefas/escolher-issues-atendimento)
- [Estimar esforço](/gestao/processos/tarefas/estimar-esforco)
- [Definir backlogs](/gestao/processos/tarefas/definir-backlogs)
- [Definir MVP da release](/gestao/processos/tarefas/definir-mvp-release)
- [Definir objetivo da release](/gestao/processos/tarefas/definir-objetivo-release)
- [Definir quantidade de sprints (prazo)](/gestao/processos/tarefas/definir-quantidade-sprints)
