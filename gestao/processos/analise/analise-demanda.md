---
title: Análise da Demanda
date: 2022-04-29T18:54:30-03:00
draft: false
---

![Processo Analise Issue](/gestao/processos/imagens/processo-de-analise-da-issue.png)

## Descrição

Assim que uma demanda é criada, ela deve ser minuciosamente analisada e detalhada. No mínimo um designer e um desenvolvedor devem ficar responsáveis pela análise de uma demanda.

Vale lembrar que uma demanda pode ser originada internamente pela equipe do GOVBR-DS, por sugestão do usuário ou por solicitação de comitê específico.

## Fase

- [Fase de Análise](/gestao/processos/analise)

## Processos pai

- [Processo de construção do produto](/gestao/processos/execucao/construcao-produto)

## Tarefas

- [Avaliar o valor da entrega](/gestao/processos/tarefas/avaliar-valor-entrega)
- [Avaliar riscos](/gestao/processos/tarefas/avaliar-riscos)
- [Avaliar viabilidade](/gestao/processos/tarefas/avaliar-viabilidade)
- [Documentar motivo da rejeição](/gestao/processos/tarefas/documentar-motivo-rejeicao)
- [Entender a issue](/gestao/processos/tarefas/entender-issue)
- [Levantar impactos de código](/gestao/processos/tarefas/levantar-impactos-codigo)
- [Levantar impactos de design](/gestao/processos/tarefas/levantar-impactos-design)
- [Preparar issue para atendimento](/gestao/processos/tarefas/preparar-issue-atendimento)
- [Reportar dúvida](/gestao/processos/tarefas/reportar-duvida)
