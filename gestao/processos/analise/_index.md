---
title: "Fase de Análise"
chapter: true
pre: '<i class="fas fa-lightbulb"></i>'
---

Fase de análise da demanda e definição de riscos, valor, impactos, etc.

## Fluxo Geral

- [Processo Geral do GOVBR-DS](/gestao/processos)

## Processos relacionados

- [Processo de análise da demanda](/gestao/processos/analise/analise-demanda)
