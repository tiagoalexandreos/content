---
title: "Gestão"
chapter: true
weight: 1
pre: '<i class="fas fa-user-cog"></i> '
---

Caro integrante do time do Design System, aqui você encontrará tudo o que você precisa para auxiliar a sua jornada de trabalho.

Tenha em mente que este documento está em constante evolução. Portanto, revisite-nos de tempo em tempo para estar sempre atualizado com as últimas decisões de atividades de gestão no processo de trabalho.

Não hesite em entrar em contato conosco para sugerir melhorias ou reportar erros. Ficaremos contentes em poder ajudar.

Tenha uma ótima experiência no **GOVBR-DS**.
