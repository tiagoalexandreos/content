---
title: Análise de Demandas
description: Checklist para Análise de Demandas
date: 05/05/2022
keywords: analise, demandas, tarefas, colaboração
---

Sempre que uma demanda é criada, ela deve ser minuciosamente analisada e detalhada por pelo menos um desenvolvedor e um designer.

Vale lembrar que uma demanda pode ser originada internamente pela equipe do GOVBR-DS, por sugestão do usuário ou por solicitação do comitê.

- [ ] As necessidades da demanda estão claras?
- [ ] A demanda é viável?
- [ ] Os impactos foram levantados?
- [ ] Os riscos para a execução foram avaliados?
- [ ] Foi estipulado o valor para a demanda baseado nos critérios utilizados?
- [ ] A atividade possui alguma restrição para sua execução? Listar as restrições ou pendências para a demanda ser executada.
- [ ] A demanda foi formalizada no ALM e GitLab?
- [ ] Caso a demanda tenha sido rejeitada, foram devidamente documentados os motivos para a rejeição?
- [ ] A atividade possui alguma restrição para sua execução? Listar as restrições ou pendências a serem executadas.

---

## Impactos

**Impactos** são todos as alterações necessárias que a nova demanda acarreta em outros componentes, documentos ou artefatos em geral.

- [ ] A atividade pode trazer algum impacto no visual a ponto de gerar **breaking change**, a possibilidade de modificar a estrutura dos componentes existentes é alta?
- [ ] Quais impactos podem acontecer da não execução?
- [ ] Quais impactos afetam os serviços que consomem o Design System?
- [ ] Quais os impactos para os apps que consomem o Design System?
- [ ] Comunicar o resultado da análise para aprovação ou rejeição do time.
- [ ] Criar tarefas e issues de correção dos impactos listados com a descrição detalhada da análise.

Especifique os impactos encontrados:

- [ ] Há impacto visual nos componentes (dependência)? Listar os elementos impactados.
- [ ] Há impacto funcional nos componentes (dependência)? Listar os elementos impactados.
- [ ] Há impacto nas diretrizes? Listar os documentos impactados.
- [ ] Há impacto nos UIKits? Listar os UIKits impactados.
- [ ] Há impacto nos códigos? Listar os documentos impactados.
- [ ] Há impacto no site? Listar as páginas impactadas.

**Atenção:** caso a atividade traga algum impacto para o Design System ou para serviços/apps existentes o **comitê** deve ser notificado e sua execução dependerá da sua aprovação.

---

## Riscos

A **análise de riscos** deve fazer parte da rotina de planejamento estratégico de todo produto. Por meio dessa prática, é possível evitar diversos tipos de problemas.

- [ ] Existem riscos identificados para o projeto ou serviços? Listar os riscos.
- [ ] Listar todas as possibilidades negativas que a demanda pode causar.
- Qual o grau para cada risco?
- [ ] Baixo
- [ ] Médio
- [ ] Alto
- [ ] Listar as estratégias definidas para cada risco.

---

## Valor

A **análise de valor** deve considerar a relevância da demanda para o projeto. Quanto mais relevante, maior o valor.

- [ ] Qual o ganho com a execução dessa atividade para o usuário?
- [ ] Qual métrica poderá ser utilizada para aferir o valor entregue?
