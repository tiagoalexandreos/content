---
title: Homologação de Entregas
description: Checklist para Homologação de Entregas
date: 23/07/2021
keywords: erros, homologação, entregas, documentações, quebrados, componentes, renderizados, inconsistências, problemas,critérios, aceitação, qualidade
---

- [ ] Documentações sem erros de imagens, escrita ou links quebrados
- [ ] Componentes são renderizados corretamente
- [ ] Erros, inconsistências e problemas foram tratados
- [ ] Sem erros de navegação
- [ ] Os critérios de aceitação foram todos atendidos
- [ ] Todas as etapas de qualidade foram executadas
