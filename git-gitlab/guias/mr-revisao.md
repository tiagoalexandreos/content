---
title: 'MR - Revisão'
description: Guia sobre como revisar um MR
---

Para aceitar um MR (Merge Request) o revisor precisa garantir que ele atende aos requisitos da tarefa/issue.

Pode ser que além da revisão o MR necessite de alguma aprovação antes de ser aceito. Sugerimos que o revisor entre em contato com os aprovadores para informar da necessidade de aprovação.

{{% notice warning %}}
**Nunca** aprove um MR sem checar todos os itens que se aplicam ao MR. É **responsabilidade de todos** garantir a qualidade do produto final.
{{% /notice %}}

## Requisitos

### Checklist do revisor (reviewer)

```markdown
- [ ] Analisar se seguiu corretamente os padrões de qualidade para execução da atividade:
    - [ ] Caso seja um artefato de design, seguir para o checklist de revisão
    - [ ] Caso seja um artefato de desenvolvimento, verificar se o code review foi feito
- [ ] Caso exista algum ajuste após a revisão, especificar o que deve ser ajustado e adicionar a label de ajuste corretamente
- [ ] Caso tenha feito alguma sugestão ou apontando algum ajuste, retorne após os ajustes para realizar a aprovação ou resolver as discussões
- [ ] Caso esteja tudo correto, realize a aprovação
```

## Melhores Práticas para Revisões de Merge Requests

- **Compreenda o contexto**: antes de iniciar uma revisão, dedique algum tempo para compreender o contexto e os objetivos do Merge Request. Leia a descrição, as conversas relacionadas e os requisitos para ter uma visão geral completa das alterações propostas.
- **Verifique o código**: analise cuidadosamente o código modificado. Verifique a qualidade, a consistência e a aderência às boas práticas de programação. Identifique possíveis erros, bugs ou más práticas de código.
- **Teste as alterações**: se possível, execute o código alterado localmente e teste as alterações para garantir que funcionem conforme esperado. Verifique se os testes automatizados são executados com sucesso.
- **Forneça feedback claro e construtivo**: ao fornecer feedback no MR, seja claro, específico e construtivo. Identifique os problemas encontrados e sugira soluções ou melhorias. Evite críticas negativas ou ofensivas e mantenha um tom profissional.
- **Envolva-se em discussões saudáveis**: esteja aberto a discussões e debates construtivos com o autor do MR e outros revisores. Expresse suas opiniões e preocupações de maneira respeitosa e saudável. Seja colaborativo e incentive o diálogo para alcançar o melhor resultado possível.
- **Siga as diretrizes de estilo e padrões**: verifique se o código segue as diretrizes de estilo e padrões estabelecidos no projeto. Isso inclui a formatação do código, a nomenclatura de variáveis e funções, e outras convenções adotadas pela equipe.
- **Avalie a funcionalidade e a usabilidade**: além de verificar o código, avalie a funcionalidade e a usabilidade das alterações propostas. Verifique se as novas funcionalidades estão bem implementadas e se a experiência do usuário é intuitiva.
- **Leia a documentação**: verifique se o MR inclui atualizações ou adições relevantes à documentação. A documentação precisa ser clara, abrangente e estar atualizada para refletir as mudanças realizadas.
- **Verifique a compatibilidade**: certifique-se de que as alterações propostas sejam compatíveis com outras partes do sistema ou com dependências externas. Isso inclui a compatibilidade com versões anteriores, caso necessário.
- **Aprovação**: se estiver satisfeito com as alterações propostas, aprove o MR. Comunique sua aprovação de maneira clara.
