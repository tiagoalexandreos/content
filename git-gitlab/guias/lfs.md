---
title: Configurando e usando o Git LFS
description: Gerenciador de arquivos grandes
---

## Introdução

Gerenciar arquivos grandes no Git sempre foi um problema, o LFS foi desenvolvido com o intuito de resolver esse problema.

## O que é LFS?

O Git não é recomendado para lidar com arquivos binários grandes (Mb, Gb, etc). O LFS (Large File Storaged) é uma extensão para o Git que adiciona a capacidade de lidar com arquivos grandes, dessa forma, é possível realizar controle de versão em arquivos binários grandes.

## Instalando o LFS

Instalar o LFS é relativamente simples, existem várias formas de instalar de acordo com cada sistema operacional.

### Linux

```bash
curl -s https://packagecloud.io/install/repositories/github/git-lfs/script.deb.sh | sudo bash
```

#### Instalado de um arquivo binário

Baixe um dos [pacotes binários](https://github.com/git-lfs/git-lfs/releases) adequado para sua máquina, eles incluem um script que irá:

- Instalar os arquivos binários do Git LFS no sistema.
- Executar o comando `git lfs install` e realizar as configurações globais necessárias.

```ShellSession
./install.sh
```

### MacOS

Execute o comando abaixo:

```bash
brew install git-lfs
```

### Windows

O Git LFS está incluído na distribuição do [Git for Windows](https://gitforwindows.org/). Alternativamente, você pode instalar uma versão recente do Git LFS do gerenciador de pacotes [Chocolatey](https://chocolatey.org/).

## Como usar

Após a instalação precisamos configurar os repositórios Git onde você necessita que os arquivos sejam gerenciados pelo LFS. Podemos rastrear as extensões que irão utilizar o LFS através do comando abaixo:

```bash
git lfs track "*.xd"
```

Ao executar esse comando iremos rastrear todos os arquivos com a extensão `.xd` no repositório git.

Após rastrear um tipo de arquivo precisamos enviar o arquivo `.gitattributes`:

```bash
git add .gitattributes
git commit -m "rastreia arquivos *.xd usando Git LFS"
```

Dessa forma podemos utilizar o repositório de forma normal e o Git LFS irá gerenciar os arquivos grandes que foram rastreados.

### Travando arquivos para edição

Edições simultâneas em arquivos binários não são recomendadas devido a dificuldade de resolução de conflitos entre esses arquivos.

Para amenizar isso o Git LFS v.2.0.0 possui a funcionalidade de bloqueio de arquivos. Isso permite que você possa bloquear um arquivo para edição e impeça que outras pessoas modifiquem esse arquivo ao mesmo tempo que você.

#### Tornando arquivos bloqueáveis com o LFS

Precisamos definir quais arquivos podem ser bloqueados em nosso repositório, utilize o comando abaixo para bloquear arquivos com extensão `.xd`:

```bash
git lfs track " *.xd " --lockable
```

Esse comando irá adicionar a seguinte linha no arquivo `.gitattributes`:

```text
*.xd filter=lfs diff=lfs merge=lfs -text lockable
```

Automáticamente o Git LFS irá alterar as permissões dos arquivos bloqueáveis para `somente leitura` no nosso sistema de arquivos local. Isso é feito para evitar que os usuários editem acidentalmente um arquivo sem bloqueá-lo primeiro.

#### Bloqueando um arquivo

Se vamos editar o arquivo button.xd, por exemplo, primeiramente precisamos travar esse arquivo, "reservando-o" em nosso nome no servidor git. Para isso, utilizamos o seguinte comando:

```bash
$ git lfs lock button/button.xd
Locked button/button.xd
```

#### Verificando quais arquivos estão bloqueados

Com o comando abaixo podemos verificar quais arquivos estão travados para nosso usuário:

```bash
$ git lfs locks
button/button.xd  LoginX ID:1411
```

#### Desbloqueando um arquivo

Após encerrar a edição do arquivo podemos destravá-lo atráves do seu nome ou id:

```bash
git lfs unlock button/button.xd
```

```bash
git lfs unlock --id=1411
```

#### Enviando suas modificações

Quando você realizar o push para enviar suas modificações o LFS irá verificar se você não modificou um arquivo bloqueado por outro usuário.

```bash
git lfs push origin main --all
```
