---
title: Mensagens de Commit
description: Criando um Commit
---
## Introdução

O Conventional Commits é uma convenção para estruturar mensagens de commit em projetos de desenvolvimento de software. Essa convenção visa fornecer uma padronização na forma como as alterações são registradas nos commits, permitindo uma melhor compreensão das mudanças feitas no código. Nesta documentação, vamos fornecer um passo a passo simples de como criar um bom commit seguindo as convenções do Conventional Commits.

{{% notice warning %}}
É **extremamente importante** a leitura da documentação sobre o [conventional commit](https://www.conventionalcommits.org/pt-br/v1.0.0/).
{{% /notice %}}

> Observe essas regras de commit ao enviar sua contribuição. Esse é um dos critérios que nosso time leva em consideração ao avaliar sua contribuição.

## Passo a passo para criar um bom commit

Aqui está um passo a passo para criar um commit seguindo as convenções do Conventional Commits:

1. **Identifique o tipo de alteração**: Determine qual é o tipo de alteração que você está realizando. Alguns exemplos comuns incluem: `feat` (nova funcionalidade), `fix` (correção de bug), `docs` (atualização da documentação), `chore` (tarefa de manutenção), entre outros. Observe os prefixos permitidos na seção abaixo.

2. **Escreva uma mensagem clara e concisa**: Escreva uma mensagem descritiva para o commit, que resuma de forma clara e concisa o que está sendo alterado. Evite mensagens genéricas ou vagas, tente ser específico e objetivo.

3. **Adicione um prefixo opcional para escopo**: Se necessário, adicione um prefixo opcional para indicar o escopo da alteração. Por exemplo, se a alteração estiver relacionada a um componente específico do sistema, você pode adicionar `(nome-do-componente)` antes da mensagem do commit.

4. **Separe a mensagem do corpo**: Se houver necessidade de adicionar mais detalhes ou contexto à mensagem do commit, separe a mensagem principal do corpo usando uma linha em branco.

5. **Adicione uma linha de "BREAKING CHANGE" (mudança que quebra), se aplicável**: Se a alteração introduzir uma mudança que quebra a compatibilidade com versões anteriores, adicione uma linha iniciando com "BREAKING CHANGE:". Nessa linha, explique claramente qual a mudança que quebra e forneça instruções ou documentação adicional para ajudar outros desenvolvedores a lidar com essa alteração.

6. **Considere adicionar informações extras**: Se necessário, você pode adicionar informações extras relevantes ao commit, como referências a issues ou MRs relacionados, links para documentação ou outros detalhes relevantes.

7. **Revise o commit**: Antes de realizar o commit, revise cuidadosamente a mensagem para garantir que ela esteja clara, correta e seguindo as convenções do Conventional Commits.

8. **Realize o commit**: Com a mensagem do commit pronta, execute o comando `git commit` seguido da opção `-m` e insira a mensagem do commit entre aspas, como no exemplo a seguir:

   ```git
   git commit -m "feat(componente): adicionar nova funcionalidade X"
   ```

   > Certifique-se de substituir o exemplo de mensagem pelo conteúdo relevante ao seu commit.

   Exemplos de mensagens bem formadas (formato), mas com o texto que **errado**:

   ```git
        - feat: nova funcionalidade do header
        - fix: correção no menu
        - fix: issue 88
        - feat: list v2.0.0
   ```

   Mensagens com o conteúdo **útil**

   ```git
        - feat: adiciona funcionalidade sticky do header
        - fix: corrige a responsividade do menu
        - fix: corrige a cor de hover do item
        - feat: adiciona o collapse para o list
   ```

9. Faça o *squash* se necessário (consulte seção abaixo para mais informações)

## Exemplo de um commit seguindo as Convenções do Conventional Commits

Aqui está um exemplo de como um commit seguindo as convenções do Conventional Commits pode ser estruturado:

```git
feat: adicionar nova funcionalidade X

Adiciona a funcionalidade X para permitir que os usuários realizem a ação Y de forma mais eficiente e intuitiva.

BREAKING CHANGE: A funcionalidade Z foi removida, pois agora a funcionalidade X a substitui. Certifique-se de atualizar o código que utiliza a funcionalidade Z para utilizar a nova funcionalidade X corretamente.
```

Nesse exemplo, o tipo de alteração é `feat` (nova funcionalidade). A mensagem principal é "Adicionar nova funcionalidade X", e um corpo é fornecido para dar mais contexto sobre a alteração. Além disso, é adicionada uma linha de "BREAKING CHANGE" para indicar uma mudança que quebra a compatibilidade com versões anteriores.

## Squash de commits

O squash de commits é uma prática útil no desenvolvimento de software para combinar vários commits relacionados em um único commit significativo. Isso ajuda a manter um histórico de commits mais limpo, conciso e fácil de entender. Nesta seção, vamos explicar quando e como fazer o squash dos commits em um projeto.

### Quando fazer o Squash dos Commits?

Recomenda-se fazer o squash dos commits em algumas situações específicas, como:

1. **Pull Requests ou Merge Requests**: Antes de fazer o merge de um pull request ou merge request, é comum revisar e consolidar os commits relacionados em um único commit. Isso ajuda a manter o histórico do projeto mais organizado.

2. **Commits temporários**: Durante o desenvolvimento, é possível fazer commits temporários com mensagens vagas ou genéricas para salvar o progresso do trabalho. Antes de finalizar uma funcionalidade ou correção de bug, é recomendado fazer o squash desses commits em um commit bem estruturado e descritivo.

3. **Correção de erros de commits**: Caso sejam identificados erros nos commits, como mensagens mal formuladas ou commits desnecessários, o squash pode ser utilizado para corrigir esses problemas e melhorar a qualidade do histórico de commits.

### Como fazer o Squash dos Commits?

Aqui está um passo a passo simples de como fazer o squash dos commits:

1. **Identifique os commits a serem squashed**: Use o comando `git log` ou ferramentas de controle de versão para identificar os commits que deseja combinar.

2. **Inicie o processo de squash**: Utilize o comando `git rebase -i HEAD~n`, onde `n` é o número de commits que você deseja combinar. Isso abrirá um editor de texto com uma lista dos commits selecionados.

3. **Escolha o commit base**: Mantenha o primeiro commit da lista como "pick" (escolhido) para ser a base do novo commit. Você pode alterar "pick" para "reword" se quiser editar a mensagem do commit base.

4. **Marque os commits para o squash**: Mude "pick" para "squash" ou "s" nos commits que deseja combinar com o commit base. Isso indica ao Git que você deseja squashear esses commits no commit base.

5. **Salve e feche o editor de texto**: Após marcar os commits, salve e feche o editor de texto. Isso iniciará o processo de squashing dos commits.

6. **Edite a mensagem do commit resultante**: Um novo editor de texto será aberto com a mensagem do commit resultante. Edite a mensagem conforme necessário para descrever o conjunto de alterações combinadas.

7. **Salve e feche o editor de texto**: Após editar a mensagem do commit, salve e feche o editor de texto. O Git agora combinará os commits selecionados em um único commit.

8. **Empurre o commit squashed**: Se o commit squashed for um commit local, você pode usar `git push --force` para atualizar o ramo remoto com o commit squashed. No entanto, tenha cuidado ao usar `--force`, pois isso pode substituir commits existentes no ramo remoto.

## Tipos (prefixos) de commit

{{% notice tip %}}
Criamos configuração padrão para facilitar o uso dos padrões de commit. Para mais detalhes: <https://gitlab.com/govbr-ds/govbr-ds-commit-config>
{{% /notice %}}

| Prefixo    | Changelog | Categoria               | SemVer | Descrição                                                                                        |
| ---------- | --------- | ----------------------- | ------ | ------------------------------------------------------------------------------------------------ |
| build      | ❌         | ---                     | ---    | Mudanças que afetam o sistema de build (ex: npm, node, webpack, vite...)                         |
| chore      | ❌         | ---                     | ---    | Alterações que não se encaixam em outros prefixos                                                |
| ci         | ❌         | ---                     | ---    | Alterações nos arquivos e scripts de configuração de ambiente (Gitlab, Pipelines, Permissões...) |
| deprecated | ✅         | Depreciado              | ---    | Recursos que são removidos em versões futuras                                                    |
| docs       | ✅         | Documentação            | Patch  | Adiciona ou altera uma documentação (tutorial, guia, etc...)                                     |
| feat       | ✅         | Novidades               | Minor  | Nova feature, recurso, elemento, comportamento, etc...                                           |
| fix        | ✅         | Correções               | Patch  | Correções em features, recursos, elementos, comportamentos (e outros) previamente criados        |
| ops        | ✅         | Atividades Operacionais | ---    | Atividades operacionais relacionadas a design e de desenvolvimento                               |
| perf       | ✅         | Performance             | Patch  | Melhoria de desempenho (tempo de execução, download, carregamento...)                            |
| refactor   | ✅         | Refatorado              | Patch  | Altera o conteúdo do código sem mudar o resultado final (ex: organização de pastas, camadas...)  |
| removed    | ✅         | Removido                | Minor  | Recurso foi excluído definitivamente do projeto                                                  |
| revert     | ✅         | Revertido               | Minor  | Reverte um commit anterior                                                                       |
| lint       | ❌         | ---                     | ---    | Mudanças que não alteram o significado/comportamento (organização, formatação, pontuações...)    |
| test       | ❌         | ---                     | ---    | Adiciona ou corrige testes                                                                       |
| wip        | ❌         | ---                     | ---    | Trabalho ainda não finalizado                                                                    |

## Breaking Changes

[Breaking changes](../breaking-changes) não é um prefixo, mas um formato que o commit deve seguir. Caso uma versão tenho ao menos 1 breaking change será gerada uma versão **major**.

## Considerações Finais

Seguir nossas convenções de commits traz trazer benefícios significativos, como melhorar a clareza e a compreensão das alterações feitas. Com esse passo a passo simples, você pode criar commits consistentes e informativos, facilitando a colaboração entre os membros da equipe e a manutenção do projeto.
