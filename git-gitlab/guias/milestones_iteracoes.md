---
title: Milestones e Iterações
description: Página com o nomes das Iterações para os diversos projetos **GOVBR-DS**
---

Nesse projeto seguimos o seguinte padrão:

A nomenclatura para os milestones e para as iterações é toda em maiúscula, possuem um prefixo referente à solução e ao projeto no qual faz parte e seguem uma ordem numérica.

## Milestones

Milestones representam as releases. Sua nomenclatura segue a seguinte representação:

```markdown
<nome-solução>-<nome-projeto>-<R##>
```

Para a *release 01* do projeto *CORE* da solução *GOVBR-DS* o nome fica dessa forma:

```markdown
GOVBR-DS-CORE-R01
```

> Cada milestones pode ter uma duração diferente dos outros, tudo varia da necessidade do projeto.

## Iterações

Iterações representam as sprints e são criadas automaticamente pelo gitlab.

> Como as iterações representam um período de tempo, somente 1 iteração pode estar ativa por projeto.

## Prefixos

- Solução: GOVBR-DS
- Design System: CORE
- Web Components do DS: WBC
