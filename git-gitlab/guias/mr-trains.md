---
title: 'MR - Merge Trains'
---

## O que são?

[Merge Trains](https://docs.gitlab.com/ee/ci/pipelines/merge_trains.html 'Merge Trains')  são uma abordagem automatizada para a integração de MRs. Quando várias MRs são enviadas simultaneamente para um ramo (branch) de destino (como o main), as merge trains trabalham para combinar e testar essas alterações de forma sequencial.

Em vez de mesclar MRs individualmente, as merge trains agrupam MRs relacionados e as fundem em um único commit. Isso ajuda a reduzir conflitos e simplificar o histórico de commits.

Os merge trains no GitLab seguem um fluxo contínuo, onde cada MR é adicionado em uma fila e passa por uma série de etapas automatizadas antes de ser mesclado no ramo de destino. Cada etapa envolve a execução de testes automatizados, validações e verificações de integração. Se um MR na merge train falhar em qualquer etapa, a pipeline será interrompida e o responsável pela correção será notificado.

## Como funcionam?

**Envio de MRs**: Os desenvolvedores enviam seus MRs para o ramo de destino (por exemplo, main). Esses MRs podem ter dependências ou relações entre si.

**Adição à fila de Merge Trains**: Cada MR é adicionado à fila de merge trains, onde espera sua vez para ser mesclado.

**Execução de pipelines**: À medida que os MRs são adicionados à fila, o GitLab inicia pipelines individuais para cada MR. Essas pipelines executam os estágios de build, teste e outras verificações definidas no projeto.

**Integração sequencial**: Quando um MR alcança o topo da fila, ele é mesclado sequencialmente no ramo de destino. O GitLab cria um commit que combina as alterações desse MR e o adiciona ao ramo de destino.

**Validação contínua**: Depois que um MR é mesclado, a pipeline continua a executar para garantir que as alterações mescladas não introduzam problemas no ramo de destino. Isso inclui a execução de testes adicionais e verificações de integração.

**Limpeza da fila**: Após a conclusão bem-sucedida de um MR, ele é removido da fila de merge trains e o próximo MR na fila é processado.

**Notificações e feedback**: Durante todo o processo, os responsáveis são notificados sobre o status de seus MRs, permitindo a correção de problemas.

## Benefícios

**Melhoria na eficiência**: Ao mesclar MRs em uma sequência lógica, o Merge Train automatiza o processo e reduz a necessidade de intervenção manual para combinar cada MR individualmente.

**Garantia de qualidade**: Ao executar testes automatizados após cada mesclagem, o Merge Train ajuda a identificar falhas ou regressões introduzidas pelas alterações antes que elas sejam propagadas para o ramo de destino.

**Redução de conflitos**: O Merge Train lida com possíveis conflitos entre MRs de forma mais eficiente, pois as mesclagens são realizadas em uma ordem pré-definida.

**Rastreabilidade e visibilidade**: A fila do Merge Train proporciona uma visão clara da ordem em que os MRs estão sendo mesclados, facilitando o acompanhamento e a rastreabilidade das alterações.
