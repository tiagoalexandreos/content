---
title: Pipelines
description: Explicação sobre como criar e como verificar logs dos pipelines
---

Os pipelines no GitLab são uma maneira automatizada de construir, testar e implantar seu código em diferentes etapas, desde a sua alteração até a entrega final.

É como uma linha de montagem que processa automaticamente o seu código e realiza várias tarefas para garantir que tudo esteja funcionando corretamente antes de ser disponibilizado ao usuário final.

Para que servem os pipelines do GitLab:

- Integração Contínua (CI): Os pipelines permitem que você automatize a construção e testes do seu código sempre que novas alterações são enviadas ao repositório. Isso ajuda a identificar e resolver problemas mais rapidamente, evitando que erros se acumulem.

- Entrega Contínua (CD): Com os pipelines, você pode automatizar a implantação do seu código em ambientes de teste ou de produção, garantindo que o código seja entregue de forma consistente e confiável.

- Feedback Rápido: Ao executar testes automaticamente, os pipelines fornecem um feedback rápido sobre a qualidade do código, permitindo que os desenvolvedores corrijam problemas prontamente.

- Automação: Os pipelines eliminam a necessidade de tarefas manuais repetitivas, economizando tempo e evitando erros humanos.

- Rastreabilidade: Com os pipelines, é possível rastrear cada alteração e saber exatamente quando e onde ocorreram problemas, facilitando a correção de bugs e a manutenção do código.

- Padronização: Os pipelines ajudam a padronizar o processo de desenvolvimento e entrega, garantindo que todos os membros da equipe sigam as mesmas práticas e ferramentas.

Em resumo, os pipelines do GitLab são uma ferramenta poderosa para automatizar o ciclo de vida do desenvolvimento de software, proporcionando maior eficiência, qualidade e confiabilidade ao seu projeto.

## Criando

Para criar um pipeline no GitLab.com, você precisa adicionar um arquivo ```.gitlab-ci.yml``` ao diretório raiz do repositório. Esse arquivo é onde você define as etapas do pipeline e as configurações de cada estágio.

Exemplo de um arquivo .gitlab-ci.yml simples:

``` yaml
# .gitlab-ci.yml
stages:
  - build
  - test
  - deploy

build_job:
  stage: build
  script:
    - echo "Construindo o projeto..."

test_job:
  stage: test
  script:
    - echo "Executando testes..."

deploy_job:
  stage: deploy
  script:
    - echo "Implantando o projeto..."
```

Neste exemplo, criamos três estágios no pipeline (build, test e deploy) e um job (tarefa) em cada estágio. Cada job contém um script que será executado quando o pipeline estiver em andamento.

## Acompanhando o pipeline

Vá para o GitLab.com, acesse o seu repositório e clique na seção "CI/CD" ou "Pipelines" para visualizar o pipeline em execução.

O GitLab.com exibirá os detalhes de cada estágio e job executado, indicando se foi bem-sucedido ou se ocorreu algum erro. Você também pode ver os logs de saída de cada job para diagnosticar possíveis problemas.

### Dentro de Merge Requests

Alguns estágios podem ser configurados para rodar quando um MR for criado. Nesses casos os pipelines podem ser visualizados dentro da aba "Pipelines" na página web do MR.

![Lista de abas de um MR com a do pipeline destacada](/git-gitlab/imagens/pipeline_tab.png)

#### Reports

Estágios que geral algum output podem exportar um relatório para ser visível dentro da aba principal dos MRs. Caso algum pipeline gere um relatório exportado, estes podem ser acessados clicando no link logo abaixo do status dos pipelines.

![Resultado do pipeline atual e link para expandir a lista de relatórios](/git-gitlab/imagens/pipeline_reports_collapsed.png)

![Resultado do pipeline atual e lista expandida de relatórios](/git-gitlab/imagens/pipeline_reports_expanded.png)

Clique na coluna **Artifact** para abrir o link de download do relatório que deseja.

Clique na coluna **Job** para acessar o log do estágio que gerou o relatório.

## Configurações adicionais

Para explorar todas as opções disponíveis e configurar pipelines mais avançados, consulte a documentação oficial do [GitLab CI/CD](https://docs.gitlab.com/ee/ci/).

## Rodando Pipelines Localmente

É uma boa prática testar os pipelines antes de enviar para o gitlab.com. Dessa forma conseguimos identificar alguns problemas antes mesmo de fazer o commit, o que agiliza todo o processo e evita desgastes.

Para isso nós temos que usar o Gitlab Runner.

### Instalação do GitLab Runner

#### Arch Linux

1. Atualize o sistema:

   ```bash
   sudo pacman -Syu
   ```

2. Instale o GitLab Runner:

   ```bash
   sudo pacman -S gitlab-runner
   ```

#### Ubuntu

1. Atualize o sistema:

   ```bash
   sudo apt update
   sudo apt upgrade
   ```

2. Instale dependências e o GitLab Runner:

   ```bash
   sudo apt install -y curl
   curl -L https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.deb.sh | sudo bash
   sudo apt install gitlab-runner
   ```

## Configuração do Pipeline

1. Crie um arquivo `.gitlab-ci.yml` na raiz do seu repositório com a configuração do pipeline. Aqui está um exemplo básico:

   ```yaml
   stages:
     - build
     - test

   build_job:
     stage: build
     script:
       - echo "Fazendo o build..."

   test_job:
     stage: test
     script:
       - echo "Executando testes..."
   ```

## Testando o Pipeline Localmente

1. Abra um terminal na raiz do seu repositório.

2. Execute o seguinte comando para iniciar a execução do pipeline localmente em modo "shell":

   ```bash
   gitlab-runner exec shell build_job
   ```

3. Verifique a saída para garantir que os comandos estão sendo executados conforme o esperado.

4. Repita o passo 2 para outros jobs definidos em seu pipeline, se houver.

Lembre-se de ajustar os scripts e comandos do pipeline conforme necessário para que tudo funcione corretamente durante a execução local.
