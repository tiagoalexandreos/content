---
title: Markdown
description: Descreve como configurar um projeto seguindo nosso padrão para Markdown
---

Para facilitar a criação, manutenção e visualização dos nossos documentos escritos em markdown definimos alguns padrões.

Para ajudar na garantia desses padrões usamos algumas ferramentas que precisam ser configuradas em **cada projeto**. Não é obrigatório que os projetos façam uso de todas as ferramentas, mas é aconselhável que uma análise das necessidades de cada projeto seja feita para escolher quais serão aplicadas.

## Ferramentas

### Markdownlint

Essa é a ferramenta base usada para identificar não conformidades. É uma ferramenta de linha de comando que roda sobre o nodeJS.

1. Instale a dependência ```markdownlint-cli```
1. copie o arquivo ```.markdownlint.yml``` para a raiz do projeto
1. Instale o ```husky``` e ```lint-staged``` para avaliar os padrões durante a criação do commit
1. Observe a documentação do  ```husky``` e ```lint-staged``` para configurá-los
1. Inclua o seguinte código no *pre-commit* hook: ```npx --no -- lint-staged```
1. Inclua o seguinte código no *package.json*:

    ```json
    "lint-staged": {
          "*.{md,mdx}": "markdownlint --fix"

    }
    ```

1. Inclua/substitua os seguintes scripts no *package.json*:

    ```json
        "lint:md": " markdownlint '**/*.md' --fix -d",
        "prepare": "chmod +x ./node_modules/husky/lib/bin.js && husky install"
    ```

Fazendo isso o markdownlint já vai estar pronto para ser usado conforme as configurações padrão. Caso seja necessário customizar alguma regra para o projeto isso pode ser feito no arquivo ```.markdownlint.yml```.

### Gitlab CI

Consiste em definir um estágio do pipeline que será executado pelo gitlab durante merge requests e na branch main.

Crie um arquivo ```.gitlab-ci.yml``` na raiz do projeto e inclua o seguinte conteúdo:

```yml
image: node:18-alpine

stages:
  - Qualidade

cache:
  paths:
    - node_modules

Markdown:
  stage: Qualidade
  artifacts:
    when: always
    paths:
      - report-markdownlint.txt
    expire_in: 1 week
    expose_as: "Markdownlint Report"
  before_script:
    - apk add nodejs npm
    - apk --no-cache add
    - npm i -g markdownlint-cli
  script:
    - markdownlint  '**/*.md' -o report-markdownlint.txt
  only:
    - main
    - merge_requests
```

Dependendo das configurações do projeto no gitlab.com o merge request pode ser bloqueado impedindo que conteúdos fora do padrão sejam aceitos.

### Visual Studio Code

Essa configuração serve apenas para projetos que usam o Visual Studio Code para criação das documentações.

Crie uma pasta ```.vscode``` na raiz do projeto e dentro dela 2 arquivos ```extensions.json``` e ```settings.json``` e copie o conteúdo abaixo para eles.

extensions.json:

```json
{
  "recommendations": [
    "bierner.github-markdown-preview",
    "bierner.markdown-checkbox",
    "bierner.markdown-emoji",
    "bierner.markdown-footnotes",
    "bierner.markdown-mermaid",
    "bierner.markdown-preview-github-styles",
    "bierner.markdown-yaml-preamble",
    "DavidAnson.vscode-markdownlint",
    "unifiedjs.vscode-mdx",
    "usernamehw.errorlens",
    "yzhang.markdown-all-in-one",
  ]
}

```

settings.json:

```json
{
  "editor.codeActionsOnSave": {
    "source.fixAll": true,
    "source.fixAll.markdownlint": true
  },
  "editor.formatOnSave": true,
  "editor.trimAutoWhitespace": true,
}

```
