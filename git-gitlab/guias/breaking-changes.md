---
title: Breaking Changes
description: Documentação sobre Breaking Changes no Desenvolvimento Frontend
---

É muito importante para **todo o DS** manter um equilíbrio entre estabilidade e novidades.

É normal o time ter vontade de inovar e criar coisas novas, mais rápidas, acessíveis, performáticas ou bonitas, mas uma quebra não intencional pode ser muito problemática para ao usuários.

Assim, toda alteração (seja em assets, cores, código-fonte, configuração, etc) deve ser feita e analisada sempre considerando seus efeitos.

## O que são *Breaking Changes*?

Breaking changes são alterações que introduzem comportamentos indesejados em sistemas ou aplicações existentes. Elas podem ocorrer em diferentes níveis de um projeto, como nas interfaces de programação de aplicativos (APIs), nas dependências de bibliotecas, *layout* das aplicações ou até mesmo nas alterações de comportamento do usuário.

> A quebra de compatibilidade nem sempre vai ser catastrófica (aplicação para totalmente de funcionar).

### Dentro da equipe de *Frontend*

A seguir, estão alguns exemplos comuns de *breaking changes* no desenvolvimento *frontend*:

1. **Alterações na API de uma biblioteca**: Quando uma biblioteca ou *framework frontend* atualiza sua API, algumas funcionalidades antigas podem se tornar obsoletas ou não serem mais compatíveis. Isso pode exigir que os desenvolvedores façam modificações significativas em seu código para adaptar-se às mudanças.

2. **Modificações nas regras de estilo**: Alterações nas regras de estilo CSS, como nomeação de classes ou seletores, podem afetar o layout e a aparência de uma página. Se essas alterações não forem tratadas corretamente em todas as partes do código que utilizam essas classes ou seletores, podem ocorrer problemas de exibição.

3. **Remoção ou alteração de funcionalidades**: Se uma funcionalidade existente em uma aplicação frontend é removida ou modificada, isso pode afetar o fluxo de trabalho ou a experiência do usuário. Os desenvolvedores que dependem dessa funcionalidade precisarão ajustar seu código para lidar com as alterações.

4. **Atualização de dependências**: Ao atualizar as dependências de um projeto frontend, pode haver alterações na forma como essas dependências funcionam ou se integram com o código existente. Isso pode exigir ajustes no código para manter a compatibilidade.

### Dentro da equipe de *Design*

No campo do *design*, a ideia de *breaking changes* pode ser bastante subjetiva, pois depende do contexto da mudança. Por exemplo, uma simples alteração de cor pode representar uma melhoria sutil ou até mesmo uma quebra completa no entendimento e da experiência na interface.

Podemos identificar um *breaking change* no *design* como qualquer modificação que altere o entendimento das orientações estabelecidas nas regras básicas dos sistemas criados, conforme definida nas diretrizes (Fundamento, Componente ou Padrão); ou alterações significativas em um sistema, produto ou interface que impactam a experiência do usuário e exigem que os usuários se adaptem a novos fluxos de trabalho, funcionalidades ou *layouts*.

Essas mudanças podem resultar em interrupções na produtividade, confusão ou frustração para os usuários, uma vez que elementos familiares são substituídos por novos, exigindo que eles reaprendam como utilizar o sistema.

A adoção de práticas de *design* centrado no usuário e testes de usabilidade pode diminuir a subjetividade do conceito e ajudar a identificar possíveis *breaking changes* antes de serem implementados, permitindo ajustes e iterações para garantir uma transição suave e uma experiência do usuário contínua.

Veja alguns exemplos das mudanças que podem ou não gerar quebra de versão:

| Diretriz   | Probabilidade | Tipo de Mudança                               | Detalhes                                                                                                                                                                               |
|------------|---------------|-----------------------------------------------|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Fundamento | Baixa         | Mudança de uma cor da paleta de cores         | Se a mudança da paleta de cores seguir todas as regras de criação, o impacto na experiência do produto pode ser imperceptível para a maioria dos usuários.                             |
| Fundamento | Alta          | Mudança nas cores de aviso                    | Como as cores de aviso desempenham um papel importante no feedback da interface, alterações podem impactar a experiência do usuário e exigir que eles se adaptem a uma nova aparência. |
| Componente | Baixa         | Adição de um novo comportamento ao botão      | Adicionar um novo comportamento pode aumentar a flexibilidade sem comprometer o funcionamento consistente do componente.                                                               |
| Componente | Alta          | Alteração do comportamento de ênfase do botão | O comportamento de ênfase é crucial para a interface, portanto, qualquer alteração pode ter um impacto significativo em vários produtos.                                               |
| Padrão     | Baixa         | Adição textual de novas boas práticas         | A criação de novas boas práticas pode melhorar o entendimento do padrão sem afetar a experiência de uso na interface.                                                                  |
| Padrão     | Alta          | Alteração do ícone identificador do Collapse  | Alterações visuais no ícone identificador podem resultar em uma experiência diferente para os usuários acostumados com a interface anterior.                                           |

> É improvável que mudanças nos *assets* de *design* (como uikits, ilustrações, modelos, etc.) resultem em quebras de versão. Geralmente, para fazer alterações drásticas desse tipo, é necessário que outros documentos tenham sido modificados anteriormente, já exigindo uma quebra de versão.

É essencial que os *designers* comuniquem e gerenciem cuidadosamente as mudanças, fornecendo documentação clara, tutoriais e suporte para minimizar o impacto negativo e facilitar a transição dos usuários para a nova versão do sistema ou produto.

Lembre-se de **sempre discutir as modificações com a equipe**. Uma alteração pode ter interpretações diferentes, dependendo do contexto. No entanto, uma quebra de versão independente no contexto do *frontend* ou *design* é considerada uma *breaking change* para o *Design System*!

## Criando um Commit Indicando uma Breaking Change (Conventional Commits)

O [Conventional commits](https://www.conventionalcommits.org/pt-br/v1.0.0/) é uma convenção para estruturar mensagens de commit que ajudam na comunicação eficaz das alterações feitas no código. Para indicar que um commit contém uma breaking change, podemos adotar uma abordagem específica.

Aqui está um exemplo de como criar um commit indicando uma breaking change usando as convenções do Conventional Commits:

```git
feat: adicionar nova funcionalidade X

BREAKING CHANGE: A funcionalidade Y foi removida, pois agora a funcionalidade X é implementada de forma diferente. Verifique a documentação para atualizar o código que utiliza a funcionalidade Y.
```

Nesse exemplo, a mensagem do commit começa com o tipo "feat" (caracterizando uma nova funcionalidade adicionada). Em seguida, uma descrição breve da alteração é fornecida. Abaixo dessa descrição, é adicionada a linha "BREAKING CHANGE:", seguida de uma explicação clara sobre a mudança que quebra e quaisquer instruções ou documentação relevantes para ajudar outros desenvolvedores a lidar com a mudança.

Ao criar um commit dessa forma, outros membros da equipe ou colaboradores que revisarem o histórico de commits poderão identificar facilmente as breaking changes introduzidas no código.

> Leia a nossa documentação sobre [commits](../commit) para mais detalhes.

## Deprecated/Depreciado

É comum que durante o ciclo de vida de um produto partes dele não sejam mais necessárias ou precisem ser trocadas por outras que se encaixam melhor no contexto do projeto. Mas nem sempre essas partes podem ser removidas imediatamente. Nesses casos é importante o uso do **deprecated**.

Isso indica que aquela parte do projeto ainda é distribuída pela equipe, mas o seu uso não é mais encorajado pois será removida em uma versão futura.

### Commit

O commit deve ter o prefixo **deprecated**. Isso assegura que o changelog vai ser gerado uma uma seção específica para os itens depreciados para dar uma visibilidade maior aos usuários.

Observe que ainda não é a remoção do item (a remoção tem o prefixo 'removed'), apenas estamos informando que essa parte está depreciada.

É muito importante que na descrição do commit a razão da depreciação seja descrita com o máximo de detalhes para facilitar a comunicação com os usuários.

```git
deprecated: funcionalidade Y foi depreciada

A funcionalidade Y foi depreciada, pois devido a ABC tivemos que substituí-la pela funcionalidade X.
```

### TODO

Em partes textuais do projeto podemos colocar uma marcação de texto para que o editor (dependendo das configurações) consiga identificar e listar sempre que necessário. Essa marcação funciona somente em partes textuais e serve apenas para interessados técnicos que vão observar o código/texto do projeto.

```javascript
// DEPRECATED: texto explicativo
```

A intenção dessa marcação é facilitar a identificação pelos criadores do projeto de partes que estão marcadas como depreciadas. Essa marcação deve ser usada apenas temporariamente para facilitar a identificação de itens depreciados.

### Tags, releases e Pacotes

Tags e releases (git) e pacotes NPM também podem ser depreciados. Isso significa que apesar da versão específica ter sido disponibilizada para o público, seu uso não é recomendado.

Um exemplo de necessidade de depreciação é a inclusão de um breaking change em uma versão em uma mesma versão major anterior. Ex: 1.3.0 > 1.3.1

Nesses casos é importante:

1. Seguir os passos do NPMJS para depreciação de pacotes para evitar que ele seja instalado durante as atualizações dos usuários.
1. Marcar a **release** como depreciada e incluir o motivo na descrição.
1. Marcar o post no discord (caso exista) como depreciado e escrever um comentário explicando o que aconteceu.

## Considerações Finais

No desenvolvimento frontend, é importante ter em mente as breaking changes e seu impacto no funcionamento e compatibilidade do sistema. Ao seguir as convenções do Conventional Commits e indicar claramente as breaking changes em commits, facilita-se a colaboração e a manutenção do código, permitindo que outros desenvolvedores se ajustem e atualizem seus sistemas adequadamente.

> Lembre-se de documentar adequadamente as mudanças e fornecer instruções claras sobre como lidar com as alterações que quebram para garantir uma transição suave para outros membros da equipe e minimizar possíveis interrupções para os usuários finais.
