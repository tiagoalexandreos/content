---
title: "Tags"
---

## Taxonomia

Formalmente, é a técnica ou metodologia que organiza, classifica e define a hierarquia dos arquivos digitais e processos dentro de uma estrutura compreensível. Em termos práticos o processo envolve a concepção, nomeação e classificação dos grupos que permitem uma leitura clara da origem e aplicação de cada informação.

### Semântica

Define cuidadosamente o campo semântico de cada agrupador e a estrutura do nome dos arquivos (palavras unidas pelo sentido, ou seja, a significação de uma palavra ou grupo de palavras) é fundamental para o sucesso da gestão de ativos digitais. Portanto, as tags no GitLab foram definidas em cinco agrupadores:

1. Responsável (cor laranja);
2. Tipo (cor azul);
3. Categoria (cor verde);
4. Fluxo (cor violeta);
5. Atributo (cor vermelha).

Os agrupadores 2, 3 e 4 são do tipo único e só podem existir um por issue. Os demais podem ser acumulativos, quando necessário.

Embora já configurado com as tags abaixo, é sempre permitida, quando necessária, a inserção de novas tags dentro dos agrupadores mencionados. Ao inserir uma nova tag, certifique-se do grupo que ela deve estar contida. Mantenha a cor do grupo e descreva de forma sucinta a função da utilização da tag criada.

A seguir, o detalhamento das tags pertencentes a cada agrupador.

1. Grupo Responsável (laranja):
    - Designer;
    - Desenvolvedor;
    - DevOps;
    - DesignOps;
    - Gestor.
2. Grupo Tipo (azul):
    - Novo;
    - Evolução;
    - Correção;
    - Suporte.
3. Grupo Categoria (verde):
   - Componente;
   - Modelo;
   - Fundamento;
   - Padrão;
   - UIkit;
   - Gestão;
   - Evento.
4. Grupo Fluxo (violeta):
   - Analisando;
   - Planejando;
   - Pronto;
   - Criando;
   - Implementando;
   - Revisando;
   - Design Review;
   - Merge Request;
   - Release;
   - Implantando;
   - Teste Hipótese;
   - Teste Intrínseco;
   - Teste Usabilidade.
5. Grupo Atributo (vermelho):
   - Crítico;
   - Suspenso;
   - Pendente;
   - Impactado;
   - Comitê;
   - Hotfix;
   - Wontfix;
   - Duplicado;
   - Ajuda.

Exemplos do uso de tags em uma issue:

- Ex 01: Designer / Nova / Modelo / Criando
- Ex 02: Desenvolvedor / Evolução / Componente / Merge Request / Crítico
- Ex 03: Desenvolvedor / Correção / Site / Homologando / Pendente / Comitê / Impactando
