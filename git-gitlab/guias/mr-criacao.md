---
title: 'MR - Criação'
description: Guia sobre como criar um MR
---

Para aprender os detalhes sobre como os MR funcionam, inclusive as aprovações, revisões e etc, leia a [documentação oficial](https://docs.gitlab.com/ee/user/project/merge_requests/ 'Gitlab Docs - Merge Request').

Antes de enviar o seu primeiro MR, leia o documento **CONTRIBUTING** do projeto para mais informações sobre como funciona o processo de contribuição.

Caso o MR ainda não esteja pronto para revisão, marque como **DRAFT**.

{{% notice note %}}
O título do MR **deve** seguir o padrão dos [commits]({{% relref "/git-gitlab/guias/commit" %}} '`Commits`') e **deve** refletir o que foi feito no MR.
{{% /notice %}}

{{% notice warning %}}
Avalie com o time quem poderá fazer a revisão do MR.
{{% /notice %}}

## Passo a passo

Observe os itens abaixo para facilitar a revisão do seu Merge Request:

- O título do MR segue o padrão dos commits
- A descrição do MR corresponde ao que foi feito (código, documentação, etc...)?
  - Use nossos templates para auxiliar
- O relacionamento entre issues, commits e MRs foi feito
- O MR atende atende aos requisitos das issues que o originaram
- Opção para deletar a branch selecionada (recomendável)
- O *squash* dos commits foi feito (avalie a necessidade)
- Os commits seguem o padrão do projeto
- Assignee foi definido
- O reviewer foi definido
- Milestone definido
- Labels aplicáveis foram selecionadas

## Relacionando MRs

Existem situações em que um MR deve ser aceito antes de outro. Nesses casos o relacionamento entre os MRs deve ser informado conforme a imagem abaixo.

![Requisitos Merge Request](/git-gitlab/imagens/mr_dep.png)
*Relacionamento entre MRs*

## Responsabilidades do Assignee

O *Assignee* fica responsável em tomar todas as decisões e configurações relacionadas ao MR. Deve analisar as considerações levantadas pelos revisores e decidir se as sugestões fazem sentido ou não, bem como dar *feedback* sobre a ação tomada ao referente revisor. Marque outros revisores responsáveis e relevantes, se necessário.
Após as revisões serem concluídas, o *assignee* deve mesclar o MR, resolvendo e tomando as decisões cabíveis durante todo o processo.

### Antes de mesclar o MR

```markdown
- [ ] Todas as discussões no MR foram resolvidas
- [ ] As aprovações solicitadas foram feitas
- [ ] O pipeline (caso exista) está passando
```

{{% notice warning %}}
Caso o squash deva ser feito é interessante preencher o campo **DE ACORDO** com as regras de commit do projeto. Isso inclui a inclusão de [breaking changes]({{% relref "/git-gitlab/guias/breaking-changes" %}}) e fechamento de issues.
{{% /notice %}}

{{% notice tip %}}
Links de apoio:

- [Code Review]({{% relref "/desenvolvimento/checklists/code-review" %}} 'Code Review')
- [Revisão de Design]({{% relref "/design/checklists/revisao" %}} 'Revisão de Design')
{{% /notice %}}

{{% notice tip %}}
Ao avaliar um MR, dê preferência para aceitar o MR iniciando um Merge Train. Veja a nossa [documentação sobre o assunto]({{< ref "/git-gitlab/guias/mr-trains.md" >}} "Merge Train") para entender mais.

Certifique-se de que todas as preocupações e problemas foram abordados antes de prosseguir com o merge.
{{% /notice %}}
