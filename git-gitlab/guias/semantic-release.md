---
title: Releases
description: Guia sobre os nossos padrões de release e versão
---

Em alguns projetos o processo de publicação das novas versões é totalmente automatizado. Esse documento é apenas para explicar alguns pontos específicos do fluxo de geração de release, para mais informações acesse o site do [semantic release](https://semantic-release.gitbook.io/semantic-release/usage/configuration 'semantic release').

{{% notice tip %}}
Criamos configuração padrão para facilitar o uso dos padrões de release. Para mais detalhes: <https://gitlab.com/govbr-ds/govbr-ds-release-config>
{{% /notice %}}

O objetivo desse documento é explicar as configurações padrão para nossas releases.

## Branches

Nossas releases usam a nomenclatura de branches de `release`, `manutenção` e `prerelease`. Essas nomenclaturas são conhecidadas como canais de distribuição.

1. Release: gera uma nova versão com base na última release estável lançada
1. Manutenção: gera uma nova versão com base em uma release anterior
    - 1.x.x: versões `patch` e `minor` podem ser geradas com base nessa branch
    - 2.1.x: somente versões `patch` poderão ser geradas
    - Caso alguém envie um commit que geraria uma versão diferente da versão definida no canal o semantic-release vai falhar e gerar um erro
1. Prerelease: gera uma versão de testes. Sufixos podem ser:
    1. next: próxima versão major (exemplo: versão atual = 2.0.0, next = 3.0.0)
    2. alpha: usada para indicar qualquer versão em estágio inicial de desenvolvimento

> Versões **next** tem o objetivo de permitir que a comunidade avalie o produto antes do lançamento oficial. Apesar de indicarem um produto com um nível de maturidade maior que as versões **alpha** isso não significa mudanças ou mesmo - [breaking changes](../breaking-changes) não ocorrerão.
>
> Já as versões **alpha** indicam que as mudanças que estão sendo implementadas estão em estágios iniciais e podem conter bugs que podem quebrar as aplicações. São usadas principalmente para testes internos da equipe.

{{% notice note %}}
As versões de prerelease não devem ser usadas em produção.
{{% /notice %}}

Sugerimos ler a documentação do [semantic-release](https://github.com/semantic-release/semantic-release/blob/beta/docs/usage/workflow-configuration.md#workflow-configuration 'Workflow') para entender melhor o fluxo de releases.

## Plugins

Cada etapa do semantic-release é resposabilidade de um plugin específico. os os plugins usados são declarados e configurados no arquivo ```.releaserc.yml```.

{{% notice info %}}
Recomendamos a leitura das documentações do semantic release junto com a documentação dos plugins usados no seu projeto antes de continuar esse texto. Aqui não vamos repetir tudo que está descrito nas páginas dos pacotes usados.
{{% /notice %}}

## Dry Run

Execute o comando abaixo para visualizar tudo que será executado pelo semantic-release de acordo com as configurações atuais do projeto:

```node
NPM_TOKEN=<npm_token> GITLAB_TOKEN=<gitlab_token> npx semantic-release --dry-run --debug
```

{{% notice warning %}}
A branch que está rodando deve estar no gitlab.com antes de rodar o 'dry-run', mesmo que localmente.
{{% /notice %}}

{{% notice warning %}}
O plugin do discord é executado mesmo no 'dry-run'. Recomendamos comentar o plugin durante os testes.
{{% /notice %}}

## Problemas encontrados

{{% expand "The pre-release branches are invalid in the `branches` configuration." %}}
Esse erro acontece *geralmente* por ter mais de uma branch por canal que se encaixa no glob pattern.

Como assim?

Se o projeto tem o canal de prerelease beta que atende ao pattern ```+([0-9])?(.{+([0-9]),x}).x-beta``` as branches abaixo vão satisfazer o requisito do regex:

- 1.0.x-beta
- 1.1.x-beta
- 1.x.x-beta

Dessa forma, se mais de 1 branch existir que satisfaça o requisito a publicação vai falhar. Assim **sempre** delete as branchs depois das versões desejadas terem sido geradas.
{{% /expand %}}
