---
title: Issues
description: Orientações para a criação de issues
---

As issues são usadas para planejar as atividades, discutir ideias e resolver problemas. Elas são o ponto de partida para quase todas as atividades que serão feitas pelo time para melhorar todos os produtos que fazem parte do **GOVBR-DS**.

{{% notice warning %}}
Não esqueça de verificar se existe alguma issue aberta sobre o assunto. Caso exista não crie uma nova issue, mas use os comentários para continuar a discussão sobre o tema tratado.
{{% /notice %}}

## Nomenclatura

O nome deve ser composto por: ```Contexto - Assunto```

**Contexto** - Define o escopo. Serve para delimitar o assunto.

**Assunto** - Descreve em poucas palavras o que deve ser tratado.

> Evite colocar frases longas.
>
> Cada item tem um documento que explica os requisitos de criação

✔️ Button - Cor de hover diferente da especificação de Design

❌ button: Cor de hover diferente da especificação de Design

❌ button - Cor de hover diferente da especificação de Design

❌ A cor do hover do button está diferente da especificação de Design

✔️ Temas - Adicionar o tema escuro ao **GOVBR-DS**

❌ Adicionar o tema escuro ao **GOVBR-DS**

❌ Temas - Recebemos um pedido para adicionar um tema escuro ao projeto até o final do mês devido a necessidade do projeto XYZ

## Passo-a-passo

Para ajudar todos os envolvidos, criamos um passo-a-passo para ajudar na criação de issues. Uma issue preenchida corretamente facilita a correta classificação dos assuntos e agiliza a avaliação do tema por alguém do time.

1. Escolha um modelo para a issue no campo *Description/Descrição*.
1. Preencha o template conforme as instruções (comentários).

- Algumas seções podem não se aplicar para a sua issue, nesse caso elas podem ser excluídas.
- Tenha em mente que você pode ser solicitado a prover mais informações sobre a issue então quanto mais detalhes, melhor.

1. Inclua outros MR e issues relacionadas.
1. Inclua todos os labels que forem relacionados ao conteúdo.

- Por padrão incluímos no template a tag `analise`. Verifique se a issue foi criada(o) com essa tag para facilitar a classificação pelos mantenedores.
- Isso ajuda a correta classificação e direcionamento da issue.
- Observe as descrições dos labels, pois alguns são para uso exclusivo em determinados contextos.

1. Caso a issue dependa de outra(o), **não esqueça** de relacionar o campo correto.

- O relacionamento é definido após a criação. Existem 3 tipos de relacionamento. Leia a documentação oficial <https://docs.gitlab.com/ee/user/project/issues/related_issues.html> para entender o que cada opção significa.
- Esse passo é **muito** importante pois precisamos saber o relacionamento entre issues para garantir que as necessidades estão sendo atendidas e nada está ficando esquecido no backlog.

1. Leve em consideração nosso código de conduta ao criar ou responder a uma issue.

## Reportando Bugs

Para reportar algum comportamento estranho use o template `Bug Tracking` ao criar a issue. Tente incluir o máximo de detalhes seguindo os passos descritos no template.

## Relacionando Issues

Issues podem se relacionar por assunto ou dependência com outras issues. Não se esqueça de criar o relacionamento conforme a imagem abaixo para facilitar a execução das tarefas.

![Requisitos Merge Request](/git-gitlab/imagens/issue_dep.png)
*Área para criar relacionamentos entre issues*

{{% notice info %}}
Caso exista uma issue fechada mas o problema persista, crie uma nova issue e faça referência a issue fechada (segundo imagem abaixo).
{{% /notice %}}
