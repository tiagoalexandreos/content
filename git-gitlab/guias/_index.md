---
title: 'Guias'
chapter: false
pre: '<i class="far fa-copy"></i> '
---

## Introdução

O Gitlab oferece um conjunto de recursos que auxiliam na gestão dos projetos, entre eles issues, labels, milestones, iterations, epics e merge requests. Para utilizá-los de forma consistente é necessário definir algumas diretrizes, para que o fluxo de desenvolvimento do projeto possa ser facilmente gerenciado e entendido por todos.

## Objetivo

Os documentos encontrados nessa sessão possuem o objetivo de instruir os usuários do projeto, sejam eles designers, desenvolvedores, gestores ou contribuinte a como organizar e configurar cada recurso que o gitlab oferece de acordo com o modelo de gestão do Padrão Digital de Governo (Design System).

Para tal, são detalhados algumas definições, metodologias, etapas, passos, tecnologias, modelos que possam auxiliar os envolvidos a entender e desempenhar suas atividades, de tal forma que todos os projetos do GOVBR-DS tenham uma organização e configuração padronizadas.

Esses documentos podem sofrer alterações ao longo do tempo, dependendo da necessidade da equipe em evoluí-los.
