---
title: "Comunidade e Contribuição"
chapter: true
weight: 1
pre: '<i class="fas fa-handshake"></i> '
---

Essa seção trata de assuntos que devem ser observados por todos que contribuem e participam, direta ou indiretamente, da construção e manutenção do GOVBR-DS. Também se aplica a todos aqueles que buscam ajuda e orientação.

Solicitamos que leiam e sigam os padrões básicos descritos nesta seção de maneira a promover um ambiente livre de assédio, seguro, confortável e agradável.

**Não toleramos assédio dos participantes de nenhuma forma.**
