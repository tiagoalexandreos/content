---
title: 'Contribuindo com o Design System'
description: Guia sobre como contribuir com o Design System
---

Antes de começarmos, obrigado por contribuir com o projeto. {{< emoji ":tada::+1:" >}}

As diretrizes abaixo foram separadas em páginas distintas para facilitar o entendimento dos assuntos tratados. Os documentos devem servir como guia para seus respectivos assuntos, mas sinta-se à vontade para propor mudanças e melhorias nos documentos (preferencialmente por Merge Request {{< emoji ":wink:" >}})

## Sobre o Projeto

O GOVBR-DS apresenta os padrões de interface que devem ser seguidos por designers e desenvolvedores para garantir a experiência única do usuário na interação com os serviços digitais de Governo.

## Status do Projeto

No Design System é possível encontrar diversas ferramentas para lhe apoiar na criação/ideação de um novo produto, que podem ser encontradas na área de download, documentações do site e wiki.

Entregamos UIKits de design, com todos os padrões existentes no Design System, assim poderá focar mais nas questões estratégicas.
No momento da implementação dos protótipos, conte com os códigos disponíveis para download, assim fica mais fácil seguir os padrões de design e manter a qualidade e consistência das interfaces.

Além dos códigos, temos uma iniciativa de construção da biblioteca de WebComponents compatível com os principais frameworks frontend para simplificar o consumo do Design System no desenvolvimento dos seus projetos.

Entramos em uma nova fase, onde tornamos o Design System público e aberto para evolução em comunidade, todos os artefatos podem ser encontrados no gitlab, assim poderão embarcar nessa jornada conosco na evolução de todo Padrão Digital de Governo.

Não deixe de conferir os princípios do Design System, pois é a nossa base de criação de todas as soluções e participar dessa comunidade, contamos com o seu apoio na colaboração com a evolução dos padrões e assim viabilizarmos a construção de soluções robustas, eficientes e inovadoras, sempre com uma olhar na experiência do usuário.

- Link para os repositórios do Design System no gitlab govbr-ds:  <https://gitlab.com/govbr-ds>.
<!-- FIXME: Roadmap quase em branco para usuários sem permissão -->
- Acesse o roadmap do projeto no git:  <https://gitlab.com/groups/govbr-ds/-/roadmap>
- Participe da comunidade no Discord:  <https://discord.gg/U5GwPfqhUP>

## Eu não quero ler tudo, eu só quero fazer uma pergunta

{{% notice tip %}}
Caso tenha dúvidas, pesquise aqui na nossa Wiki (ela é o nosso repositório de conhecimento).
{{% /notice %}}

{{% notice info %}}
Por favor **não** crie issues para fazer perguntas...
{{% /notice %}}

Caso não consiga encontrar a informação que deseja, use nossos canais abaixo para tirar suas dúvidas:

- Pelo nosso email <govbr-ds@serpro.gov.br>
- Usando nosso canal no discord <https://discord.gg/U5GwPfqhUP>

## Princípios e Valores

### Princípios

- Experiência Única
- Eficiência e Clareza
- Acessibilidade
- Reutilização
- Colaboração
Para saber mais sobre os princípio dos Design System acesse a página: <https://www.gov.br/ds/introducao/principios>

### Valores

- Segurança - Criar um produto robusto, íntegro e confiável.
- Excelência - Trabalhar todos os padrões web, garantindo assim aspectos de performance, qualidade, usabilidade e acessibilidade.
- Consistência - Interfaces intuitivas e fáceis de relembrar.
- Inovação - Trazer o foco para o estratégico no lugar do operacional.

## Código de Conduta do Design System

Esse projeto e todos os contribuintes devem seguir o [Código de Conduta do Design System]({{< ref "/codigo-de-conduta" >}} "Código de Conduta do Design System").

É esperado que todos que contribuam para o projeto sigam esse código.

Por favor reporte comportamentos inaceitáveis pelo email <govbr-ds@serpro.gov.br> ou pelo nosso canal no discord: <https://discord.gg/U5GwPfqhUP>.

## Como eu posso contribuir?

Se deseja contribuir com a evolução ou proposição de um novo fundamento, componente, padrão, modelo ou template para o Design System, basta abrir uma issue no projeto Core para as questões de Design ou FrontEnd: <https://gitlab.com/govbr-ds/dev/govbr-ds-dev-core/-/issues/new>

Caso queira contribuir na evolução das bibliotecas de componentes, pode abrir uma issue ou criar um fork e nos enviar um Merge Request seguindo os padrões definidos na wiki: [Desenvolvimento](/desenvolvimento).

> Lembre-se de adicionar um título e uma descrição que deixe claro sua contribuição :partying_face:

Caso tenham alguma dúvida utilize o nosso canal no discord:

- Primeiramente você deve estar cadastrado e autenticado na plataformas
- Acesse o link e participe da comunidade: <https://discord.gg/U5GwPfqhUP>.
