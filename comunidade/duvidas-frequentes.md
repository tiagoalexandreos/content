---
title: Dúvidas Frequentes
description: Perguntas e respostas frequentes sobre o Design System
---

{{% expand "É obrigatório o uso do Design System?" %}}
<!-- FIXME: Reformular esse texto quando sair o decreto/portaria sobre obrigatoriedade de uso do DS -->
Atualmente não existe nenhuma obrigatoriedade no uso do padrão de governo (<https://www.gov.br/ds/>). O que existe são recomendações. Temos informações de que várias universidades, institutos federais e órgãos estão aderindo ao padrão devido a facilidade e outros ganhos (no desenvolvimento, para experiência do usuário...) ao adotar o Design System do Governo.
