---
title: Glossário
description: Este documento é um glossário que contém palavras ou expressões mais relevantes, utilizadas no Design System 
date: 27/10/2021
keywords: design system, princípios, tokens, fundamento visual, componentes, padrões, modelos, funcionalidades, aplicações, atomic design 
---

### Design System

Design System é documento vivo com todos os componentes e propriedades de um produto ou serviço com objetivo de facilitar e padronizar a comunicação e o desenvolvimento do time.

[https://brasil.uxdesign.cc/entendendo-design-system-f375bbb6f704](https://brasil.uxdesign.cc/entendendo-design-system-f375bbb6f704)

### Princípios

Os princípios de um Design System devem ser encarados como os objetivos do produto, que resolvem os problemas operacionais entre o time. A ideia central é direcionar as decisões do time envolvido na criação, divulgação, capacitação, manutenção, evolução e suporte.

Exemplo dos princípios: Eficiência, Clareza, etc.

### Design Tokens

Os tokens de design são coleções de atributos visuais que representam as pequenas e repetidas decisões de design que compõem um sistema de design. Os tokens substituem valores estáticos (como códigos hexadecimais para cores, valor em pixel para um espaçamento, etc.) por nomes autoexplicativos.

O uso de Tokens permite que o sistema de design permaneça escalável e consistente, além de aumentar a customização dos sistemas.

Um token possuem 2 partes:

- Um nome semântico que é dado ao valor (geralmente focado na sua função), como por exemplo **color-dark**;
- O valor do estilo, como por exemplo `#000000`

Exemplo final de um token no padrão de variável para CSS : `--color-dark: #000000`

### Fundamento Visual

Os Fundamentos Visuais são elementos primordiais para criação dos componentes ou de qualquer interface que queira seguir os princípios e a  identidade visual do Design System.

Cada fundamento é detalhado por regras para trazer consistência visual e comportamental para serem utilizados em qualquer tipo de interface de usuário.

Exemplo de fundamentos: Cor, Espaçamento, Tipografia, etc.

### Componente

É um item de interface que carrega consiga as definições do Design System. Os componentes são um dos principais blocos de construção do sistema de design. Sua reutilização sistemática ajuda a criar consistência visual e funcional entre os produtos.

Cada componente foi projetado e codificado para resolver problemas específicos de interface do usuário, como apresentar uma lista de opções, permitir o envio de um formulário, fornecer feedback ao usuário e assim por diante. Todos os componentes do Design System foram projetados para funcionar harmoniosamente juntos, como partes de um todo maior.

Exemplo de componentes: Modal, Tab, etc.

### Padrão

Os padrões são soluções de práticas recomendadas (boas práticas/orientações) para um usuário atingir um objetivo.

Também podemos entender como assuntos trazendo estudo e definições claras de UX que ajudam a definir a forma de uso, conteúdos mais adequados: Como por exemplo, instruir qual o componente mais adequado para uma determinada situação.

No Design System criamos padrões para comportamentos recomendados que mostram combinações reutilizáveis com base em fundamentos visuais e componentes tendo o objetivo de atender necessidades comuns dos usuários, utilizando-se de sequências e/ou fluxos descritivos ou visuais.

Um padrão deve guiar e instruir o usuário com soluções para resolver problemas relacionados a um determinado assunto. Geralmente são problemas recorrentes que a solução acaba por virar uma padrão.

Exemplo de padrões: Densidade, Navegação, etc.

### Modelo

Um modelo é a representação visual de um fundamento, de um componente ou de um padrão, que pode ser utilizado como um exemplo visual ou como um ponto de partida para criação de uma interface contendo uma estrutura predefinida de forma que facilite o desenvolvimento do conteúdo.

Em resumo, representa um elemento composto por componentes, uma tela ou fluxo de navegação, e serve de base para construção de uma interfaces para os usuários.

Os modelos são construídos levando em consideração todas as regras básicas de uso dos componentes, fundamentos e padrões. Como todas as regras já estão aplicadas, o processo de criação de telas são agilizados.

No GOVBR-DS é disponibilizado alguns modelos focados em padrões de tela descrevendo uma funcionalidade especifica, ou um fluxo. Muitas pessoas conhecem esse tipo de modelo como *template*. Além disso, disponibilizamos modelos de componentes dentro das diretrizes (exemplos interativos) e em bibliotecas de componentes (os **UIKits**), isto é, um conjunto de modelos organizados em um mesmo artefato.

Exemplo de modelos: Tela de sessão de Usuário, Tela de Erro, Fluxo de CRUD, etc.

### Funcionalidade

Funcionalidade é qualquer interação criada baseado em um estudo de UX.

Exemplo: upload de documentos, Pagamento, etc.

### Aplicações

Aplicações são sistemas que consomem e estão aderentes ao Design System.

Exemplo: Portal do Embrapa, App da carteira Estudantil, etc.

## Modelo de Documento

Modelos de documentos são modelos previamente criados com finalidade de agilizar e facilitar o entendimento e o preenchimento das regras definidas pelos guias, agilizando o processo de trabalho da equipe do Design System.

---

### *Atomic Design*

> O *Atomic Design* é uma metodologia que nos ajuda a pensar na interface do usuário (UI) de maneira hierárquica e reforça a importância da qualidade de *pattern libraries* eficazes, e apresenta técnicas para otimizar o fluxo de trabalho do design e desenvolvimento em equipe. O *Atomic Design* também detalha o que acontece durante a criação e a manutenção de sistemas de design, permitindo a implementação de UIs com mais consistência e qualidade.

*Por Brad Frost.*

[http://atomicdesign.bradfrost.com/](http://atomicdesign.bradfrost.com/)

#### Átomos

Átomos são a menor parte da matéria e servem como os blocos de construção fundamentais que vão compor as interfaces de usuário.

#### Moléculas

Moléculas são grupos relativamente simples de elementos da interface funcionando juntos como uma unidade. Como *button*, *tags*, *sliders*, etc.

#### Organismos

Organismos são grupos de moléculas que formam uma parte distinta e relativamente complexa da interface. Geralmente correspondem a seções do site como *header*, *footer*, *sidebar*, etc.

#### *Templates*

*Templates* são responsáveis pela estrutura da página onde distribuímos os componentes dando forma ao layout e definindo modelos a serem reutilizados eventualmente. Podemos pensar em *templates* como *wireframes* de baixa fidelidade.

#### Páginas

Páginas são instâncias específicas dos *templates* e mostram como é a interface do usuário com um conteúdo representativo real.

---

#### Atomic Design, Design Patterns e Design System

Dentro do Design System é trabalhado diversos conceitos com a finalidade de simplificar desenvolvimento, manter a consistência e facilitar o uso pelos usuários.

- **Fundamentos Visuais:** são os átomos no *atomic design*! Além dos princípios de design, gostamos de pensar que os fundamentos são as unidades mínimas para criação de qualquer outro componente ou interface. São conceitos e características visuais básicas que compõem qualquer outro elemento.
- **Componentes:** são as moléculas e organismo (*atomic design*). Podemos dizer que os componentes são os elementos que promovem a interação da página com o usuário. São objetos ou regiões que permitem que a interação entre a página e o usuário aconteça. Podem ser entendimentos como soluções para resolver problemas comuns dentro de uma interface, ou seja, os *ui patterns*.
- **Modelos:** seguem o mesmo conceito de páginas do *atomic design*. É a representação visual de uma solução pronta. As áreas de uma página podem definir componentes e elementos que devem ser utilizados para um determinado objetivo.
- **Padrões**: conhecido como *design patterns*. São soluções típicas para problemas comuns criados para serem reutilizados de forma sistemática em diversas soluções. Todos os itens citados anteriormente utilizam este conceito, sejam representados por elementos de interface do usuário ou por boas práticas relacionadas a uma compreensão cognitiva do comportamento humano (*persuasive design*).
