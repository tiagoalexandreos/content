---
title: Iniciativas
description: Este documento é uma página onde estão concentrados todos os links relacionados a iniciativas ligadas ao projeto Design System
date: 15/09/2022
keywords: design system, bibliotecas, react, web components
---

Iniciativas que implementam o Padrão Digital de Governo.

## GOVBR-DS - Web Components

Biblioteca de Web Components.

### Objetivo

Construir uma biblioteca de componentes que faça uso das tecnologias de Web Components e que seja reutilizável em vários frameworks, bibliotecas e browsers.

### Tecnologias

Esse projeto é desenvolvido usando:

1. [VueJS 3](https://v3.vuejs.org/ 'VueJS 3')
1. [Storybook](https://storybook.js.org/ 'Storybook')
1. [Jest](https://jestjs.io/ 'Jest')

Para saber mais detalhes sobre Web Components sugerimos que consulte o [MDN](https://developer.mozilla.org/pt-BR/docs/Web/Web_Components 'Web Components | MDN').

>Conheça a iniciativa em [Web Components](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc) e colabore com a evolução do projeto.

## GOVBR-DS - React Components

Biblioteca de componentes React.

### Objetivo

Construir uma biblioteca de componentes, usando [ReactJS](https://reactjs.org/), de acordo com os componentes especificados pelo [Padrão Digital de Governo](https://www.gov.br/ds).

### Tecnologias

Esse projeto é desenvolvido usando:

1. [React](https://reactjs.org/)
2. [TypeScript](https://www.typescriptlang.org/)
3. [Jest](https://jestjs.io/) & [React Testing Library](https://testing-library.com/docs/react-testing-library/intro/)
4. [Prettier](https://prettier.io/), [ESLint](https://eslint.org/) & [EditorConfig](https://editorconfig.org/)
5. [Storybook](https://storybook.js.org/)
6. [ViteJS](https://vitejs.dev/) para empacotamento

Para saber mais detalhes sobre React sugerimos que consulte a [Documentação Oficial](https://pt-br.reactjs.org/docs/getting-started.html).

> Conheça a iniciativa em [React](https://gitlab.com/govbr-ds/dev/react/react-components) e colabore com a evolução do projeto.

## Compartilhe sua iniciativa

Caso possua uma iniciativa em alguma tecnologia, entre em contato e compartilhe conosco através do [nosso canal no discord](https://discord.gg/NkaVZERAT7) ou através do email [govbr-ds@serpro.gov.br](mailto:govbr-ds@serpro.gov.br)
