# GOVBR-DS - Wiki (Conteúdo)

Esse o repositório que contém o conteúdo em markdown da Wiki.

## Como contribuir?

Antes de abrir um Merge Request tenha em mente algumas informações:

- Esse é um projeto opensource e contribuições são bem-vindas.
- Para facilitar a aprovação da sua contribuição, escolha um título curto, simples e explicativo para o MR, e siga os padrões da nossa [wiki](https://gov.br/ds/wiki/ 'Wiki').
- Quer contribuir com o projeto? Confira o nosso guia [como contribuir](./CONTRIBUTING.md 'Como contribuir?').

## Precisa de ajuda?

> Por favor **não** crie issues para fazer perguntas...

Use nossos canais abaixo para obter tirar suas dúvidas:

- Site do GOVBR-DS <http://gov.br/ds>

- Pelo nosso email <govbr-ds@serpro.gov.br>

- Usando nosso canal no discord <https://discord.gg/U5GwPfqhUP>

### Commits

Nesse projeto usamos um padrão para branches e commits. Por favor observe a documentação na nossa [wiki](https://gov.br/ds/wiki/ 'Wiki') para aprender sobre os nossos padrões.

## Licença

Nesse projeto usamos a [CC0 1.0 Universal](https://creativecommons.org/publicdomain/zero/1.0/ 'CC0 1.0 Universal').
