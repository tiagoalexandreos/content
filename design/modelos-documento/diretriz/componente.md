---
version: x.y.z
date: 27/10/2021
keywords: componente, anatomia, tom e voz, detalhamento, tipos, comportamentos, melhores práticas, especificação 
---

# Nome do Componente [em inglês no singular]

Uma rápida descrição. Uma simples oração respondendo às seguintes perguntas: O que é? Para que serve? Qual a principal função?

[Adicione uma imagem representando o componente. Pode ser um modelo do componente.]

![Modelo do Componente](imagens/imagem.png)
*Legenda da imagem.*

[Texto respondendo à pergunta: Quando utilizar o componente?]

**Sugestão**: recomenda-se iniciar o texto com algo como: "Utilize o [nome do componente] quando..." Caso seja interessante, pode-se fazer duas listas melhor detalhando **quando utilizar** e **quando não utilizar** o componente.

[Com uma simples lida nesta descrição, o usuário deve ser capaz de responder a seguinte pergunta: "Esse componente serve para resolver meu problema?"]

---

## Tom e voz [opcional]

Existe algum cuidado na comunicação com o usuário durante o uso? Quais são esses cuidados?

---

## Anatomia

Liste os fundamentos, componentes, áreas ou padrões que são necessários para construção do componente. Especifique quais itens são opcionais e obrigatórios. Quais elementos/áreas são necessários para a criação do componente em questão?

**Atenção:** características visuais (como cores e sombras) não devem ser listadas na anatomia, mas sim na criação dos tokens (veja mais adiante).

- **ID**: os itens devem ser listados em ordem numérica crescente, caso se tratem de elementos, ou por letras, quando relativos às áreas.
- **Nome**: nomeie os itens referentes a elementos/áreas. O ideal é utilizar a função desempenhada  para a obtenção de nomes adequados. É de suma importância a criação de bons nomes pois estes nomes serão utilizados ao longo de toda a documentação.
- **Referência**: trata-se de um fundamento, componente ou padrão? Especifique também qual o elemento dessa categoria. Determine o link para a diretriz referente. Caso seja um fundamento, não há necessidade de especificar a categoria, apenas o nome do fundamento.
- **Uso**: especifique se se trata de um elemento obrigatório ou opcional.

| ID | Nome | Referência | Uso |
| ------ | ------ | ------ | ------ |
| 1 | Label | [Tipografia](url) | Obrigatório |
| 2 | Avançar | [Componente Button](url) | Obrigatório |
| A | Área de Header | [Padrão Base](url) | Obrigatório |
| B | Área de Informação | --- | Opcional |

[Adicione uma imagem do componente com marcações visuais ilustrando a tabela criada acima.

É permitida a utilização de várias imagens para ilustrar o componente de forma mais adequada.]

![Exemplo Anatomia](imagens/imagem.png)
*Legenda da imagem.*

---

## Detalhamento dos itens

Detalhe todos os elementos/áreas listados anteriormente. Como utilizá-lo? Quais as possíveis formas de uso? Existe algum cuidado específico na sua utilização? Algum comportamento especial? O elemento possui um comportamento diferente do comum durante uma rolagem de conteúdo ou na responsividade? O componente deve ter uma atenção especial quando aplicado neste contexto? Caso haja conteúdo textual é necessário mencionar o tom e voz ou um comportamento específico?

Lembre-se de informar se o item é opcional ou obrigatório e, se for opcional, quando deve ser utilizado.

Organize os itens pela ordem exibida na anatomia, adicionando o **ID** e o **Nome**. As demais informações como **Uso obrigatório** ou **opcional** deve estar descrito no texto introdutório do elemento.

Detalhe cada item especificado na anatomia, seja ele um elemento novo ou um componente já conhecido dentro do Design System. Muitos elementos listados na anatomia podem possuir uma documentação própria, porém, a documentação não pode cobrir todas as aplicações possíveis dentro de uma interface. Neste caso, sempre que um componente ou um elemento descrito na anatomia possuir um comportamento, tom e voz, tipos, ou outra característica mais específicas, devem estar descritas de forma detalhada e separada no contexto atual.

**Atenção:** não é necessário detalhar um componente se ele já possui documentação e se comportar de forma padrão.

---

## Tipos [opcional]

Quando o elemento apresentar variações, destaque-os por tipos. Descreva e liste cada um desses tipos.

Após a listagem (numérica), faça o detalhamento de cada tipo separadamente.

Detalhe cada tipo do elemento de forma separada, descrevendo as características fundamentais que distingue cada um. Características como comportamento, tom e voz, entre outros, podem ser descritos.

Coloque o nome do tipo junto com o número da listagem no detalhamento.

Os tipos podem apresentar comportamentos, tom e voz ou outra característica específica. Ao se criar um tipo, deve haver uma clara a diferença funcional entre eles. A diferença não deve ser puramente visual.

Qual a diferença entre um tipo e os demais? Em qual momento deve-se utilizar cada um?

**Atenção 1:** se não for nítida a diferença entre os tipos, ou se é possível utilizá-los sem uma lógica clara, não há necessidade da sua existência. Portanto, é sempre necessária a justificativa da existência de um tipo.

**Atenção 2: tipo de componente é diferente de aplicação do componente**. Enquanto os tipos são variações do mesmo componente sendo utilizando em um determinado contexto (podendo ou não alterar suas características visuais), a aplicação é o uso de um determinado tipo em determinadas situações distintas, porém, sempre no mesmo contexto do componente.

Ao utilizar exemplos de aplicação, explique porque o componente foi utilizado ou qual a finalidade dele dentro da interface/exemplo.

[Adicione uma imagem do tipo.]
![Exemplo Tipo](imagens/imagem.png)
*Legenda da imagem.*

---

## Comportamentos [opcional]

Descreva os comportamentos relativos à interação do elemento dentro da interface. Comportamentos como responsividade, estados, densidades, barras de rolagem podem ser descritos nesta seção. O componente é exibido de forma diferente dependendo da interação do usuário ou quando algo é selecionado na interface? Enquanto os dados são carregados, como o componente se comporta? Existem outras opções de visualização do componente? São permitidas duas linhas de texto dentro do componente? Caso exista limite de linhas de texto, como deve ser exibido o conteúdo que ultrapassar este limite?

**Atenção:** em alguns casos, o comportamento pode ser descrito dentro do detalhamento dos tipos do componente caso seja algo específico para aquele caso.

Numere os comportamentos:

```pt-br
### 1. Nome do Comportamento

### 2. Nome do Comportamento
```

Confira uma lista de comportamentos básicos que devem ser listados, caso faça sentido: [Comportamentos Básicos](/design/checklists/comportamentos)

![Exemplo Comportamento](imagens/imagem.png)
*Legenda da imagem.*

---

### Melhores Práticas

Apesar de boas práticas se tratarem de um comportamento, é interessante destacar o assunto dentro da diretriz.

Detalhe melhor o componente: Como devo utilizá-lo? Quais as formas possíveis de uso? Existe algum cuidado específico na sua utilização?

Detalhe tendo visão global do componente. Boas práticas focadas em um item específico da anatomia devem ser especificadas na parte de "Detalhamento dos Itens".

---

## Especificação

Especifique o componente e seus tipos listando-os em forma de **tokens**.

Utilize os tokens de fundamento sempre que possível.

Organize os tokens por fundamentos e tipos. Utilize os elementos descritos na anatomia para criar referência a fim de especificá-los pelos tokens já criados nos fundamentos ou, caso não existam os tokens, por valores.

Veja o [documento de tokens](/referencias/tokens) para maiores detalhes.

Caso o elemento especificado não esteja listado na anatomia, adicione esse elemento na lista, para que ele possa ser especificado nos tokens (essa etapa auxilia para que o documento seja criado adequadamente).

Caso necessite exibir alguma imagem para detalhar as informações do token (como imagem ilustrando um espaçamento, por exemplo) posicione sempre abaixo da especificação textual. A imagem serve apenas como complemento, a **informação textual deve sempre estar presente**.
