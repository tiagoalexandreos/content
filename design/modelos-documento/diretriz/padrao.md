---
version: x.y.z
date: 27/10/2021
keywords: padrão, tom e voz, anatomia, detalhamento, tipos, layout, estrutura, comportamento, melhores práticas, especificação, referências 
---

# Nome do Padrão [no singular, de preferencialmente em português]

Elabore uma breve descrição. Uma simples oração respondendo às seguintes perguntas: O que é? Para que serve? Qual a principal função?

[Adicione uma **(e apenas uma!)** imagem representando o comportamento. Quando mais completa for a imagem para ilustrar o padrão melhor.]

![Nome do Padrão](imagens/imagem.png)
*Legenda da imagem.*

Elabore um texto respondendo à pergunta: Quando utilizar o Padrão?

**Sugestão**: recomenda-se iniciar o texto com algo como: "Utilize o [nome do componente] quando...". Quando necessário, é permitido apresentar duas listas detalhando melhor **quando utilizar** e **quando não utilizar** o componente.

Com uma simples lida nesta descrição, o usuário deve ser capaz de responder à seguinte pergunta: "Esse padrão serve para resolver meu problema?"

---

## Tom e voz [opcional]

Existe algum cuidado na comunicação com o usuário durante o uso? Quais são esses cuidados? Foque em questões textuais.

---

## Anatomia [opcional]

Liste os fundamentos, componentes, áreas e etc. que são necessários para construção do padrão. Lembre-se de mencionar se o item é opcional ou obrigatório.

Quais elementos/áreas são necessários para utilizar o padrão corretamente?

**Atenção:** características visuais (como cores e sombras) não devem ser listadas na anatomia mas sim na criação dos componentes, se existirem tokens (veja adiante).

- **ID**: os itens devem ser listados em ordem numérica crescente, caso se tratem de elementos, ou por letras, quando relativos às áreas.
- **Nome**: nomeie os itens referentes a elementos/áreas. O ideal é utilizar a função desempenhada  para a obtenção de nomes adequados. É de suma importância a criação de bons nomes pois estes nomes serão utilizados ao longo de toda a documentação.
- **Referência**: trata-se de um fundamento, componente ou padrão? Especifique também qual o elemento dessa categoria. Determine o link para a diretriz referente. Caso seja um fundamento, não há necessidade de especificar a categoria, apenas o nome do fundamento.
- **Uso**: especifique se se trata de um elemento obrigatório ou opcional.

| ID | Nome | Referência | Uso |
| ------ | ------ | ------ | ------ |
| 1 | Label | [Tipografia](url) | Obrigatório |
| 2 | Avançar | [Componente Button](url) | Obrigatório |
| A | Área de Header | [Padrão Base](url) | Obrigatório |
| B | Área de Informação | --- | Opcional |

[Adicione uma imagem exibindo e demarcando os elementos listados acima. Pode ser utilizado várias imagens pra indicar de forma mais adequada.]

![Exemplo Anatomia](imagens/imagem.png)
*Legenda da imagem.*

---

## Detalhamento dos Itens

Detalhe todos os elementos/áreas listados anteriormente. Como devo utilizá-lo? Quais as formas possíveis de uso? Existe algum cuidado específico na sua utilização? Algum comportamento específico? O elemento possui um comportamento diferente do comum durante uma rolagem de conteúdo ou na responsividade? O componente deve ter um cuidado especial quando aplicado dentro deste contexto? Caso possua conteúdo textual é preciso se preocupar com o tom e voz, ou um comportamento específico?

Organize os itens pela ordem exibida na anatomia, adicionando o ID e o Nome. Outras informações, como uso obrigatório ou opcional escreva no texto introdutório do elemento.

Muitos elementos listados na anatomia podem possuir uma documentação própria, porém, a documentação não pode cobrir todas as aplicações possíveis dentro de uma interface. Neste caso, sempre que um componente ou um elemento descrito na anatomia possuir um comportamento, tom e voz, tipos, ou outra característica mais específicas, devem estar descritas de forma detalhada e separada no contexto atual.

**Atenção:** não é necessário detalhar um componente se ele já possui documentação e se comportar de forma padrão.

---

## Tipos [opcional]

Quando o padrão possuir variações, destaque-as por tipos. Descreva e liste cada um desses tipos.

Após a listagem (numérica), faça o detalhamento de cada tipo separadamente.

Detalhe cada tipo do elemento de forma separada, descrevendo as características fundamentais que distingue cada um deles. Características como comportamento, tom e voz, entre outros, podem ser detalhados nesta seção.

Para criar um tipo, deve ser clara a diferença entre um e outro. A diferença não deve ser puramente visual.

Qual a diferença entre um tipo e os demais? Qual momento que devo utilizar um ou outro?

**Atenção:** se não for nítida a diferença entre os tipos, ou se é possível utilizá-los sem uma lógica clara, não há necessidade da sua existência. Portanto, é sempre necessária a justificativa da existência de um tipo

**Atenção 2: tipo de componente é diferente de aplicação do componente**. Enquanto os tipos são variações do mesmo componente sendo utilizando em um determinado contexto (podendo ou não alterar suas características visuais), a aplicação é o uso de um determinado tipo em determinadas situações distintas, porém, sempre no mesmo contexto do componente.

Ao utilizar exemplos de aplicação, explique porque o componente foi utilizado ou qual a finalidade dele dentro da interface/exemplo.

[Adicione uma imagem do tipo de forma completa (com todos os elementos possíveis)]

![Exemplo Tipo](imagens/imagem.png)
*Legenda da imagem.*

---

## Layout e Estrutura [opcional]

Detalhe como o padrão está estruturado de forma visual. Quais regras devem ser seguidas e aplicadas de forma consistente dentro da interface. Como os elementos devem ser organizados? Existe uma grid a ser seguida? Os elementos estão organizados por colunas, linhas, por outros componentes?

---

## [Outro Subtítulo]  [opcional]

Como os padrões podem apresentar diferentes aplicações e necessidades, crie sub-títulos novos para detalhar características específicas.

Exemplo: uma regra específica, tecnologias que podem ser utilizadas, etc.

---

### Comportamento [opcional]

Descreva os comportamentos relativos à interação do elemento dentro da interface. Comportamentos como responsividade, estados, densidades, barras de rolagem podem ser descritos nesta seção. O componente é exibido de forma diferente dependendo da interação do usuário ou quando algo é selecionado na interface? Enquanto os dados são carregados, como o componente se comporta? Existem outras opções de visualização do componente? São permitidas duas linhas de texto dentro do componente? Caso exista limite de linhas de texto, como deve ser exibido o conteúdo que ultrapassar este limite?

**Atenção:** em alguns casos, o comportamento pode ser descrito dentro do detalhamento dos tipos do componente caso seja algo específico para aquele caso.

Veja uma lista de comportamentos básicos que devem ser listados, caso faça sentido: [Comportamentos Básicos](/design/checklists/comportamentos)

O comportamento **acessibilidade** pode apresentar um hierarquia mais complexa (por causa da sua importância dentro do Design System). Por isso, não é necessário mencioná-lo dentro de comportamentos. Pode ser inserido em um novo subtítulo.

[Adicione imagens sempre que possível para ilustrar os comportamentos]

![Exemplo Comportamento](imagens/imagem.png)
*Legenda da imagem.*

---

### Melhores Práticas

Apesar de boas práticas se tratarem de um comportamento, é interessante destacar o assunto dentro da diretriz.

Detalhe melhor o componente: Como devo utilizá-lo? Quais as formas possíveis de uso? Existe algum cuidado específico na sua utilização?

---

## Especificação

Especifique o componente e seus tipos listando-os em forma de **tokens**.

Utilize os tokens de fundamento sempre que possível.

Organize os tokens por fundamentos e tipos. Utilize os elementos descritos na anatomia para criar referência a fim de especificá-los pelos tokens já criados nos fundamentos ou, caso não existam os tokens, por valores.

Veja o [documento de tokens](/referencias/tokens) para maiores detalhes.

Caso o elemento especificado não esteja listado na anatomia, adicione esse elemento na lista, para que ele possa ser especificado nos tokens (essa etapa auxilia para que o documento seja criado adequadamente).

Caso necessite exibir alguma imagem para detalhar as informações do token (como imagem ilustrando um espaçamento, por exemplo) posicione sempre abaixo da especificação textual. A imagem serve apenas como complemento, a **informação textual deve sempre estar presente**.

---

### Referências

Liste as referências utilizadas na criação do documento.

Evite o uso de listagem de URLs. Neste caso, prefira o uso de hiperlinks.
