---
version: x.y.z
date: 27/10/2021
keywords: fundamento, princípio, experiência única, eficiência e clareza, acessibilidade, reutilização, colaboração, design tokens, referências 
---

# Nome do Fundamento Visual [em português no singular]

Elabore um resumo descrevendo a definição ou significado do fundamento. Uma simples oração respondendo às seguintes perguntas: O que é? Para que serve? Qual a principal função?  Qual a importância dentro do Design System?.

[Adicione uma imagem representando o fundamento.]

![Modelo do Fundamento](imagens/imagem.png)
*Legenda da imagem.*

---

## Princípios

Descreva, detalhando (de forma objetiva) como cada um dos princípios abaixo devem ser utilizados dentro do fundamento que está sendo documentado. É possível criar um checklist das descrições destes princípios para verificar se está  seguindo corretamente o princípio? Qual característica e parâmetro dentro do fundamento pode representar o princípio?

### Experiência Única

Quais os padrões estruturais e visuais do fundamento? A experiência precisa conferir confiança aos usuários, diminuindo sua curva de aprendizagem. A interface deve ser consistente, porém flexível, e adaptável de acordo com o contexto de quem a utiliza.

### Eficiência e Clareza

Como aplicar o fundamento de maneira simples e eficaz? Como deve ser organizado e estruturado para solucionar de forma rápida e clara as necessidades dos usuários? Quais são as informações essenciais para o uso do fundamento?

### Acessibilidade

O sistema de serviços deve cumprir as diferentes necessidades de acessibilidade, atendendo às orientações do e-MAG - Modelo de Acessibilidade em Governo Eletrônico - e, principalmente, considerar os diferentes usos e acessos ao portal. Quais as questões de acessibilidades que devem ser levadas em consideração ao utilizar o fundamento?

### Reutilização e Colaboração

Informações relacionadas à importância da reutilização do fundamento ao longo do projeto e como o fundamento pode ser evoluído de forma colaborativa junto aos usuários.

---

**[Detalhando o Fundamento]**
Defina e detalhe todas as características e parâmetros do fundamento, mantendo a consistência com os princípios definidos e que possam manter a mesma identidade dentro do Design System.
Todas as características descritas serão utilizadas para formação de novos componentes que venham a utilizar o fundamento.

Estruture a documentação por subtítulos que façam sentido para melhorar o entendimento e a aplicação do fundamento por parte do usuário (seja ela responsável por prototipar uma interface ou apenas criador de um novo componente para o Design System).
Não economize no uso de imagens para exemplificar o texto.

**[Design Tokens]**
É importante que o fundamento traga valores consistentes e de forma escalonável para ser utilizado dentro da interface. O designer deve estabelecer esses padrões e valores, enquanto o desenvolvedor fica encarregado da nomenclatura do token.

Tenha em mente que sem o token o desenvolvedor front-end não conseguirá utilizar o fundamento dentro da interface.

## Referências [Opcional]

Liste as referencias utilizadas na criação do documento.

Evite o uso de listagem de URLs, neste caso, prefiro o uso de hiperlinks.
