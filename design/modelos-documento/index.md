---
title: 'Modelos de Documento'
pre: '<i class="fas fa-asterisk"></i> '
---

{{% notice note %}}
Os textos tachados estão obsoletos/depreciados. Aguarde nova versão.
{{% /notice %}}

Os **modelos de documentos** são estruturas pré-construídas, respeitando as orientações e regras adotadas pelo Design System que servem de base para a construção de novos documentos.

Entenda como um "*wireframe*" para documentos.

Utilize os modelos de documento de changelog, diretriz, elemento atômico, tokens e vários outros para agilizar facilitar o seu trabalho.

## Arquivo fonte

Modelos de arquivos de prototipação para os três principais artefatos trabalhados no dia a dia do Design System:

- [Fundamento | Formato XD - xxx KB](elemento-atomico/fundamento.xd)
- [Componente  | Formato XD - xxx KB](elemento-atomico/componente.xd)
- [Padrão  | Formato XD - xxx KB](elemento-atomico/padrao.xd)

## Diretriz

Modelos de diretriz para os três principais artefatos trabalhados no dia a dia do Design System:

- {{% download "diretriz/fundamento.md" "Fundamento | Formato MD - xxx KB" %}}
- {{% download "diretriz/componente.md" "Componente  | Formato MD - xxx KB" %}}
- {{% download "diretriz/padrao.md" "Padrão  | Formato MD - xxx KB" %}}

## Modelo

Modelos para a construção de modelos de telas:

- [Criar Modelos no Figma  | Formato FIG - xxx KB](modelo/modelo.fig)
- [Criar Modelos no Adobe XD  | Formato XD - xxx KB](modelo/modelo.xd)

## Revisão

Modelo de documento para a realização do processo de revisão.

- {{% download "revisao/revisao.md" "Revisão de Design  | Formato MD - xxx KB" %}}

## ~~Changelog~~

~~Modelo de documento para a realização do processo de versionamento.~~

- {{% download "changelog/changelog.txt" "Changelog  | Formato TXT - xxx KB" %}}
