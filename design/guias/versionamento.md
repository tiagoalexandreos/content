---
title: Versionamento
description: Este documento detalha as orientações e padrões para criação e manutenção do versionamento dos artefatos de design
date: 29/10/2021
keywords: versionamento, artefatos, versionamento semântico, release, branch, major, minor, patch, changelog, fluxo, desenvolvimento, validação, revisão, responsabilidades
---


**Todas** as releases serão versionadas seguindo o sistema baseado no versionamento semântico versão 2.0.0 ([https://semver.org/](https://semver.org/lang/pt-BR/spec/v2.0.0.html) ou [https://fjorgemota.com/](https://fjorgemota.com/versionamento-semantico-ou-como-versionar-software/))

Sempre que existir qualquer mudança de artefato, uma nova versão deve ser criada (nova release) seguindo todas as regras do versionamento semântico. Além disso, uma documentação da mudança deve ser registrada no que é chamado de **Changelog** (veja adiante).

## Consulta rápida

A regra define nomes para x, y e z para uma versão: x.y.z. Onde:

- x é a versão **maior** (ou MAJOR version);
- y é a versão **menor** (ou MINOR version);
- z é a versão de **correção** (ou PATCH version).

Criando o seguinte formato MAJOR.MINOR.PATCH.

### MAJOR (Evolução do Artefato)

Mudanças incompatíveis com a versão anterior. Quando for necessário alterar esta versão, deve existir uma reunião com o time para avaliar e decidir se haverá ou não a mudança.

### MINOR (Nova Funcionalidade)

Adiciona funcionalidade de uma maneira compatível com versão atual MAJOR. Adicionar comportamento ou um tipo novo no componente é um bom exemplo de necessidade de alterar a versão MINOR do artefato.

### PATCH (Correção de BUGS)

Correções de erros compatíveis com a MINOR ativa.

São modificações de ajustes como correção de algo que foi criado de forma equivocada ou melhorias de uma funcionalidade do artefato.

Essas alterações, não alteram o conceito do objeto e deve manter a consistência com as funcionalidades/elementos já criadas.

### PRE-RELEASE (Versão instável)

Sinalização de não cumprimento de padrão de compatibilidade (ex: -alpha, -beta).

Quando o artefato não segue todas os padrões, apresentam bugs ou não está preparado para o usuário (seja o time ou público externo).

Dentro da equipe de design existe a seguinte PRE-RELEASE (pré-lançamento):

- **-devX:** utilizado quando o arquivo está na fase de desenvolvimento.

**Obs:** o X é um valor de incremento, começando em 01. Devendo ser alterado (adicionando +1 ao valor) sempre que for realizado ajuste dentro do fluxo de versionamento, sendo retirado ao ser gerado uma nova release.

## Changelog

Um changelog é uma lista selecionada, ordenada cronologicamente, de mudanças significativas para cada versão de um projeto.

Mais informações: [https://keepachangelog.com](https://keepachangelog.com/pt-BR/1.0.0/)

Para cada pacote de design criado, deve existir um changelog específico. Esse arquivo deve estar no formato TXT com o nome changelog (changelog.txt) ou uma região dentro do seu artefato (como um arquivo para Adobe XD).

### Princípios fundamentais

- Changelogs são para humanos, não máquinas;
- Deve haver uma entrada para cada versão;
- Alterações do mesmo tipo devem ser agrupadas;
- A versão mais recente devem vir em primeiro lugar;
- A data de lançamento de cada versão é exibida (dd/mm/aaaa).

### Tipos de mudanças

- **Added/Adicionado** para novos recursos.
- **Changed/Modificado** para alterações em recursos existentes;
- **Deprecated/Obsoleto** para recursos que serão removidos nas próximas versões;
- **Removed/Removido** para recursos removidos nesta versão;
- **Fixed/Corrigido** para qualquer correção de bug;
- **Security/Segurança** em caso de vulnerabilidades.

O responsável por criar o changelog é o desenvolvedor do artefato. A nova release é criada pelo responsável na Fase de Release.

---

## Fluxo de versionamento

Para um designer criar ou atualizar um arquivo, deve se passar pelo fluxo que contém as seguintes fases:

- Fase de Desenvolvimento
- Fase Validação/Viabilidade
- Fase de Revisão de Qualidade
- Fase de Release.

Enquanto estiver no fluxo de versionamento, deve se utilizar os valores de PRE-RELEASE e somente no final, uma versão estável será criada, retirando assim os valores e criando uma nova versão de release.

Para entender os cuidados que cada responsável deve ter em cada fase, veja os detalhes na parte de **Responsabilidade** após o detalhe das fases.

### Fase de Desenvolvimento

O designer deve criar uma branch com a versão mais atual do artefato. Para Isso ele deve COPIAR a versão mais recente disponível no diretório **release** para o diretório **branch**, alterar a versão do arquivo referente as mudanças que devem ser realizadas e **acrescentar** o valor de PRE-RELEASE de desenvolvimento (-devX) no diretório, no caso: **-dev01**

- *Caso esteja trabalhando em ajuste de de correção no componente button e a última versão disponível é "1.2.3", ele copia esse versão para o diretório branch, altera a versão (1.2.4) e adicionando o valor de PRE-RELEASE (-dev01). Ficando algo como "1.2.4-dev01".*
- *Caso não tenha nenhuma versão de release disponível, utilize a versão 1.0.0 como referência: "1.0.0-dev01".*

**ATENÇÃO**: nunca mova ou edite qualquer conteúdo existente no diretório **release**! Nenhum arquivo ou diretório deve sofrer modificações, por ninguém, em NENHUMA SITUAÇÃO!

Os diretórios no branch é chamado de pacote de versionamento, ou pacote de pre-release. E só pode existir **um pacote** dentro do diretório branch.

Os diretórios na release são chamados de pacote de versionamento, ou pacote de release. E pode existir inúmeros pacotes. **O pacote de release é criado na fase de Release, e somente o responsável pela release pode criá-las**!!

Antes de iniciar os trabalhos, o designer deve realizar reuniões em conjunto com a pessoa responsável pela validação/viabilidade (podendo também adicionar a pessoa da revisão) para entender melhor os problemas e criarem soluções que sigam as orientações descritas no Design System.

Durante a criação, o designer poderá pedir auxílio ao desenvolvedor responsável afim de sanar dúvidas técnicas sobre a construção do elemento atômico.

Após a criação de uma proposta, o designer deve apresentar a solução ao responsável da validação/viabilidade a fim de receber feedbacks importantes (e saber se tudo foi realizado como o combinado), no caso, o designer deve encaminhar o pacote da versão para a revisão (Fase de Revisão).

**Importante**: sempre que o responsável pela desenvolvimento for fazer alguma alteração a pedido do responsável da validação/viabilidade ou da revisão, o designer deverá aumentar o número de incremento da PRE-RELEASE do pacote!

### Fase de Revisão (Qualidade)

Após realizar a revisão, o responsável deve seguir entre dois caminhos que dependerá se algum artefato precisará ou não de algum ajuste:

- **Em caso de ajuste:** o designer fica como responsável novamente pelo artefato e deverá aumentar o valor de incremento da PRE-RELEASE, indicando que houve mudanças desde na versão anterior.

*“1.2.4-dev03” para “1.2.4-dev04” ou “1.0.0-dev02” para “1.0.0-dev03”.*

- **Não necessitando de ajuste:** o designer deve encaminhar o pacote da versão para a validação/viabilidade (Fase de Validação/Viabilidade).

### Fase de Validação/Viabilidade

Após realizar a validação/viabilidade, o pacote pode seguir 2 caminhos que dependerá se existe artefato precisando ou não de algum ajuste:

- **Em caso de ajuste:** o designer fica como responsável novamente pelo artefato e deverá aumentar o valor de incremento da PRE-RELEASE, indicando que houve mudanças desde na versão inicial.

*“1.2.4-dev01” para “1.2.4-dev02” ou “1.0.0-dev01” para “1.0.0-dev02”.*

O designer deverá realizar os ajustes necessários ou justificar qualquer dúvida pertinente com o responsável da validação/viabilidade. Após tudo ser realizado, deverá ser enviado o pacote novamente para uma nova validação/viabilidade. Este fluxo se repete até que o responsável da validação/viabilidade ache que não há necessidade de ajustes.

- **Não necessitando de ajuste:** o designer deve encaminhar o pacote da versão para a release (Fase de Release)

### Fase de Release

O designer deve retirar o valor de PRE-RELEASE de desenvolvimento do arquivo.

*1.2.4-dev02 para “1.2.4” ou "1.0.0-dev02" para "1.0.0".*

Além de retirar o valor de PRE-RELEASE, deve se seguir as seguintes ações:

- Ajustar a versão de release no do pacote changelog (changelog.txt);
- Ajustar a versão de release no arquivo fonte do elemento atômico (.xd);
- Ajustar a versão de release no modelo (.xd), caso exista;
- Ajustar ou adicionar a versão de release na arquivo diretriz (.md): ex: [version]: # (x.y.z);
- E finalmente, mover o pacote de versionamento para o diretório release.

Caso o arquivo precise ser adicionado na área de Download do site (como um UIKit ou um modelo), deve-se gerar um pacote adicional para ser colocado na área de download:

- Crie um arquivo ".zip" com o pacote que foi gerado da release;
- Renomeie o nome do pacote com o nome do elemento atômico e adicione a versão. Siga os padrões de nomenclatura para este caso ("nomedoartefato_versao"). Exemplo: *"button_1.2.4", "button_1.0.0"...*
- Coloque o arquivo no diretório de download. Atualmente são adicionados no seguinte diretório: [Download Site](https://serprodrive.serpro.gov.br/apps/files/?dir=/_solucoes/Governo%20Digital/Design_System_Gov/8-Download%20Site&fileid=109790504). Escolha o diretório dentro dele que mais faça sentido, coloque na raiz ou converse com time para saber o lugar mais adequado.
- Gere o link para download. Para gerar o link de download, clique no botão direito e escolha **Detalhes**. Depois escolha as opções **Compartilhando**. Em "Link de compartilhamento" selecione o ícone "+" para gerar o link.

Após todos esses passos terem serem seguidos, indicará que o arquivo está finalizado e já está pronto para ser utilizado pelos usuários ou para ser desenvolvido, finalizando então o Fluxo de Versionamento.

## Responsabilidade

Baseando-se no Fluxo de Versionamento Pré-Release, temos os papéis do designer, do revisor, do desenvolvedor e da release. Cada papel possui uma responsabilidade que deve ser seguida e entendida pela pessoa encarregada, tornando a construção e processo do Design System mais eficiente possível.

### Do designer (dsg)

O designer é responsável pela criação e atualização do Artefato fonte e da Diretriz, ou de qualquer artefato necessário para conclusão da atividade. Suas atribuições estão descritas na responsabilidade técnica e administrativa.

O designer deve ser responsável pela release. Ele deve ser capaz de gerar uma versão para todos os artefatos criados. Também deve gerar os arquivos de designers que serão utilizando dentro do site do Design System pelos usuários finais (área de Download).

#### Responsabilidade Técnica - dsg

- Seguir os padrões de Nomenclatura
- Geral
- Design Token
- Assets
- Seguir as "regras de versionamento" no que couber, tais como:
- Fluxo de Versionamento Pre-Release
- Fase de desenvolvimento
- Criação dos Changelog
- Utilizar os tipos de Mudança
- Seguir a "Estrutura Básica de Diretórios e Artefatos" quando criar/modificar/adicionar qualquer Elemento Atômico
- Diretórios Básicos
- Artefatos Básicos
- Seguir o "Guia de Arquivo Fonte" quando criar/atualizar qualquer Elemento Atômico
- Importar o UIkit
- Criar e utilizar as Pranchetas corretamente
- Criar os Elementos Gráficos/Atômicos corretamente:
- Utilizar os Ativos do UI KIT
- Utilizar as Nomenclaturas para assets
- Utilizar sempre valores inteiros
- Criar Ativos
- Utilizar Cotas e Legendas Padrões
- Seguir o "Guia de Diretriz" quando criar/modificar/adicionar qualquer diretriz de um Elemento Atômico
- Fundamento
- Componente
- Template
- Seguir o "Guia para Imagens" para criação de imagens que serão utilizadas nas diretrizes
- Seguir as "regras de versionamento" no que couber, tais como:
- Fluxo de Versionamento Pre-Release
- Fase de release
- Criar arquivos que será utilizado na área de download quando existir a necessidade;
- Gerar o link de download e repassar para o desenvolvedor.
- Caso a fase de design não tenha sido concluída, verificar qual etapa ainda deve ser realizada, detalhando os impactos descritos pelo designer assim como outras atividades a serem realizada, se necessário.

#### Responsabilidade Administrativa - dsg

- Atualizar diariamente o Serpro Drive com os artefatos que estão sendo criados.
- Estimar o tempo de criação de cada artefato.
- Utilizar corretamente o ALM
- Listar os impactos gerados pelo criação/atualização do elemento atômico (caso exista)
- Registrar as ações no local correto: [Utilizando o Git](/Design/Padrões/Utilizando Git.md)
- Avisar ao time que as release foi gerada e informar a localização das mesmas.
- Caso a [fase de design](/fase-criacao) tenha sido concluída, avisar para equipe de desenvolvedores que a etapa de design foi concluída;
- Registrar as ações no local correto: [Utilizando o Git](/design/guias/utilizando-git)

### Do revisor (rev)

O revisor deve verificar se os padrões de design foram seguidos nas documentações, na construção do componente. O revisor também deve verificar se os requisitos foram atendidos de forma satisfatória nos artefatos criados. Verificar se os artefatos foram construídos corretamente sem apresentar inconsistência com padrões estabelecidos pela equipe. Analisar se tudo foi criado corretamente pelo designer para gerar uma release do artefato.

#### Responsabilidade Técnica - rev

- Seguir as "regras de versionamento" no que couber, tais como:
- Fluxo de Versionamento Pre-Release
- Fase de revisão
- Ler a demanda (item de backlog) para entender melhor a atividade;
- Ler toda a diretriz do componente e propor melhorias no texto para que o usuário tenha melhor entendimento das instruções e informações descritas.
- Informar corretamente ao designer os ajustes necessários;

#### Responsabilidade Administrativa -rev

- Avisar ao designer quando a revisão for concluída, deixando claro se existe ajustes ou não.
- Registrar as ações no local correto: [Utilizando o Git](/Design/Padrões/Utilizando Git.md)

### Do desenvolvimento (dev)

O responsável pelo desenvolvimento deve ser capaz de comunicar com o design e participar da atividade com a finalidade de dar sugestões e principalmente alertar sobre possíveis inviabilidades dentro do desenvolvimento relacionadas a atividade.

#### Responsabilidade Técnica - dev

- Ler o documento de design e verificar se existe algo que foge do padrão de implementação já utilizado ou que não seja viável a implementação por parte da equipe de desenvolvimento;
- Gerar os nomes dos tokens que devem ser utilizados nas diretrizes, a partir dos valores gerados pelos designers.
- Orientar melhor os designers sobre os tokens já utilizados dentro do projeto.

#### Responsabilidade Administrativa - dev

- Avisar ao designer quando a validação for concluída, deixando claro se existe ajustes ou não.
- Registrar as ações no local correto: [Utilizando o Git](/Design/Padrões/Utilizando Git.md)
