---
title: Serpro Drive - Estrutura de diretórios e artefatos
description: Este documento lista as orientações para organização dos diretórios e artefatos do Design System, no repositório do Serpro Drive 
date: 13/01/2021
keywords: estrutura, diretórios, artefatos, pastas, branch, release, versões
---


Para criação de artefatos voltados para design (design assets) será utilizado o diretório/pasta **3-ui** encontrado dentro do diretório oficial do Design System no Serpro Drive ([Serpro Drive](https://serprodrive.serpro.gov.br/)).

Atualmente o diretório se encontra no seguinte endereço: [Serpro Drive](https://serprodrive.serpro.gov.br/apps/files/?dir=/_solucoes/Governo%20Digital/Design_System_Gov/3-ui)

Dentro do diretório **3-ui** será colocado todos os produtos de design. A partir dele, todos os diretórios e arquivos criados deverão seguir aos padrões definidos neste documento (antes dele, o padrão poderá ser outro. Por este motivo que o nome 3-ui, está diferente das instruções deste documento). Atualmente existem 8 diretórios filhos:

- **Fundamentos**: diretório relacionado aos os átomos (atomic design) do Design System.
- **Componentes**: diretório relacionado as moléculas e os organismos dentro do Design System.
- **Templates**: diretório relacionado aos templates do Design System.
- **Páginas**: diretório relacionado as Pages do Design System .
- **Padrões**: diretório relacionado aos Padrões do Design System.
- **UIKit**: diretório relacionados ao arquivos do kit de ferramenta de design
- **Showcase:** diretório relacionado ao site do Design System.
- **DesignOps**: diretório onde poderão ser encontrados todos os arquivos relacionados aos processos de design dentro do projeto, além de ferramentas que possam melhorar a produção dentro da equipe.

Criando a seguinte estrutura hierárquica:

- **3-ui**
- **componentes**
- **designops**
- **fundamentos**
- **padroes**
- **paginas**
- **showcase**
- **templates**
- **uikit**

Os diretórios **fundamentos**, **componentes**, **padroes**, **paginas** e **templates** estão diretamente inseridos no modelo de Atomic Design, desta forma, foi criado uma estrutura que deve ser seguida ao criar qualquer tipo de elemento atômico (seja fundamento, componente, template...). Para entender melhor o que significa cada elemento atômico veja a área do [Glossário](/referencias/glossario).

## Criando um elemento atômico

Para criar um elemento atômico precisamos nos preocupar com a estrutura de diretórios e de artefatos básicos e como construir esses artefatos. Para explicar melhor como seguir esses passos, será dividido em 3 partes:

- **Diretórios Básicos:** quais diretórios devem ser criados ao criar um elemento.
- **Artefatos Básicos:** quais arquivos devem ser criados ao criar um elemento.
- **Construindo Artefatos:** como criar os artefatos básicos.

### Parte 1 - Diretórios Básicos

Primeiro passo é a criação dos diretórios básicos onde organizará cada elemento atômico e suas versões:

- Crie um diretório com o nome “**nome do elemento**” (onde "nome do elemento" é o nome do elemento atômico que será criado. Ex: button, tab, tag….) dentro de uma das categorias/diretórios base do Design System (fundamento, componente, template...).
- Dentro deste diretório, crie mais dois diretórios chamados: **release** e **branch.**

Essa é a construção padrão para criação de qualquer elemento atômico dentro do Design System. Por exemplo, se for preciso criar um novo Fundamento chamado “Espaçamento”, a estrutura hierárquica, seguindo todos os padrões de nomenclatura e de estrutura, ficaria da seguinte forma:

- **fundamentos**
- **espacamento**
- **release**
- **branch**

É claro que podemos ter outros diretórios dentro dessa hierarquia, como um diretório *estudo* por exemplo. Mas a construção básica deve ser seguida em todos os casos onde se queira criar um novo elemento atômico.

O detalhamento de cada diretório será descrito mais adiante, porém, em resumo, podemos dizer que ambos os diretórios (release e branch) concentra versões do elemento atômico: a **branch** com versões ainda em desenvolvimento/pré-release; e a **release** com versões estáveis/release, todos eles seguindo o [Fluxo de Versionamento](/design/guias/versionamento).

Por enquanto, como foco é a criação de um novo elemento atômico, vamos nos concentrar apenas no diretório branch...

No segundo passo é a criação de diretórios onde ficarão os artefatos de design criados para concepção do elemento atômico, ou seja, os pacotes de versões!

No diretório **branch** encontraremos o pacote de versão chamado "**pacote de pré-release**". Atualmente só deve existir 1 pacote.

No "pacote de pré-release" é onde encontraremos os artefatos de design necessários para construção de qualquer elemento atômico. Como ele segue diretamente o fluxo de versionamento, a nomenclatura dele também segue algumas regras:

- **Versão referência para desenvolvimento** + **label de pré-release**. (Lembre-se: caso não tenha nenhuma versão de referência, a versão será 0.0.0).

Como no exemplo, estamos trabalhando em um novo elemento atômico, a nomenclatura ficaria: **0.0.0-dev01**. Para maiores detalhes sobre criação da pacote de pré-release,veja em [fluxo de versionamento](/design/guias/versionamento).

Voltando para o nosso exemplo, temos a seguinte estrutura:

- **fundamentos**
- **espacamento**
- **release**
- **branch**
- **0.0.0-dev01**

Após criado o diretório do pacote de pré-release, já será possível criar os artefatos básicos de design. Porém, para organizar estes artefatos, deve-se criar mais 2 diretórios:

- O diretório **diretriz**.
- E dentro de diretriz, o diretório chamado **imagens**.

Temos finalmente a seguinte organização (levando em conta o exemplo anterior):

- **fundamentos**
- **espacamento**
- **release**
- **branch**
- **0.0.0-dev01**
- **diretriz**
          -  **imagens**

A seguir, a explicação de cada diretório:

- **“Nome do elemento”**: diretório que terá o nome do elemento atômico que será construído. Conterá todas as releases estáveis do elemento, as "branchs" voltadas para desenvolvimento e atualizações, além de artefatos de referência e suporte.
- **Release**: diretório obrigatório. Conterá todas as versões estáveis do elemento atômico em questão. Para criar uma release, deve-se passar pelo [fluxo de versionamento](/design/guias/versionamento).
- **Branch**: Conterá os diretórios (também conhecidos como pacotes de pré-release) ainda em desenvolvimento pela a equipe de design. Caso nao tenho nenhum diretório dentro dele, ele pode ser excluído e, posteriormente, recriado caso seja necessário.
- **Pacotes de pré-release**: Conterá todos os artefatos de design relacionados ao elemento. Sua nomenclatura e sua existência são diretamente ligados ao fluxo de versionamento. Após a finalização de um pacote de desenvolvimento, ele deverá ser movido para o diretório Release.
- **Diretriz**: diretório onde encontrará a documentação que descreve o próprio sistema de design daquele artefato, no caso as Diretrizes de Design.
- **Imagens**: diretório onde ficará todas as imagens utilizadas dentro do documento de diretriz de design.

### Parte 2 - Artefatos Básicos

Dentro dos pacotes de versão comentados no passo anterior, além do padrão de estrutura de diretórios, devemos seguir um padrão para criação dos artefatos, ou seja, arquivos que serão a base para criação do elemento atômico.

Seguindo o mesmo conceito, vamos focar do diretório branch. A seguir, detalhamento do arquivos necessários para criação do elemento atômico; o arquivo fonte e a diretriz.

#### Arquivo Fonte do Elemento Atômico

Dentro do **pacote de pré-release** deve existir o **arquivo fonte** do elemento atômico, onde será detalhado a construção, especificação, comportamentos, exemplos de uso entre outros detalhes visuais do elemento em questão. Este arquivo terá o mesmo nome do diretório pai.

Seguindo o mesma ideia dos exemplos anteriores, o fundamento “Espaçamento” deve existir um arquivo chamado: **espacamento.xd**

Este arquivo será responsável por construir as imagens e especificações utilizadas no documento de diretrizes, além de ser o artefato base para que será utilizado dentro do UI Kit do DS do governo.

Caso tenha a necessidade de criar algum outro arquivo fonte relacionado ao artefato, como uma animação explicando algum comportamento do artefato por exemplo, este arquivo ficará neste diretório. Em outras palavras, TODOS os arquivos fontes devem ser localizados neste diretório.

A construção deste arquivo fonte será descrito em detalhes mais adiante. Veja Construindo Artefatos para maiores detalhes.

O arquivo de **changelog** deve também está dentro desse diretório. Veja [Fluxo de Versionamento](/design/guias/versionamento) para maiores detalhes.

#### Diretriz do Elemento Atômico

Resumidamente, diretriz é o manual do elemento atômico criado no arquivo fonte!! Esse artefato deve conter todas as definições, instruções, exemplos, especificações do elemento atômico criado pelo arquivo fonte. Deve ser possível recriar o elemento em qualquer tecnologia, e utilizá-lo somente com uso desse diretriz.

Essa diretriz é criada no diretório **diretriz**, e deve está no formato markdown (.md), seguindo todas as regras dessa linguagem (tendo o mesmo nome do diretório “nome do elemento”) e seguir demais padrões definidos para arquivos, além de imagens que fazem parte dessa diretriz.

Seguindo o mesma ideia dos exemplos anteriores, o fundamento “Espaçamento” deve existir um arquivo chamado: **espacamento.md**

Todas as imagens utilizadas nessa documentação (criadas no arquivo fonte do elemento atômico) devem ser exportadas no formato **png** para uso no arquivo markdown para dentro do diretório **imagens**. Caso não esteja utilizando nenhuma imagem, o diretório imagens ficará vazio.

A estrutura e detalhamento de como criar a documentação/diretriz será descrita mais adiante. Veja Construindo Artefatos para maiores detalhes.

Finalmente, baseado ainda no exemplo anteriores, caso tenha seguido todas as orientações, temos a estrutura final:

- **fundamentos**
- **espacamento**
- **release**
- **branch**
- **0.0.0-dev01**
- **espacamento.xd**
- **changelog.tx**
- **diretriz**
          -  **espacamento.md**
          -  **imagens**
  - **imagem-exemplo.png**
  - **imagem-outroexemplo.png**

### Parte 3 - Construindo Artefatos

Para construção dos artefatos básicos será necessário seguir um padrão que é específico para cada artefato, esse padrão será chamado de guia.

Com exceção do changelog da diretriz (que é tratado em Versionamento) cada um dos outros tipos de artefatos terá um guia específico que o design deverá seguir durante o desenvolvimento.

Como visto anteriormente, temos 4 tipos artefatos básicos atualmente e cada um segue seu guia específico:

- Changelog: veja [Fluxo de Versionamento](/design/guias/versionamento)
- Arquivo fonte do elemento atômico: **Guia de Arquivo Fonte**
- Diretriz do elemento atômico: **Guia de Diretriz**
- Imagens utilizadas na diretriz: **Guia para imagens**
