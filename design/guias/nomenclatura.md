---
title: Guia de nomenclatura
description: Este documento detalha as orientações e padrões para nomenclaturas dos diretórios e artefatos de design 
date: 17/11/2021
keywords: nomenclaturas, arquivos, diretórios, artefatos, versões, imagens, tokens, assets, elemento atômico  
---

{{% notice note %}}
Os textos tachados estão obsoletos/depreciados. Aguarde nova versão.
{{% /notice %}}

Nomenclatura são as regras e metodologias descritas, a fim de criar um vocabulário próprio e consistente dentro do Design System. Utilizar essas regras de forma correta, permite que a comunicação dentro dos times aconteça de forma mais eficiente.

A seguir é exibida a **nomenclatura geral**, que deve ser seguida em todos os contextos e, em seguida, algumas **nomenclaturas específicas**, que seguem a versão geral porém com algumas considerações necessárias para cada situação.

Em caso de dúvidas, sempre utilize a nomenclatura geral.

## Estrutura padrão de arquivos e diretórios

A estrutura padrão para criação de arquivos e diretórios, seguem a seguinte regra:

- ~~Arquivos: `[nome]` `[versao]` `[extensao]`.~~
- Diretório: `[nome]`
- ~~Diretório de Release: `[versao]`~~

Onde:

- **nome**: é o nome do arquivo ou diretório que segue as regras de nomenclatura geral.
- ~~**versao**: é versão do arquivo (segue as regras de versionamento do Design System). A versão do arquivo deve possuir o caractere “underline” (_) antes de descrever a versão. O "diretório de release" não coloque o underline.~~
- **extensao**: extensão do arquivo. Siga as regras de nomenclatura geral para definição da extensão. a extensão do arquivo deve sempre possuir o caractere de ponto (.) antes de definir a extensão.

Exemplos de arquivos e diretórios, seguindo a estrutura básica:

- modelo-padrao_1.0.0.pdf
- botao_0.2.5.xd
- leiame_1.0.0.txt
- email-recebido.ico
- arquivos-antigos
- imagens
- 1.0.5

A `versao` não é obrigatória. Por tanto, caso seu arquivo não tenha controle de versão, não há necessidade de uso.

Para saber como elaborar um bom nome para seu arquivo ou diretório e como criar a versão do seu arquivo, veja as regras de nomenclatura e de versionamento a seguir.

## Nomenclatura geral

A nomenclatura geral define a base para a nomeação de qualquer artefato que não tenha algo específico já descrito. Ela também é a base para qualquer outra nomenclatura existente neste documento (veja adiante).

1. Não utilize caixa alta. Todos os caracteres devem ser em caixa baixa. Acrônimos, e demais abreviações, também devem ser escritos em caixa baixa;
2. Utilize caixa baixa nas extensões dos arquivos;
3. Jamais utilize acentos e caracteres especiais, como: ç, \$, &, %, (), {} e etc.;
4. Nunca utilize espaços. Utilize hífen “-” para separar palavras;
5. Evite nome de arquivos muito extensos. Simplifique sempre que possível (uso de palavras-chaves pode ajudar). Palavras que não contribuem para o significado como “o’, “a”, “de”, “um”, etc. devem ser removidas;
6. Utilize palavras que fazem parte do contexto. O nome deve ser o mais claro e representativo possível;
7. Caso utilize números sequenciais, utilize no mínimo dois dígitos (01, 02, 03, …);
8. Se utilizar datas, prefira o formato AAAA, AAAAMM ou AAAAMMDD;
9. Os termos devem ser consistentes com as nomenclaturas utilizadas por outras equipes. Veja adiante como ficam as nomenclaturas especificas. Converse com o time em caso de dúvida;
10. Utilize sempre a estrutura básica de arquivos e de diretórios quando possível, mantendo o bom senso;
11. Para utilizar um versionamento, separe o nome do arquivo com underline “_” e em seguida a versão do arquivo.

### Exemplos problemáticos

- **IMG_0054.JPG**

    Esse nome não é muito significativo, não é? Separe os nomes por hífen ao invés de underline. Não utilize caixa-alta.

    Correto: icone-erro-01

- **Images.jpg:**

    Evite uso de palavras genéricas e repetitivas.

    Correto: usuario.jpg

- **Acentuação.pdf**
    Acento? Caracteres especiais? Caixa alta? Nem pensar!

    Correto: acentuacao.pdf

- **pasta de trabalho.doc**

    Sem espaços lembra?

    Correto: pasta-trabalho.doc

- **fotodaminhacasa1.png**

    É possível simplificar o nome desse arquivo?

    Sugestão: foto-minha-casa.png ou minha-casa.jpg

## ~~Nomenclatura para Design Token~~

~~Para criação de nomes para design token, siga as orientações de nomenclatura geral, com as seguintes exceções:~~

~~1. Evite uso de palavras compostas, quando for necessário, mantenha as palavras compostas juntas;~~
~~2. Siga a estrutura hierárquica de design token (veja adiante mais informações);~~
~~3. Utilize dois traços “--” para indicar um artefato (fundamento ou componente), um traço “-” para nomes de categoria (variações, tipos, propriedades.) e de meta (nome da variação, nome do tipo, nome da propriedade), e dois pontos e espaço “: ” para separar o valor referente ao token.~~

### ~~Estrutura hierárquica de Design Token~~

~~A estrutura do design token: **--elemento-categoria-meta: valor**.~~

~~Onde:~~

- ~~**elemento:** nome do fundamento ou do componente que está descrevendo.~~

    ~~**Ex:** --color, --font, --button.~~

- ~~**Categoria:** nome da variações/tipos/propriedades/função relacionado ao elemento.~~

    ~~**EX:** -primary, -secondary, -simple.~~

- ~~**Meta:** variação da variação do elemento (nome semântico da categoria). As vezes um token pode exibir vários metadados.~~

    ~~**Ex:** -dark, -light, -max, -min.~~

- ~~**Valor:** valor válido que especifica aquele token. Pode ser um número inteiro, decimal, hex ou qualquer outro valor referente a uma especificação visual.~~

    ~~**Ex**: #786324, 14, 0.6, arial.~~

~~Exemplos de tokens, seguindo a estrutura:~~

- ~~--color-primary: #1351B4~~
- ~~--color-secondary-01: #EDEDED~~
- ~~--font-weight-regular: 400~~
- ~~--font-size-base: 14px~~

**Importante**: atualmente a equipe de desenvolvimento é responsável pela criação dos nomes dos tokens. Designers apenas especificam os valores.

## Nomenclatura para Assets/Ativos

Para criação de nomes dos objetos e ativos dentro de programas gráficos siga as orientações de nomenclatura geral.

Prefira o uso do idioma em ingles para nomear seus assets.

**Dica**: elementos genéricos podem ter nomes genéricos. Exemplo recomendados: recipientes como "surface", textos como “font”, título como “title”, ícone como “icon” e assim por diante.

### Nomenclatura dos elementos atômicos

É importante deixar claro que o Design System tem como foco principal usuários brasileiros, o qual o idioma oficial é o português. Porém, sabemos que alguns termos são mais conhecidos em outras idiomas e também a questão técnica deve ser levado em consideração. Manter uma definição que atenda a todas as situações pode ser muitas vezes difícil ou até mesmo impossível. Pensando em todas essas questões recomendamos:

Utilize nomes no singular e o idioma utilizado vai depender do elemento a ser tratado:

- **Fundamentos:** Português;
- **Componentes:** Inglês;
- **Padrão:** Português ou Inglês (o termo que for mais difundido. Isso acontece porque vários comportamentos vêm de sistemas e eles podem ser mais conhecidos pelo termo em inglês). Porém, sempre que possível, prefira termos em português.

Se trouxer benefício para a compreensão, nomes em inglês podem ter o apoio do nome do termo em português entre parênteses.

Exemplos:

- Collapse (Retrair/Expandir);
- DropDown (Elementos Flutuantes).
