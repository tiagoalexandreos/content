---
title: Fase de Criação
description: Este documento lista as orientações sobre as etapas, entregas e fluxo das atividades de design 
date: 17/11/2021
keywords: etapas, design, fluxo, criação, atualização, conclusão, entrega, preparado, impactos, uikit, fluxo, versionamento, revisão 
---


Essa fase o designer entrega artefatos com a finalidade de concluir uma demanda e deixá-la apta para o estado "preparado para desenvolvimento" ou para ser atualizada no site do Design System.

**Preparado para Desenvolvimento**: significa que não existe nenhuma pendência pelos designers e que as atividades da equipe de desenvolvimento podem ser iniciadas.

Atualmente a fase de design é dividida em 3 etapas:

1. **Criação**: cria ou edita um elemento atômico;
2. **Impactos**: resolve todos os impactos gerados pela etapa de criação;
3. **Atualizar UIKits**: cria ou atualiza um novo asset na biblioteca de cada ferramenta. Geralmente voltadas para fundamentos e componentes.

A etapa de **Atualizar UIKits** pode ser realizada em conjunto com os trabalhos de desenvolvimento, portanto, ir para a terceira etapa significa que o item **não possui nenhuma pendência**. Porém, para ser atualizado no site é importante que **todas** as etapas sejam concluídas!

Em todas essas fases existe um fluxo chamado de **versionamento** que será responsável por manter a qualidade e o padrão de trabalho em cada etapa.

Esse fluxo de versionamento pode ser encontrado no [documento de versionamento](/design/guias/versionamento).

## Detalhamento a Fase de Design

### 1. Etapa de Criação

Onde o designer vai criar ou editar um elemento atômico (Fundamento Visual, Componente, Padrão ou Modelo).

Todo o **fluxo de versionamento** será utilizando nessa etapa.

Nessa etapa existirá pelo menos um designer responsável pelo desenvolvimento da tarefa e um desenvolvedor responsável pela viabilidade/validação do trabalho. Estes, deverão interagir e conversar durante TODO o fluxo. O ideal é que todos os envolvidos no fluxo participem dessa interação!!

#### Após concluir a etapa

- Caso o designer tenha listado os impactos, deve-se fazer a Etapa 2.
- Caso não tenha gerado impactos, deve-se passar para Etapa 3 e a atividade já pode ser considerada como "Preparado para Desenvolvimento".
- Caso não tenha gerado impactos e não há necessidade de atualizar os UIKits, a etapa de design está concluída!

### 2. Etapa de Impacto

O designer responsável por realizar os impactos deverá ajustar todos os documentos que sofreram mudanças.

O fluxo de versionamento também será utilizado, porém, a Validação pode não ser necessária, pois ela já foi feita na etapa anterior.

#### Após concluir a etapa

- Enviar para etapa 3, e a atividade já pode ser considerada como "Preparado para Desenvolvimento".
- Caso não tenha necessidade de atualizar os UIKits, a etapa de design está concluída!

### 3. Etapa Atualizar UIKits

Nessa etapa o designer responsável pela atualização, atualizará os UIKits do Design System baseando-se nos changelogs gerados nas etapas anteriores.

A etapa o fluxo de versionamento é utilizado de uma forma mais simples, onde a viabilidade/validação não é necessária!

#### Após concluir a etapa

- A etapa de design está concluída!
