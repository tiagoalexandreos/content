---
title: Guia de Arquivo Fonte
description: Este documento lista as orientações para criação de um arquivo fonte de elemento atômico
date: 05/11/2021
keywords: arquivo fonte, guia, artefato, elemento atômico, uikit, pranchetas, cabeçalhos, elementos gráficos, componentização, nomeclaturas, cotas, legendas 
---

Atualmente, o artefato arquivo fonte do elemento atômico é desenvolvido dentro da ferramenta **Adobe XD**, dessa forma todas as instruções descritas neste documento tem como premissa o uso desta ferramenta.

**Atenção**: para facilitar a construção de um arquivo fonte utilize os [arquivos de modelo](/design/modelos-documento) referente ao seu elemento atômico.

## Organização interna

No arquivo fonte deve existir duas grandes categorias (ou regiões) que organizam as pranchetas e o conteúdo criados:

1. **Elemento Atômico**: também chamado de **“Detalhamento” ou “Anatomia"**, **"Tipos"** e **"Comportamentos"**. É a área destinada para criação do elemento atômico. Detalha visualmente a anatomia, variações de tipos, comportamentos e outras características que determinam como o elemento atômico deve ser exibido na interface.
É basicamente a área que será tratada neste guia.

2. **Diretriz**: área destinada para a criação das imagens utilizadas no artefato diretriz de design do elemento atômico. Cada prancheta representa uma das imagens utilizadas dentro do documento de diretriz de design relacionada aquele artefato. A organização visa facilitar a atualização do documento quando necessário.
Siga os padrões estabelecidos no [Guia para imagens](/design/guias/imagem) para configurar e criar corretamente as pranchetas.

A organização das pranchetas nestas regiões será visto mais [adiante na documentação](#utilizando-as-pranchetas).

**Importante**: caso essas as duas regiões não estejam definidas no arquivo fonte, o designer será responsável em determinar, de forma clara, estas regiões (por meios de rótulos e organização por agrupamento, por exemplo):

- Ordene as regiões mantendo o detalhamento do elemento atômico a esquerda e a regiões da diretriz a direita.
- O espaçamento entre as regiões é de `200px`.

Além das duas áreas citadas anteriormente, o designer deve utilizar os assets do UIKit oficial do GOVBR-DS para representar qualquer componente já documentado no Design System, importando corretamente o correspondente UIKit para seu documento.

## Importando o UIKit do GOVBR-DS

Para usar corretamente os fundamentos/design tokens e componentes já definidos no UIKit, o arquivo fonte deve ser capaz de consumir essas informações de forma consistente. Em outras palavras, caso o designer necessite utilizar algum ativo que já tenha sido definido, utilize a última versão do UIKit do GOVBR-DS como base para criação deste novo ativo.

**Exemplo de uso:** o designer está construindo um componente novo que consome os componentes *cards* e *buttons* na sua anatomia. Dessa maneira, esses componentes (ativos) devem ser importados do UIKit.

Para usar o UIKit, [baixe a última versão (ou outra versão desejada)](https://www.gov.br/ds/downloads/assets)no site GOVBR-DS.

Recomendamos que sigam os seguintes passos:

- Após baixar o arquivo, crie uma biblioteca pelo Adobe XD para que possa ser utilizado em qualquer arquivo. Caso esteja trabalhando em equipe, compartilhe a biblioteca com os demais integrantes;
- No Adobe XD va na opção **Gerenciar Bibliotecas** e ative o uso da biblioteca caso ela já não esteja ativada.

Caso esteja entrando em uma equipe que já trabalhe com GOVBR-DS, entre em contato com o líder da equipe responsável de design para que seja compartilhado a biblioteca e ela apareça na sua lista.

**Atenção:** é muito importante que a equipe esteja utilizando a mesma versão do UIKit. Sempre verifique se o UIKit utilizado é o que foi definido pelo Time.

## Utilizando as pranchetas

Para criar pranchetas siga as seguintes instruções:

- Cor de fundo das pranchetas `#E4E4E4`.
- Sobre largura:
- Para região **Elemento Atômico**: largura padrão de `1100px` ou `1600px`, utilize a menor possível dentro da sua necessidade.
- Para região **Diretriz**: a largura é variável conforme a imagem utilizada. Cuidado com as margens internas da prancheta (veja adiante) e evite passar de uma largura máxima de 1200px, pois, caso tenha algum texto (como legenda), ela poderá ficar ilegível quando houver redimensionamento da imagem dentro da documentação da diretriz;
- Organize as pranchetas nas regiões adequadas (Elemento Atômico e Diretriz).
- Na região do **Elemento Atômico**, recomenda-se o uso de duas colunas de pranchetas para tamanho padrão (1100px), e uma coluna para outros tamanhos (1600px). Dessa forma as pranchetas ficarão uma abaixo da outra, organizadas por colunas.
- Na região da **Diretriz** não há limite ou especificação em relação à quantidade de colunas, mas pode ser interessante organizar a prancheta pela ordem de leitura encontrada no documento de diretriz.
- Por padrão, o espaço **entre as pranchetas** (ou qualquer objeto fora dela) deve ser de `100px` superior/inferior e `70px` nas laterais.
- Sempre que possível, mantenha um **espaçamento interno** de `40px` entre a prancheta e os elementos visuais dentro dela.
- Ao utilizar qualquer unidade de medida (pixel, cm, mm) sempre utilize valores inteiros, seja para determinar eixos, larguras ou qualquer outra informação.
- Caso a prancheta seja exportada para algum formato de imagem, a imagem herdará o nome especificado na prancheta, por tanto, utilize as regras de nomenclatura para criação dos nomes das pranchetas.
- **Não deve existir** o mesmo nome para duas ou mais pranchetas. Adicione incrementos para evitar repetição (como "prancheta-01", "prancheta-02", …). Pranchetas com mesmo nome podem causar problemas quando forem exportadas.
- Evite criar pranchetas para detalhar o mesmo assunto, crie apenas uma uma prancheta. A região de Diretriz é a exceção (pode ser necessário criar mais de uma prancheta para detalhar algo para o usuário), porém, utilize o bom senso.

**Atenção**: quando construir um elemento atômico do tipo **componente**, existem cabeçalhos especiais que devem ser inseridos (na região de elemento atômico) dentro da prancheta para definir a função de cada uma delas.

Existem regras especificas para essas pranchetas que serão [detalhadas mais adiante](#cabeçalho-especial).

**Dica:** a equipe de design do GOVBR-DS desenvolveu um plugin para o Adobe XD para auxiliar o controle de espaçamento interno das pranchetas.

Entre na seção [plugins](/design/plugins) e procure por: `resize-artboard-govbr.xdx`

### Cabeçalho especial

Os cabeçalhos especiais servem para classificar os tipos de pranchetas existentes no seu artefato. Atualmente é utilizado na região do Elemento Atômico dos componentes e no artefato de UIKit. Basicamente são rótulos que são adicionados no topo de cada prancheta.

O importante é que o conteúdo criado esteja organizado e que outros usuários possam compreender do que se trata o elemento gráfico criado.

A seguir, serão detalhados os cabeçalho especiais de componentes:

- São divididos em três classificações: **Anatomia**, **Tipo** e **Comportamento**.
Procure organizar as pranchetas na ordem descrita ao distribuí-la na região do Elemento Atômico:
- **Anatomia**: o rótulo deste cabeçalho é "Anatomia". Essa prancheta define visualmente os elementos atômicos e/ou outros componentes que formam o componente criado: superfícies, textos, botões, menus, etc. Separe cada elemento para que fique claro quantos e quais são eles. Só deve existir uma prancheta de anatomia.
- **Tipo**: O rótulo deste cabeçalho é "Tipo". Deve-se exibir todos os tipos na mesma prancheta. Caso não haja nenhuma variação, ainda sim forneça essa prancheta exibindo o componente "base". Só deve existir uma prancheta de tipo.
- **Comportamento**: essas pranchetas detalham um comportamento do componente. Pode existir quantas pranchetas de comportamentos forem necessárias, porém, apenas uma prancheta para cada comportamento. O nome do rótulo do cabeçalho será "Comportamento: + `nome do comportamento`”. Exemplos: `Comportamento: Estados`, `Comportamento: Densidade`.

É importante definir a representação visual de cada cabeçalho especial, para que eles mantenham a consistência entre documentos criados dentro do time. Podem existir ativos visuais de cabeçalhos especiais disponíveis dentro dos UIKits. Entre em contato com o líder do time de design para entender como funciona ou, se preferir, faça uma proposta para o time.

## Criando elementos gráficos

Cada designer possui uma forma de criação própria, porém, para que o arquivo final seja de fácil atualização e compreendido por outros profissionais (inclusive por não designers), alguns padrões devem ser levados em consideração ao criar qualquer elemento gráfico dentro dos programas de edição gráfica.

Além da organização vista anteriormente é importante entender alguns conceitos e algumas regras ao criar qualquer elemento gráfico.

### Utilize conceito de "componentização" (*Atomic Design*)

Sempre que possível, utilize os ativos do UIKit. Seja para determinar uma cor de borda, cor de fundo do objeto, tamanho da fonte, cor da fonte, ou para utilizar outros assets. Isso garantirá uma manutenção adequada e uma consistência dentro do Design System. É basicamente o uso de design tokens dentro dessas ferramentas.

Evite criar comportamentos diferentes para algo que já está definido. Recorra ao comportamento padrão.

Utilize como guia todos os conceitos descritos na diretriz do elemento atômico que esteja se referindo, tomando cuidado para não alterar alguma especificação importante. Caso seja realmente necessário, justifique essa alteração detalhando corretamente todas as mudanças na diretriz do elemento atômico atual.

Lembre-se que a diretriz não é um limitador de uso. Ela funciona para que todos possam usar de forma consistente respeitando os princípios do definidos no GOVBR-DS.

{{% notice info %}}
Alterações das instruções das diretrizes devem ser evitadas mas não são proibidas. Os casos em que as necessidades não forem atendidas, é possível uma alteração, contanto que de forma justificada e respeitando os fundamentos propostos pelo GOVBR-DS.
{{% /notice %}}

**Importante:** o uso do UIKit é somente um facilitador para construção de uma interface ou de um novo elemento atômico. As diretrizes serão sempre importante para o entendimento adequado de qualquer elemento atômico. Em caso de dúvida, siga as orientações das diretrizes.

### Utilize as regras de nomenclatura nos elementos

Utilize a [nomenclatura para assets](/design/guias/nomenclatura) em **todos** os elementos visuais criados. Lembre-se que todos os elementos que fazem parte do construção dos assets utilizam a nomenclatura dos assets, enquanto os demais (como as pranchetas) utiliza a nomenclatura geral.

### Utilize valores inteiros

Sempre que possível, utilize valores inteiros nas **bordas**, **dimensões** e **eixos** de qualquer elemento criado dentro das pranchetas.

### Crie ativos

Sempre que possível, crie um novo ativo, principalmente se o elemento se repetirá ao longo das pranchetas. Isso facilitará a manutenção desses elementos e a consistência dos mesmos em todo o arquivo.

A utilização de ferramentas que possam auxiliar a manutenção da consistência e repetição ao longo do artefato também são bem vindas. Um exemplo é o recurso de “Repetição de Grade” existente no Adobe XD.

### Padronize as cotas e legendas

Legendas são utilizadas para organizar os elementos dentro da prancheta. Utilize as cotas e linhas das cotas parar marcar uma área ou detalhar algo muito específico que seja importante para o usuário e que será detalhado na diretriz (como tamanho máximo ou mínimo, por exemplo). Siga as seguintes dicas para construção de cotas e legendas:

- A superfície deve ter `25px` de altura, espaçamento interno de `12px`, e cor `#FF3FB9`;
- Linhas contínuas (espessura `2px`) devem ser utilizadas para indicação de áreas e elementos, na mesma cor da superfície;
- Linhas pontilhadas (espessura e espaço `2px`) para especificar espaçamento, na mesma cor da superfície.
- Textos devem utilizar a fonte `Rawline SemiBold 14px` na cor `#FFFFFF`.
- Objetos detalhados na anatomia dentro de uma diretriz devem apresentar a legenda por textos (`Rawline Medium 16px`), dentro de um contêiner circular (`26px` de diâmetro);
- Sempre que possível, mantenha o alinhamento (**centralizado**) entre as linhas e cotas/legendas.
- As legendas em formato de "box" (informações textuais) dem ser alinhados na parte superior/esquerda do objeto referente.
- Legenda relacionadas a “Faça/Recomendável”, “Não Faça” e/ou “Não Recomendável” seguem as mesmas recomendações acima, porém com as cores relacionadas aos estados de aviso ("sucesso", "erro" e "alerta" respectivamente).
Maiores detalhes veja o [guia de diretriz](/design/guias/diretriz).

**Importante:** evite utilizar cota ou legenda na região do Elemento Atômico para não prejudicar o entendimento das pranchetas.

Podem existir ativos visuais de cotas disponíveis dentro dos UIKits. Entre em contato com líder do time de design para entender como funciona ou, se preferir, faça uma proposta para o time.
