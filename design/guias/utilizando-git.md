---
description: Este documento detalha as orientações e padrões para utilização do Git, como ferramenta de trabalho de design
date: 22/10/2021
keywords: git, processo de design, fluxo, etapas, atividades, issues, comunicação, repositório, roadmap, workflow, backlog
---

# Utilizando o Git

{{% notice note %}}
Os textos tachados estão obsoletos/depreciados. Aguarde nova versão.
{{% /notice %}}

O Git é ferramenta utilizada para controlar as atividades de design, assim como os itens de backlog que passam por diferentes equipes até sua conclusão.

Toda a comunicação do time em relação a uma atividade deve ser realizada dentro do Git. Evite uso de e-mail ou outra ferramenta. É fundamental manter todo o histórico da evolução da tarefa dentro do próprio git.

O [Serpro Drive](https://serprodrive.serpro.gov.br/) será usado para armazenar os artefatos, de arquivos fonte de design (.XD, .AI, .PSD, entre outros), criados na fase de design. Os arquivos de documentação (.MD) e arquivos de imagens e vídeos relacionados (.PNG, .JPG, .MP4, etc) serão armazenados e versionados no GitLab.

A estrutura básica dos diretórios, no Serpro Drive, está detalhada no [Documento de Estrutura](/design/guias/estrutura).

Dentro do git podem ter tarefas de backlog e as tarefas de design. Porém, é através da tarefa de backlog são geradas as tarefas para serem tratadas na etapa de design e de desenvolvimento.

~~A seguir estão listadas as etapas pelas quais as tarefas em backlog devem passar, podendo seguir esta ordem de forma linear ou pular algumas etapas, caso necessário:~~

- ~~**Aberta:** tarefa foi aberta, mas ainda não existe nenhum detalhamento da atividade;~~
- ~~**Análise:** tarefa está na fase de detalhamento das atividades;~~
- ~~**Pendência:** tarefa foi detalhada, mas falta algumas pendências para finalizar o detalhamento ou para dar início as atividades pelo time;~~
- ~~**Ready:** tarefa foi detalhada e não existe nenhuma pendência. Já pode ser iniciada pelo time de design ou de desenvolvimento;~~
- ~~**Design:** tarefa está sendo tratada pelo time de design. Maiores detalhes, veja [Fase de Criação](/design/guias/fase-criacao.md);~~
- ~~**Desenvolvimento:** tarefa está sendo tratada pelo time de desenvolvimento;~~
- ~~**Homologação:** tarefa foi tratada pela equipe de design e/ou pela equipe de desenvolvimento. Já está sendo implementada no site;~~
- ~~**Close:** tarefa foi finalizada. Foi tratada conforme suas especificações.~~

A fase de Criação é detalhada no [documento Fase de Criação](/design/guias/fase-criacao.md) e possui as seguintes etapas:

- **Criação:** criação e atualização dos artefatos;
- **Impactos:** Atualização nos artefatos acordo com que foi feito na etapa de criação;
- **Atualizar UIKits:** Atualizar UIKits com as evoluções ou criações realizadas na etapa de criação.

No Git pode ser ainda acontecer duas etapas extras:

- **Aberta:** tarefa foi criada;
- **Close:** finalizada a etapa de design.

## Cuidados nas tarefas de backlog

- Sempre que houver nova geração de release em alguma Fase de Design, o responsável pela release deve atualizar a tarefa de backlog com as releases criadas. Informe o nome do pacote que foi gerado, sua versão, e o link direto para o repositório.

- Caso a tarefa necessite gerar um arquivo para download dentro do site do Design System, o link para o download deve ser gerado direto do Serpro Drive e colocado a URL na tarefa do backlog (siga as a [documentação de versionamento](/design/guias/versionamento) para criar o arquivos corretamente.)

- Na tarefa, o registro pode está da seguinte forma:

    ```markdown
    Sem Download
    - [nome-do-pacote versão-release](https://lorem.ipsum)

    Com Download
    - [nome-do-pacote versão-release](https://lorem.ipsum) - [Download](https://lorem.ipsum)
    ```

    Exemplo:
- *[Button 1.3.4](https://lorem.ipsum)*
- *[Tag 2.0.0](https://lorem.ipsum)*
- *[Modelo Sessão 1.0.1](https://lorem.ipsum) - [Download](https://lorem.ipsum)*
- *[UIKIT Figma 4.0.0](https://lorem.ipsum) - [Download](https://lorem.ipsum)*

- Após a conclusão das fase de design, o responsável da release deve informar na tarefa de backlog que a tarefa foi finalizada (etapa de design) e  avançá-la para a próxima fase (geralmente, de desenvolvimento).

- Após a conclusão da fase de criação, o responsável pela release deve informar na tarefa de backlog que ela está finalizada e avançá-la para a próxima fase (geralmente, de implementação).

## Cuidados nas tarefas de design

- Seguir tudo que está descrito no documento de [Fase de Criação](fase-criacao) e em [versionamento](versionamento).
- Toda a comunicação da etapa de versionamento deve ser feito dentro da atividade de design.
- Após gerar qualquer release, deve informar tanto na tarefa de design como na backlog e avançar a tarefa no "fluxo de design" (impacto, atualizar UIKit ou concluir a tarefa).
