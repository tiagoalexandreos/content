---
title: Criar pacotes de ilustração
description: Este documento detalha as orientações e padrões para criação de pacotes de ilustração 
date: 22/04/2021
keywords: ilustração, pacote, imagens, nomenclaturas, arquivos, tema, pranchetas, artefatos  
---

Pacotes de ilustração são conjuntos de ilustrações (organizados por um tema) que servem como modelos que podem ser utilizados para construção de uma interface. As ilustrações destes pacotes devem seguir as [diretrizes de Ilustração](https://gov.br/ds/fundamentos-visuais/ilustracao) do Design System.

Abaixo é apresentado o passo a passo de como construir pacotes de forma que possam facilitar o uso e/ou modificações pelos usuários.

**Observação:** nas ferramentas de design, o termo "elemento" visto na documentação pode ser entendido como caminho, objeto (não confundir com objeto de ilustração), forma, etc.

## 1. Nomenclatura

- Utilize a [nomenclatura padrão](/design/guias/nomenclatura) para nomear tudo. Sejam pranchetas, elementos ou camadas dentro de um arquivo, ou o próprio arquivo e diretórios.
- Alguns elementos podem ter uma nomenclatura própria, que será detalhada quando necessário (como os diretórios de arquivos PNG).

## 2. Organizar os arquivos por temas

- Organize os arquivos por assuntos. Algumas vezes pode ser necessário dividir em vários arquivos quando tratar de assuntos distintos.
- Mesmo que a ilustração trate de diferentes temas/assuntos, foque somente em apenas um.
- Exemplo de temas/assuntos: fundos, objetos, personagens, erro, empty states e assim por diante.
- O nome do arquivo deve ser condizente com o tema.

## 3. Organize as ilustrações em pranchetas

- Cada ilustração deve possuir uma prancheta equivalente e nomeada adequadamente.
- Não insira duas ou mais ilustrações em uma mesma prancheta.
- Utilize a dimensão (px) que mais se encaixe na sua ilustração. Existem somente 3 dimensões possíveis: `100x100`, `220x220` ou `500x500`.
- Centralize a ilustração dentro da prancheta (verticalmente e horizontalmente).
- O nome das pranchetas podem ter o mesmo nome do arquivo/tema. É importante que o nome seja semântico e intuitivo (evite o nome "prancheta").
- Lembre-se que o nome da prancheta será utilizado para gerar os nomes dos arquivos PNGs (veja adiante). Portanto, nunca crie pranchetas com mesmo nome, adicione incrementos como sufixos para evitar nomes iguais: fundo01, fundo02, fundo03 e assim por diante.

## 4. Simplifique a ilustração

- Exclua os elementos que não façam parte da anatomia da ilustração. Eventualmente podem ocorrer elementos que foram criados em algum momento da concepção da ilustração mas que posteriormente não fazem sentido existir. Além de deixar o arquivo final mais pesado, esses elementos podem gerar mais trabalho na próxima etapa do passo a passo.
- Elementos invisíveis também devem ser excluídos.
- Sempre que possível "expanda" os elementos. Expandir o elemento significa transformá-los (todos) em caminhos simples (úteis para simplificar efeitos de transparência, elementos 3D, padrões, gradientes, filtros, traçados, misturas, clarões, envelopes ou símbolos). Essa ação é discutível, pois pode dificultar a modificação de alguns atributos, mas por outro lado, aumenta a compatibilidade do arquivo com outros programas vetoriais, além de outras versões da mesma aplicação.
- Sempre que possível mescle os elementos com a mesma finalidade (no mínimo tente agrupar os elementos) para que sejam tratados como uma unidade (usar uma recurso de criar Caminho Composto pode ser uma boa solução).
- Evite agrupar de forma desnecessária.

## 5. Nomeie os elementos

- Todos os elementos criados devem ser devidamente nomeados.
- Caso exista uma quantidade muito grande de um mesmo elemento que dificulte a nomeação, junte-os e nomeie o grupo.
- Utilize o nome referente à anatomia (diretriz) da ilustração para nomear os elementos. Exemplo: personagem, contorno, cor, fundo, sombra, textura, etc.

## 6. Exporte no formato PNG

- As ilustrações devem ser exportadas em PNG em três tamanhos distintos: `1x`, `2x` e `3x`.
- Organize cada tamanho em seu diretório: `1x`, `2x` e `3x`.
- Coloque todos esses diretórios e arquivos dentro de uma pasta chamada `png`.
- Lembre-se de ativar a opção de transparência quando for o caso.

## 7. Finalizando e organizando todos os artefatos

- Para que o pacote de ilustração esteja completo, deve existir tanto um arquivo vetorial (como arquivos de Illustrator `.ai`) para o usuário poder manipular/editar como arquivos para serem utilizados nos protótipos (bitmap no formato `png`);
- Os arquivos devem ser organizados nos diretórios de forma semântica e já detalhados em outros passos.

### Exemplos de um pacote organizado

#### Exemplo 1

- saude
- fundo
- fundo. ai
- png
- 1x
- fundo01.png
- fundo02.png
- 2x
- fundo01.png
- fundo02.png
- 3x
- fundo01.png
- fundo02.png

#### Exemplo 2

- saude
- personagens
- saudepersonagens. ai
- png
- 1x
- saudepersonagens01.png
- saudepersonagens02.png
- 2x
- saudepersonagens01.png
- saudepersonagens02.png
- 3x
- saudepersonagens01.png
- saudepersonagens02.png
- moleculas
- saudemoleculas. ai
- png
- 1x
- saudemoleculas01.png
- saudemoleculas02.png
- 2x
- saudemoleculas01.png
- saudemoleculas02.png
- 3x
- saudemoleculas01.png
- saudemoleculas02.png
