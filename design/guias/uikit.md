---
title: Criar UIKits
description: Este documento detalha as orientações e padrões para criação e atualização dos artefatos de uikits
date: 25/10/2021
keywords: uikits, web, mobile, wireframe, adobe xd, figma, ios, material design, assets, tokens, release
---

{{% notice note %}}
Os textos tachados estão obsoletos/depreciados. Aguarde nova versão.
{{% /notice %}}

Atualmente existem cinco tipos de UIKits no projeto de GOVBR-DS:

- **govbr-componente**: voltado para os componentes disponíveis nas diretrizes do GOVBR-DS.
- **govbr-wireframe**: versão minimalista dos componentes, para ser usado na construção de wireframes. Foco no uso na definição da estrutura de uma interface. Apesar de ter como base os componentes do GOVBR-DS, também pode ser utilizado em situações genéricas, pois são construídos de forma simplificada.
- **ios-govbr**: componentes do IOS estilizados com tema padrão do GOVBR-DS.
- **material-govbr**: componentes do Material Design estilizados com tema padrão do GOVBR-DS.
- **time**: elementos gráficos genéricos, que podem ser utilizados em qualquer situação dentro do Serpro. A ideia é fornecer elementos para expressar interações e/ou que possam  auxiliar na construção de qualquer tipo de interface. Exemplos: cursores, gestos, gráficos, grids, templates simples, etc. Atualmente se encontra nos estágios iniciais de desenvolvimento.

É altamente recomendável que todos os UIKits possuam versões para as ferramentas **Adobe XD** e **Figma**.

Existem algumas regras gerais, que devem ser seguidas independentemente da ferramenta, e outras que dependem da ferramenta utilizada para atualizar ou criar um UIKit.

## Regras gerais de Criação

As regras a seguir são focadas no UIKit `govbr-componente`, mas que podem ser utilizadas em qualquer outro. Utilize as regras gerais (e as específicas) sempre que fizerem sentido, na criação ou atualização de qualquer UIKit.

- Seguir o [documento de nomenclatura](/design/guias/nomenclatura) descritos em "Nomenclatura para Assets/Ativos";
- O nome do asset deve ser o mesmo do componente, e o restante dos elementos internos devem está preferencialmente em inglês;
- Os assets devem estar organizados em pranchetas, exibindo todos os tipos e comportamentos que foram representados;
- A versão do asset, segue a versão descrita na diretriz referente, e deve ser especificada e atualizada sempre que finalizar qualquer asset (veja as regras específicas abaixo para entender como especificar as versões em cada ferramenta);
- Seguir as mesmas regras de versionamento descrita no [documento de versionamento](/design/guias/versionamento), inclusive no uso de changelog;

### Cuidados ao atualizar/criar um asset

A criação ou atualização de um asset é algo subjetivo, pois o designer deve ler e entender a diretriz, assim como o changelog, e entender como construir um asset que possa reproduzir as questões estéticas e funcionais definidas; e por fim visualizar como o usuário final do UIKit pode manipular o asset, personalizando e adaptando-o em diferentes interfaces. Além dessa interpretação é importante ter cuidado nos seguintes pontos:

- **Responsividade**: quando possível, o usuário deve ser capaz de alterar as dimensões dos assets (e também os elementos textuais) sem gerar problemas no comportamento dos elementos internos dos assets.
- **Atomic design**: utilizar assets criados para representar eles mesmo dentro de outros assets. Os assets baseados em fundamentos (cores, iconografia e tipografia) também devem ser utilizados em todas as situações ao invés de serem especificados manualmente.
- **Evitar criar vários assets para representar uma diretriz**: Tente aproveitar ao máximo as características de cada ferramenta e reproduzir os tipos, interações e outros comportamentos em um mesmo asset. Quando não for possível essa organização no mesmo asset, crie o asset pensando sempre em como o usuário pode utilizar essa organização sem se confundir;

**Observação:** O atomic design pode facilitar o uso, a criação e principalmente, a atualização dos assets. Porém, apresenta alguns efeitos colaterais que devemos ter cuidado independentemente da ferramenta:

- As ferramentas de design não conseguem ainda trabalhar de forma eficiente com assets que contém várias outras camadas de assets. Caso o asset fique pesado, simplifique o componente. Representar o elemento asset apenas visualmente (ele vai perder todo o comportamento e cada vez que o componente for atualizado, ele deve ser atualizado manualmente nestes assets).
- Quando é atualizado um componente é importante verificar outros componentes que o utilizem na sua anatomia, pois podem se tornar "desconfigurados". Após atualizar um asset, teste todos os assets que utilizam aquele componente como "filho".

## Regras para Figma

- ~~O versionamento segue o mesmo padrão descrito no documento de versionamento: obter o arquivo mais atualizado, alterá-lo, atualizar o changelog, etc.;~~
- ~~A descrição da versão do asset é feita na descrição da prancheta: selecione a prancheta e na descrição, coloque a versão do asset referente a diretriz;~~
- Os assets são criados todos na página Assets;
- Utilize as `variant` e `property` para organizar os tipos e comportamentos do componente. Faça de forma clara, seguindo a diretriz do componente como guia. As `property` são os tipos (*types*) e os nomes dos comportamentos, enquanto a `variant` são as variações de cada um dessas propriedades definidas.

## Regras para Adobe XD

- No adobe XD além dos arquivos estarem nos repositórios, os arquivos ficam também na nuvem da Adobe para que o UIKit seja compartilhado com as equipes do Serpro. Dessa forma, é importante que a alteração seja feita primeiramente na nuvem e depois exportado o arquivo para ser versionado corretamente no diretório. No diretório o versionamento segue o mesmo padrão descrito no documento de versionamento: obter o arquivo mais atualizado, alterá-lo (com o arquivo que será exportado na nuvem), atualizar o changelog, etc.;
- Existem duas versões na nuvem do UIKit: a de **Desenvolvimento** e outra de **Produção**. A versão de desenvolvimento deve ter um sufixo DEV. Ela deve ser atualizada primeiro. Somente após estar tudo atualizado, atualize a versão de produção (ela utiliza a versão de desenvolvimento como base, ou seja, é só clicar no botão atualizar para que tudo seja atualizado. Não esqueça de atualizar a publicação da biblioteca para que todos recebam essa atualização automaticamente);
- Além de gerar a versão do arquivo no diretório normalmente, registre a versão atual do arquivo na nuvem;
- Utilize os cabeçalhos especiais (semelhantes aos descritos no [guia de arquivo fonte](/design/guias/arquivo-fonte) para organizar as versões dos assets por pranchetas;
- Nunca coloque o asset mestre/principal na prancheta. Mantenha apenas as instâncias nas pranchetas. Isso é útil para o componente não seja inadvertidamente alterado;
- Como o Adobe XD não possui algo parecido como as `variant` e `property` encontrado no Figma, toda a organização de tipos e comportamentos do componente devem ser feitos na área de component/componente que é utilizado para estados. Utilize de forma que não fique confuso para o usuário. Caso a organização fique muito complexa, pode ser mais interessante criar outro asset para representar algum outro tipo ou comportamento.

**Atenção 1:** criamos dois arquivos na nuvem do UIKit para que, durante as alterações e ajustes, não haja influência na biblioteca que o time trabalha. Ela só será atualizada após tudo esteja finalizado. Por isso, testes e outros ajustes podem ser feitos livremente no arquivo de desenvolvimento.

**Atenção 2:** tome cuidado ao trabalhar com arquivos na nuvem. Mesmo tendo a opção de voltar para versões anteriores, ela não é muito confiável, ou seja, trabalhe como se essa opção não existisse.

## ~~Gerando release de um UIKit~~

~~**Quem está criando/atualizando o UIkit é responsável pelo gerenciamento das versões dos assets**. Eles devem ter a mesma versão dos componentes ou da diretriz referente. Porém, **para gerar a release do próprio arquivo do UIKit, é o responsável pela release quem deve realizar essa etapa**, da mesma forma que outros processos:~~

- ~~Seguir todos os processos de versionamento descritos no documento de versionamento;~~
- ~~Abrir o arquivo de UIKit e atualizar a versão, assim como é feito em um arquivo fonte (existirá um cabeçalho especial com o nome do UIKit e a sua versão correspondente).~~
