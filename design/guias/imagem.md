---
title: Guia para exportar imagens
description: Este documento detalha as orientações e padrões para exportação imagens
date: 14/01/2021
keywords: imagens, diretrizes, arquivo fonte, dimensões, pranchetas, formato, PNG, GIF, nomenclatura,  
---

As imagens utilizadas no Design System, são geralmente geradas pela equipe de design por meio de ferramentas gráficas (*Adobe Illustrator*, *Photoshop*, *Gimp* e outros).

A ação de transformar projetos gráficos em um arquivo de imagem (png, jpg, etc.) é chamado **exportação de imagem**.

Algumas regras devem ser seguidas ao exportar imagens. Por exemplo determinar uma extensão especifica ou um tamanho máximo do arquivo final.

Este documento visa tratar as regras para a adequada exportação de imagens dentro do Design System.

É importante saber que, independentemente da imagem, ela deve seguir as regras de nomenclatura descritas no [Guia de nomenclatura](/design/guias/nomenclatura).

## Uso em diretrizes

Todas as imagens utilizadas nas diretrizes são criadas dentro do artefato “arquivo fonte do elemento atômico”, atualmente utilizando a ferramenta **Adobe XD**, após a criação, essas imagens devem ser exportadas para dentro da pasta **imagens** (diretriz/imagens), veja o [Guia de Estrutura](/design/guias/estrutura) para maiores informações.

Para detalhar esse processo, siga as orientações a seguir (apesar de alguns termos usados fazerem referências diretas à ferramenta Adobe XD, podem ser também utilizados com o mesmo contexto):

- Além do [guias de estrutura](/design/guias/estrutura), o conhecimento prévio dos [guias de arquivo fonte](/design/guias/arquivo-fonte) e de [diretriz](/design/guias/diretriz) é importante para entender melhor alguns termos aqui listados.
- As imagens serão criadas e exportadas a partir do artefato “arquivo fonte” (veja o guia arquivo fonte).
- Dentro do arquivo fonte, há uma região identificada como “**Diretriz**” onde devem ser criadas as pranchetas para exportação das imagens. TODAS as imagens das diretrizes, devem estar localizadas nessa região reservada.
- Cada prancheta representa uma imagem.
- Sempre que possível, mantenha o espaço interno (margens entre o objeto e a prancheta) de `40px`.
- A **dimensões** das pranchetas dependem do tamanho da imagem criada. Importante ter bom senso para nao criar imagens com dimensões muito grandes (acima de 1600px por exemplo).
- Cuidado ao nomear as pranchetas. As imagens herdarão o nome especificado na prancheta ao ser exportada do Adobe XD. Por tanto, siga as regras de nomenclatura para criar os nomes das pranchetas, e não utilize nomes duplicados.
- As imagens devem ser exportadas no formato `PNG`. Caso haja imagens animadas, pode ser utilizado o formato `GIF` (porém o formato oficial para vídeos no Design System é o `MP4`).
- Independentemente do formato, as imagens devem ser exportadas com **fundo transparente**. Lembre-se de desmarcar a cor de fundo das pranchetas ao exportá-las.
- Quando utilizar cotas e objetos que especificam algo na imagem, siga os mesmos padrões descritos no guia de arquivo fonte.
- Exporte todas as imagens diretamente para o diretório **imagens**.
**Lembre-se:** somente as imagens que serão utilizadas nas diretrizes (região diretriz) devem ser exportadas.
- Caso haja imagens antigas que não estejam mais sendo utilizada na diretriz, exclua da pasta imagens (`diretriz/imagens`), além de excluir a pancheta correspondente da região Diretriz.

**Observação**: as imagens devem possuir o fundo transparente, e a cor será adicionada dinamicamente pelo time de desenvolvedores dentro do site do Design System. Atualmente é aplicado o valor `#E4E4E4`. O designer pode utilizar esta cor nas pranchetas do arquivo fonte do artefato com o pretexto de visualizar como a artefato ficará sobre o fundo dessa cor, porém, deve se lembrar de exportar as imagens com o fundo transparente.
