---
title: Guia de Diretriz
description: Este documento lista as orientações para criação ou edição de uma diretriz de design
date: 27/10/2021
keywords: diretriz, elemento atômico, anatomia, detalhamento, imagens, legendas, ortografia, listas, títulos
---

{{% notice note %}}
Os textos tachados estão obsoletos/depreciados. Aguarde nova versão.
{{% /notice %}}

A forma de criar um documento pode variar dependendo do elemento atômico descrito, por isso, podem existir vários regras específicas detalhando cada um deles. Porém, independentemente do tipo de elemento, algumas dicas básicas devem ser seguidas.

**Atenção:** enquanto esse documento visa detalhar as regras "básicas", o [modelo de documento da diretriz](/design/modelos-documento) determina as regras especificas.

## Dicas para antes de documentar uma diretriz

1. ~~Para controle de versão dos desenvolvedores, adicione a seguinte informação no início do seu documento de diretriz: **[version]: # (x.y.z)**. O nome **x.y.z** é o nome da a versão do arquivo que será adicionada no momento da criação da release;~~
2. Sempre utilize imagens para ilustrar e exemplificar o conteúdo abordado (“Uma imagem vale mais que mil palavras!”), porém, nunca deixe de explicar/descrever de forma textual o que a imagem está representando. Lembre-se que a imagem deve servir como **um elemento de suporte ao entendimento do conteúdo**, e não como o **único** meio de entendimento do conceito;
3. Quando utilizar imagens para representar um elemento atômico, evite uso de outros elementos “de fundo” que possam competir ou confundir o usuário (seja no aspecto visual, de anatomia ou de comportamento);
4. Utilize exemplos como “Faça/Recomendável”, “Não Faça” e/ou “Não Recomendável” (com auxílio de imagens) para detalhar o assunto abordado;
    - Utilize somente quando for comparar pelo menos duas práticas;
        - **"Faça/Não faça":** é taxativo, isto é, faça ou não faça de uma determinada forma;
        - **"Recomendável/Não Recomendável":** trata-se de uma recomendação ou sugestão de como fazer ou não fazer;
    - Exiba a "comparação" em uma única imagem;
    - A representação visual do aviso deve ser um texto dentro de uma superfície retangular (descrito em [padrão de cotas e legendas](/design/guias/arquivo-fonte#padronize-as-cotas-e-legendas)), alinhado à esquerda superior da imagem;
    - Utilize somente três cores (`sucesso`, `erro` e `alerta`) baseadas nas cores de aviso definidas pela paleta cromática do GOVBR-DS;
        - **Faça e Recomendável (`sucesso`):** usado para indicar boas práticas que o usuário deveria seguir. Deve ser SEMPRE o primeiro exemplo na imagem;
        - **Não Faça (`erro`):** usado para indicar maneira incorreta dentro do entendimento do proposto pelo Design System;
        - **Não Recomendável (`alerta`):** usado em casos muito específicos. O exemplo não fere as "regras" do Design System, porém deve ser utilizado com cautela ou em casos restritos;
5. Sempre que existir na anatomia alguma informação textual (títulos, subtítulos, ou rótulos) detalhe o **"Tom e Voz"** que deve ser utilizado para que o conteúdo seja mais facilmente compreendido pelo usuário;
6. Sempre que houver opções ou qualquer outro tipo de subdivisões, detalhe cada um de forma isolada, exemplificando e justificando a sua existência dentro da documentação;
7. Insira **legenda** em todas as imagens. Qualquer texto explicando a imagem deve ser considerado legenda da imagem, havendo o cuidado com a formatação do *Markdown*: atualmente a legenda é coloca abaixo da imagem, quebrando linha, dentro de um "asterisco" (*);
8. O documento não deve ser rígido: alguns assuntos são opcionais, a ordem dos assuntos apresentada no modelo pode ser alterada, isso vai dependendo de cada situação. O importante é compreender que os termos utilizados devem ser consistentes nas e entre as documentações e neles devem conter informações suficientes para que aquele elemento atômico seja replicado e entendido independentemente do tipo de tecnologia;
9. Não existem versões para portal ou sistema, o que existe são aplicações. Um mesmo elemento atômico pode ser utilizado em um portal ou em um sistema. Tudo depende do contexto e da situação de uso;
10. Evite classificações específicas e foque no contexto: ao invés de criar “versão mobile” tente algo como “versão em telas reduzidas” ou melhor "versão compacta/alta densidade". Dessa forma o conteúdo não fica restrito a um único dispositivo, mas a um determinado contexto. Essa regra deve ser utilizada inclusive ao criar subdivisões;
11. A imagem de *preview* (primeira imagem da diretriz),  deve apresentar o elemento na versão "completa", exibindo todos os elementos (incluindo os itens opcionais). Por padrão coloque apenas a versão "padrão" do elemento, caso existam variações, estas também podem ser exibidas. Não utilize a versão *wireframe* do elemento!;

### Regras de formatação de escrita

1. Nos títulos, somente a primeira palavra deve iniciar com letra maiúscula. Caso seja necessário enfatizar uma palavra, ela também pode apresentar a primeira letra maiúscula;
2. Em textos corridos, para chamar atenção, utilize negrito (`**`);
3. As aspas (`""`) devem ser utilizadas quando uma palavra sugerir sentido não literal;
4. Texto em itálico devem ser utilizados para qualquer palavra não pertencente à lingua portuguesa. Palavras inglesas incorporadas ao idioma português não necessitam estar em itálico;
5. Em listas, utilize ponto e virgula (`;`) para separar os itens, porém, último item deve ser finalizado com ponto final (`.`);
6. O título principal da diretriz deve seguir algumas regras de nomenclatura, que vai depender do elemento atômico. Maiores informações veja o [documento de nomenclatura](/design/guias/nomenclatura#nomenclatura-dos-elementos-atomicos);
7. Sempre que definir os princípios, indicar qual o princípio que está se referindo. Exemplo: "Princípios do Design System", "Princípios do Movimento";
8. Para categorizar um determinado item, prefira sempre o termo "propriedade". Evite utilizar "atributo" por questões de consistência.
