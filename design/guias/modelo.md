---
title: Criar Modelo
description: Este documento detalha as orientações e padrões para criação ou atualização de modelos 
date: 29/10/2021
keywords: modelo, estático, interativo, fundamento, componente, padrão, fluxo, protótipo, download  
---

Basicamente a criação de um modelo segue todas as regras estabelecidas no documento [Arquivo Fonte](/design/guias/arquivo-fonte), com exceção alguns detalhes listados abaixo:

- Não é necessário criar espaçamentos internos nas pranchetas;
- Utilize o cabeçalho especial para organizar as pranchetas sobre fluxos, tipos ou um determinado assuntos relacionado (procure manter a largura igual ao espaço utilizados pelas pranchetas);
- Crie um diretório chamado `modelo` dentro da pasta raiz do pacote de versão do elemento criado;
- Dentro do diretório coloque o aquivo do mesmo nome do elemento referente (igual a um arquivo fonte);
- Utilize um UIKit para criar os modelos;
- Evite utilizar o **"UIKit Wireframe"** durante criação, exceto se a intenção for apresentar o conceito estrutural. Os modelos devem ser o mais fiel possível a realidade atual do GOVBR-DS. (Lembre-se que wireframes servem como base para a criação de algo maior.);
- Siga todas as regras de nomenclatura que façam sentido;
- **Muito importante:** Tenha em mente que o modelo pode ser utilizado por um usuário "comum" (não designer).  Estruture da forma mais simples e semântica possível para que o usuário entenda a organização e possa aproveitar o modelo para criar algo a partir dele.

**Dica**: Utilize um [modelo de documento](/design/modelos-documento) para criar seu artefato.
