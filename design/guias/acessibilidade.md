---
title: 'Acessibilidade'
description: Principais recomendações de acessibilidade para artefatos de design
date: 22/04/2021
keywords: acessibilidade, inclusão, deficiência visual, navegação via teclado, interação, contraste de cores, semântica de código, teclas de atalho, legibilidade, W3C, WCAG, padrões web
accessibility: true
---

Os padrões de acessibilidade compreendem recomendações ou diretrizes que visam tornar o conteúdo Web acessível a todas as pessoas, inclusive às pessoas com deficiência, destinando-se aos autores de páginas, projetistas de sítios e aos desenvolvedores de ferramentas para criação de conteúdo.

A observação destes padrões facilita o acesso ao conteúdo da Web independente da ferramenta utilizada (navegadores Web para computadores de mesa, laptops, telefones celulares, ou navegador por voz) e de certas limitações de ordem técnicas como por exemplo, uma conexão lenta, a falta de recursos de mídia, etc.

{{% notice info %}}
O documento visa responder a seguinte pergunta: **Quais itens de acessibilidade o designer deve se preocupar?**
{{% /notice %}}

## Recomendações

### Contraste de cores

Ofereça contraste mínimo entre plano de fundo e primeiro plano:

- Siga os padrões da  [WCAG 2.0](https://www.w3.org/TR/WCAG20/) de contraste mínimo para o GOVBR-DS (AA). A diretriz [Cores](https://gov.br/ds/fundamentos-visuais/cores), apresenta um documento detalhado para a construção elementos com bom contraste de cores.
- O contraste visual deve estar adequado aos usuários com deficiências visuais (como daltonismo, baixa visão, etc.).

### Tecnologia assistiva

Tecnologias assistivas reside em ampliar a comunicação, a mobilidade, o controle do ambiente, as possibilidades de aprendizado, trabalho e integração na vida familiar, com os amigos e com a sociedade em geral.

- O usuário deve conseguir ter acesso à informação ou executar ações utilizando dispositivos como leitores de tela, ferramentas de ampliação e aparelhos auditivos.

### Hierarquia da informação

Ao criar elementos em uma interface é fundamental considerar a hierarquia entre eles pois garantem a ordem de importância e a subordinação dos conteúdos, facilitando a leitura e compreensão.

Existem alguns pontos que podem ser trabalhados para que o entendimento seja feito de forma mais eficiente (podendo ser tratados isoladamente ou em conjunto):

- **Posicionamento**: posicione ações importantes na parte superior ou inferior da tela. Mapas de calor ou *"heat maps"* podem ser ferramentas eficientes para a determinação do posicionamento.
- **Agrupamento**: itens relacionados devem estar localizados próximos uns aos outros.
- **Visual**: crie elementos com contrastes (pesos) e tamanhos diferentes quando as suas relevâncias forem distintas.
- **Foco**: o foco de entrada segue a ordem do layout visual, geralmente fluindo da parte superior para a parte inferior da tela. Ele pode ir do item mais relevante ao menos importante.

### Teclas de atalho

Devem ser disponibilizados atalhos por teclado para pontos estratégicos da página, permitindo que o usuário possa ir diretamente a esses pontos, além de construir uma navegação consistente entre os elementos interativos.

- Os atalhos que devem existir nas páginas do Governo são os seguintes: para ir ao conteúdo, para ir ao menu principal, para ir à caixa de pesquisa.
- Os atalhos padrões do Governo são (os atalhos valem para o navegador Chrome, e funcionam em qualquer página do portal):
- Ir diretamente ao começo do conteúdo principal da página (Alt + 1).
- Ir diretamente ao início do menu principal (Alt + 2).
- Ir diretamente em sua busca interna (Alt + 3).
- Ir diretamente ao rodapé do site (Alt + 4).
- Os atalhos devem seguir os padrões da  WCAG 2.0 para acessibilidade no teclado e adicionar uma descrição da tecla de interação. Aprimore com rótulos ARIA (Accessible Rich Internet Application) quando necessário.
- Crie teclas de atalho para ações muito importante ou de acesso repetitivo dentro de uma interface (menus de navegação, conteúdos, ações de conversão ou início de um fluxo importante, etc.)
- As interações comuns do teclado incluem o uso da tecla "TAB" para selecionar diferentes elementos interativos em uma página e o uso da tecla "ENTER" ou da tecla "spacebar" para ativar um elemento em foco.
- Indique o foco corretamente. A tecla "TAB" navega por todos os elementos interativos em uma página na ordem em que aparecem no documento HTML. Um indicador visual padrão deve ser exibido pelo navegador da web em uso (veja Foco em [Fundamento Estados](https://gov.br/ds/fundamentos-visuais/estados)). Quando um elemento está em foco, ele pode ser ativado posteriormente usando o teclado.
- Crie ordem de navegação lógica e previsível. Um fluxo comum pode começar com o cabeçalho, seguido pela navegação principal, então a navegação de conteúdo (da esquerda para a direita, de cima para baixo) e terminar com o rodapé. Procure oferecer a todos usuários a mesma experiência.

### Área mínima de interação (clique e toque)

Área mínima de interação é a menor área da tela prevista para responder à entrada/interação do usuário. Muitas vezes essa área se estende além dos limites visuais de um elemento.

Por exemplo, um hiperlink com dimensões 24x24px pode oferecer um espaço de 48x48px como área mínima de interação.

- **Área mínima de clique**: 44x44px.
- **Área mínima de toque**: 48x48px.
- A área mínima de interação deve pertencer à **área de segurança do elemento** (veja Fundamento [Espaçamento](https://gov.br/ds/fundamentos-visuais/espacamento)).

É importante ter em mente que outros elementos localizados próximos à área mínima de um elemento pode tornar o layout confuso e gerar dificuldades de compreensão da informação por parte do usuário.

### Informação, conteúdo e feedback

A cor ou outras características sensoriais como forma, tamanho, localização visual, orientação ou som não devem ser utilizadas como o único meio para transmitir informações, indicar uma ação, pedir uma resposta ao usuário ou distinguir um elemento visual.

- Qualquer classificação importante feita por cor deve possuir também uma identificação textual.
- Deve ser fornecida uma descrição para as imagens da página (chamado também de **texto alternativo**). As descrições devem ser sintéticas, em poucas palavras ou em uma frase curta (limitada a 125 caracteres). Como o texto visa descrever apenas imagens, não há necessidade de adicionar “imagem de” ou “imagem sobre” ao texto alternativo. O texto alternativo é mostrado quando a imagem não é carregada pelo dispositivo ou quando a imagem é lida por pelo leitor de tela.
- Caso haja informações essenciais incorporadas à imagem como textos, inclua essas informações no texto alternativo.
- Para descrições longas, use **legendas** em vez de texto alternativo, pois elas estão disponíveis para todos os usuários. Legendas são o texto que aparece abaixo da imagem. Elas explicam as informações contextuais: quem, o quê, quando e onde.
- Tenha cautela ao repetir o conteúdo de texto alternativo e das legendas, os usuários com leitores de tela podem ficar entediados.
- Para imagens complexas que exijam uma descrição mais detalhada, como infográficos, por exemplo, forneça, além do "alt", a descrição no próprio contexto (texto adjacente) ou um link para a descrição longa logo após a imagem.
- **Texto adjacente** ou **descrição do contexto** é o texto próximo à imagem que pode explicar a imagem dentro de uma narrativa. Se o texto adjacente explica o recurso, o texto alternativo pode não ser necessário.
- O texto alternativo e as legendas contêm informações diferentes. O texto alternativo só é útil quando o texto adjacente e a legenda não explicam as características da imagem que são importantes para atender aqueles que não podem ver a imagem, como descrições das cores, tamanhos e localização de um objeto.
- Dados simples podem ser representados tanto graficamente quanto em forma de tabelas.
- Forneça links redundantes relativos a cada região ativa caso utilize o mapa de imagem.
- Ao invés de descrever a aparência de um elemento, pode ser importante indicar uma ação. Verbos de ação indicam o que um elemento ou link faz quando acionado. Isso descreve o que um elemento faz sem depender da acuidade visual.
- Faça uso dos conceitos de **microcopy** em botões, formulários e campos de instrução. Microcopy é um pequenos textos que têm o objetivo de instruir, convencer e aliviar a preocupação do leitor. Dessa forma, reduz-se o atrito e facilita a realização da ação esperada.
Alguns dicas gerais:
- **Escreva para uma pessoa no singular.**
  - Não recomendado: As pessoas que vieram até aqui, chegaram ao final do curso!
  - Recomendado: Você chegou ao final do curso, parabéns!
- **Escreva como você fala.**
  - Não recomendado: Os produtos que você deseja comprar.
  - Recomendado: Sua lista de compras.
- **Use a voz ativa e não a passiva.**
  - Não recomendado: Como você gostaria de pagar?
  - Recomendado: Selecione seu método de pagamento preferido.
- **Mantenha os conectores da frase.**
  - Não recomendado: Detalhes do pedido
  - Recomendado: Detalhes do seu pedido
- **Ressalte o benefício da ação, não o processo.**
  - Não recomendado: Uma variedade de funções para controlar o processo comercial da sua empresa
  - Recomendado: Controle seu processo comercial
- **Evite gíria, regionalismo e abreviação.**
  - Não recomendado: wpp
  - Recomendado: WhatsApp
- **Em formulários, coloque as etiquetas na frente.**
  - Não recomendado: Os campos devem conter “nome (primeiro)”
  - Recomendado: Os campos devem conter “primeiro nome”
- **Utilize o mínimo de palavras possível, corte o que não é crucial.**
  - Não recomendado: Basta preencher seu e-mail favorito abaixo e você terá acesso instantâneo à primeira aula.
  - Recomendado: Digite seu e-mail para acessar à primeira aula

### Layout responsivo

A página deve continuar legível e funcional mesmo quando redimensionada (para até 200%) e o layout deve adequar-se à resolução de tela do dispositivo pelo qual está sendo acessado.

- Quando a página for redimensionada, não deve ocorrer sobreposições ou conteúdos flutuantes de forma desordenada.
- Evite o uso barra de rolagem horizontal.
- Construa layouts e componentes considerando as faixas de resolução (*"breakpoints"*) descritos no documento de fundamento [Grid](https://gov.br/ds/fundamentos-visuais/grid)).
- Construa layouts considerando as "áreas nobres" e a "zona do polegar". Essas áreas podem ser diferentes para cada dispositivo. **Áreas nobres** são as áreas na tela onde o usuário geralmente focam atenção quando entram pela primeira vez em uma interface desconhecida. **Zona do polegar** é a área mais confortável para o toque usando um dispositivo móvel com uma mão.
- Todas as ações devem ser possíveis com uso de cliques e toques.

### Espaçamento e legibilidade

Para que agrupamentos e legibilidade de um texto sejam eficientes é importante que os espaçamentos sejam empregados de forma consistentes.

- Siga as regras de espaçamento descritas na diretriz [Espaçamento](https://gov.br/ds/fundamentos-visuais/espacamento).
- Utilize sempre que possível a **escala de layout** entre objetos interativos. Evite o uso da escala de ajuste.
- Para **entrelinhamentos** utilize valores de 1.45x até a fonte base (veja o valor na diretriz  [Tipografia](https://gov.br/ds/fundamentos-visuais/tipografia)), após isso, utilize o valor de 1,15x. Para fontes que não sejam a oficial do GOVBR-DS (Rawline), permita-se utilizar a regra geral de 1.5x.
- Alguns outros espaçamentos mínimos que podem ajudar na legibilidade:
- **Entre caracteres (letter spacing/tracking)**: 0.12x o valor da fonte.
- **Entre parágrafos:** 2x o valor da fonte.
- **Entre palavras:** 0.16x o valor da fonte.

### Suporte a idiomas

Dependendo do idioma, o comprimento das palavras podem sofrer grandes variações. Esse ponto deve ser levado em consideração quando existem questões textuais e preocupações de alterações do conteúdo para algo mais ou menos denso (dependendo do idioma escolhido).

- Certifique-se de que haja espaço suficiente para fontes grandes e em idiomas estrangeiros.
- **Altura**: muitos sistemas de escrita requerem mais espaço vertical, portanto, a interface deve prever espaço vertical suficiente para esses diferentes sistemas de escrita.
- **Comprimento**: palavras podem variar muito em diferentes idiomas, mesmo aqueles que usam glifos semelhantes, como português e o alemão.
- **Alinhamento**: alguns sistemas de escrita, como árabe e hebraico, são exibidos com caracteres que aparecem da direita para a esquerda. Essas fontes podem parecer menores do que as latinas no mesmo tamanho de fonte, exigindo ajustes no espaçamento e alinhamento entre linhas para que a tipografia funcione bem na mesma interface para todos os idiomas.
- **Composição vertical**: embora raramente utilizada, pode exibir caracteres verticalmente ao invés de horizontalmente. A tipografia da China, Japão e Coreia é normalmente monoespaçada, o que significa que cada letra ocupa a mesma quantidade de espaço que as demais. Frequentemente, é definida da esquerda para a direita, de cima para baixo. Também pode ser definida verticalmente: de cima para baixo e da direita para a esquerda.
- Pode ser interessante usar outra fonte para acomodar outros idiomas que utilizem alfabetos não latinos. Recomenda-se o uso da fonte [Noto](https://fonts.google.com/specimen/Noto+Sans) (da Google) sempre que a fonte Rawline não suportar a linguagem desejada. Ela é projetada para ser visualmente harmoniosa em todos os idiomas e scripts, gerando alturas e espessuras de traços compatíveis. Para maiores detalhes entre no site da fonte [Noto](https://www.google.com/get/noto/).
- Construa layouts que suportem bidirecionalidade, ou seja, a linguagem LTR (left-to-right) e a linguagem RTL (right-to-left). Textos, linhas de tempo e imagens/ícones (que comunicam a direção) devem ser espalhados para que sejam entendidos entre uma linguagem e outra. Números e textos (na língua nativa) não necessitam ser espelhados.

### Som e movimento

Forneça alternativas para áudio e vídeo e mecanismos para controle dos mesmos. Movimento dentro da interface é utilizado para orientar o foco entre as telas e elementos existentes.

- É importante que haja uma alternativa sonora ou textual para vídeos que não incluem faixas de áudio.
- Forneça legendas no idioma natural da página para vídeos que contenham áudio falado.
- Vídeos que transmitam conteúdo visual que não está disponível na faixa de áudio devem possuir uma audiodescrição. A audiodescrição consiste na descrição clara e objetiva de todas as informações apresentadas de forma visual e que não façam parte dos diálogos. Essas descrições são apresentadas nos espaços entre os diálogos e nas pausas entre as informações sonoras.
- Áudios gravados devem possuir uma transcrição descritiva.
- É desejável que os vídeos com áudio apresentem alternativa na **Língua Brasileira de Sinais (Libras)**.
- Assegure o controle do usuário sobre as alterações temporais do conteúdo. Forneça mecanismo para parar, pausar, silenciar ou ajustar o volume de qualquer objeto que se reproduza na página, principalmente para animações que iniciem automaticamente na página.
- Evite que o conteúdo seja disparado automaticamente sem o controle do usuário, mas quando acontecer, configure para que o conteúdo (que se mova, com rolagem ou que pisque) seja pausado, interrompido ou ocultado quando durar mais de cinco segundos.
- Não devem ser utilizados efeitos visuais piscantes, intermitentes ou cintilantes. Limite o conteúdo piscante para três vezes em um período de um segundo para atender aos [limites de flash e flash vermelho](https://www.w3.org/TR/WCAG20/#general-thresholddef).
- Evite [piscar grandes regiões centrais da tela](https://www.w3.org/TR/2015/NOTE-WCAG20-TECHS-20150226/G176).

### Formulários

É importante que o usuário consiga preencher campos de formulário independentemente do dispositivo utilizado. Além de ser importante adicionar somente informações relevantes. Maiores detalhes sobre construção formulários podem ser vistos em Padrão [Formulário](https://gov.br/ds/padroes/formulario).

- Os campos de formulário devem estar associados a rótulos.
- Organize os campos de forma lógica. Uma boa organização facilita tanto o preenchimento como a navegação interna entre os campos (teclas de atalho). Evite "forçar" uma navegação diferente (utilizando o atributo "tabindex"). Utilize somente em casos extremamente necessários.
- Evite navegação automática após selecionar ou preencher um campo. Mudanças de contexto são apropriadas apenas quando está claro que tal mudança ocorrerá em resposta à ação do usuário. Adicione um botão ou hiperlink para permitir que o usuário tenha o controle da situação.
- Forneça instruções para entrada de dados. Utilize de forma consistente o *placeholder*, elementos de ajuda, informações de campo obrigatório/opcional e texto adicionais.
- Quando um erro de entrada de dados for automaticamente detectado, identifique e explique para o usuário por meio de texto em linguagem clara e objetiva.
- Sempre confirme o envio das informações.
- Agrupe campos de formulário. É recomendado que os campos com informações relacionadas sejam agrupados utilizando o elemento **FIELDSET**, principalmente em formulários longos. O agrupamento deverá ser feito de maneira lógica, associando o elemento **LEGEND** explicando claramente o propósito ou natureza dos agrupamentos. No caso do elemento **SELECT**, pode ser utilizado o elemento **OPTGROUP** para agrupar os itens da lista de seleção.
- Recomenda-se uma combinação de diferentes estratégias para serviços mais seguros e acessíveis para substituir o uso de **CAPTCHA**. Caso o uso de CAPTCHA seja estritamente necessário, o mesmo deverá ser fornecido em forma de pergunta simples de interpretação (CAPTCHA Humano), e este preferencialmente só deve ser apresentado após pelo menos 2 tentativas de envio do formulário, por exemplo. É preciso garantir que a pergunta não seja de difícil resolução, permitindo que a mesma possa ser respondida por pessoas de variadas culturas e níveis de instrução. No entanto, é preciso tomar cuidado para que esses testes não sejam facilmente “quebrados” por determinados programas. Uma alternativa é solicitar que o usuário escreva o resultado do teste matemático por extenso, como “escreva por extenso quanto é 2 + 3”, ou ainda “responda por extenso quanto é dois mais três”.

### Documentação

Disponibilizar documentos em formatos acessíveis. O **ODF (Open Document Format)** é um formato aberto de documento adotado pela e-PING (Arquitetura de Interoperabilidade em Governo Eletrônico) que pode ser implementado em qualquer sistema. O ODF engloba formatos como: ODT (Open Data Text) para documentos de texto, ODS (Open Data Sheet) para planilhas eletrônicas, ODP (Open Data Presentation) para apresentações de slides, entre outros.

- Os documentos devem ser disponibilizados preferencialmente em HTML. De acordo com a [e-PING](http://eping.governoeletronico.gov.br/) (Arquitetura de Interoperabilidade em Governo Eletrônico), é possível disponibilizar os arquivos para download em outros formatos:
- Texto puro (TXT por exemplo).
- Open Document ODF 1.2 - [especificação OASIS](http://docs.oasis-open.org/office/v1.2/OpenDocument-v1.2.html).
- [EPUB 3.0.1](http://idpf.org/epub/301)
- Portable Document Format - PDF ISO 32000-1:2008
- Portable Document Format - PDF/A NBR ISO 19005-1:2009(11)., quando necessária a preservação digital de documentos.
- Sempre que disponibilizar um documento, coloque a informação do formato do arquivo utilizado e o tamanho do arquivo. Exemplos:
- [Download do documento](http://www.governoeletronico.gov.br/documentos-e-arquivos/e-PING_v2017_20161221.pdf) | Formato PDF - 692 KB
- [Guia de Bolso ePING](http://www.governoeletronico.gov.br/documentos-e-arquivos/Guia%20de%20Bolso%20ePING%202016.pdf) | Formato PDF - 6,7 MB
- Considere alguns itens importantes ao criar um documento:
- Forneça aos usuários tempo suficiente para ler e utilizar o conteúdo;
- Ofereça melhor qualidade textual, tornando o conteúdo mais fácil de ler e compreender;
- Faça com que o ambiente ou documento funcione de forma previsível;
- Auxilie os usuários a prevenir e corrigir possíveis erros, etc.

### Navegação

O usuário deve ter controle da navegação, além de entender de forma clara como funcionam os elementos interativos da interface.

- Não crie páginas com atualização automática periódica, prefira o uso de ações manuais para o usuário (como botões).
- Não utilize redirecionamento automático de páginas.
- Forneça alternativas para modificar limite de tempo.
- Assegure o controle do usuário sobre as alterações temporais do conteúdo.
- Descreva os hiperlinks de forma clara e sucinta. Garanta que o texto do link faça sentido mesmo quando isolado do contexto da página.
- Identifique claramente o destino de cada link, informando, inclusive, se o link remete a outro produto (veja estado Interativo no fundamento [Estados](https://www.gov.br/ds/fundamentos-visuais/estados)).
- Elementos interativos devem possuir contraste diferenciado de outros elementos.
- Forneça um mecanismo que permita ao usuário orientar-se dentro de um conjunto de páginas, permitindo que ele saiba onde se encontra em cada momento.
- Em links de arquivos para download, informe a extensão e o tamanho do arquivo.
- É importante que os links abram na guia ou janela atual de navegação, pois os usuários com deficiência visual podem ter dificuldade em identificar que uma nova janela foi aberta.

### Teste e pesquisa

As diretrizes de acessibilidade auxiliam na melhoria da acessibilidade em seu projeto, mas não garantem uma experiência totalmente acessível. Recomenda-se que sejam feitos testes e pesquisas com a proposta criada.

- Teste sua solução para completar a tarefa (do começo ao fim), com várias tecnologias assistivas ativadas.
- Permita que usuários com variados tipos deficiências testem suas interfaces.
- Reflita como elementos individuais podem se tornar mais acessíveis e, ao mesmo tempo, se encaixar em diferentes fluxos de usuário.
- Torne as tarefas principais da interface o mais utilizáveis possível para uma maior gama de usuários.
- Converse com seus usuários, especialmente aqueles que usam tecnologia assistiva, para conhecer mais sobre suas necessidades, o que esperam do seu aplicativo, quais ferramentas utilizam e como as fazem. Familiarize-se com essas ferramentas para que possa proporcionar a melhor experiência  possível.

## Referências

- <https://www.gov.br/ds/>
- <http://emag.governoeletronico.gov.br/>
- <http://eping.governoeletronico.gov.br/>
- <https://www.w3.org/TR/WCAG20/>
- <https://material.io/design/usability/accessibility.html>
- <https://material.io/design/usability/bidirectionality.html>
- <https://material.io/design/typography/language-support.html>
- <https://www.carbondesignsystem.com/guidelines/accessibility/overview/>
- <https://spectrum.adobe.com/page/tray/>
- <https://resultadosdigitais.com.br/blog/o-que-e-microcopy/>
