---
title: "Design"
chapter: true
weight: 1
pre: '<i class="fas fa-palette"></i> '
---

Caro designer, aqui você encontrará tudo o que você precisa para auxiliar a sua jornada de trabalho no Design System.

Tenha em mente que este documento está em constante evolução. Portanto, revisite-nos de tempo em tempo para estar sempre atualizado com as últimas decisões de design no processo de trabalho.

Não hesite em entrar em contato conosco para sugerir melhorias ou reportar erros. Ficaremos contentes em poder ajudar.

Tenha uma ótima experiência no **Design System**.
