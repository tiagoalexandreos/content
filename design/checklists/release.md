---
title: Release
description: Este documento lista os aspectos a serem considerados durante a execução da release.
date: 29/10/2021
keywords: release, versionamento, changelog, diretórios, repositório, diretriz, elemento atômico
---

{{% notice note %}}
Os textos tachados estão obsoletos/depreciados. Aguarde nova versão.
{{% /notice %}}

Utilize esse checklist para gerar **release** dos artefatos de design.

Detalhes podem ser encontrados no arquivo [Versionamento](/design/guias/versionamento) no diretório de [Guias de Design](/design/guias).

~~- Seguir o fluxo de versionamento pré-release~~

~~- [ ] Gerar a versão da release baseando-se no arquivo de changelog.~~

~~- [ ] Ajustar a versão do arquivo/pacote de versão.~~

~~- [ ] Ajustar a versão de release no changelog.~~

~~- [ ] Ajustar a versão de release no arquivo fonte do elemento atômico (Adobe XD/Figma).~~

~~- [ ] Ajustar a versão de release no arquivo fonte de modelo (Adobe XD/Figma), caso exista.~~

~~- [ ] Ajustar ou adicionar a versão de release no arquivo diretriz (Markdown).~~

~~- [ ] Mover o pacote de versionamento para o diretório release.~~=

```markdown
**Caso seja necessário adicionar arquivos na área de download do sítio Design System:**
- [ ] Criar e nomear corretamente o arquivo ".zip" com o pacote que foi gerado da release.
- [ ] Salvar o arquivo no diretório de download ([Serpro Drive](https://serprodrive.serpro.gov.br/apps/files/?dir=/_solucoes/Governo%20Digital/Design_System_Gov/8-Download%20Site&fileid=109790504)).
- [ ] Gerar o link para download.
- [ ] Atualizar as tarefas de design e backlog do git [utilizando o Git](/design/guias/utilizando-git).
```
