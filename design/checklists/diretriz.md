---
title: Diretriz
description: O documento lista os aspectos a serem seguidos durante a criação e atualização das diretrizes de design.
date: 17/06/2021
keywords: diretriz, estrutura padrão, modelo, markdown, padrões de design.  
---

Use este checklist para criar e atualizar a Diretriz (Markdown).

Detalhes podem ser encontrados no arquivo de [Diretriz](/design/guias/diretriz) dos arquivos [Guias de Design](/design/guias).

```markdown
-   [ ] Seguir a estrutura padrão do modelo (Fundamento, Componente, Padrão, Template...)
-   [ ] Seguir o Checklist de [Comportamentos](/design/checklists/comportamentos)
```
