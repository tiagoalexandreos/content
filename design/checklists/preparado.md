---
title: Preparado
description: O documento lista os aspectos a serem considerados durante a análise de problemas em preparação à criação de tarefas de design.
date: 26/10/2021
keywords: análise, problema, definição, justificativa, exemplos, restrições, riscos, valor, critérios de aceitação, impactos
---

{{% notice note %}}
Os textos tachados estão obsoletos/depreciados. Aguarde nova versão.
{{% /notice %}}

```markdown
Este checklist é utilizado para a criação de tarefas para a equipe de designer.

- Definir o problema e/ou a necessidade (e não um componente específico).
    - [ ] Descrever a justificativa de forma clara, informando o problema ou necessidade que originou a demanda (seja direto, impessoal preferindo redigir no infinitivo).
    - [ ] Definir pontos-chave a serem resolvidos.
    - [ ] Exemplificar como o problema foi resolvido em outras plataformas (adicionar links, imagens, etc.).
    - [ ] Listar elementos (fundamentos, componentes, padrões, etc.) já criados pelo time, que tenham funções similares.
    - [ ] Listar comportamentos específicos, se houver, que precisam ser detalhados (somente comportamento específico e que não esteja listado no [checklist de comportamento](/design/checklists/comportamentos).
    - [ ] Caso a demanda seja emergencial deve ser citado.
- Risco da atividade
    - [ ] Informar/listar os riscos da não execução da demanda.
- Valor da atividade
    - [ ] Listar os resultados a ser alcançados a partir do atendimento dessa demanda.
- Restrição da atividade (lista de dependências)
    - [ ] Existem elementos já criados suficientes para a criação desse novo elemento atômico?
    - [ ] Os elementos existentes precisam ser evoluídos.
    - [ ] Quais itens precisam ser criados?
- Impactos
    - [ ] Informar/listar os componentes ou produtos impactados por essa demanda.
- Critérios de aceitação (lista de checklists que devem ser seguidos para o estado pronto.)
    - [ ] Informar os critérios de aceitação para validar a demanda como concluída.
    - [ ] Deve possuir pelo menos um critério de aceite (descrição simples de como deve funcionar).
    Exemplos:
        - Estar atualizado no site;
        - Ser atualizado os componentes;
- Alinhamento com alguma pessoa responsável pelo desenvolvimento e pelo design (ou até com time todo) sobre a demanda de forma a gerar uma compreensão compartilhada sobre o problema.
    - [ ] Possibilidades e limitações tecnológicas de cada plataforma.
    - [ ] Análise de impacto para a atividade a ser executada com os devidos registros (impactos e riscos, se houver).

```

Após essa análise a demanda pode ir para as seguintes fases, dependendo da análise:

~~- Caso tenha restrição da atividade, vá para fase de pendências até que todas as pendências sejam resolvidas;~~

~~- Caso não tenha restrição, vá para a fase de pronto/ready, ficando disponível para se iniciar as atividades (estado preparado);~~

~~- Caso a análise verifique que não há necessidade de ser feito nada, a demanda pode ser fechada.~~
