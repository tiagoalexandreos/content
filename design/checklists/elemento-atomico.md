---
title: Elemento Atômico
description: O documento lista os aspectos a serem seguidos durante a criação e atualização dos artefatos de elemento atômico (arquivos fontes - adobe XD e Figma)
date: 16/06/2021
keywords: elemento atômico, UIKit, assets, anatomia, tipos, cabeçalhos, nomenclaturas, imagens, legendas, comportamentos.  
---

Usado para criar e atualizar os Elementos Atômico (Arquivo Fonte, arquivo do adobe XD).

Detalhes podem ser encontrados no arquivo [Arquivo Fonte](/design/guias/arquivo-fonte) dentro do diretório [Guias de Design](/design/guias).

```markdown
- [ ] Criar as regiões: Elemento Atômico e Diretriz
- [ ] Importar o UIKIT com atualização mais recente
- [ ] Criar e utilizar as Pranchetas corretamente
    - [ ] Criar e utilizar Cabeçalho Especiais
    - [ ] Evite criar pranchetas para detalhar o mesmo assunto
    - [ ] Ter apenas uma prancheta para Anatomia e Tipo (Região do Elemento Atômico)
    - [ ] Utilizar o Guia para Imagens (Na Região Diretriz)
- [ ] Criar os Elementos Gráficos/Atômicos
    - [ ] Utilizar o conceito de componentização, seguindo todas as regras descritas das diretrizes já criadas.
    - [ ] Utilizar as Nomenclaturas Geral em todos os elementos
    - [ ] Utilizar sempre valores inteiros
    - [ ] Criar Assets/Ativos para facilitar a manutenção
    - [ ] Utilizar Cotas e Legendas Padrões
    - [ ] Utilizar os tokens corretos e atualizados
```
