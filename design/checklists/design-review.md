---
title: Design Review
description: O documento lista os aspectos que o designer precisa revisar ao final da fase de implementação (CSS/HTML/JS) dos artefatos produzidos pela equipe de desenvolvimento. 
date: 26/05/2021
keywords: fundamentos, comportamentos, cores, elevação, iconografia, espaçamentos, estados, superfície, tipografia, grid, anatomia, tipos
---

Este checklist visa garantir a consistência do **Design Review** da entrega de  um artefato pelo desenvolvimento.

```markdown
- O componente foi desenvolvido conforme diretriz de design.
    -   [ ] Os itens presentes na anatomia existem e foram corretamente utilizados (opcional e obrigatórios).
    -   [ ] Os itens da anatomia se comportam como descrito no "Detalhamento dos itens".
    -   [ ] Os tipos (quando previstos) funcionam conforme descrito.
    -   [ ] Os comportamentos funcionam conforme descrito.
- Fundamentos (aplicação dos tokens)
    -   [ ] Cores - As cores aplicadas correspondem as definidas no layout.
    -   [ ] Elevation - A sombra foi aplicada corretamente.
    -   [ ] Iconografia - O ícone aplicado corresponde ao definido.
    -   [ ] Espaçamentos - Os espaçamentos seguem os padrões definidos.
    -   [ ] Estados - Os estados foram aplicados corretamente.
    -   [ ] Superfície - As bordas, os cantos, a opacidade seguem o que foi definido.
    -   [ ] Tipografia - A tipografia, peso, tamanho foi aplicado corretamente.
    -   [ ] Grid - funciona corretamente nos breakpoints:
        -   [ ] X-Small (Phone Portrait)
        -   [ ] Small (Phone Landscape e Tablet Portrait)
        -   [ ] Medium (Tablet Landscape)
        -   [ ] Large (Desktop)
        -   [ ] X-Large (Big Desktop)
 - Compatibilidade Crossbrowser
    - [ ] O componente/artefato apresenta os comportamentos corretos nos principais navegadores do mercado
    - [ ] O componente/artefato apresenta os aspectos de fundamentos visuais preservados nos principais navegadores do mercado
```

## Comportamentos comuns

São comportamentos mais comuns vistos na diretriz que conferem maior destaque. Isso não quer dizer que outros pontos não devem ser levados em consideração, ou seja, todos os itens de comportamento descritos na diretriz precisam ser analisados.

```markdown
-   [ ] O componente é flexível e funciona bem quando o conteúdo for maior ou menor do que o projetado (densidade informacional).
-   [ ] As densidades (alta, média e baixa) correspondem ao definido.
-   [ ] Acessibilidade foi tratada conforme a diretriz.
-   [ ] A responsividade foi tratada conforme a diretriz.
-   [ ] Os conteúdos textuais estão aplicados conforme recomendação na diretriz.
-   [ ] De forma geral, os comportamentos listados no [documento de comportamento de design](/design/checklists/comportamentos) foram utilizados na diretriz e foram aplicados corretamente.
```
