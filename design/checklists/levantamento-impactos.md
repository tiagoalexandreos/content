---
title: Levantamento de Impactos
description: O documento lista os aspectos a serem considerados durante o levantamento de impactos, causados pela criação ou atualização dos artefatos de design.
date: 20/08/2021
keywords: impactos, mudanças, diretrizes, elemento atômico, fundamentos, templates, padrões, páginas, imagens
---

Utilize este checklist para realizar o levantamento de impactos que venha a ocorrer ao finalizar a entrega de um artefato.

```markdown
Usado para entender as mudanças e impactos na atualização/criação dos artefatos.

- [ ] Listar os problema encontrados que justificam a mudança (o impacto).
    - [ ] Listar pontos positivos e negativos da criação/atualização.
- [ ] Listar elementos atômicos (fundamentos, componentes, templates, padrões, páginas, etc.) ou artefatos (arquivo fonte XD, diretriz, imagens, etc.) que sofreram/sofrerão impactos
    - [ ] Definir onde foi o impacto para cada impacto listado (anatomia, comportamento, imagens da diretriz, definição, etc.)
    - [ ] Definir solução para tratar cada impacto listado.
```
