---
title: UIKit
description: Este documento lista os aspectos a serem considerados durante a atualização do artefato de UIKIT.
date: 25/10/2021
keywords: uikit, adobe XD, figma, componentes, assets, changelog, elemento atômico, tokens
---
{{% notice note %}}
Os textos tachados estão obsoletos/depreciados. Aguarde nova versão.
{{% /notice %}}

Utilize esse checklist para atualizar o artefato de **UIKIT**, independentemente da tecnologia utilizada.

Maiores informações podem ser encontradas no documento [UIKit](/design/guias/uikit).

```markdown
- [ ] Ler a issue/tarefa e seguir todas as recomendações e informações listadas.
- [ ] Caso haja mais de um asset para ser alterado/criado, ordenar por ordem de complexidade, começando pelo mais simples em questões de atomic design.
- [ ] Ler a diretriz referente ao asset que deve ser alterado/criado.
- [ ] Atualize a versão de cada asset alterado ou criado.
```

- ~~[ ] Analisar o changelog referente ao asset que deve ser alterado/criado.~~
- ~~[ ] Atualizar/criar o asset conforme o entendimento no changelog e da diretriz.~~
- ~~[ ] Atualizar o changelog do UIKit com as informações atualizadas. É importante identificar a versão do artefato/pacote utilizado para essa atualização.~~
