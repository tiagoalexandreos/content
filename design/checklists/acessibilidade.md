---
title: Acessibilidade
description: O documento visa responder quais itens de acessibilidade o designer deve se preocupar ao produzir um artefato de design
date: 22/04/2021
keywords: acessibilidade, inclusão, deficiência visual, navegação via teclado, interação, contraste de cores, semântica de código, teclas de atalho, legibilidade, W3C, WCAG, padrões web
accessibilty: true
---

O checklist abaixo deve ser utilizado para construção de qualquer elemento ou interfaces (componente, padrões, fluxos, etc.) dentro do Design System, principalmente para criação de suas diretrizes.

O documento é voltado para designers, portanto, podem existir pontos importantes não explorados por não fazer sentido para este tipo de usuário (como por exemplo, codificação semântica do HTML, etc.)

O detalhamento de cada item no checklist pode ser encontrado nos documento de [Recomendações de Acessibilidade](/design/guias/acessibilidade).

```markdown
- [ ] Contraste de cores
- [ ] Tecnologia assistiva
- [ ] Hierarquia
- [ ] Teclas de Atalho
- [ ] Área mínima de Interação (Clique e Toque)
- [ ] Informações, Conteúdos e Feedback
- [ ] Layout Responsivo
- [ ] Espaçamento e Legibilidade
- [ ] Suporte a Idiomas
- [ ] Som e Movimento
- [ ] Formulários
- [ ] Documentação
- [ ] Navegação
- [ ] Teste e Pesquisa
```
