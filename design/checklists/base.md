---
title: Base
description: Este checklist é usado para criação ou atualização de qualquer artefato de design
date: 14/01/2021
keywords: padrões, design token, assets, changelog, fluxo de versionamento, workflow
---

{{% notice note %}}
Os textos tachados estão obsoletos/depreciados. Aguarde nova versão.
{{% /notice %}}

Utilize este checklist para criar ou atualizar qualquer artefato de design.

Detalhes podem ser encontrados em [Guias de Design](/design/guias).

```markdown
- [Utilizar padrões de nomenclatura](/design/guias/nomenclatura)
    - [ ] Geral.
    - [ ] Design Token.
    - [ ] Assets.

- [Criar Estrutura Básica de Diretórios e Artefatos](/design/guias/estrutura)

    - [ ] Diretórios Básicos.
    - [ ] Artefatos Básicos.
    - [ ] Liberar o arquivo para próxima fase do workflow quando TODOS os checklist atribuídos forem concluídos.
```

~~- Fluxo de versionamento pré-release:~~

~~- [ ] Etapa de entendimento. Discussão inicial com desenvolvedor. (Nome do desenvolvedor)~~

~~- [ ] Validar com o desenvolvedor que foi feito. (Nome do desenvolvedor)~~

~~- [ ] Criar changelog.~~

~~- [ ] Documentar changelog (Tipo de mudança, descrições, etc.).~~
