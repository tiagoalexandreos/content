---
title: Comportamentos
description: O documento lista os aspectos nos quais o designer precisa se preocupar, no item comportamento, ao criar ou atualizar artefatos de design. 
date: 05/05/2021
keywords: estados, densidade, densidade informacional, contraste de cores, interações via teclado, boas práticas, área de proteção, área mínima interativa, acessibilidade
---

Use este checklist para prever as informações básicas que podem ocorrer nos artefatos de design.

Os itens devem ser detalhados e criados no arquivo fonte e/ou na diretriz e sempre que possível na área específica de **comportamentos**.

```markdown
-   [ ] **Estados**
      Detalhar quais estados (fundamento estado) fazem sentido (hover, foco, desativado...)
      Exibir como os estados são aplicados no elemento atômico (fundo claro e escuro).
-   [ ] **Densidade**
      Detalhar a densidade Alta, Média e Baixa. Caso tenha necessidade de outras densidades, detalhar corretamente cada uma delas.
-   [ ] **Densidade Informacional**
      Incluir melhores práticas para resolver problemas de layout quando existir muito conteúdo textual (como no caso de ter duas linhas de texto ou mais). Soluções mais utilizadas: overflow, truncation, wrapping...
-   [ ] **Fundo Claro VS Fundo Escuro**
      Detalhar se o estilo visual quando aplicado em diferentes brilhos de uma superfície.
-   [ ] **Responsividade/Adaptabilidade**
      Detalhar o comportamento em diferentes tipos de GRIDs disponíveis no Design System. Veja o conteúdo Responsividade no Fundamento Grid para maiores informações.
-   [ ] **Interações via Teclado**
      Documentar as interações possíveis via teclado quando existir uma interação exclusiva por Mouse ou outro dispositivo de entrada. Seguir os padrões WCAG 2.0 de acessibilidade.
-   [ ] **Boas Práticas**
      Incluir boas práticas de uso em diferentes comportamentos e situações previstas.
-   [ ] **Internacionalização**
      A interface do usuário para idiomas lidos da direita para a esquerda (RTL), como árabe e hebraico, devem ser espelhados para garantir que o conteúdo seja compreendido.
-   [ ] **Área de Proteção**
      Detalhar como funciona o espaçamento mínimo (margem de segurança) quando existir elementos próximos. **Obs**: Não usar valor!! O valor deve ser detalhado na parte de tokens!
-   [ ] **Área Mínima Interativa**
      Caso tenha algum elemento interativo, exibir/detalhar a área mínima de interação para interfaces por toque/touch (recomendação atual é que tenha pelo menos 48px de área e 8px de espaçamento externo entre um objeto interativo e outro) e por clique/click (metade dos valores descritos no toque).
-   [ ] **Acessibilidade**
      Seguir o [checklist de acessibilidade](/design/checklists/acessibilidade).

```
