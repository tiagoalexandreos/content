---
title: Prototipação
description: O documento lista os aspectos a serem considerados durante a construção de protótipos.
date: 30/07/2021
keywords: protótipo, adobe XD, figma, rascunho, UX, interações, fluxo, fundamentos, navegação, apresentação, ações, usuário
---

Utilize esse checklist para auxiliar a atividade de **Prototipação**.

```markdown
- [ ] Seguindo os [Princípios do Design System](https://cdngovbr-ds.estaleiro.serpro.gov.br/introducao/principios).
- [ ] Faça um rascunho de todas as interações e comportamentos que o componente deve prever
- As leis de UX foram consideradas:
    - [ ] Efeito Von Restorff
        - Planeje informações importantes ou ações-chave visualmente distintas.
        - Use a contenção ao dar ênfase aos elementos visuais para evitar que eles concorram entre si e para garantir que itens importantes não sejam identificados erroneamente como anúncios.
        - Considere usuários com deficiências de visão de cor ou baixa visão, confiando exclusivamente na cor para comunicar contraste.
        - Considere cuidadosamente os usuários com sensibilidade a movimento ao comunicar contraste.
    - [ ] Lei de Hick
        - Minimize as escolhas quando os tempos de resposta são críticos para aumentar o tempo de decisão.
        - Quebre tarefas complexas em etapas menores, a fim de diminuir a carga cognitiva.
        - Evite sobrecarregar os usuários destacando as opções recomendadas.
        - Use o onboarding progressivo para minimizar a carga cognitiva para novos usuários.
        - Tenha cuidado para não simplificar ao ponto de abstração.
    - [ ] Lei de Fitt
        - Os alvos de toque devem ser grandes o suficiente para que os usuários os selecionem com precisão.
        - Alvos de toque devem ter amplo espaçamento entre eles (nesse caso, deve-se valer do bom senso equilibrando as distâncias dos elementos).
        - Os alvos de toque devem ser colocados em áreas de uma interface que lhes permitam ser facilmente acionados.
    - [ ] Efeito Zeigarnik (as pessoas lembram mais das atividades não concluídas e interrompidas do que as concluídas).
        - Forneça progresso em direção a um objetivo ajudará a garantir que os usuários tenham mais chances de ter a motivação para completar essa tarefa.
        - Forneça uma clara indicação de progresso, a fim de motivar os usuários a completar tarefas.
    - [ ] Efeito Serial Position.
        - Posicionar os itens menos importantes no meio das listas pode ser útil porque esses itens tendem a ser armazenados com menos frequência na memória de longo prazo e de trabalho.
        - Posicionar ações-chave na esquerda e direita dentro de elementos como navegação pode aumentar a memorização.
    - [ ] Lei da Região Comum
        - Adicionar borda em torno de um elemento ou grupo de elementos é uma maneira fácil de criar uma região comum.
        - A região comum pode ser criada definindo um fundo por trás de um elemento ou grupo de elementos.
    - [ ] Lei da Proximidade
        - A proximidade ajuda a estabelecer uma relação com objetos próximos.
        - A proximidade auxilia os usuários a entender e organizar informações de forma mais rápida e eficiente.
- [ ] [Fundamentos visuais](https://www.gov.br/ds) foram seguidos.
- [ ] Os [Padrões](https://www.gov.br/ds) existentes foram considerados.
- [ ] Em caso de grandes impactos, envolva as partes interessadas (SGD e SERPRO)
- [ ] Foi realizada análise de viabilidade junto ao desenvolvedor.

```
