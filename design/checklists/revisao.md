---
title: Revisão
description: Este documento lista os aspectos a serem considerados durante a revisão de qualidade dos artefatos de design.
date: 14/01/2021
keywords: revisão, padrões, checklists, qualidade, diretriz, changelog, elemento atômico, workflow
---

{{% notice note %}}
Os textos tachados estão obsoletos/depreciados. Aguarde nova versão.
{{% /notice %}}

Use esse checklist para realizar a revisão de qualidade dos artefatos de design.

Detalhes podem ser encontrados no diretório de [Guias de Design](/design/guias).

```markdown
- O designer seguiu todos os checklists propostos corretamente.
    - [ ] Listar todos os ajustes necessários para atender o item acima.
- Analisar se a demanda foi realizada conforme proposto na tarefa (pronto).
    - [ ] Listar todos os ajustes necessários para atender o item acima.
- Verificar se o texto criado na documentação está compreensível e bem redigido. Melhorias podem ser sugeridas.
    - [ ] Listar todos os ajustes necessários para atender o item acima.
- [ ] Verificar se os ajustes foram realizados corretamente ou se existe alguma justificativa objetiva e clara.
- [ ] Liberar o arquivo para próxima fase do workflow quando não houver mais ajustes a serem realizados.
```

- ~~[ ] Seguir o [Fluxo de Versionamento](/design/guias/versionamento) Pré-Release~~
