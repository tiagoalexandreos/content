---
title: "Checklists"
chapter: true
pre: '<i class="far fa-check-circle"></i> '
---

Utilize os diversos checklists que preparamos para orientar a execução de cada tarefa e garantir que todas as etapas esperadas nas entregas tenham sido consideradas.

Cada checklist proposto é baseado em uma etapa ou atividade específica.

Clique no ícone no topo à direita para copiar os checklists para sua *issue* e siga todas as etapas mencionadas.
