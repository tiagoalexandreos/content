---
title: Publicação NPM
description: Publicacao NPM
date: 26/04/2021
keywords: npm, gitlab
---

## Publicação de pacotes no npm

1. No repositório Core verificar no arquivo package.json esteja com o nome e versão nova ,o npm não republica ou é possível deletar uma versão já publicada.
1. Verificar se a variavel NPM_TOKEN esteja atualizada nas configurações CI/CD do gitlab
1. criar Tag com a versão que vai publicar
1. Executar o pipeline branch da tag e depois executar o publish_npm dentro do pipeline executado para publicar no npm

## Problemas de publicação

### Erro code E404

  Verifique se o NPM_TOKEN ou a versão já esteja publicada no npm
