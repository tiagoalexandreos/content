---
title: 'Criação de release de site '
description: Guia de criação de release de site
---

## Processo de criação de Release do Site

1. Verificar se tem problema de responsividade, funcionalidade e integração no site de desenvolvimento
1. Criar branch de release com a versão que vai ser gerada como release/v1.0.0
1. Verificar se a versão mostrada no header é a mesma que vai ter o release
1. Rodar o comando npx standard-version para gerar tag
1. Mande a tag e branch gerada para o repositório
