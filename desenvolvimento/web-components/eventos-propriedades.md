---
title: Eventos e Propriedades Nativas
description: Explicação sobre o eventos e propriedades nativas do browser
---

Para entender melhor sobre propriedades e eventos nativos e customizados, os conceitos sobre custom elements tornam-se importantes.

Existem dois tipos de custom elements:

**Autonomous custom elements** — são autônomos eles não herdam de elementos HTML padrão. Você os usa em uma página, literalmente escrevendo-os como um elemento HTML. Por exemplo `<popup-info>`, ou document.createElement("popup-info").

``` javascript
class PopupInfo extends HTMLElement {
  constructor() {
      // Sempre chame super primeiro no construtor super();
      super();
      // Funcionalidade do elemento escrita aqui ...
  }
}
```

**Customized built-in elements** - herdam de elementos HTML básicos. Para criar um deles, você deve especificar qual elemento eles estendem (como implícito nos exemplos acima), e eles são usados escrevendo o elemento básico, mas especificando o nome do elemento personalizado no atributo is (ou propriedade). Por exemplo `<p is="word-count">`, ou `document.createElement("p", { is: "word-count" })`.

``` javascript
class WordCount extends HTMLParagraphElement {
  constructor() {
      // Sempre chame super primeiro no construtor super();
      super();
      // Funcionalidade do elemento escrita aqui ...
  }
}
```

No código acima criamos um web component que implementa nativamente todos os eventos e atributos diretamente do [atributo parágrafo do html](https://developer.mozilla.org/pt-BR/docs/Web/HTML/Element/p) `<p>` não sendo necessário implementá-los novamente dentro do nosso componente.

Para saber mais sobre os elementos personalizados do html e para ter ideia de qual elemento herdar acesse [Hierarquia de elementos html](https://www.falkhausen.de/Java-10/org.w3c.dom/html/HTMLElement-Hierarchie.html).

**Observação**:

- Deverá ser avaliado caso a caso, pois para alguns determinados componentes não faz muito sentido absorver essas propriedades nativas.

- Avaliar e gerar um estudo para saber quais componentes do Design System serão *autonomous custom* e quais são *customized built-in*.

- Avaliar quais dos frameworks existentes hoje são capazes de fazer o papel de *customized built-in*. Quais são capazes de implementar as propriedades nativas sem a necessidade muito esforço ou reescrita de métodos e propriedades dentro do web component.
