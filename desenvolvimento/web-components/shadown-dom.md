---
title: Shadow DOM
description: Explicação sobre o shadown DOM
---

## O que é Shadow DOM?

Imagine os componentes/elementos que usamos em nossa página da web como pequenos reinos. Esses reinos possuem fortificações, e você poderá interagir com estes reinos sob certas regras ou canais fornecidos por eles (ou não). As fortificações, neste caso, são formadas pelo Shadow DOM.

Em contraste, a árvore DOM com a qual estamos acostumados (diretamente acessível por meio da variável de documento em JavaScript) é conhecida como Light DOM.

Dentro de um limite do Shadow DOM, existe um nó raiz conhecido como Shadow Root. A Shadow Root deve ser hospedada por um nó, que normalmente existe no Light DOM. O elemento de hospedagem é, portanto, conhecido como Shadow Host.

## Características do Shadow DOM

- Árvore DOM isolada: o shadow DOM é autocontido, ou seja, o exterior não pode consultar elementos no interior. Por exemplo, `document.querySelector` não retorna nós de dentro do shadow DOM.

- CSS com escopo definido: os estilos definidos no shadow DOM não vazarão e os estilos externos não invadirão.
- Composição: com o uso de `<slot />` é possível pegar nós externos do DOM e colocá-los em posições específicas dentro do shadow DOM.

A combinação de elementos personalizados com shadow DOM provê o isolamento de estilos e encapsulamento de DOM, que é perfeito para componentes reutilizáveis e ​​autocontidos.

## Usar ou não o shadow DOM

Criar um Web Component que não use shadow DOM é perfeitamente normal e, em alguns casos, não é aconselhável o uso do Shadow DOM.

Recomendamos fortemente utilizar o Shadow DOM, uma vez que elementos dentro do shadow tem escopo definido e os estilos fora do componente não se aplicam.

Porém, deve-se avaliar se o componente realmente será autônomo com seu próprio conjunto de funcionalidades. Por exemplo, imagine o componente LIST. Sua construção depende da existência e estilo de outros componentes:

- Item
- Divider
- Checkbox
- Button

Se o componente LIST for declarado com a flag `shadow: true`, o único estilo aplicado será aquele designado na classe do LIST.

Isto ocorre porque este componente tem uma shadow root aplicada ao elemento de host ( `<br-list>`) e os elementos internos ao componente não podem ser estilizados por meio de classes CSS definidas fora da shadow root do componente.

> Estilos globais não vão parar no Shadow DOM!

É possível reverter o problema acima de duas formas:

- incluindo todos os outros estilos (item, divider, checkbox e button) dentro do shadow root do componente LIST. Se o sistema de design existente depende de variáveis ​​_sass_, também precisamos importá-las na folha de estilo do componente, ou

- desligando o shadow DOM do LIST.
