---
title: Uso de JSON ou Slot
description: Análise sobre passagem de dados usando slots ou JSON
---

Esse documento é resultado de uma POC (Proof of Concept) sobre o uso de JSON ou de slot na construção do componente. Foram usados os componentes **breadcrumb** e **footer** para realizar esse estudo.

O estudo possibilitou levantar algumas características relacionadas a essas duas abordagens.

## Abordagem por JSON

Ao usar a abordagem por **JSON** para escrever os webcomponents observou-se as seguintes características:

1. O código do componente fica totalmente isolado do ambiente externo. Isso facilita a implementação e a aderência do componente aos padrões do Design System, pois o desenvolvedor do componente tem todo o controle de sua estrutura, estilo e comportamento.

2. A passagem de dados se dá por props previamente definidas e cadastradas no componente. Essas props são atributos HTML. Dessa forma a passagem de dados acontece por meio do uso de atributos nos elementos HTML. Esses atributos recebem como valor uma string.

3. O uso do JSON ocorre no valor dos atributos. Considerando que os atributos HTML podem receber os valores entre aspas simples, pode-se escrever um JSON com a sintaxe correta e colocá-lo entre aspas simples como valor do atributo.

    > O componente pode esperar que o o JSON seja passado sempre da forma correta, deixando a cargo do desenvolvedor que usa o compoente tratar esse JSON.

4. O JSON pode ser montado como um objeto em um script JavaScript e transformado para JSON antes de ser passado como valor ao atributo HTML. Isso pode ser manipulado no próprio script usando a API de manipulação do DOM, evitando escrever uma string gigante no próprio HTML.

    ```js
    document.querySelector('<componente>').setAttribute('<atributo>', JSON.stringify(<objeto>))
    ```

5. Dependo do caso de uso, o encapsulamento fechado do código do componente pode não ser adequado. As vezes o desenvolvedor que usa o componente pode querer adaptar alguma parte do componente para as suas necessidades específicas. A abordagem exclusiva por JSON impede que essa adaptação seja feita.

## Abordagem por Slot

Ao usar a abordagem por **slot** para escrever os webcomponents observou-se as seguintes características:

1. Uso de mixins CSS ao trabalhar com subcomponentes:
Ao criar subcomponentes, utilize mixins CSS para reutilizar estilos comuns e facilitar a manutenção. Os mixins CSS permitem definir regras de estilos compartilhados que podem ser aplicadas a vários componentes, evitando a duplicação de código e promovendo uma abordagem modular na construção dos componentes.

2. Reutilização de componentes prontos na montagem do componente:
Aproveite componentes já existentes, como IconBase e Divider, para facilitar a montagem do seu componente. Isso permite reutilizar estilos e funcionalidades já implementadas, economizando tempo e esforço.

3. Consideração do Atomic Design para componentes mais atômicos:
Ao projetar componentes, siga as diretrizes do Atomic Design. Pense em componentes mais granulares e reutilizáveis, de forma a facilitar a manutenção e promover a coesão entre os elementos da interface.

4. Responsabilidades individuais dos componentes:
Cada componente deve ter suas próprias responsabilidades bem definidas e não deve depender em excesso de outros componentes ou da aplicação como um todo. Isso ajuda a manter a modularidade e a escalabilidade do sistema.

5. Exibição da ordem das árvores de componentes:
Considere fornecer uma visualização clara da estrutura de árvore dos componentes. Isso pode ser especialmente útil em cenários complexos, pois ajuda a compreender a hierarquia e o fluxo de dados entre os subcomponentes.

6. Adaptação à mudança da estrutura HTML em tempo de execução:
Se a estrutura do HTML do componente puder ser alterada dinamicamente durante a execução, isso pode representar um desafio adicional na implementação. Certifique-se de lidar corretamente com essa dificuldade para garantir o bom funcionamento do componente.

7. Dificuldade na validação do conteúdo do slot:
Ao utilizar slots para permitir a inserção de conteúdo personalizado no componente, pode ser difícil validar o que foi colocado dentro do slot. Tenha em mente que, ao optar por validar o conteúdo do slot, você pode limitar a liberdade do usuário. Considere cuidadosamente se a validação é realmente necessária para o caso de uso específico.

8. Exploração adequada de eventos:
Não deixe de explorar a utilização de eventos nos subcomponentes. Adicione eventos de clique nas tags dos subcomponentes e verifique se eles são chamados corretamente. Certifique-se de que a funcionalidade dos eventos esteja funcionando conforme esperado. Se houver problemas, verifique se o atributo inheritAttrs está definido como false durante a construção do componente.

## Conclusão

Considerando os tópicos levantados durante a POC relacionado ao uso de JSON e slot pode-se concluir que as duas abordagens funcionam e podem ser implementadas, porém cada uma possui suas próprias características e situações que melhor se aplicam.

A abordagem por JSON é mais indicada quando se quer isolar por completo o componente mantendo um controle maior sobre a estrutura, estilo e comportamento do componente.

A abordagem do slot é mais indicada quando se quer mais reuso, modularização e escalabilidade do sistema, pois o componente oferece mais liberdade para adaptações.

Ao levar em consideração essas características levantadas, você estará adotando boas práticas de arquitetura e design, levando em consideração o reuso, modularização e escalabilidade do seu sistema.
