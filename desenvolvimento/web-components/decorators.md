---
title: Decorators e Imports
description: Explicação sobre o decorators e imports
---

Os decoradores aplicam os templates com base em seletores CSS e JavaScript para criar mudanças visuais e comportamentais. O CSS em conjunto com as *Media Queries* seria possível criar uma poderosa ferramenta de Responsive Web Design utilizando decoradores.

- O elemento `content` inserido dentro do template será substituído com o conteúdo do elemento de decoração.

- Através do atributo `selector` é possível especificar a posição exata do elemento particular na marcação que será produzida.

Exemplo de uso de um decorador:

``` javascript
@CustomElement({
    selector: 'ce-my-name'
})
```

Com **HTML Imports** podemos importar documentos HTML inteiros para dentro de nossas páginas. De uma maneira semelhante à importação de um arquivo JavaScript ou CSS, o HTML Import cria uma nova requisição ao servidor para realizar o download do arquivo. Essa tecnologia auxilia no encapsulamento dos componentes em arquivos externos possibilitando importá-los posteriormente.

Importações HTML se destina a ser o mecanismo de empacotamento para componentes da web, mas você também pode usar Importações HTML por si só.

Antes do HTML Imports, as poucas alternativas de importarmos documentos HTML eram através de elementos iframes e requisições AJAX, não muito bem vistas do ponto de vista do código limpo.

``` html
<link rel="import" href="/html-components/userlogin-header.html" >
```
