---
title: Testes Unitários
description: Testes unitários com Jest e Vue Test Utils no Vue.js
---

## Introdução

Os testes automatizados são uma parte essencial do desenvolvimento de qualquer aplicativo. Eles ajudam a garantir que o código funcione corretamente e permaneça estável ao longo do tempo. No ecossistema Vue.js, o Vue Test Utils e o Jest são duas ferramentas populares para escrever testes.

## Vue Test Utils (VTU) e JEST

**Vue Test Utils (VTU)** é a biblioteca oficial de funções utilitárias de teste de unidade para Vue.js. Ele fornece alguns métodos para montar e interagir com componentes Vue de maneira isolada.

**Jest** é uma estrutura de testes JavaScript que tem como foco a simplicidade.

## Estrutura de pastas e arquivos

As Specs são os arquivos de testes, e devem ser disponibilizados na pasta do respectivo componente em: `/govbr-ds-wbc/src/library/components/<Componente>.`

## Comandos de execução dos testes Jest

- `npm run test:unit`- executa todos os specs de todos os componentes do projeto. É possível identificar breaking changes quando as alterações afetam outros componentes.

- `npx jest <component>`- executa apenas os testes do componente especificado.

## Criação dos testes unitários

### 1. Importação de dependências

Importe as dependências necessárias, incluindo o componente que você deseja testar e o Vue Test Utils.

- `import { shallowMount } from '@vue/test-utils'` - importar o método `mount()` ou `shallowMount()` para renderizar um componente no VTU (Vue Test Utils).

- `import <Component> from './<Component>.vue'` - importar o(s) arquivo(s) do(s) componente(s) sendo testado(s) e seu(s) subcomponente(s).

### 2. Montagem do componente

Use a função mount ou shallowMount do Vue Test Utils para montar o componente em um wrapper. Agora será possível escrever asserções para verificar se o componente está sendo renderizado corretamente ou se o comportamento está de acordo com o esperado.

- `describe` - nome do teste.

- `test` - breve descrição do cenário de teste.

- `mount` ou `shallowMount` - utilizado para montar um componente, atribuindo o resultado a uma variável chamada wrapper (por convenção). O shallowMount, assim como o mount, cria um wrapper que contém o componente montado e renderizado, mas no shallowMount os componentes filhos são fragmentados ('stubbed'). Ambos recebem um segundo parâmetro para definir a configuração do componente.

> - [Clique aqui para verificar as opções de montagem para `mount` e `shallowMount`](https://v1.test-utils.vuejs.org/api/options.html#mounting-options)

### 3. Realização de testes

#### De verificação do estado

A verificação do estado consiste em avaliar se o componente se comportou conforme o esperado. Isso pode envolver a verificação do estado interno do componente, a verificação da saída renderizada, a verificação de alterações em propriedades ou dados, ou qualquer outro resultado esperado após a ação ter sido realizada. Você pode usar asserções do Jest, como expect, para verificar se os resultados correspondem ao que você espera.

- `expect` -  o método auxiliar que nos ajuda a fazer as asserções necessárias, quais são as expectativas com o resultado final.

> <https://v1.test-utils.vuejs.org/api/selectors.html>
> <https://v1.test-utils.vuejs.org/api/wrapper/>

#### De interação ou operação

Testes de interação ou operação incluem a simulação de um evento de clique, a alteração de um valor em um campo de entrada, a chamada de um método ou qualquer outra ação que afete o comportamento do componente. Essa etapa geralmente envolve a chamada de métodos ou a simulação de eventos no wrapper do componente.

- <https://v1.test-utils.vuejs.org/guides/#testing-key-mouse-and-other-dom-events>

- <https://v1.test-utils.vuejs.org/guides/#asserting-emitted-events>

### Limpeza

Após cada teste, é uma prática recomendada limpar qualquer estado ou efeito colateral gerado pelo teste. Isso pode envolver a reinicialização de variáveis, a restauração de mocks ou a limpeza de dados temporários. A limpeza garante que cada teste comece em um estado limpo e isolado. Avaliar a necessidade.

- <https://v1.test-utils.vuejs.org/guides/#lifecycle-hooks>

## Dicas sobre o que testar

### Mocking de dependências

Às vezes, um componente depende de outros componentes, serviços ou APIs externas. Para isolar o componente que você está testando e garantir a consistência dos resultados dos testes, é comum fazer o mock dessas dependências. O Jest oferece recursos para criar mocks de forma fácil e flexível.

### Testando eventos e interações

Testar eventos e interações do usuário é uma parte importante dos testes de componentes Vue. O Vue Test Utils fornece métodos e utilitários para simular eventos e interações de forma programática, permitindo que você verifique se o comportamento do componente é o esperado em resposta a essas ações.

### Renderização correta

Verifique se o componente é renderizado corretamente, ou seja, se o HTML gerado pelo componente está de acordo com o esperado. Você pode testar a estrutura, classes, atributos e conteúdo renderizados.

- `expect(wrapper.element).toMatchSnapshot()` : Quando executado pela primeira vez, captura o estado atual do elemento renderizado e o salva como uma referência. Nas execuções seguintes, o teste verifica se o estado atual do elemento é igual ao snapshot de referência. Se houver alguma diferença, o teste falhará, indicando que houve uma mudança no comportamento ou na aparência do componente sendo testado. Caso uma mudança intencional tenha sido feita, é possível atualizar o snapshot de referência para que os testes subsequentes sejam executados com base na nova versão do componente.

### Comportamento interativo

Teste o comportamento interativo do Web Component. Simule interações do usuário, como cliques, foco, digitação, preenchimento de formulários, para verificar se o componente reage e se comporta corretamente.

- <https://v1.test-utils.vuejs.org/guides/#testing-key-mouse-and-other-dom-events>

### Manipulação de propriedades

Verifique se as propriedades são manipuladas corretamente. Teste a definição e atualização de propriedades, bem como os efeitos que elas têm no estado interno e na renderização do componente.

### Eventos emitidos

Se o Web Component emite eventos personalizados, teste se esses eventos são emitidos corretamente e se carregam os dados esperados. Use a simulação de eventos para acionar eventos personalizados e verifique se as ações associadas são executadas conforme o esperado.

- <https://v1.test-utils.vuejs.org/guides/#asserting-emitted-events>
- <https://v1.test-utils.vuejs.org/guides/#emitting-event-from-child-component>

### Tratamento de erros

Teste o tratamento de erros do Web Component. Simule situações de erro e verifique se o componente lida adequadamente com elas, exibindo mensagens de erro apropriadas ou tomando outras ações esperadas.

### Testes de acessibilidade

Certifique-se de que seu Web Component é acessível. Verifique se ele atende aos padrões de acessibilidade, como marcação correta, uso adequado de atributos ARIA, suporte a leitores de tela e teclado, contraste adequado, entre outros. Use ferramentas de teste de acessibilidade, como Lighthouse ou Axe, para ajudar nesse processo.

## Exemplo de teste

O exemplo abaixo apresenta um trecho do arquivo de testes `HeaderAction.spec.js`, que testa o subcomponente HeaderAction e verifica se o método onResize é chamado corretamente:

```javascript
import { shallowMount } from '@vue/test-utils'
import BrButton from '../Button/Button.ce.vue'
import IconBase from '../IconBase/IconBase.ce.vue'
import BrItem from '../Item/Item.ce.vue'
import BrList from '../List/List.ce.vue'
import BrSignIn from '../SignIn/SignIn.ce.vue'
import BrHeader from './Header.ce.vue'
import BrHeaderAction from './HeaderAction.ce.vue'
import BrHeaderActionSearch from './HeaderActionSearch.ce.vue'

describe('HeaderAction', () => {
  const slotActionsFunctions = `
  <div slot="headerAction">
    <br-header-action
      title-functions="Funcionalidades do Sistema"
      list-functions="[
        {
          icon: 'chart-bar',
          name: 'Funcionalidade 1',
          url: '#'
        }
      ]"
      title-links="Acesso Rápido"
      list-links="[
      {
        name: 'Link de acesso 1',
        href: '#'
      },
      {
        name: 'Link de acesso 2',
        href: '#'
      }
      ]"
    >
    </br-header-action>
  </div>
  `


  test('checks if the onResize method is called', () => {
    const onResize = jest.spyOn(BrHeaderAction.methods, 'onResize')
    const wrapper = shallowMount(BrHeader, {
      slots: {
        headerAction: slotActionsFunctions,
      },
      global: {
        stubs: {
          'br-header-action': BrHeaderAction,
          'br-list': BrList,
          'br-item': BrItem,
          'br-button': BrButton,
        },
      },
    })
    global.innerWidth = 200
    global.dispatchEvent(new Event('resize'))
    wrapper.find('.header-links.dropdown > button').trigger('click')
    wrapper.vm.$nextTick(() => {
      expect(wrapper.element).toMatchSnapshot()
      expect(wrapper.find('.header-functions.dropdown.show').exists()).toBe(false)
      expect(wrapper.find('.header-links.dropdown.show').exists()).toBe(true)
      // muda a resolução para chamar o método onResize
      global.innerWidth = 1500
      global.dispatchEvent(new Event('resize'))
      wrapper.vm.$nextTick(() => {
        expect(onResize).toHaveBeenCalled()
        expect(wrapper.find('.header-links.dropdown.show').exists()).toBe(false)
        expect(wrapper.element).toMatchSnapshot()
      })
    })
  })

```

1 - O teste começa importando vários componentes necessários, incluindo BrButton, IconBase, BrItem, BrList, BrSignIn, BrHeader, além do próprio BrHeaderAction e subcomponente BrHeaderActionSearch.

2 - Em seguida, o teste é definido usando a função describe, que agrupa os testes relacionados. Dentro do bloco describe, há um teste específico definido usando a função test. Esse teste verifica se o método onResize é chamado corretamente.

3 - Dentro do teste, há a definição de uma constante slotActionsFunctions que contém um trecho de HTML usado no slot `headerAction` para o componente BrHeader. Esse slot personalizado contém uma instância do componente BrHeaderAction com algumas propriedades definidas.

4 - Observa-se a chamada da função `jest.spyOn()`, que permite criar uma "espionagem" (spy) em um objeto e monitorar a chamada de um método específico desse objeto. A sintaxe básica do jest.spyOn(): `jest.spyOn(objeto, 'método')`:

- objeto - objeto que contém o método que você deseja espionar.
- método - nome do método que você deseja espionar.

5 - A seguir, o componente BrHeader é renderizado usando shallowMount (que renderiza somente o componente em questão, sem renderizar seus componentes filhos). Várias opções são passadas para shallowMount, incluindo os slots personalizados definidos anteriormente e a configuração global com alguns componentes substituídos por stubs (componentes simulados).

6 - Após a renderização do componente, o tamanho da janela é simulado definindo a propriedade innerWidth global para 200 e disparando um evento de redimensionamento (resize).

7 - Em seguida, é feito um clique em um elemento por meio da classe (seletor) do botão que aciona o dropdown da listagem de links dentro do componente BrHeader.

8 - A instância vue [vm](https://v1.test-utils.vuejs.org/api/wrapper/#vm) é acionada juntamente com a função `.nextTick()`, usado para executar código após uma atualização reativa ter sido concluída e o DOM ter sido renderizado.

9 - São feitas algumas verificações usando a função expect do Jest. É verificado se o elemento renderizado pelo wrapper é igual a um snapshot (uma referência armazenada anteriormente), se um elemento com a classe .header-functions.dropdown.show não existe e se um elemento com a classe .header-links.dropdown.show existe.

10 - Em seguida, o tamanho da janela é simulado definindo innerWidth para 1500 e disparando um evento de redimensionamento. Novas verificações são realizadas neste novo cenário.

## Como saber se já testei o suficiente?

Não é possível garantir que todo software funcione sem qualquer presença de erros, mas o ideal é que se avalie a importância, a criticidade e os requisitos de cada componente, aplicando um bom julgamento para determinar se os testes criados são suficientes para atender as necessidades.

### Cobertura de código

A cobertura de código mede a porcentagem de código que é exercido pelos testes. Uma alta cobertura de código indica que a maioria dos cenários foi testada. Embora uma cobertura de 100% não seja sempre necessária ou possível, uma cobertura adequada para as partes críticas do código pode ser um bom indicador de testes suficientes.

A ferramenta Jest fornece uma visão geral por meio de um relatório visual no terminal durante a sua execução:

File      |  % Stmts | % Branch |  % Funcs |  % Lines | Uncovered Line #s |
----------|----------|----------|----------|----------|-------------------|
All files |      100 |      100 |      100 |      100 |                   |
 data.js  |      100 |      100 |      100 |      100 |                   |
----------|----------|----------|----------|----------|-------------------|

Entenda esse relatório de cobertura:

- File (arquivo): nome do arquivo que está sendo testado
- % Stmts (declarações): indica a porcentagem de declarações (linhas de código) que foram executadas durante os testes em relação ao total de declarações existentes no arquivo. Isso mede a cobertura de instruções (statements) do código.
- % Branch (ramificações): Os valores que aparecem nessa coluna não condizem com a cobertura de sua branch do git, mas sim com as ramificações de seu código que foram avaliadas durante os testes em relação ao total de ramificações existentes no arquivo. Podemos entender como ramificação qualquer trecho de código que divide a execução de nosso programas em duas ou mais partes, ou seja: trechos com blocos if/else, switch/case ou ternários.
- % Funcs (funções): indica a porcentagem de funções (métodos ou rotinas) que foram chamadas durante os testes em relação ao total de funções existentes no arquivo.
- % Lines (linhas): porcentagem de linhas de código executadas durante os testes em relação ao total de linhas existentes no arquivo.
- Uncovered Line #s (Linhas não cobertas): ao contrário do item anterior, indica quais linhas não foram executadas nos testes.

### Casos de teste de sucesso e falha

Certifique-se de testar não apenas os casos em que você espera que o componente funcione corretamente (casos de sucesso), mas também os casos em que o componente deve falhar ou produzir resultados inesperados (casos de falha). Isso ajuda a garantir que o componente se comporte corretamente em todas as situações.

### Cenários de borda e limite

Teste cenários de borda e limite para verificar como o componente lida com situações extremas ou com valores nos limites permitidos. Isso inclui testar com valores mínimos, máximos ou inválidos, bem como testar casos extremos que podem levar a comportamentos inesperados.

### Testes de regressão

Ao adicionar novos recursos ou corrigir bugs, é importante garantir que os testes existentes não sejam quebrados. Execute testes de regressão para verificar se as alterações recentes não introduziram erros em funcionalidades previamente testadas.

### Feedback e revisão

Outros membros da equipe de desenvolvimento devem realizar revisão para ajudar a identificar áreas em que os testes podem ser aprimorados ou casos de teste que podem ter sido esquecidos.

## Referências

[Curso com algumas aulas gratuitas no VueSchool](https://vueschool.io/courses/learn-how-to-test-vuejs-components?friend=vth)

[Documentação Oficial do Vue Test Utils](https://v1.test-utils.vuejs.org/)

[Definição de testes unitários na documentação Vue](https://br.vuejs.org/v2/guide/unit-testing.html)

[Testando eventos no DOM](https://v1.test-utils.vuejs.org/guides/#testing-key-mouse-and-other-dom-events)

[Mount](https://v1.test-utils.vuejs.org/api/#mount)

[ShallowMount](https://v1.test-utils.vuejs.org/api/#shallowmount)
