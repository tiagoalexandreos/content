---
title: Boas Práticas
description: Boas Práticas
accessibilty: true
---

## Performance

Com relação a performance dos recursos do Web Components relacionadas a importação de HTML e Shadow DOM, o que temos na verdade são verdadeiras incógnitas.

O que sabemos atualmente é que quando importamos documentos HTML geramos novas requisições ao servidor. Além das próprias requisições do documento importado, o mesmo pode conter outras importações e requisições de imagens, folhas de estilo ou arquivos de scripts caindo em um verdadeiro request hell.

> Para resolver o problema do request hell podemos utilizar o plugin [vulcanize](https://www.npmjs.com/package/vulcanize). O vulcanize deve ser usado com a tecnologia nativa de construção de Web Components e serve para concatenar as múltiplas importações de arquivos dos Web Components para um único arquivo. E assim ele evita que varias requisições sejam feitas para carregar os Web Components dentro do seu projeto. O que fará com que o vulcanize não seja mais necessário, será a implementação do HTTP 2.0, que trará uma série de melhorias. Dentre elas podemos destacar a paralelização das requisições com multiplexação, então poderemos realizar várias requisições ao mesmo tempo e receber suas respectivas respostas quando estiverem prontas, de forma paralela e assíncrona.

O isolamento do Shadow DOM é realmente ótimo para evitar que estilos indesejados alterem as características de seus componentes, mas pode se tornar um verdadeiro incômodo quando queremos reaproveitar estilos comuns para nossos componentes nos levando à utilização de estilos incorporados.

A utilização de regras CSS incorporadas diretamente na página é algo considerado ruim tanto para performance quanto para modularização, e vem sendo evitada a bastante tempo por desenvolvedores que têm apreço por desempenho e reutilização de código. Estilos declarados fora do escopo do Shadow DOM não conseguem acessar os elementos devido ao seu isolamento.

A solução é declarar regras de estilo direto no escopo do elemento isolado, ou seja, tag `<style> </style>` direto no elemento.

Novos seletores têm aparecido e nos permitirão estilizar o conteúdo isolado no Shadow DOM sem que tenhamos que declarar estilos incorporados toda vez que precisarmos estilizar nossos elementos. Os pseudo-elementos `::shadow` e o `/deep/combinator` nos permitem penetrar a barreira do Shadow DOM para que possamos acessar os elementos dentro da árvore do Shadow DOM.

Os seletores `::shadow` e `/deep/` são semelhantes, sendo que o segundo é bem mais poderoso. Ele ignora completamente as barreiras do Shadow DOM e permite acessar determinado elemento em qualquer nível da árvore que o mesmo se encontre. Abaixo um exemplo de customização do componente de vídeo usando o `deep`.

``` html
<style>
    video /deep/ input[type="range"] {
      background: #ff0;
    }
    video /deep/ div {
      color: #00ff00;
    }
</style>
<video src="http://techslides.com/demos/sample-videos/small.ogv" controls>
    Texto de fallback...
</video>
```

---

## Acessibilidade

Recomendamos fortemente a leitura do texto sobre [acessibilidade](/desenvolvimento/acessibilidade/), elaborado para nortear a criação de componentes acessíveis para o Design System.

Ao construir Web Components torna-se importante certificar-se de que os mesmos estarão acessíveis, especialmente por estarmos criando novos tipos de elementos. Verifique abaixo algumas preocupações quando estamos trabalhando com Web Components:

### ARIA e ROLE

> "Torne seu componente acessível usando funções ARIA apropriadas. Nos casos em que a herança de um elemento de base semântica é, por qualquer motivo, impossível ou inviável, certifique-se de adicionar um atributo role = "", se aplicável." [webcomponents.org](https://www.webcomponents.org/)

Ao personalizarmos os elementos às nossas necessidades reais, devemos adotar a prática de estender as funcionalidades de um elemento nativo de maneira que este web component herde as regras implícitas definidas pelos navegadores e tecnologias assistivas para esse elemento.

Por exemplo, não há necessidade de inclusão do atributo `role` nas tags HTML `<ul>` e `<li>`, uma vez que implicitamente a tag `<ul` possui `role=”list”`, e `<li>` possui `role=”listitem”`.

Para isso, observe a [lista de interfaces HTML](http://html.spec.whatwg.org/multipage/indices.html#elemhasent-interfaces). É possível, por exemplo, estender o comportamento nativo do `button` com `HTMLButtonElement` e personalizá-lo ao adicionar nosso próprio comportamento: `class br-button extends HTMLButtonElement {`

Podemos também definir o que um elemento é com uma `role`, indicando o contexto de como o usuário poderá iteragir com o elemento. Veja alguns links interessantes sobre ARIA:

- [definições e regras de ARIA](https://www.w3.org/TR/wai-aria-1.2/#role_definitions)
- [estados e propriedades ARIA](https://www.w3.org/TR/wai-aria-1.0/states_and_properties)

Essas semânticas adicionais ajudam as tecnologias assistivas a identificar propriedades, relacionamentos e estados em suas interfaces com o usuário.

## Shadow DOM e Atributo ID

 É possível que o desenvolvedor se defronte com a necessidade do usar o atributo ID, que estabelece uma conexão entre dois elementos HTML.

[No artigo sobre acessibilidade da Salesforce](https://developer.salesforce.com/blogs/2020/01/accessibility-for-web-components.html), encontra-se a indicação de como utilizar ou não o atributo ID com Shadow DOM.

### Quando você não pode

Se houver atributos que requerem IDs, eles precisam estar dentro do mesmo limite de sombra. Por exemplo, para a entrada personalizada abaixo, deve-se passar o atributo de label para dentro do componente em vez de deixá-lo do lado de fora porque o DOM do web component é separado:

✔️ Fazer:

``` html
<custom-input label="City">
  #shadow-root (open)
    <label for="my-input">City</label>
    <input id="my-input" type="text" />
</custom-input>
```

❌ Não Fazer:

``` html
<label for="my-input">City</label>
<custom-input>
  #shadow-root (open)
    <input id="my-input" type="text" />
</custom-input>
```

Não funcionará porque, quando é renderizado, a entrada está em uma árvore DOM separada do label, o que significa que o navegador não pode mais vinculá-los.

### Quando você pode

Se o componente tem um slot que está dentro do limite da sombra, é possível fazer referência a IDs dentro do slot. Isso ocorre porque o conteúdo dos slots está, na verdade, no light DOM, não no shadow DOM do componente.

Por exemplo, no componente abaixo, o elemento de entrada vai para um slot e deixo o label fora do componente.

``` html
<label for="another-input">Country</label>
<form-element-container>
  <input id="another-input" type="text" slot="input-slot"/>
</form-element-container>
```

O navegador exibirá o DOM assim:

``` html
<label for="another-input">Country</label>
<form-element-container>
  #shadow-root (open)
    <slot name="input-slot">
      #input
    </slot>
  <input id="another-input" type="text" />
</form-element-container>
```

A principal desvantagem dos slots é que, uma vez que estão no light DOM, todos os estilos e JavaScript na página afetarão o conteúdo dos slots, portanto, você não pode esperar que tenham a mesma aparência ou se comportem da mesma forma em todos os lugares.

---

## Testes

Quando estamos falando de testes automatizados em Web Components, imaginamos que iremos encarar um grande desafio por conta do Shadow DOM. Porém, existem atualmente boas ferramentas que suportam Web Components e Shadow DOM.

São abordados abaixo alguns tipos de testes que poderão ser feitos nos Web Components criados para o Design System:

> Lembre-se: A execução de um tipo de teste nunca substitui a realização do outro tipo de teste!

### Testes Ponta a Ponta (E2E testing)

O teste ponta a ponta é uma metodologia usada para testar se o fluxo de um aplicativo está funcionando conforme o que foi projetado, do início ao fim.

Para a realização dos testes E2E são construídos cenários, que simulam ações de usuários e validam todo tipo de comportamento possível dentro da aplicação. Vários cenários reunidos constituem uma suíte de testes.

Neste tipo de teste, a interação é diretamente com a interface da aplicação, o que naturalmente leva a escrita de muito código assíncrono. A página web também deverá estar acessível para o browser durante a execução dos testes.

Algumas preocupações a serem tomadas ao realizar testes ponta a ponta de Web Components:

- Aguardar o elemento ficar disponível no DOM para que conteúdo seja renderizado no corpo da página.
- Acessar o Shadow Root para realizar as asserções. Para garantir que o elemento se comporta/parece o mesmo, avaliar se sua estrutura DOM permanece a mesma. Uma asserção de teste pode incluir pré-requisitos que devem ser verdadeiros para que a asserção de teste seja válida.
- Testar as interações com o web component com a finalidade de avaliar se os eventos acionados conduzem com a renderização esperada.
- Testar eventos customizados/personalizados: são uma funcionalidade central e usados na comunicação entre os Web Components.

### Testes Funcionais

Compreendem o processo de garantia de qualidade de um produto, testando funções e funcionalidades do componente ou sistema em geral de acordo com os requisitos de especificação.

Vários parâmetros de teste:

- interface do usuário
- APIs
- teste de segurança
- teste de cliente e servidor
- funcionalidades básicas do site

O teste funcional permite que os usuários executem testes manuais e automatizados. É realizado para testar as funcionalidades de cada recurso do site.

### Testes Unitários

Consistem em técnicas de teste geralmente realizadas pelo próprio desenvolvedor, onde cada unidade do sistema será isolada para análise e identificação de problemas, cujos defeitos deverão ser corrigidos.

Vantagens deste tipo de teste:

- redução de defeitos nos recursos desenvolvidos ou alterados recentemente;

- reduz o custo do teste, uma vez que os defeitos são capturados na fase inicial;

- melhor refatoração do código;

Não há necessidade de conhecimento de sistemas interconectados.

### Testes de Regressão Visual Automatizados

O propósito dos testes de regressão consiste em verificar se mudanças no código não geraram impactos indesejados. Quando se fala sobre testes de regressão visual a preocupação consiste em evitar alterações não intencionais no visual da aplicação ou componentes.

Por exemplo, após realização de manutenção do componente Button, como ter certeza se os componentes que dependem do Button estão sendo impactados? A estratégia mais comum e prática consiste em capturar imagens (screenshots) antes e depois da modificação utilizando alguma ferramenta que automatiza esse processo e aponta os impactos visuais.

É possível realizar a configuração dos testes visuais para execução em navegadores e diferentes dispositivos.

## Lints e Hints

Lints e Hints se referem a ferramentas que analisam código-fonte para acusar erros de programação, bugs, erros estilísticos, e construções suspeitas. Na construção dos Web Components do GOVBR-DS estamos utilizando a especificação criada pela equipe do GOVBR-DS. Abaixo os links para a leitura

> [Documentação Lint GOVBR-DS](/desenvolvimento/lints)

## Versionamento

Os sistemas de gerenciamento de código-fonte permitem monitorar as alterações no código, obter uma revisão do histórico do código e reverter versões anteriores de um projeto quando for necessário. Temos toda uma documentação criada para tratar estes itens.

> Não deixe de ler a [Documentação oficial de Versionamento](/git-gitlab/).

## Links de referência

<https://www.webcomponents.org/community/articles/web-components-best-practices>

<https://developers.google.com/web/fundamentals/web-components/best-practices>

<https://project-awesome.org/mateusortiz/webcomponents-the-right-way>
