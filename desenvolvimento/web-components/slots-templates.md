---
title: Slots e Templates
description: Explicação sobre slots e templates
---

## Templates

O `<template>` é um mecanismo para conteúdo HTML que não deve ser renderizado imediatamente quando uma página é carregada, mas pode ser instanciado posteriormente durante o tempo de execução usando JavaScript.

Quando você precisa reutilizar as mesmas estruturas de marcação repetidamente em uma página da web, faz sentido usar algum tipo de modelo em vez de repetir a mesma estrutura indefinidamente.

Pense em um modelo como um fragmento de conteúdo que está sendo armazenado para uso posterior no documento.

Abaixo um exemplo:

``` html
<template id="my-paragraph">
  <p>My paragraph</p>
</template>
```

Isso não aparecerá em sua página até que você pegue uma referência a ele com JavaScript e, em seguida, anexe-o ao DOM, usando algo como o seguinte:

``` javascript
let template = document.getElementById('my-paragraph');
let templateContent = template.content;
document.body.appendChild(templateContent);
```

## Slots

O elemento HTML `<slot>` parte do pacote de tecnologia de Web Components - é um espaço reservado dentro de um componente da Web que você pode preencher com seu texto ou html, o que permite criar árvores DOM separadas e apresentá-las juntas.

Os slots são identificados por seu atributo `name` e permitem definir e marcar a posição em seu html. O conteúdo interno da posição do slot pode ser preenchido com html ou apenas texto.

Portanto, se quisermos adicionar um slot em nosso nosso componente seria assim:

``` html
<p>
  <slot name="my-text">My default text</slot>
</p>
```

Se o conteúdo do slot não for definido quando o elemento for incluído no html, ou se o navegador não suportar slots, apenas conterá o conteúdo "Meu texto padrão".
