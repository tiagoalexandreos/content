---
title: " Web Components"
chapter: true
weight: 1
pre: '<i class="fas fa-boxes"></i> '
---

Essa seção é dedicada as documentações de Webcomponents no **GOVBR-DS**.

Você pode encontrar os repositórios dessa biblioteca no [nosso grupo do gitlab](https://gitlab.com/govbr-ds/dev/wbc "GOV.BR-DS - Web Components").
