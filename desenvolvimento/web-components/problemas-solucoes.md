---
title: 'Problemas e Soluções'
description: 'Relação de alguns problemas já enfrentados pelo time'
---

Nessa seção colocamos alguns problemas enfrentados e como o time conseguiu superar cada um deles.

## Comunicação na árvore de componentes

Quando necessário, utilizar `provide` e `inject` para realizar a comunicação entre componente pai e seus filhos.

![Infográfico Provide e Inject](/imagens/provide-inject.png)

Esta comunicação funciona não apenas quando a relação pai/filhos é feita com composição explícita de componentes, mas também quando estes filhos são slots. Um exemplo pode ser visto na comunicação entre o [br-list](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/-/blob/main/src/components/List/List.ce.vuee) e o [br-item](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/-/blob/main/src/components/Item/Item.ce.vue), onde o br-item precisa saber propriedades do br-list (no caso, `data-toggle` e `data-unique`) para condicionar seu template.

Referência: <https://v3.vuejs.org/guide/component-provide-inject.html>

## Variáveis carregadas com data() que são derivadas de props não aparecem no Storybook

Utilizar propriedades computadas (`computed`) pois elas aguardam a atualização da prop antes de manipulá-la, evitando valores nulos inesperados. A diferença entre derivar uma prop com `computed` e com `method` é que com `computed` existe um cache onde a lógica não é executada novamente para um mesmo valor da prop.

Um exemplo de refatoração onde foi atendida esta necessidade ocorreu no [br-menu](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/-/blob/main/src/components/Menu/Menu.ce.vue), onde a lógica que faz o parse de um string para um json com os dos dados do menu, em que uma prop `list` (String) é convertida em uma variável `menu` (JSON Object), foi transferida de uma variável `data()` para uma `computed`. Após essa refatoração, o menu passou a ser exibido no Storybook.

Este problema, porém, não traz reflexos apenas para o Storybook. Trata-se, também, de um indicativo de que o web component não funcionaria como esperado em uma aplicação Vue e, possivelmente, em outras bibliotecas de front-end como React, Next, Angular, Svelte etc.

Referência: <https://v3.vuejs.org/guide/computed.html>

## Renderização condicional de slots

A necessidade de validar se determinado slot estava sendo usado ocorreu para que pudessem ser feitas condições nos templates dos componentes de acordo com o uso do slot.

O br-header, por exemplo, possui esse trecho de código (adaptado para o exemplo) em seu template:

```vue
<div class="header-menu">
  <div class="header-menu-trigger">
    <slot name="headerMenu" />
  </div>
  <div class="header-info">
    <div v-if="title" class="header-title">
      {{ title }}
    </div>
    <div v-if="subtitle && showSubtitle" class="header-subtitle">
      {{ subtitle }}
    </div>
  </div>
</div>
```

Vemos que só faria sentido renderizar a `<div class="header-menu-trigger">`, ascendente do `<slot name="headerMenu" />`, se o slot estivesse preenchido. Exemplo do slot sendo preenchido: `<br-header><br-menu slot="headerMenu">...</br-menu></br-header>`

No projeto com Vue 2 e vue-custom-element, sem uso de shadow dom, era possível utilizar a propriedade `$slots` da instância do componente para prever se um slot estava sendo usado. Logo, era possível fazer `<div class="header-menu-trigger" v-if="$slots.headerMenu">` para saber se `headerMenu` estava sendo usado e condicionar a renderização da div.

No projeto com Vue 3, em que utilizamos o shadow-dom (conforme recomendado pela especificação), o mesmo procedimento não é possível pois a propriedade `$slots` não é preenchida. Pelo que pude ler em alguns materiais, principalmente [aqui](https://github.com/WICG/webcomponents/issues?q=is%3Aissue+is%3Aopen+slot), isto se trata de algo da especificação dos web componentes (**a qual não seguíamos antes ao não usar shadow-dom**): apenas os filhos conseguem saber quem é seu pai antes de renderizar. O pai não tem como, antes de renderizar, saber previamente que um filho está inserido no slot.

Uma alternativa para se fazer renderizações condicionais de slots seria adicionar props que sirvam para tais condições, conforme já está sendo feito no [br-header](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/-/blob/main/src/components/Header/Header.ce.vue). O código acima ficaria assim, utilizando uma prop `hasMenu`:

```vue
<div class="header-menu">
  <div v-if="hasMenu" class="header-menu-trigger">
    <slot name="headerMenu" />
  </div>
  <div class="header-info">
    <div v-if="title" class="header-title">
      {{ title }}
    </div>
    <div v-if="subtitle && showSubtitle" class="header-subtitle">
      {{ subtitle }}
    </div>
  </div>
</div>
```

O uso seria: `<br-header has-menu><br-menu slot="headerMenu">...</br-menu></br-header>`

Uma outra alternativa seria tentar implementar de uma forma que o próprio componente que virá por slot tenha tudo o que ele precisa pra se encaixar no estilo do componente pai.
