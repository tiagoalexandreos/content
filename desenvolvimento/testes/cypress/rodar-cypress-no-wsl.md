---
title: 4. Como rodar o Cypress dentro do WSl
description: Guia sobre como rodar o Cypress dentro do WSl
date: 14/04/2022
keywords: cypress, guia, wsl
---


1. Instalar o [vcxrv](https://sourceforge.net/projects/vcxsrv/ "vcxrv")

2. Executar os seguintes comandos no ubuntu:

    `sudo apt update`

    `sudo apt upgrade`

    `sudo apt install libgtk-3-dev libnotify-dev libgconf-2-4 libnss3 libxss1 libasound2`

3. Definir o ip como display:

    `export DISPLAY=$(cat /etc/resolv.conf | grep nameserver | awk '{print $2; exit;}'):0.0`

4. Iniciar o serviço:

    `sudo /etc/init.d/dbus start`

5. Testar se ele vai abrir um app execute o comando:

   `xeyes`

6. Executar o cypher
    `npm run test`

> Essa documentação é baseada em <https://nickymeuleman.netlify.app/blog/gui-on-wsl2-cypress>
