---
title: Testes Frontend com Cypress e Lambdatest
description: OD24.2.1 - Criar um documento de referência indicando os tipos de testes e as boas práticas relacionadas ao assunto
date: 12/01/2023
keywords: testes, frontend, cypress, lambdatest
contact: govbr-ds@serpro.gov.br
---

## Testes de Regressão Visual  

### Automatizado

Nesta seção vamos abordar como realizar testes de regressão visual utilizando a funcionalidade SmartUi na ferramenta Lambdatest em conjunto com a ferramenta Cypress. A regressão visual consiste em armazenar screenshots de referência (baseline) e comparar a diferença com as screenshots em determinada linha do tempo.

Os testes são escritos em Cypress, mas executados na Lambdatest por meio do comando `lambdatest-cypress run` no terminal do linux. A Lambdatest compactará os arquivos de testes e os enviará para nuvem da ferramenta, onde serão executados e seus resultados acessados no site <https://smartui.lambdatest.com/>.  

Exemplos de testes escritos em cypress podem ser vistos no repositório [Frontend Testing](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-frontend-testing/-/tree/main/cypress/integration).

#### 1. Preparando o ambiente (para projetos iniciais)

{{% notice info %}}
Não realizar os comandos deste tópico caso esteja utilizando um projeto que contém as configurações iniciais já definidas (Cypress e Lambdatest). Caso esteja utilizando um projeto já configurado, apenas utilizar o comando `npm install`
{{% /notice %}}

##### Instalando o Cypress via NPM usando o terminal do linux

Usando o npm (gerenciador de pacotes Node) navegue até o diretório do projeto que será criado e execute o seguinte comando (**pré-requisito para versões em linux (Ubuntu/Debian)**):

`apt install libgtk2.0-0 libgtk-3-0 libgbm-dev libnotify-dev libgconf-2-4 libnss3 libxss1 libasound2 libxtst6 xauth xvfb`

O comando abaixo cria o arquivo package.json com informações iniciais para instalar o cypress.

`npm init`

Instalar a versão 9.6.0 do cypress conforme o comando:

`npm install cypress@9.6.0 --save-dev`

##### Instalando e configurando o Lambdatest

Utilizar o comando para instalar a dependência do Lambdatest (pacote LambdaTest-Cypress CLI) para ser usado em testes automatizados juntamente com o cypress(Test Runner):  

`npm install –g lambdatest-cypress-cli`

Inicializar as configurações necessárias que criará o arquivo lambdatest-config.json:

`lambdatest-cypress init`

O arquivo lambdaTest-config.json possui algumas configurações específicas, tais como:

| Opção | Descrição |
| ------ | ----------- |
| username  | nome do usuário para login no site do lambdatest podendo ser utilizado variáveis de ambiente |
| access_key | chave fornecida pela [Lambdatest](https://accounts.lambdatest.com/detail/profile), pode ser usada variável de ambiente para esconder a senha |
| build_name | nome que o usuário dará ao seu build |
| specs | caminho do diretório onde estão os arquivos de testes a serem executados |
| smart_ui | nome do projeto, necessariamente o mesmo nome criado no SmartUi da Lambdatest (necessário para a ligação dos nomes e realização dos testes de forma automática junto com o cypress |

As outras configurações destes arquivos são mais intuitivas, como nome do browser a ser testado, versão, etc.

LambdaTest-config.json

``` json
{
  "lambdatest_auth": {
    "username": "<Your_lambdatest_username>",
    "access_key": "<Your LambdaTest access key>"
  },
  "browsers": [
    {
      "browser": "Chrome",
      "platform": "Windows 10",
      "versions": ["86.0"]
    },
    {
      "browser": "Firefox",
      "platform": "Windows 10",
      "versions": ["82.0"]
    }
  ],
  "run_settings": {
    "cypress_config_file": "cypress.json",
    "build_name": "build-name",
    "parallels": 1,
    "specs": "./*.spec.js", //caminho do diretório dos arquivos de testes
    "ignore_files": "",
    "npm_dependencies": {"cypress": "9.6.0" },
    "smart_ui": {
        "project": "<nome do projeto criado no teste automático>"
    }
    "feature_file_suppport": false 
  },
  "tunnel_settings": {
    "tunnel": false,
    "tunnelName": null
  }
}
```

#### 2. Executando os Testes

Os testes automatizados da LambdaTest com Cypress serão executados no(s) arquivo(s) de testes especificado(s) na chave “specs” dentro do arquivo lambdatest-config.json através do comando:

`lambdatest-cypress run`

Este comando verificará se não existem erros de sintaxe nos arquivos e, se estiver tudo certo, enviará uma cópia zipada para o teste ser executado na nuvem da ferramenta LambdaTest.

> Referência para criação de testes no Cypress:
>
> [Cypress - Introdução](/desenvolvimento/testes/cypress/1.introducao/)

Em seguida, o usuário deverá:

1. Logar na ferramenta [Lambdatest](https://accounts.lambdatest.com/login)
2. Acessar o menu localizado no lado esquerdo, opção Smart UI, onde serão visualizados os resultados dos projetos de testes automatizados (usando cypress e lambdatest)
3. Escolher um dos projetos para ter acesso as builds do projeto desejado.  

A página seguinte será apresentada para escolha da build a ser comparada:
![Lambdatest SmartUi](./imagens/1SmartUi.png?classes=border,shadow)

A primeira build enviada será sempre a build de baseline, a partir da qual as outras builds farão as comparações das imagens. Depois de enviar mais de uma build, pode-se escolher outra como baseline.

Neste exemplo em questão, os componentes de ambiente de produção (baseline) são comparados com os componentes correspondentes em ambiente de homologação, com a finalidade de verificar os resultados dos testes de regressão visual.

Ao selecionar a build 2 será mostrada a seguinte tela contendo as imagens dos dois ambientes (produção e homologação):

![Comparação Produção e Homologação](./imagens/2ComparaProdHom.png?classes=border,shadow)

Se houver divergências nas imagens, essas diferenças serão visualizadas no item **mismatch** que indicará a porcentagem de diferença entre as imagens.

![Mismatch de comparação de imagens](./imagens/3Mismatch.png?classes=border,shadow)

Quando as imagens não tem diferenças a tela será desta forma:

![Comparação sem diferenças entre imagens](./imagens/4SemDiferencas.png?classes=border,shadow)

### Manual

Existe também a possibilidade de executar um teste de regressão visual de forma manual, através da opção **More Tools --> UI Comparations**.

Nesta funcionalidade o usuário deve fazer o upload de imagens (lado esquerdo da tela no botão sinal de **+**) para se tornarem imagens de baseline. Em seguida realiza o upload de imagens para comparação (lado direito da tela, botão **Upload Comparison Image**).
A ferramenta mostrará as diferenças existentes, bem como as imagens que permanecerem idênticas.

![Regressão visual manual](./imagens/5RegressaoManual.png?classes=border,shadow)

## Testes E2E

O teste E2E (end-to-end) é uma metodologia utilizada para testar o fluxo de um processo, avaliando se o mesmo está sendo executado conforme esperado, do início ao fim. Avaliar todo o processo de ponta a ponta envolve muito tempo e esforço e pode estar sujeito a erros quando feito manualmente. Portanto, automatizar o processo é a solução.

Os cenários dos fluxos devem ser criados e executados no Cypress, mas pode ser utilizado em conjunto com testes de regressão visual na Lambdatest.

## Testes Cross Browser

### Dispositivos Reais

Cross browser testing consiste em executar testes em diferentes browsers (navegadores), versões, dispositivos e sistemas operacionais. Este tipo de teste na ferramenta LambdaTest será realizado manualmente na opção Real Device:

![CrossBrowser Testing em dispositivos reais](./imagens/6CrossBrowser.png?classes=border,shadow)

Nesta tela o usuário insere o endereço do site (url) desejado para o teste, escolhe o sistema operacional (Android e IOS), depois a marca, o modelo e o browser. O botão **Start** deve ser pressionado para iniciar o teste. O resultado será apresentado conforme imagem abaixo:

![CrossBrowser Testing em dispositivos reais em execução](./imagens/6CrossBrowserStart.png?classes=border,shadow)

Na tela de resultado existe um menu lateral, onde se encontram funções como: marcar um erro, gravar vídeo, fazer uma captura de tela, modificar os requisitos de testes, entre outras.

> Quando o sistema operacional escolhido for o IOS, a inserção da URL, será feito após clicar no botão Start, inserindo diretamente no browser do dispositivo. Esta é ainda uma limitação da ferramenta LambdaTest.

### Dispositivos Emulados - Real Time Testing

A ferramenta LambdaTest possui também a opção de real time testing, que são testes de crossbrowser em dispositivos simulados (desktops e celulares). O usuário poderá escolher o dispositivo, o browser, a versão do browser, o sistema operacional, a resolução da tela e se necessário alguma extensão do browser. Após escolher os itens desejados, inserir a url para teste e clicar no botão Start para iniciar os testes.

![CrossBrowser Testing em dispositivos emulados](./imagens/8CrossBrowserEmulados.png?classes=border,shadow)

#### Como realizar **testes locais** em dispositivos - tempo real

Para realizar testes locais em dispositivos reais na Lambdatest, é necessário configurar o Tunnel seguindo os passos propostos pela ferramenta:

![Lambdatest - Configurando o Tunnel](./imagens/22TestesLocais.png?classes=border,shadow)

Desta forma, será preciso:

1. Clicar no ‘Configure Tunnel’, disponível no canto superior direito.
1. Fazer o download do arquivo, extraindo-o em uma pasta de sua preferência: <https://downloads.lambdatest.com/tunnel/v3/linux/64bit/LT_Linux.zip>
1. Copiar o comando disponibilizado pela ferramenta, conforme imagem acima.
1. Abrir um terminal e executar o comando copiado.
1. Na ferramenta Lambdatest, selecionar a opção ‘Real Time Testing -> Browser Testing’.
1. Inserir o endereço local, lembrando sempre de trocar o domínio de ‘127.0.0.1’ por ‘localhost’:

![Lambdatest - Selecionar Real Time Testing e inserir localhost](./imagens/22TestesLocaisURL.png?classes=border,shadow)

A ferramenta se encarregará de fazer o tunelamento, apresentando o servidor local no dispositivo real escolhido:

![Lambdatest - Tunelamento em andamento para testes locais](./imagens/24TestesLocaisStart.png?classes=border,shadow)

## Dashboard

Na funcionalidade Dashboard (Painel de Controle), temos uma visão geral dos resultados dos testes, com opção de Logs das Sessões Realtime, Teste de Automação e Dispositivos reais.  São apresentados os testes mais recentes e problemas relatados (issues).

É exibido um gráfico mostrando a quantidade de testes executados por dia, em uma semana ou dentro de um mês, com a possibilidade de filtragem por usuário e tipo de teste a ser visualizado.

![Lambdatest - Dashboard](./imagens/9Dashboard.png?classes=border,shadow)

## Analitics

Nesta funcionalidade pode-se criar dashboards para que tenhamos a opção de inserir resultados de testes Snapshot, teste de OS (sistema operacional), teste de browser, erros e visão geral dos testes. Os resultados são visualizados em forma de gráficos analíticos.

![Lambdatest - analitics](./imagens/10Analitics.png?classes=border,shadow)

A seguinte tela será disponibilizada, com todos os gráficos escolhidos durante o processo de criação do dashboard:

![Lambdatest - Analitics - gráficos 1](./imagens/11Analitics.png?classes=border,shadow)

![Lambdatest - Analitics - gráficos 2](./imagens/12Analitics.png?classes=border,shadow)

![Lambdatest - Analitics - gráficos 3](./imagens/13Analitics.png?classes=border,shadow)

![Lambdatest - Analitics - gráficos 4](./imagens/14Analitics.png?classes=border,shadow)

![Lambdatest - Analitics - gráficos 5](./imagens/15Analitics.png?classes=border,shadow)

## Manage

Funcionalidade que contempla as opções de gerenciamento de bugs (issues), test logs e criação de projetos.

![Lambdatest - Manage - Bug Tracker](./imagens/16ManageBugTracker.png?classes=border,shadow)

![Lambdatest - Manage - Test Logs](./imagens/17ManageTestLogs.png.png?classes=border,shadow)

![Lambdatest - Manage - Projects](./imagens/18ManageProjects.png.png?classes=border,shadow)

## Integrations

A ferramenta LambdaTest permite integrações com outras ferramentas de testes, bug tracker, gerenciamento de projetos, análises, comunicação, ci/cd, codeles automations, entre outros.

![Lambdatest - Integrações - imagem 1](./imagens/19Integrations.png?classes=border,shadow)

![Lambdatest - Integrações - imagem 2](./imagens/20Integrations.png?classes=border,shadow)

## LT-Browser

É uma ferramenta gratuita que pode ser baixada no seguinte endereço <https://downloads.lambdatest.com/lt-browser/LTBrowser.AppImage>.

Pode ser usada para fazer testes manuais de responsividade e E2E. É possível tirar screenshots de tela, gravar vídeos e fazer análise de performance com o Lighthouse.

Não precisa de instalação, apenas baixar e abrir o arquivo, que ele abrirá a tela do navegador LT-Browser.

![Lambdatest - Integrações - imagem 2](./imagens/21LTBrowser.png?classes=border,shadow)

## Referências

- Testes Automatizados com Cypress e Lambdatest:

<https://www.lambdatest.com/support/docs/getting-started-with-cypress-testing/>

<https://docs.cypress.io/guides/overview/why-cypress>

- Testes de Regressão Visual:
  
<https://www.lambdatest.com/support/docs/smart-ui-cypress/>

- Real Time Testing:

<https://www.lambdatest.com/support/docs/real-time-browser-testing/>
