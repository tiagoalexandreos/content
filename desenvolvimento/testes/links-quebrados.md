---
title: Links Quebrados
description: Ações para Implementar Quando Encontrar Links Quebrados
date: 03/11/2021
keywords: links, link, quebrados, ações, âncoras, markdown, mudança, referência, sempre, caminho, migração, arquitetura
---

## Introdução

Links quebrados as vezes podem surgir devido a uma migração de site, mudança de arquitetura entre outros problemas.

Está wiki tem a finalidade de ajudar os participantes do Design System a executarem ações corretivas no caso de encontrarem links quebrados no site do Design System.

## Identificando links quebrados

O ponto mais importante antes de partir para a correção dos links é detectar quais links estão quebrados.

Recomendamos que depois de uma migração ou mudança de arquitetura os designers ou desenvolvedores do projeto executem ações para identificar os links quebrados.

Para isso existem duas maneiras:

1. Executar uma varredura no site utilizando uma `ferramenta que identifique os links quebrados`
2. Executar `manualmente a navegação no site` com a finalidade de encontrar links quebrados

## Boas práticas na definição de links e âncoras no markdown

Antes de corrigir os links quebrados é importante saber como definir links e âncoras no markdown.

### Links

Existem duas formas de inserir link em Markdown, através de um **link direto** ou usando um **texto-âncora**:

**Texto-âncora**: utilize os caracteres `[]()`, adicionando entre chaves o texto que você quer que apareça, e entre os parênteses, o endereço de destino, no formato `[exemplo](https://exemplo.com/)`.

**Link direto**: envolva o endereço da web em chaves `<>`. O endereço ficará visível e será clicável pelo usuário. O endereço em forma de link direto tem o formato `<https://exemplo.com/>`.

### Âncora

No Markdown padrão, coloque uma âncora na `<a name="abcd"></a>` ao qual deseja vincular e faça referência a ela na mesma página por `[link text](#abcd)`.

> Lembrando que se houverem duas âncoras com o mesmo nome o link quebra também. Então sempre verifique se nao existem nomes de âncoras duplicados.
>
> Ancora na mesma pagina deve colocar o a url relativa como exemplo \[a link\](/introducao/como-comecar#requisitos)

## Corrigindo os links e âncoras quebrados no markdown do site

Após identificar os links quebrados no item [Identificando links quebrados](#identificando-links-quebrados) , devemos tomar as seguintes ações para corrigir dentre elas:

1. **`Url externa incorreta`** - verificar se a url que você informou não está incorreta. Antes de inserir um novo link externo sempre visitar o mesmo para verificar se ele de fato funciona.
2. **`url interna incorreta`** - ao trabalhar com links a partir de arquivos do próprio projeto, prefira sempre usar caminho relativo nos links ao invés de caminhos absolutos , que são links com o nome do site como exemplo [`http://gitlab.com`](http://gitlab.com).

   Prática ruim (caminho absoluto):

   `[a link](`<https://gitlab.com/user/repo/blob/branch/other_file.md>`)`

   Boa prática (caminho relativo):

   `[a relative link](other_file.md)`

   ou

   `[a relative link](`user/repo/blob/branch/other_file.md`)`
3. **`links incorretos/removidos`** - quando a página contiver um link interno ou externo que não esteja navegável. Porque o link está fora do ar ou o dono do outro domínio removeu o link, então o arquivo do link não existe mais. As ações as serem tomadas são as seguintes:
4. Encontrar outra referência para o link e substituir
5. Remover o link e a referência do site quando não houver a possibilidade de substituir
6. **`links com erro de certificado`** - no caso de encontrar links que apresentam erros de certificado. Podemos realizar duas ações pra resolver dentre elas:
7. Mudar de http para https o endereço do site
8. Remover os itens que de fato não puderem ser corrigidos com a mudança sugerida pelo item 1
