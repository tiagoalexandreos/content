---
title: 4. Testando Template
description: Guia para geração/manutenção de cenários de testes de componentes
date: 14/04/2022
keywords: cenário de teste, backstopjs, template
---

## Template

Template consiste na composição, contextualização e funcionamento de células e organismos em uma estrutura de layout.

## Pré-requisitos

1. Ao trabalhar remotamente, você deve estar conectado na OpenVPN Serpro para realizar a correta captura das telas (referência e testes).

2. O projeto *govbr-ds-dev-frontend-testing* deve estar devidamente clonado em sua máquina.

3. O Backstopjs deve estar instalado. Caso não esteja, entrar na pasta *backstop_data* e inserir via terminal o seguinte comando:

```terminal
\$ npm install backstopjs --save
```

**É muito importante a leitura prévia do documento '1. Introducao', que contém informações gerais sobre como realizar testes de regressão visual para o Design System no Backstopjs.**

## Criação cenários de teste para Template

1. Criar o arquivo de teste **`<nome-do-template>.js`** para o template na pasta **ovbr-ds-dev-frontend-testing/backstop_data/tests/template**. .

1. No arquivo gerado, inserir o código:

   ```javascript
   const templateUrl = 'dist/components/<nome-do-template>/examples.html'

   module.exports = options => {
     return {
       scenarios: [
         {
           label: '<nome-do-template / ação-realizada>',
           url: `${options.baseUrl}/${templateUrl}`
         }
       ]
     }
   }
   ```

1. Ajustar o `<nome-do-componente>` da constante `componentUrl`.

1. Ajustar o `label`, inserindo o componente que está presente neste cenário e as ações de usuário (ex: hover, click) que estão sendo testadas. Para saber mais sobre a anatomia de um cenário, verificar o documento **'Anatomia de um Cenário'**.

1. Inserir novos cenários para cada componente presente no template. Lembrar de inserir no mínimo as propriedades obrigatórias em sua construção. **Recomenda-se que o primeiro cenário seja o template completo, onde nenhum seletor deverá ser acionado.**

1. Por fim, deve-se inserir o cenário gerado na pasta `backstop_data/tests/util/scenarios-template.js`, adicionando uma nova linha em `module.exports = (options) => {const <nome-do-template> = require("../template/<nome-do-template>")(options).scenarios;}` e também em `return {"scenarios": [...<nome-do-template>,]}`.

Exemplo:

  ```javascript
  const templateUrl = "templates/base.html";

  module.exports = options => {
      return {
          "scenarios": [{
                  "label": "Template",
                  "url": `${options.baseUrl}/${templateUrl}`,
                  "referenceUrl": `${options.baseUrl}/${templateUrl}`,
              },
              {
                  "label": "Template - header",
                  "url": `${options.baseUrl}/${templateUrl}`,
                  "referenceUrl": `${options.baseUrl}/components/header.html`,
                  "selectors": [".br-header>.container-lg"],
                  "misMatchThreshold": 0.002
              },
  ```
