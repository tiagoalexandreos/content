---
title: 3. Testando Componente
description: Guia para geração/manutenção de cenários de testes de componentes
date: 14/04/2022
keywords: cenário de teste, backstopjs, componente
---

## Pré-requisitos

1. Ao trabalhar remotamente, você deve estar conectado na OpenVPN Serpro para realizar a correta captura das telas (referência e testes).

2. O projeto *govbr-ds-dev-frontend-testing* deve estar devidamente clonado em sua máquina.

3. O Backstopjs deve estar instalado. Caso não esteja, entrar na pasta *backstop_data* e inserir via terminal o seguinte comando:

```terminal
npm install backstopjs --save
```

**É muito importante a leitura prévia do documento 'Introducao' ou 'Guia Rápido', que contém informações gerais sobre como realizar testes de regressão visual para o Design System no Backstopjs.**

## Criação cenários de teste para Componentes

1. Acessar a pasta `govbr-ds-dev-frontend-testing/backstop_data/tests/components` e verificar se o arquivo `<nome-do-componente>.js` referente ao componente desejado já existe. Criar o arquivo se o mesmo estiver ausente.
2. Para o arquivo existente ou recém criado, o código abaixo deve ser/estar inserido:

   ```javascript
   const componentUrl = 'dist/components/<nome-do-componente>.html'

   module.exports = options => {
     return {
       scenarios: [
         {
           label: '<nome-do-componente>',
           url: `${options.baseUrl}/${componentUrl}`
         }
       ]
     }
   }
   ```

3. Ajustar o `<nome-do-componente>` da constante `componentUrl` e da propriedade `label`. O cenário representado acima possui apenas os campos obrigatórios. Para saber mais sobre a anatomia de um cenário, verificar o documento **'Anatomia de um Cenário'**.

4. Caso seja necessário representar as ações de usuário (ex: hover, click), recomenda-se inserir novos cenários. Para isso, deve-se inserir no mínimo as propriedades obrigatórias em sua construção:

   ```javascript
   const componentUrl = 'dist/components/<nome-do-componente>/examples.html'

   module.exports = options => {
     return {
       scenarios: [
         {
           label: '<nome-do-componente>',
           url: `${options.baseUrl}/${componentUrl}`
         },
         {
           label: '<nome-do-componente> - <ação-a-ser-realizada>',
           url: `${options.baseUrl}/${componentUrl}`
         }
       ]
     }
   }
   ```

5. Verificar se os cenários do componente gerado/ajustado está inserido no arquivo `/govbr-ds-dev-frontend-testing/backstop_data/tests/util/scenarios.js`. Se não estiver, deve-se adicionar (de preferência em ordem alfabética) uma nova linha em `module.exports = (options) => {const <nome-do-componente> = require("../components/<nome-do-componente>")(options).scenarios;}` e também em `return {"scenarios": [...<nome-do-componente>,]}`.
6. Após realizar a criação dos cenários de testes, é hora de gerar as imagens de referência (se ainda não foram geradas) e realizar os testes. Para mais informações sobre este assunto, veja o documento **'1. Introducao'**
