---
title: 5. Testando All
description: Guia para geração/manutenção de cenários de testes de componentes
date: 14/04/2022
keywords: cenário de teste, backstopjs, template
---


## Testando All

### Pré-requisitos

1. Ao trabalhar remotamente, você deve estar conectado na OpenVPN Serpro para realizar a correta captura das telas (referência e testes).

2. O projeto *govbr-ds-dev-frontend-testing* deve estar devidamente clonado em sua máquina.

3. O Backstopjs deve estar instalado. Caso não esteja, entrar na pasta *backstop_data* e inserir via terminal o seguinte comando:

```terminal
\$ npm install backstopjs --save
```

**É muito importante a leitura prévia do documento 'Introducao', que contém informações gerais sobre como realizar testes de regressão visual para o Design System no Backstopjs.**

## Criação cenários de teste para ALL

- No arquivo da pasta **backstop_data/tests/all/all.js**, verifique se o conteúdo do designSystem (variável `designSystem`) desejado ou mesmo se o componente (variável `components`) está incluído na lista. Se estiver ausente, o mesmo deve ser inserido.

```javascript
    let  designSystem = [
    '/',
    'ds/introducao/como-comecar',
    'ds/introducao/principios',
    'ds/introducao/sobre',
    'ds/introducao/prototipando',
    ...
    'legislacao',
    'downloads'
];

let components = [   
    'ds/components/accordion',
    'ds/components/avatar',
    'ds/components/badge',
    'ds/components/breadcrumb',
    ...
    'ds/components/upload',
    'ds/components/wizard',
]
```

- Após realizar a criação dos cenários de testes, é hora de gerar as imagens de referência (se ainda não foram geradas) e realizar os testes. Para mais informações sobre este assunto, veja o documento **'1. Introducao'**
