---
title: Guia Rápido - BACKSTOPJS
description: Guia rápido para instalação, criação/manutenção de cenários e uso do BackstopJS
date: 14/04/2022
keywords: cenário, backstopjs, instalação
---

## Etapa 1: Instalar projeto

1. Clone o repositório em uma pasta

   ```terminal
   git clone git@gitlab.com:govbr-ds/dev/govbr-ds-dev-frontend-testing.git
   ```

2. Entre na pasta *govbr-ds-dev-frontend-testing*

   ```terminal
   cd govbr-ds-dev-frontend-testing
   ```

3. Execute o comando

   ```terminal
   npm install
   ```

## Etapa 2: Criar ou atualizar cenários

1. Verifique se os cenários do componente encontram-se disponíveis em: `govbr-ds-dev-frontend-testing/backstop_data/tests/components`. Se não estiverem (componente novo, será necessário criá-lo: `<nome-do-componente>.js`

2. Verifique se os cenários do componente estão mapeados no arquivo: `/govbr-ds-dev-frontend-testing/backstop_data/tests/util/scenarios.js`.

3. Dúvidas referentes a criação de cenários: consulte o documento **'Anatomia-Cenario.md'**.

## Etapa 3: Gerar bitmaps de teste

As imagens de referência estarão armazenadas no repositório e serão atualizadas conforme abaixo:

- `local` (localhost): pelo desenvolvedor sempre que houver alteração nos componentes ou houver inclusão de novos cenários.

- `dev` (ambiente de desenvolvimento): sempre que houverem grandes mudanças ou de acordo com a necessidade.

- `hom` (ambiente de homologação) e `prod` (ambiente de produção):  sempre que houver implantação nestes ambientes.

Desta maneira, o desenvolvedor neste momento não deve se preocupar com os ambientes `dev`, `hom` e `prod`. Ou seja, ele deve realizar apenas ajuste dos cenários do componente no ambiente local e promoção das imagens para referência (descrito na [Etapa 5: Aprovar mudanças](#etapa-5-aprovar-mudanças)). As novas telas de referência deverão ser revisadas durante o **Merge Request**.

Para gerar as imagens de referência e executar os testes, o seguinte comando deve ser executado no terminal pelo desenvolvedor depois de fazer alterações CSS quantas vezes forem necessárias:

```terminal
npm run local-test
```

Para criar capturas de tela de teste de *um componente específico* e compará-las com suas capturas de tela de referência, deve-se inserir o nome do componente ao final do comando:

```terminal
npm run local-test <nome-do-componente>
```

Os testes realizados com estes comandos serão realizados no ambiente local, com a captura das telas de teste e geração de nova pasta na pasta `local_test`. Além disso, a ferramenta Backstop realiza automaticamente a comparação com as tela de referência, disponibilizadas na pasta `local_reference`.

Um relatório visual será aberto automaticamente no navegador, mostrando o número de testes visuais aprovados e reprovados. Caso contrário, seguir para a [Etapa 5: Aprovar mudanças](#etapa-5-aprovar-mudanças).

## Etapa 4: Gerar relatório

Para que seja disponibilizado no navegador o último relatório de testes realizado, execute o comando:

```terminal
npm run local-report
```

Caso o relatório não seja aberto de forma automática no navegador, o relatório pode ser acessado em: `/govbr-ds-dev-frontend-testing/backstop_data/<dev/hom/prod>_html_report/index.html`

## Etapa 5: Aprovar mudanças

Ao executar o comando abaixo, todas as imagens (com alterações ou novas imagens) de seu lote de teste mais recente serão promovidas para sua coleção de referência. Os testes subsequentes serão comparados com seus arquivos de referência atualizados.

> **Observação:** Uma vez que TODO o lote de teste mais recente será promovido com este comando, lembre-se de deixar na pasta de testes gerada APENAS os arquivos que deverão ser promovidos.

```terminal
npm run local-approve
```

## Etapa 6: Comparação de imagens entre ambientes

Além dos testes de comparação que a ser realizado dentro do ambiente local, também existe a necessidade de realizar testes de regressão visual comparando diferentes ambientes. Desta forma será possível avaliar o impacto de mudanças tanto no componente alterado como nos demais componentes.

Para isso, o desenvolvedor poderá rodar o comando abaixo para gerar imagens de referência do ambiente local de todos os cenários dos componentes:

```terminal
npm run reference
```

Para comparar as imagens como referência do ambiente local com os demais ambientes, rodar os seguintes comandos, de acordo com a necessidade de cada ambiente:

```terminal
npm run dev-test-reference
npm run hom-test-reference
npm run prod-test-reference
```

Um relatório visual será aberto automaticamente no navegador, mostrando o número de testes visuais aprovados e reprovados. Caso não seja aberto automaticamente, execute o comando abaixo de acordo com o ambiente testado:

```terminal
npm run dev-report-reference
npm run hom-report-reference
npm run prod-report-reference
```

Por fim, para promover as imagens testadas para a pasta de referência, execute o comando:

> **Observação:** Uma vez que TODO o lote de teste mais recente será promovido com este comando, lembre-se de deixar na pasta de testes gerada APENAS os arquivos que deverão ser promovidos.

```terminal
\$ npm run dev-approve-reference
\$ npm run hom-approve-reference
\$ npm run prod-approve-reference
```
