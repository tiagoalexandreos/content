---
title: 1. Introdução
description: Introdução aos testes de regressão visual e uso do BackstopJS no Design System
date: 14/04/2022
keywords: regressão visual, backstopjs, design system
---

## Testes de Regressão Visual

Reutilizar CSS é uma abordagem comum e uma mudança no CSS pode causar muitas mudanças visuais na interface. Não testar essas mudanças pode incorrer em erros visuais que podem diminuir a qualidade da aplicação. É extremamente importante saber onde a mudança está afetando, e uma ferramenta de teste de regressão visual auxilia nos testes de interface durante o processo de evolução da aplicação.

O teste de regressão visual pode ser dividido em 3 partes principais:

- linha de base: a captura de tela gravada originalmente como referência
- Teste: a captura de tela atual. Sempre que executado, uma nova captura de tela é gerada e realizada a comparação com a imagem original. Se não houver diferenças entre as imagens, o teste retornará “passed”, mas, caso as imagens não coincidam, o teste retornará “failed” e uma nova imagem será gerada, mostrando a diferença entre as duas imagens (Dif).
- Dif: com a comparação da linha de base e o teste, uma diferença será indicada e falhará no teste. As alterações podem ser aprovadas para atualizar a captura de tela da linha de base.

Lembre-se que o teste de regressão visual não é perfeito! Dependendo da configuração e dos recursos da ferramenta, podem haver falsos positivos. Problemas podem ser causados, por exemplo, devido a conteúdo dinâmico, animação e até mesmo velocidade da página do site.

---

## Backstopjs

Ferramenta de teste de regressão visual, que renderiza o teste com o Chrome Headless via Puppeteer. São características do Backstopjs:

- relatório gerado no navegador com inspetor visual de diferenças
- configuração simples
- simulação das interações do usuário com scripts do Puppeteer
- poucos comandos para aprender
- integração Docker, para eliminar problemas de renderização multiplataforma
- pode ser utilizado o controle de revisão, tornando-o compartilhável entre os desenvolvedores

Problemas encontrados:

- Bugs (ex.: screenshots vem em branco em certas condições)
- apenas Chrome
- Flakiness (testes instáveis) - é aquele que passa e falha periodicamente, sem nenhuma alteração de código.

---

## Passos para uso do Backstopjs

> **Lembrar que:**
>
> - Se estiver trabalhando remotamente, você deve estar conectado na OpenVPN Serpro para realizar a correta captura das telas (referência e testes).
> - Se estiver trabalhando via WSL, consultar o documento **"Como Rodar Backstop no WSL"**.

## 1. Instalar backstopjs

Após clonar o projeto *govbr-ds-dev-frontend-testing*, entrar na pasta *backstop_data* e inserir via terminal o seguinte comando:

```terminal
npm install backstopjs --save
```

## 2. Criar cenários de teste

Existem quatro tipos de testes que podem ser realizados no Design System com a ferramenta Backstopjs:

1. *Reference*: testes gerados com a finalidade única de comparar diferenças existentes entre os ambientes do Design System. A princípio são geradas imagens de referência do ambiente de produção e, a partir do comando de execução de teste em quaisquer outros ambientes, serão gerados screenshots das telas neste outro ambiente para comparar com o ambiente de produção. Atualmente os cenários de testes são aqueles previamente criados para os testes dos componentes, ou seja, aqueles cenários listados no arquivo `backstop_data/tests/util/scenarios.js`

2. *Component*: testes específicos para views dos componentes, melhor explicado no documento **Testando_Componente.md**. Sua pasta de cenários encontra-se disponível em `govbr-ds-dev-frontend-testing/backstop_data/tests/components`.

3. *All*: testes gerados para avaliar possíveis modificações realizadas no conteúdo do Design System. Veja o documento **Testando_All**. Pasta de cenários disponível em `govbr-ds-dev-frontend-testing/backstop_data/tests/all`.

4. *Template*: testes de templates, documento **Testando_Template.md**. Sua pasta de cenários encontra-se disponível em `govbr-ds-dev-frontend-testing/backstop_data/tests/template`.

## 3. Gerar bitmaps de referência

Comando para criar arquivos de referência (sem visualização prévia) para o ambiente local (local), de desenvolvimento (dev), de homologação (hom) e de produção (prod).
Existem quatro tipos de comandos para a geração de bitmaps de referência:

- **reference**

Gera screenshots dos cenários criados no ambiente de produção, a serem armazenados na pasta comum: **reference**.

```terminal
npm run reference
```

{{% notice info %}}
É possível parametrizar esse comando, alterando o ambiente que vai gerar as telas de referência (--refHost) e para qual componente será gerado separadamente (--component), bastando inserir o trecho abaixo logo depois do atalho. Exemplo:
{{% /notice %}}

```terminal
npm run reference -- --refHost="http://127.0.0.1:9000" --component=breadcrumb
```

- **component**
  
Testes específicos para views dos componentes.

Chama as propriedades `LOCAL_URL` (localhost) `DEFAULT_URL`(ambiente de desenvolvimento), `HOM_URL` (ambiente de homologação) e `PROD_URL` (ambiente de produção), respectivamente.

```terminal
npm run local-reference
npm run dev-reference
npm run hom-reference
npm run prod-reference
```

As URL's estão configuradas no arquivo de constantes, disponível em `/govbr-ds-dev-frontend-testing/backstop_data/tests/util/constants.js`.

Para realizar a criação de arquivos de referência de determinado componente, deve-se adicionar o `<nome-do-componente>` ao final do comando.

```terminal
npm run local-reference <nome-do-componente>
npm run dev-reference <nome-do-componente>
npm run hom-reference <nome-do-componente>
npm run prod-reference <nome-do-componente>
```

- **all**
  
Para a geração de imagens de referência de todo o conteúdo do Design System, de acordo com o ambiente escolhido (local/dev/hom/prod).

```terminal
npm run local-reference-all
npm run dev-reference-all
npm run hom-reference-all
npm run prod-reference-all
```

- **template**

Geração de imagens de referência dos templates nos ambientes local, desenvolvimento, homologação e produção, respectivamente.

```terminal
npm run local-reference-template
npm run dev-reference-template
npm run hom-reference-template
npm run prod-reference-template
```

## 4. Gerar bitmaps de teste

Para criar um conjunto de capturas de tela de teste e comparar com suas capturas de tela de referência. Execute-o depois de fazer alterações CSS quantas vezes forem necessárias.

Um relatório visual será aberto automaticamente no navegador, mostrando o número de testes visuais aprovados e reprovados.

Para cada teste, é possível acessar a captura de tela detalhada para a linha de base/referência, o teste, o diff e um depurador para compará-los lado a lado.

Existem quatro tipos de comandos para a realização de testes:

- **reference**

```terminal
npm run local-test-reference
npm run dev-test-reference
npm run hom-test-reference
npm run prod-test-reference
```
  
Gera screenshots dos cenários criados para o ambiente escolhido (pasta `<ambiente>_test`) e compara com as imagens armazenadas na pasta comum: `reference`. Atualmente os cenários são aqueles previamente criados para teste dos componentes, ou seja, aqueles listados no arquivo `backstop_data/tests/util/scenarios.js`

- **component**

Testes específicos para views dos componentes (screenshots gerados na pasta `<ambiente>_test`), comparando com as imagens de referência da pasta `<ambiente>_reference`.

```terminal
npm run local-test
npm run dev-test
npm run hom-test
npm run prod-test
```

Para criar capturas de tela de teste de *determinado componente* e compará-las com suas capturas de tela de referência, deve-se inserir o nome do componente ao final do comando:

```terminal
npm run local-test <nome-do-componente>
npm run dev-test <nome-do-componente>
npm run hom-test <nome-do-componente>
npm run prod-test <nome-do-componente>
```

- **all**

Gera imagens atuais de todo o conteúdo do Design System para o ambiente escolhido (`<ambiente>_test_ALL`</ambiente>) e compara com as imagens de referência da pasta `<ambiente>_reference_ALL`
  
```terminal
npm run local-test-all
npm run dev-test-all
npm run hom-test-all
npm run prod-test-all
```

- **template**

Testes de templates nos ambientes local, desenvolvimento, homologação e produção, respectivamente. Compara cenários de testes (pasta `<ambiente>_test_TEMPLATE`) com cenários de referência da pasta `<ambiente>_reference_TEMPLATE`

## 5. Gerar relatório

Abre no navegador o último relatório de testes realizado. Caso nenhum teste de comparação com as telas de referências tenha sido realizado anteriormente, a seguinte mensagem de erro será gerada:

```terminal
openReport | Attempting to ping 
openReport | Remote not found. Opening backstop_data/dev_html_report/index.html
```

- **reference**
  
```terminal
npm run local-report-reference
npm run dev-report-reference
npm run hom-report-reference
npm run prod-report-reference
```

Caso o relatório não seja aberto de forma automática no navegador, o relatório pode ser acessado em: `govbr-ds-dev-frontend-testing/backstop_data/<dev/hom/prod>_html_report_reference/index.html`

- **component**

```terminal
npm run local-report
npm run dev-report
npm run hom-report
npm run prod-report
```

Caso o relatório não seja aberto de forma automática no navegador, o relatório pode ser acessado em: `govbr-ds-dev-frontend-testing/backstop_data/<dev/hom/prod>_html_report/index.html`

- **all**

```terminal
npm run local-report-all
npm run dev-report-all
npm run hom-report-all
npm run prod-report-all
```

Caso o relatório não seja aberto de forma automática no navegador, o relatório pode ser acessado em: `govbr-ds-dev-frontend-testing/backstop_data/<dev/hom/prod>_html_report_ALL/index.html`

- **template**

```terminal
npm run local-report-template
npm run dev-report-template
npm run hom-report-template
npm run prod-report-template
```

Caso o relatório não seja aberto de forma automática no navegador, o relatório pode ser acessado em: `govbr-ds-dev-frontend-testing/backstop_data/<dev/hom/prod>_html_report_TEMPLATE/index.html`

## 6. Aprovar mudanças

Ao executar este comando, todas as imagens (com alterações) de seu lote de teste mais recente serão promovidas para sua coleção de referência. Os testes subsequentes serão comparados com seus arquivos de referência atualizados.

- **reference**

```terminal
npm run local-approve-reference
npm run dev-approve-reference
npm run hom-approve-reference
npm run prod-approve-reference
```

- **component**

```terminal
npm run local-approve
npm run dev-approve
npm run hom-approve
npm run prod-approve
```

- **all**

```terminal
npm run local-approve-all
npm run dev-approve-all
npm run hom-approve-all
npm run prod-approve-all
```

- **template**

```terminal
npm run local-approve-template
npm run dev-approve-template
npm run hom-approve-template
npm run prod-approve-template
```
  
---

## Arquivo config.js

Este arquivo substitui a configuração do arquivo backstop.json, onde são criados os cenários de testes e configuradas as resoluções a serem testadas.

Propriedades de configuração importantes do config.js:

- **id** - Usado para nomear screenshot. Caso seja omitida essa propriedade, o BackstopJS irá gerar um id automaticamente para evitar colisões de nomes com os recursos do BackstopJS.

- **viewports**- Uma matriz de objetos de tamanho de tela em que seu DOM será testado. Adicione quantos quiser - no mínimo um.

- **scenarios** - Configuração de testes reais.

Demais configurações:

- **report** - Além do relatório HTML, é possível ativar o relatório de CI (no formato JUnit) e fazer com que o servidor de CI realize analise e controle as estatísticas. A configuração adotada pelo projeto habilita relatórios HTML (browser) para humanos e relatórios JUnit (CI) para máquinas:
  `"relatório": ["navegador", "CI"]`

O relatório HTML pode ser acessado em:
`govbr-ds-dev-frontend-testing/backstop_data/<dev_/hom_/prod_>html_report/index.html`.

Já o relatório de CI, é armazenado por padrão em `backstop_data/ci_report/xunit.xml`

---

## .gitignore

Criar um .gitignore em sua pasta BackstopJS com as seguintes entradas:

```terminal
backstop_data/bitmaps_reference
backstop_data/bitmaps_test
backstop_data/html_report
backstop_data/dev_reference
backstop_data/dev_test
backstop_data/dev_html_report
backstop_data/hom_reference
backstop_data/hom_test
backstop_data/hom_html_report
backstop_data/prod_reference
backstop_data/prod_test
backstop_data/prod_html_report
```

Isso garantirá que os instantâneos de teste e os relatórios HTML gerados pelo Backstop sejam ignorados pelo Git.

---

## Documentação Oficial

<https://github.com/garris/BackstopJS>
