---
title: 2. Anatomia de um cenário de teste
description: Descreve as propriedades obrigatórias e opcionais de um cenário de teste
date: 14/04/2022
keywords: cenário, backstopjs, teste, propriedades
---

- **label [obrigatório]** - Rótulo/etiqueta usada para nomear imagens. Inserir o nome do componente.
- **url [obrigatório]** - Inserir a url do componente a ser testado.

**DICA**: nenhuma outra propriedade abaixo é obrigatória, mas podem ser adicionadas de acordo com a necessidade:

- **onBeforeScript** - Usado para configurar o estado do navegador. Por exemplo: cookies
- **cookiePath** - Importar cookies no formato JSON (disponível com o onBeforeScript padrão, consulte a configuração de cookies abaixo)
- **referenceUrl** - Estado ou ambiente diferente ao criar a referência. Ao adicionar a propriedade referenceUrl, será realizado teste de regressão visual comparando diferentes ambientes. Ex: ambiente de dev (url) x ambiente de homologação/produção (referenceUrl)
- **readyEvent** - Aguardar até a string determinada neste campo seja carregada no console
- **readySelector** - Aguardar até que este seletor informado exista, antes de continuar
- **delay** - Aguarde x milissegundos. Este é um parâmetro útil caso o carregamento da página seja mais lento, a mídia não esteja totalmente carregada ou o JS não tenha sua execução concluída. Exemplo: `"delay": 4000` = aguarde 4s antes da captura de tela.
- **hideSelectors** - Array de seletores configurados com a visibilidade: oculto (hidden). Se um site contiver componentes como uma seção de anúncio, vídeos e outro conteúdo que não seja importante para a área de teste, esses componentes podem ser ocultados antes de fazer uma captura de tela. Exemplo: `"hideSelectors": [ "#someAdsBlock"]`
- **removeSelectors** - Array de seletores configurados para exibir: nenhum (none)
- **onReadyScript** - Depois que as condições acima forem atendidas - use este script para modificar o estado da UI antes das capturas de tela, por exemplo hovers, clicks etc
- **keyPressSelectors** - Array de seletores e string -- simula iterações que envolvem pressionamento de teclas sequenciais.
- **hoverSelector** - Mova o ponteiro do mouse sobre o elemento DOM especificado antes da captura de tela. Os menus modernos usam principalmente alguma forma de navegação horizontal suspensa. Para testar cada menu, por exemplo, podemos usar hoverSelector. Exemplo: `"hoverSelector": ".primary .link--services a"`. Para um menu que usa o método 'clique para abrir suspenso', podemos usar o clickSelector, descrito abaixo.
- **hoverSelectors** - *Puppeteer somente* - simula várias interações de foco sequenciais. Recebe um array de seletores.
- **clickSelector** - Clique no elemento DOM especificado antes da captura de tela. Outra forma de simular a interação do usuário é usar o script global em backstop_data/engine_scripts/puppet/onReady.js. Em vez de repeti-lo para todos os cenários, isso pode ser tratado globalmente no evento onReady.
- **clickSelectors** - *Puppeteer somente* - simula várias interações de cliques sequenciais. Recebe um array de seletores.
- **postInteractionWait** - Aguarde um seletor após interagir com hoverSelector ou clickSelector (opcionalmente, aceita o tempo de espera em ms. Idéia para uso com uma transição de elemento de clique ou foco. Disponível com onReadyScript padrão)
- **scrollToSelector** - Buscar o elemento DOM especificado para exibição antes da captura de tela (disponível com onReadyScript padrão)
- **selectors** - Array de seletores a serem capturados. Se omitido, o padrão é o `document` . Use `viewport` para capturar o tamanho da viewport. É possível definir que ele faça uma Screenshot apenas do Footer ou do Menu ou de qualquer seletor. Consulte a documentação oficial (*targeting elements*) para obter mais informações.
- **selectorExpansion** - Consulte a documentação oficial (*targeting elements*) para obter mais informações.
- **misMatchThreshold** - Percentual de pixels diferentes permitidos para passar no teste
- **requireSameDimensions** - Se definido como `true`, qualquer mudança no tamanho do seletor irá causar uma falha no teste.
- **viewports** - Um array de objetos de tamanho de tela em que seu DOM será testado. Esta configuração substituirá a propriedade viewports atribuído no documento `backstop_data/tests/util/constants.js`. Pode ser utilizado os viewport pré-definidos com a finalidade de restringir especificamente os viewport que determinado cenário será testado, ou mesmo realizar a definição de novos viewports.

- Exemplo de cenário

```javascript
const componentUrl = "/dist/components/button/examples.html";

module.exports = options => {
  return {
    "scenarios": [
      {
        label: "Button",
        url: `${options.baseUrl}/${componentUrl}`,
        referenceUrl: `${options.refUrl}/${componentUrl}`,
        hideSelectors: [
            ".loading"
        ]
      },
      {
        label: "Button - Click",
        url: `${options.baseUrl}/${componentUrl}`,
        referenceUrl: `${options.refUrl}/${componentUrl}`,
        clickSelector: ".br-button.secondary",
        hideSelectors: [
            ".loading"
        ]
      },
      {
        label: "Button - Hover",
        url: `${options.baseUrl}/${componentUrl}`,
        referenceUrl: `${options.refUrl}/${componentUrl}`,
        hoverSelector: ".br-button.secondary",
        hideSelectors: [
            ".loading"
        ],
        viewports:[{
            "label": "Smartphone Portrait",
            "width": 575,
            "height": 760,
          }]
      },
    ]
  }
}
```
