---
title: Testes Frontend
description: OD24.2.1 - Criar um documento de referência indicando os tipos de testes e as boas práticas relacionadas ao assunto
date: 08/03/2022
keywords: testes, frontend, acessibilidade, automatização, boas práticas
---

## 1. Teste de Software

### 1.1 Definição

Teste de software é o processo de execução de um produto para determinar se ele atingiu suas especificações de funcionamento corretamente no ambiente para o qual foi projetado, atendendo as necessidades do usuário (especificações de requisitos ou estórias de usuário) e de outras partes interessadas em seu(s) ambiente(s) operacional(is) .

É uma maneira de avaliar a qualidade da aplicação e reduzir o risco de falha em operação. Planejamento, análise, modelagem e implementação dos testes, relatórios de progresso, resultado e avaliação da qualidade, também são partes de um processo de testes.

### 1.2. Vantagens e finalidades dos testes

Os testes têm as seguintes vantagens e finalidades:

- Buscam por erros ou anomalias em requisitos funcionais e não funcionais;
- Através dos testes obtemos um controle de qualidade que pode envolver etapas desde a escolha das condições em que a aplicação vai ser colocada à prova até a simulação de uso real dela e o desenvolvimento de relatórios sobre os resultados obtidos;
- Avaliar os produtos de trabalho (requisitos, estórias de usuário, modelagem e código);
- Verificar se todos os requisitos especificados foram atendidos;
- Validar se o objeto de teste está completo e funciona como os usuários e outras partes interessadas esperam;
- Criar confiança no nível de qualidade do objeto de teste;
- Evitar defeitos;
- Encontrar  falhas e defeitos;
- Fornecer  informações suficientes às partes interessadas para permitir que elas tomem decisões, especialmente em relação ao nível de qualidade do objeto de teste;
- Reduzir o nível de risco de qualidade de software inadequada (ex.: falhas não detectadas anteriormente que ocorrem em produção);
- Cumprir com requisitos ou normas contratuais, legais ou regulamentares, e/ou  verificar o cumprimento do objeto de teste com tais requisitos ou normas.

### 1.3. Pirâmide de teste

É uma representação gráfica para demonstrar de forma simples os tipos de testes, seus níveis, velocidade de implementação, complexidade dos testes realizados e custo dos mesmos.

Analisando a pirâmide podemos chegar ao custo de implementar e manter cada nível de teste, além de nos fornecer informações de qual nível devemos testar primeiro – e por quê.

A forma padrão de representar a pirâmide testes é a seguinte:

- Base: Testes de Unidade
- Meio: Testes de Integração
- Topo: Testes Ponta a Ponta (E2E, UI ou Testes de Interface)

![Pirâmide de testes](imagens/piramide.png)

Testes de Unidade (Unitário): Seu objetivo é garantir que uma unidade ou classe funcione de forma adequada aos requisitos. Geralmente é de responsabilidade do próprio desenvolvedor da unidade. Os testes são derivados da experiência do desenvolvedor. São os testes mais simples, rápidos e tem menor custo. E justamente por isso, são os testes indicados para se começar na realização da bateria de testes.

Testes de Integração: São os testes onde as unidades são testadas em conjunto, avaliando suas interações. Uma unidade isolada que apresente pleno e correto funcionamento, ao interagir com outras unidades, poderá não funcionar da mesma forma ou apresentar comportamentos inesperados/diversos. Por exemplo: Um formulário composto por inputs e botões.

Testes Ponta a Ponta (E2E, UI ou INTERFACE): seu principal objetivo é simular o comportamento de um usuário final na aplicação, ou seja, simulando o ambiente real. Os testes E2E são os testes mais caros, complexos e necessitam de maior tempo para execução, devido a isso a equipe de testes tem que ser bem criteriosa para criação de quais cenários terão cobertura de teste nesse nível da pirâmide. Normalmente se testam em front-end as partes mais críticas e estratégicas do negócio. Por exemplo: fluxo de telas de login, fluxo de telas de compras e pagamentos.

### 1.4. Erros, Defeitos e Falhas

Erro, pode ser entendido como a diferença entre o resultado obtido e o resultado esperado, cuja causa tem por origem uma ação humana (engano no código ou requisito).

Defeito (fault), é a manifestação de um erro em um software. Também é uma etapa, processo ou definição de dados incorreto que pode gerar uma falha. É a percepção externa de que uma parte ou todo o artefato de software não está funcionando.

Falha(Failure), a falha acontece quando o artefato de software não executa uma função necessária dentro dos limites especificados, ou seja, em conformidade com os seus requisitos.

### 1.5. Testes automatizados

São testes realizados através de scripts que são criados para testar a aplicação como um todo ou parte dela. A utilização de ferramentas facilita a criação desses scripts.

Tendo em mente a definição de escopo e metas bem claras, é possível chegar a resultados mais precisos e coesos.

A automação dos testes possibilita:

- Cobrir casos de testes e executar esses testes em diferentes versões de navegadores, dispositivos móveis e sistemas operacionais;
- Diminuir o tempo de ciclo de regressão e aumentar a frequência na realização dos testes
- Diminuir sensivelmente a reescrita de cenários de testes e consequentemente aumenta a produtividade.

Não esquecer que testes manuais e os testes automatizados são complemento um do outro, ou seja, devemos ter ambos. Novamente aqui vai a recomendação de não ser necessário a automação de “tudo”, mas sim do que é crítico na aplicação.

## 2. Tipos de testes front-end

No front-end, a equipe do Design System realiza os seguintes testes:

- Teste de Acessibilidade
- Teste de Análise Estática de Código
- Teste de Regressão Visual
- Teste Unitário
- Teste de Integração
- Teste E2E
- Teste de Snapshot
- Teste de Performance
- Teste de Segurança
- Teste de Usabilidade

### 2.1 Teste de Acessibilidade

Os padrões de acessibilidade compreendem recomendações ou diretrizes que visam tornar o conteúdo Web acessível a todas as pessoas, inclusive às pessoas com deficiência, destinando-se aos autores de páginas, projetistas de sítios e aos desenvolvedores de ferramentas para criação de conteúdo.

A observação destes padrões também facilita o acesso ao conteúdo da Web, independente da ferramenta utilizada (navegadores Web para computadores de mesa, laptops, telefones celulares, ou navegador por voz) e de certas limitações de ordem técnicas, como, por exemplo, uma conexão lenta, a falta de recursos de mídia, etc.

O guia de acessibilidade que orienta os desenvolvedores do Design System a implementarem componentes acessíveis e de fácil utilização pela maioria dos meios de acesso está disponível na íntegra em:

[Guia de acessibilidade](/design/guias/acessibilidade)

Além do guia, a equipe de Designers faz uso do checklist abaixo para construção de qualquer elemento ou interfaces (componente, padrões, fluxos etc) dentro do Design System, principalmente para criação de suas diretrizes. O documento visa responder a seguinte pergunta: Quais itens de acessibilidade o designer deve se preocupar?

- [ ] Contraste de cores
- [ ] Tecnologia assistiva
- [ ] Hierarquia
- [ ] Teclas de Atalho
- [ ] Área mínima de Interação (Clique e Toque)
- [ ] Informações, Conteúdos e Feedback
- [ ] Layout Responsivo
- [ ] Espaçamento e Legibilidade
- [ ] Suporte a Idiomas
- [ ] Som e Movimento
- [ ] Formulários
- [ ] Documentação
- [ ] Navegação
- [ ] Teste e Pesquisa

Detalhes sobre cada item do checklist pode ser encontrado em [Design/Acessibilidade](/design/guias/acessibilidade/).

### 2.2. Teste de Análise Estática de Código

Os testes de análise estática são realizados por meio de softwares automatizados que avaliam o código fonte sem executá-lo, sinalizando instantaneamente erros de programação, bugs, erros de estilo, construções suspeitas, entre outros.

[Checklist do Lint](/desenvolvimento/lints/)

### 2.3 Testes de Regressão Visual

São testes de alto nível que conseguem expor qualquer alteração visual de um sistema, seja em uma página específica ou em um determinado fluxo que o usuário irá percorrer. No Design System foi utilizada a ferramenta [Backstopjs](https://github.com/garris/BackstopJS), mas estuda-se a adoção do [Lambdatest](https://www.lambdatest.com/) como ferramenta para execução dos testes de regressão visual automatizado.

Aspectos a serem testados com relação à RESPONSIVIDADE:

- [ ] Aderência às grids
- [ ] Aderência aos breakpoints
- [ ] Legibilidade da informação (texto e imagens)
- [ ] integridade do layout

Aspectos a serem testados com relação à COMPATIBILIDADE:

- [ ] Principais browsers do mercado ( Chrome, Firefox, Edge… )
- [ ] Plataformas diferenciadas ( Windows, Mac, Linux )
- [ ] Versões (navegador) mínimas aceitáveis

### 2.4. Teste Unitário

Consistem em técnicas de teste geralmente realizadas pelo próprio desenvolvedor, onde cada unidade do sistema será isolada para análise e identificação de problemas, cujos defeitos deverão ser corrigidos.

Vantagens deste tipo de teste:

- redução de defeitos nos recursos desenvolvidos ou alterados recentemente;
- reduz o custo do teste, uma vez que os defeitos são capturados na fase inicial;
- melhor refatoração do código;
- Não há necessidade de conhecimento de sistemas interconectados.

### 2.5. Teste de Integração

Se os testes de unidade verificam o comportamento de um bloco ou componente, os testes de integração garantem que os blocos funcionem conforme o esperado quando estão em conjunto.

### 2.6. Teste E2E

Um teste E2E, ou End-to-End, é um método de teste utilizado para testar um fluxo da aplicação desde o começo até o fim. Seu intuito é replicar cenários reais feitos pelos usuários com a intenção de validar que o sistema esteja funcionando como o esperado. Pode-se utilizar o Cypress como ferramenta para este tipo de teste.

Para a realização dos testes E2E são construídos cenários, que simulam ações de usuários e validam todo tipo de comportamento possível dentro da aplicação. Vários cenários reunidos constituem uma suíte de testes.

Neste tipo de teste, a interação é diretamente com a interface da aplicação, o que naturalmente leva a escrita de muito código assíncrono. A página web também deverá estar acessível para o browser durante a execução dos testes.

Algumas preocupações a serem tomadas ao realizar testes ponta a ponta de webcomponents:

- [ ] Aguardar o elemento ficar disponível no DOM para que conteúdo seja renderizado no corpo da página.
- [ ] Acessar o Shadow Root para realizar as asserções. Para garantir que o elemento se comporta/parece o mesmo, avaliar se sua estrutura DOM permanece a mesma. Uma asserção de teste pode incluir pré-requisitos que devem ser verdadeiros para que a asserção de teste seja válida.
- [ ] Testar as interações com o web component com a finalidade de avaliar se os eventos acionados conduzem com a renderização esperada.
- [ ] Testar eventos customizados/personalizados: são uma funcionalidade central e usados na comunicação entre os Web Components.

[Testes E2E com Cypress](/desenvolvimento/testes/cypress/)

### 2.7. Teste de Snapshot

Testes de snapshot visam garantir que a interface gráfica do usuário (UI) não seja alterada inesperadamente.

Em um teste de regressão visual comum, um componente de UI é renderizado para que seja capturado um snapshot, logo em seguida esta imagem será comparada para com uma imagem de referência armazenada. O teste irá falhar se as duas imagens não coincidirem: seja por mudança inesperada ou mesmo devido a necessidade de atualização da versão da captura de tela por atualização do componente da UI.

Nos testes snapshot utilizando a ferramenta JEST no desenvolvimento dos webcomponents, em vez de renderizar a UI, que iria precisar construir o aplicativo inteiro, é utilizado um renderizador de teste para gerar rapidamente o código de construção do webcomponent. Com isso, é possível avaliar se a estrutura está de acordo com o esperado e se as propriedades foram utilizadas da maneira correta.

### 2.8.  Teste de Performance

Para garantir que o usuário tenha a melhor experiência ao utilizar o front-end e melhorar a velocidade, é essencial garantir um bom desempenho dos componentes desenvolvidos. Para isso, torna-se extremamente importante avaliar a performance, especialmente (e não apenas) nos seguintes quesitos:

- [ ] Primeiro aparecimento de conteúdo: tempo que leva para a primeira imagem ou texto aparecer
- [ ] Índice de velocidade: tempo que leva para o conteúdo de uma página ser preenchido visivelmente, ou seja, para que pareça pronto aos olhos do usuário
- [ ] Maior renderização de conteúdo: tempo de renderização da maior imagem ou bloco de texto visível na janela de visualização (viewport), em relação a quando a página começou a carregar pela primeira vez.
- [ ] Tempo para iteragir: tempo até que a página se torne totalmente iterativa
- [ ] Mudança Cumulativa de Layout: mede a estabilidade visual, quantificando a frequência com que os usuários experimentam mudanças inesperadas de layout

Existem ferramentas gratuitas no mercado que auxiliam na otimização da performance, gerando métricas e indicando oportunidades de melhorias através de diagnósticos gerados automaticamente.

[Ferramenta LightHouse](https://web.dev/lighthouse-performance/)

### 2.9. Teste de Segurança

Como a segurança de um sistema deve ser de responsabilidade de todos os envolvidos, é importante que também seja aplicada e testada em todas as etapas de desenvolvimento e evolução deste sistema. Abaixo foram listadas alguns exemplos de medidas de prevenção que podem ser aplicadas e testadas no front-end:

- [ ] Filtrar entrada de dados (input) - Filtre os dados com base no que é esperado ou na entrada válida antes de enviá-la para o back-end. Evita vulnerabilidades como injeção de SQL, clickjacking, etc.
- [ ] Limpar a saída de dados (output) - Realizar a limpeza de mensagens ou conteúdos recuperados do back-end, que são apresentados ao usuário diretamente, removendo possíveis scripts maliciosos.
- [ ] Use cabeçalhos de resposta apropriados- Em respostas HTTP que não devem conter HTML ou JavaScript, pode-se usar os cabeçalhos Content-Type e X-Content-Type-Options para garantir que os navegadores interpretem as respostas da maneira pretendida. Por exemplo. Um dado JSON nunca deve ser codificado como text/html, para evitar a execução acidental.
- [ ] Não defina o valor innerHTML com base na entrada do usuário e use textContent em vez de innerHTML tanto quanto possível.
- [ ] Desative a incorporação de iframe - O desativador iframe pode evita o ataque de clickjacking. Usar o cabeçalho "X-Frame-Options": "DENY" na solicitação que proíbe a renderização do site em um frame.
- [ ] Manter erros genéricos - Um erro como “Sua senha está incorreta” pode ser útil para o usuário, mas também para os invasores.
Utilizar erros ambíguos como “Informações de login incorretas”.
- [ ] Auditar dependências regularmente - Executar 'npm audit' regularmente para obter uma lista de pacotes vulneráveis ​​e atualizá-los para evitar problemas de segurança. O GitHub também é capaz de sinalizar dependências vulneráveis. Outras ferramentas também são capazes de avaliar o código-fonte automaticamente e abrir solicitações de pull para alterar versões.
- [ ] Compartimentalize sua aplicação - Um aplicativo pode ser dividido em partes públicas, autenticadas e administrativas, cada uma hospedada em subdomínios separados.
- [ ] Evite usar type="hidden" para ocultar dados confidenciais e armazenar chaves, tokens de autenticação, etc, no armazenamento na memória do navegador, tanto quanto possível. Existem ferramentas de inspeção no navegador que podem expor esses valores aos invasores se eles encontrarem uma maneira de injetar um script.

### 2.10. Teste de Usabilidade

Este teste consiste em observar o produto para investigar questões relacionadas a navegabilidade e comportamento (necessidades, dificuldades, interesses etc), buscando sempre a melhor experiência do usuário ou cliente em uma plataforma.

Para conhecer mais sobre os diferentes métodos de teste de usabilidade,acessar o documento [Design/Guias/Testes de Usabilidade](/design/guias/testes-usabilidade/).

## 3. Boas práticas

Alguns exemplos de boas práticas para testes de software:

- Os desenvolvedores devem ser os responsáveis pela criação de testes, pois podem aumentar as chances de sucesso nos projetos, visto que são eles quem conhecem melhor o sistema;
- Nas revisões de requisitos ou refinamento das estórias de usuário, poderá haver detecção de defeito nesse momento inicial;
- Durante a fase de projeto do sistema, aumentando o entendimento de cada parte do código e como testá-lo;
- No processo total de desenvolvimento do software, validando desde as menores partes do código;
- Fazer testes antes da implementação;
- Testes devem ser simples, expressivos e com intenção;
- Verificar e validar o software antes de enviar para homologação ou produção, podendo detectar os defeitos que podem ter passado nos testes anteriores, evitando assim futuras falhas;

Com a finalidade de garantir qualidade nas entregas dos produtos do Design System e facilitar a padronização de ações a serem seguidas por designers e desenvolvedores, a equipe elaborou os seguintes checklists:

Checklist de risco/impacto:

- [Análise de Risco](/gestao/checklists/analise-risco/)

- [Análise de Impacto](/gestao/checklists/analise-impacto/)

Checklists do Desenvolvedor:

- [Qualidade](/desenvolvimento/checklists/implementacao/)

Checklists do Designer:

- [Acessibilidade](/design/checklists/acessibilidade/)
- [Prototipação](/design/checklists/prototipacao/)

## 4. Bibliografia

Ian Sommerville. Engenharia de Software, 9ª Edição. Pearson Education, 2011.

IEEE Std 1044-2009, IEEE Standard Classification of Software Anomalies.

IEEE Std 610.12-1990, IEEE Standard Glossary of Software Engineering.

<https://blog.onedaytesting.com.br/piramide-de-teses/>

<https://blog.onedaytesting.com.br/automatizacao-de-testes/>

<https://blog.betrybe.com/desenvolvimento-web/jmeter/>

<https://blog.tecnospeed.com.br/teste-de-software/>

<https://cwi.com.br/blog/o-que-e-teste-de-software-por-que-e-necessario/>

<https://dev.to/thayseonofrio/guia-rapido-de-testes-no-front-end-2lpc>

[Best Practices | Cypress Documentation](https://docs.cypress.io/guides/references/best-practices)

[Clean Coder Blog](https://blog.cleancoder.com/uncle-bob/2013/09/23/Test-first.html)

<https://autociencia.blogspot.com/2019/05/entenda-diferenca-entre-erro-defeito-e-falha-de-software.html>

[Understanding Frontend Security](https://medium.com/@manojsingh047/understanding-frontend-security-ff6585395534)
