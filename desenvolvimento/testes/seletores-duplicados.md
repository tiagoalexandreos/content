---
title: Seletores Duplicados
description: Seletores Duplicados
date: 03/11/2021
keywords: testes, seletor, script, duplicado
---

**Problema:**
Ao rodar um seletor dentro do script na maioria das vezes são gerados elementos duplicados.

**Solução:**
usar :nth-child() ou criar uma classe específica para o componente objeto do teste que se repete.
