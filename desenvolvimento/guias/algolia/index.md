---
title: Indexar Conteúdo para Busca
description: Guia sobre como gerar os indexes para a busca e como enviar para o Algolia
---

## Gerar os indexes

Para a busca, nós usados o [Algólia](https://www.algolia.com/). Criamos um script na pasta src/assets/js/algolia.js dentro do Site que cuida de gerar o JSON para ser enviado para indexação pelo Algólia. Basta rodar o comando `npm run index:docs` no Site para criar o arquivo pronto para indexação pelo Algólia. Atualmente o envio é manual e depende das permissões.

## Enviar indexes

Para enviar os indexes você deve seguir os seguintes passos:

- Logar na sua conta do Algolia (com seu e-mail ou conta do time)
- Entre na página `Search > Configure > Index`
- No topo da página escolha a Aplicação e Index que deseja configurar

![Escolher index](imagens/index.png)

- Na opção `Manage Index` você pode remover os indexes antigos caso deseje

![Configurar index](imagens/manage_index.png)

- Na opção de `Add Records` você pode enviar o arquivo gerado pelo site com os conteúdos indexados

![Adicionar itens](imagens/records.png)

> Parabéns! Você acabou de gerar um novo conjunto de indexes para a busca no site. ::tada::

<!-- TODO: Esse conteúdo só serve por enquanto que não disponibilizamos múltiplas versões do site. Quando fizermos isso vamos precisar avaliar como o Algolia funciona e criar uma nova documentação -->
