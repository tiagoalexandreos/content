<!-- Botões de ação-->
<div class="row mb-6">
  <div class="col-sm">
    <button class="br-button block auto-sm" type="button">
      <i class="fas fa-eraser mr-1" aria-hidden="true"></i>
      Limpar Dados
    </button>
  </div>
  <div class="col-sm-auto mt-4 mt-sm-0">
    <button class="br-button secondary block auto-sm" type="button">
      Cancelar
    </button>
  </div>
  <div class="col-sm-auto mt-4 mt-sm-0">
    <button class="br-button primary block auto-sm" type="button">
      Enviar Dados
    </button>
  </div>
</div>
