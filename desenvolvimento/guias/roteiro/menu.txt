<!-- Menu-->
<div class="br-menu" id="main-navigation">
  <div class="menu-container">
    <div class="menu-panel">
      <div class="menu-header">
        <div class="menu-title">
          <span>Sistema de Ouvidoria Pública</span>
        </div>
        <div class="menu-close">
          <button class="br-button circle" type="button" aria-label="Fechar o menu" data-dismiss="menu">
            <i class="fas fa-times" aria-hidden="true"></i>
          </button>
        </div>
      </div>
      <nav class="menu-body">
        <a class="menu-item divider" href="cadastro.html">
          <span class="content">Cadastro</span>
        </a>
      </nav>
      <div class="menu-footer">
        <div class="menu-links">
          <a href="javascript: void(0)">
            <span class="mr-1">Órgãos do Governo</span>
            <i class="fas fa-external-link-square-alt" aria-hidden="true"></i>
          </a>
          <a href="javascript: void(0)">
            <span class="mr-1">Acesso à Informação</span>
            <i class="fas fa-external-link-square-alt" aria-hidden="true"></i>
          </a>
          <a href="javascript: void(0)">
            <span class="mr-1">Legislação</span>
            <i class="fas fa-external-link-square-alt" aria-hidden="true"></i>
          </a>
          <a href="javascript: void(0)">
            <span class="mr-1">Acessibilidade</span>
            <i class="fas fa-external-link-square-alt" aria-hidden="true"></i>
          </a>
        </div>
      </div>
    </div>
    <div class="menu-scrim" data-dismiss="menu" tabindex="0"></div>
  </div>
</div>
