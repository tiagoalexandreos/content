---
title: Changelog
description: Changelog de Desenvolvimento
date: 15/12/2020
keywords: desenvolvimento, changelog
---

O changelog de desenvolvimento é criado automaticamente conforme as mensagens de commit. Assim não é necessário alterar o arquivo manualmente para incluir as mudanças de desenvolvimento, mas é **extremamente importante** que os desenvolvedores observem o padrão de mensagens de commit e como identificar as[breaking changes]({{% relref "/git-gitlab/guias/breaking-changes" %}}).
