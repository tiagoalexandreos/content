---
title: Roteiro para uso do Design System
description: Este roteiro serve para criar uma página usando os componentes e utilitários do Design System.
date: 08/09/2022
keywords: documentação, desenvolvedor, componente, javascript, código
---

Este roteiro serve para criar uma página usando os componentes e utilitários do Design System.

A página será criada no formato estático, mas serve de base para uso em qualquer framework ou CMS.

Neste roteiro estamos focando o uso das ferramentas Figma e vscode **por serem compatíveis com qualquer Sistema Operacional**, mas você poderá usar qualquer outra ferramenta que tenha mais afinidade.

{{% notice info %}}
Este roteiro é compatível com a versão 3.0.1 do Design System!
{{% /notice %}}

## 1. Preparação do ambiente de desenvolvimento

Recomendamos usar o editor **Visual Studio Code** (vscode). Ele pode ser baixado em <https://code.visualstudio.com/>.

1. Criar a pasta "projeto" em Desktop
1. Abrir a pasta com **vscode**
1. Instalar as extensões:
    - **EditorConfig for VS Code**
    - **Prettier - Code formatter**
    - **Live Server**
1. Habilitar a configuração **Format on Save** no editor
1. Gerar arquivo `.editorconfig` e configurar:

    {{< highlight ini >}}
    root = true

    [*]
    indent_style = space
    indent_size = 2
    end_of_line = lf
    charset = utf-8
    trim_trailing_whitespace = false
    insert_final_newline = false
    {{< /highlight >}}

1. Abrir uma janela de terminal no editor
1. Conferir versão mínima do node (**14.15.4**)
1. Iniciar um projeto node com `npm init -y`
1. Instalar o DS com `npm i @govbr-ds/core`

## 2. Visualizar o modelo da página

1. Entrar em <http://gov.br/ds/> e navegar para **Downloads**
1. Clicar na aba **Modelos** e baixar "CRUD Figma"
1. Entrar em <https://www.figma.com/> e importar o arquivo baixado
1. As páginas de referência são **add-06** e **add-mobile-06** localizada em "Modelos para Cadastro de Dados"

**Atenção!** A página implementada será uma adaptação dos modelos e poderá ficar diferente.

Serão usados os seguintes componentes e utilitários:

| Componentes  | Utilitários   |
| ------------ | ------------- |
| Button       | Collapse (\*) |
| Header (\*)  | Cores         |
| Input        | Display       |
| Menu (\*)    | Espaçamento   |
| Message (\*) | Flexbox       |
| Radio        | Grid          |
| Select (\*)  | Textos        |
| Textarea     | Tooltip (\*)  |

{{% notice note %}}
**Elementos com (\*) precisam ser instanciados para funcionar.**
{{% /notice %}}

## 3. Criar a página

1. Criar o arquivo **cadastro.html**
1. Configurar "Formatador Padrão" para **Prettier** no arquivo
1. Entrar em <http://gov.br/ds/> e navegar para **Introdução/Como começar**, copiar e colar código de **Template Inicial** no arquivo
1. Alterar chamada de "core-init.js" para **core.min.js**
1. Adicionar `<title>Cadastro</title>` em `<head>`
1. Abrir página usando **Live Server**

{{< highlight html >}}
<!DOCTYPE html>
<html lang="pt-br">{{% include "content/desenvolvimento/guias/roteiro/head.txt" %}}
<body>
  <!-- Conteúdo-->

  <!-- Scripts de componentes-->
  <script src="node_modules/@govbr-ds/core/dist/core.min.js"></script>
</body>
</html>
{{< /highlight >}}

## 4. Inserir Componente Header

1. Entrar em <http://gov.br/ds/> e navegar para **Componentes/Header**
1. Usar exemplo **Header Compacto**
1. Ajustar dados:
    - **Alterar Logo**: imagem gov.br
    - **Colocar Título**: Sistema de Ouvidoria Pública
    - **Colocar Subtítulo**: Governo
    - **Colocar Links**: Órgãos do Governo, Acesso à Informação, Legislação e Acessibilidade
    - **Remover Funcionalidades**
    - **Remover Autenticar**
    - **Remover Busca**

Código do Header:

{{< highlight html >}}
{{% include "content/desenvolvimento/guias/roteiro/header.txt" %}}
{{< /highlight >}}

{{% notice note %}}
**Este componente precisa ser instanciado** para funcionar corretamente.
{{% /notice %}}

Exemplo de instanciação do Header:

{{< highlight js >}}
{{% include "content/desenvolvimento/guias/roteiro/header-js.txt" %}}
{{< /highlight >}}

No exemplo a seguir, o componente está com o conteúdo alinhado em **Largura Fixa**. A mudança para largura fluida é feita trocando a classe `container-lg` por `container-fluid`.

Outros exemplos de uso do Header podem ser vistos em <https://www.gov.br/ds/templates/base>.

## 5. Inserir Componente Menu

1. Entrar em <http://gov.br/ds/> e navegar para **Componentes/Menu**
1. Usar exemplo **Menu Principal Flutuando (Off Canvas)**
1. Ajustar dados:
    - **Cabeçalho do menu**: Sistema de Ouvidoria Pública
    - **Item de 1º nível**: Cadastro
    - **Links externos**: Órgãos do Governo, Acesso à Informação, Legislação e Acessibilidade

**Atenção!** O "Menu Off Canvas" não obriga a criar uma coluna para a área de conteúdo.

Para usar o "Menu Principal Empurrando (Push)" veja os exemplos em <https://www.gov.br/ds/templates/base>.

Código do Menu:

{{< highlight html >}}
{{% include "content/desenvolvimento/guias/roteiro/menu.txt" %}}
{{< /highlight >}}

{{% notice note %}}
**Este componente precisa ser instanciado** para funcionar corretamente.
{{% /notice %}}

Exemplo de instanciação do Menu:

{{< highlight js >}}
{{% include "content/desenvolvimento/guias/roteiro/menu-js.txt" %}}
{{< /highlight >}}

## 6. Alinhar o Conteúdo

Vamos manter o mesmo alinhamento do Header e incluir um espaço entre ele e o conteúdo.

Exemplo de código para o conteúdo:

{{< highlight html >}}
<div class="container-lg mt-3">
  <!-- Conteúdo-->
</div>
{{< /highlight >}}

Utilitários usados:

- **Espaçamento**: <https://www.gov.br/ds/utilities-css/espacamento>
- **Grid**: <https://www.gov.br/ds/utilities-css/grid>

## 7. Inserir Componente Message

1. Entrar em <http://gov.br/ds/> e navegar para **Componentes/Message**
1. Usar exemplo **Mensagem de Informação**

Código do Message:

{{< highlight html >}}
{{% include "content/desenvolvimento/guias/roteiro/message.txt" %}}
{{< /highlight >}}

{{% notice note %}}
**Este componente precisa ser instanciado** para funcionar corretamente.
{{% /notice %}}

Exemplo de instanciação do Message:

{{< highlight js >}}
{{% include "content/desenvolvimento/guias/roteiro/message-js.txt" %}}
{{< /highlight >}}

## 8. Título da página

Inserir a tag de título e colocar o texto "Cadastro de Manifestação".

Exemplo de código para o título da página:

{{< highlight html >}}
<!-- Conteúdo-->
<main>
  <!-- Título da Página-->
  <h1>Cadastro de Manifestação</h1>
</main>
{{< /highlight >}}

## 9. Blocos de Preenchimento

- Os blocos de preenchimento usam o utilitário Collapse e seu conteúdo possuem fundo cinza
- Vamos usar o utilitário Flexbox para ajustar o alinhamento do título do bloco e o botão de collapse
- O espaçamento e a disposição de conteúdo serão modificados dependendo do breakpoint
- Na versão de celular os assuntos do bloco "Dados Pessoais" ficarão destacados em caixa alta

Exemplo de código para cada **título de bloco**:

{{< highlight html >}}
<!-- Título do bloco-->
<div class="d-flex align-items-center">
  <strong class="mr-2">Título do bloco</strong>

  <!-- Botão de collapse-->
  <button class="br-button circle small" type="button" data-toggle="collapse" data-target="id-do-bloco" aria-label="Abrir/fechar bloco">
    <i class="fas fa-angle-down" aria-hidden="true"></i>
  </button>
</div>
{{< /highlight >}}

Mais detalhes sobre o **Componente Button** em <https://www.gov.br/ds/components/button>.

Exemplo de código para cada **bloco**:

{{< highlight html >}}
<div class="bg-gray-2 px-2 py-3 p-md-6 mt-1 mb-6" id="id-do-bloco">
  <!-- Conteúdo interno do bloco-->
</div>
{{< /highlight >}}

Exemplo de instanciação do Collapse:

{{< highlight js >}}
{{% include "content/desenvolvimento/guias/roteiro/collapse-js.txt" %}}
{{< /highlight >}}

Utilitários usados:

- **Collapse**: <https://www.gov.br/ds/util/collapse>
- **Cores**: <https://www.gov.br/ds/utilities-css/cores>
- **Display**: <https://www.gov.br/ds/utilities-css/display>
- **Espaçamento**: <https://www.gov.br/ds/utilities-css/espacamento>
- **Flexbox**: <https://www.gov.br/ds/utilities-css/flexbox>
- **Grid**: <https://www.gov.br/ds/utilities-css/grid>

### 9.1 Bloco "Tipos de Manifestação"

Os campos de seleção são divididos em colunas usando a Grid. Na versão de celular os campos são listados um abaixo do outro enquanto que na versão desktop os campos ficam lado a lado.

Exemplo de código:

{{< highlight html >}}
<div class="bg-gray-2 px-2 py-3 p-md-6 mt-1 mb-6" id="tipo-manifestacao">
  <div class="row">
    <div class="col-md col-lg-3">...</div>
    <div class="col-md col-lg-3 mt-4 mt-md-0">...</div>
    <div class="col-md mt-4 mt-md-0">...</div>
  </div>
</div>
{{< /highlight >}}

Neste bloco serão usados os componentes:

- **Radio**: <https://www.gov.br/ds/components/radio>
- **Select**: <https://www.gov.br/ds/components/select>

{{% notice note %}}
**O Select precisa ser instanciado** para funcionar corretamente.
{{% /notice %}}

{{< highlight js >}}
{{% include "content/desenvolvimento/guias/roteiro/select-js.txt" %}}
{{< /highlight >}}

Utilitários usados:

- **Collapse**: <https://www.gov.br/ds/util/collapse>
- **Cores**: <https://www.gov.br/ds/utilities-css/cores>
- **Espaçamento**: <https://www.gov.br/ds/utilities-css/espacamento>
- **Flexbox**: <https://www.gov.br/ds/utilities-css/flexbox>
- **Grid**: <https://www.gov.br/ds/utilities-css/grid>
- **Textos**: <https://www.gov.br/ds/utilities-css/textos>

{{% notice tip %}}
Ir para código completo do [Bloco "Tipos de Manifestação"](codigo-139).
{{% /notice %}}

### 9.2 Bloco "Dados Pessoais"

Neste bloco há uma divisão interna por assuntos.

Cada assunto terá um tooltip explicativo e uma linha com colunas de preenchimento.

Exemplo de código para **assunto**:

{{< highlight html >}}
<!-- Assunto-->
<div class="d-flex align-items-center mt-3 mt-md-0 mb-3">
  <span class="text-up-01 text-uppercase text-md-capitalize text-weight-extra-bold text-md-weight-semi-bold">
    Assunto
  </span>

  <!-- Botão tooltip-->
  <button class="br-button circle small ml-2" data-tooltip-text="Texto do tooltip" aria-label="Texto do tooltip">
    <i class="fas fa-exclamation-circle" aria-hidden="true"></i>
  </button>
</div>
{{< /highlight >}}

Exemplo de código para **linhas com colunas de preenchimento**:

{{< highlight html >}}
<div class="bg-gray-2 px-2 py-3 p-md-6 mt-1 mb-6" id="dados-pessoais">
  <!-- Assunto: Informações básicas-->
  <div class="d-flex align-items-center mt-3 mt-md-0 mb-3">...</div>
  <!-- Linha de dados-->
  <div class="row mt-md-4 mb-10x mb-md-8x">
    <div class="col-md-4">...</div>
    <div class="col-4 col-md-2 mt-4 mt-md-0">...</div>
    <div class="col-10 col-md-3 mt-4 mt-md-0">...</div>
    <div class="col-md-12 col-lg-auto mt-4 mt-lg-0">...</div>
  </div>

  <!-- Assunto: Dados para contato-->
  <div class="d-flex align-items-center mt-10x mt-md-8x mb-3">...</div>
  <!-- Linha de dados-->
  <div class="row mt-md-4">
    <div class="col-md col-lg-4">...</div>
    <div class="col-8 col-md-3 col-lg-2 mt-4 mt-md-0">...</div>
    <div class="col-8 col-md-3 col-lg-2 mt-4 mt-md-0">...</div>
  </div>
</div>
{{< /highlight >}}

Neste bloco serão usados os componentes:

- **Button**: <https://www.gov.br/ds/components/button>
- **Input**: <https://www.gov.br/ds/components/input>
- **Radio**: <https://www.gov.br/ds/components/radio>

Utilitários usados:

- **Collapse**: <https://www.gov.br/ds/util/collapse>
- **Cores**: <https://www.gov.br/ds/utilities-css/cores>
- **Espaçamento**: <https://www.gov.br/ds/utilities-css/espacamento>
- **Flexbox**: <https://www.gov.br/ds/utilities-css/flexbox>
- **Grid**: <https://www.gov.br/ds/utilities-css/grid>
- **Textos**: <https://www.gov.br/ds/utilities-css/textos>
- **Tooltip**: <https://www.gov.br/ds/util/tooltip>

{{% notice tip %}}
Ir para código completo do [Bloco "Dados Pessoais"](codigo-224).
{{% /notice %}}

### 9.3 Bloco "Detalhamento do Relato"

Neste bloco é usado o componente Textarea.

Exemplo de código:

{{< highlight html >}}
{{% include "content/desenvolvimento/guias/roteiro/bloco-relato.txt" %}}
{{< /highlight >}}

Mais detalhes sobre o **Componente Textarea** em <https://www.gov.br/ds/components/textarea>.

## 10. Botões de ação

No celular os botões ocupam toda a largura e ficam um abaixo do outro.

Nos outros breakpoints eles são redimensionados e ocupam a mesma linha.

Exemplo de código:

{{< highlight html >}}
{{% include "content/desenvolvimento/guias/roteiro/botoes-acao.txt" %}}
{{< /highlight >}}

## 11. Código Final

```html
<!DOCTYPE html>
<html lang="pt-br">{{% include "content/desenvolvimento/guias/roteiro/head.txt" %}}
<body>
  {{% include "content/desenvolvimento/guias/roteiro/header.txt" %}}
  {{% include "content/desenvolvimento/guias/roteiro/menu.txt" %}}
  <div class="container-lg mt-3">
    {{% include "content/desenvolvimento/guias/roteiro/message.txt" %}}
    <!-- Conteúdo-->
    <main>
      <!-- Título da Página-->
      <h1>Cadastro de Manifestação</h1>

      {{% include "content/desenvolvimento/guias/roteiro/bloco-tipo-manifestacao.txt" %}}
      {{% include "content/desenvolvimento/guias/roteiro/bloco-dados-pessoais.txt" %}}
      {{% include "content/desenvolvimento/guias/roteiro/bloco-relato.txt" %}}
      {{% include "content/desenvolvimento/guias/roteiro/botoes-acao.txt" %}}
    </main>
  </div>
  <!-- Scripts de componentes-->
  <script src="node_modules/@govbr-ds/core/dist/core.min.js"></script>
</body>
</html>
```
