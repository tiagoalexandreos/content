---
title: Documentação de desenvolvedor
description: Essa documentação é voltada para os desenvolvedores. Ela deve orientar o implementador no uso dos códigos de forma que consigam aproveitar todas as funcionalidades de forma correta e fácil.
date: 11/07/2022
keywords: documentação, desenvolvedor, componente, javascript, código
---

{{% notice info %}}
Esse é um template para documentação de DESENVOLVEDOR. Cada componente é único e pode exigir pequenas variações desse documento. Cabe a quem escrever a documentação avaliar quais seções são necessárias para os componentes.
{{% /notice %}}

## Introdução

Essa seção não faz parte da documentação, ela é apenas para explicar melhor como esse template funciona.

Essa documentação é voltada para os desenvolvedores. Ela deve orientar o implementador no uso dos códigos de forma que consigam aproveitar todas as funcionalidades de forma correta e fácil.

A primeira coisa que a documentação de desenvolvedor tem que ter é a versão da documentação de design utilizada como base para a construção do componente. Isso porque pode acontecer da documentação de design ser atualizada e o componente implementado e sua documentação continuarem sem alterações.
Ex: doc. design: #X.0.0

Sugestões são bem-vindas e devem ser discutidas com o time antes de serem adotadas.

O título é recuperado do arquivo config.json e a descrição é única para as documentações de DESIGNER e DESENVOLVEDOR. Caso seja necessário atualizar, atualize o arquivo correspondente.
Ex: no componente button o arquivo de descrição é button.md

### Recomendações

1. Informar a versão da documentação de DESIGN usada como base para o desenvolvimento
1. Ao colocar o código-fonte coloque a linguagem correta. Ex ``` JavaScript
1. Tente pedir para alguém seguir a documentação e verificar se conseguem usar o componente/template
1. Seja direto nos textos de explicação

### Exemplos de código para instanciação

Usar 1 dos exemplos abaixo dentro do Modelo de documento a seguir.

1. Instanciação simples:

    ```javascript
    const checkboxList = []

    for (const brCheckbox of window.document.querySelectorAll('.br-checkbox')) {
        checkboxList.push(new govbrds.BRCheckbox('br-checkbox', brCheckbox))
    }
    ```

1. Instanciação com passagem de argumentos:

    ```javascript
    const cookiebarList = []

    for (const brCookiebar of window.document.querySelectorAll('.br-cookiebar')) {
        const params = {
        name: 'br-cookiebar',
        component: brCookiebar,
        json: json,
        lang: 'pt-br',
        mode: 'default',
        callback: callback,
        }
        cookiebarList.push(new govbrds.BRCookiebar(params))
    ```

1. Instanciação com função adicional:

    ```javascript
    const uploadList = []

    function uploadTimeout() {
        return new Promise((resolve) => {
        // Colocar aqui um upload para o servidor e retirar o timeout
        return setTimeout(resolve, 3000)
        })
    }

    for (const brUpload of window.document.querySelectorAll('.br-upload')) {
        uploadList.push(new govbrds.BRUpload('br-upload', brUpload, uploadTimeout))
    }
    ```

## Modelo de documento

```markdown
---
version: X.0.0
---

## Como usar

### Propriedades obrigatórias

<!-- Deve mostrar os atributos HTML que são obrigatórios para o uso do componente -->

| Atributo | Tipo    | Valor padrão | Descrição                                            |
| -------- | ------- | ------------ | ---------------------------------------------------- |
| title    | string  | undefined    | Et deserunt adipisicing culpa cupidatat consequat    |
| timeout  | number  | 10           | Amet enim labore Lorem qui labore sint ipsum ullamco |
| validade | boolean | false        | Exercitation minim eiusmod enim irure dolore ullamco |

### Propriedades Adicionais

<!-- Deve mostrar os outros atributos possíveis que definem os possíveis comportamentos -->

| Atributo        | Tipo     | Valor padrão | Descrição                          |
| --------------- | -------- | ------------ | ---------------------------------- |
| max-date        | data     | now()        | Enim excepteur laborum cillum quis |
| min-date        | data     | now()        | Consectetur ea eu sunt aliquip     |
| after-selection | function | undefined    | Anim cillum nulla excepteur sit    |

### Estados

<!-- Propriedades que são relacionadas ao estado do componente devem ficar nessa seção e não nas de cima -->

| Estado     | propriedade | Valores    |
| ---------- | ----------- | ---------- |
| Desativado | disabled    | true/false |
| Focado     | foucused    | true       |

## Instanciação do Componente

### HTML

Incluir o arquivo **core.min.js** ao final da tag `<body>`.

### Javascript

Informar os seguintes parâmetros:

- Nome da classe
- Objeto referenciando a raiz do componente DOM

<!-- Usar aqui 1 dos exemplos de instanciação -->

### Informações adicionais (Opcional)

<!-- Informações adicionais sobre javascript, exemplo Função de Callback, Uso alternativo etc. -->

## Dependências

<!-- Caso o componente dependa de algum outro ou de algo externo, listamos aqui colocando links para a documentação da dependência -->

- [Button](/ds/components/button)
- [Input](/ds/components/input)
- [Select](/ds/components/select)
- [Popper.js](https://popper.js.org/)
```
