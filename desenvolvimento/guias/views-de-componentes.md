---
title: Views de componentes
description: Padrão de views de componentes
date: 08/10/2020
keywords: padrão, views, componente
---

1. Não é necessário colocar explicações nas views. A documentação dev já tem essa função!
1. **Em celulares a leitura é vertical**. Caso faça sentido colocar exemplos lado a lado pode-se usar grid no celular também
1. **Em tablet, desktop ou tv a leitura pode ser horizontal desde-que faça sentido**, caso contrário deve-se manter a visualização vertical
1. **Cada bloco de exemplo deve possuir um título**. O título é representado pela tag `<p>` com a formatação da classe `.h5`
1. **Cada bloco de exemplo deve ser espaçado pela classe** `.mt-5`. Não usar `<hr>`, pois pode gerar confusão com componentes que possuam borda
1. Caso um bloco tenha mais de um exemplo interno eles devem ser espaçados com `.mt-3`
1. Nos ajustes de breakpoints, zerar os espaçamentos verticais para elementos que viraram horizontais, por exemplo `class="mt-5 mt-sm-0"`

Veja o exemplo a seguir:

```javascript
// Obrigatório!
// Necessário para estender o template padrão de componentes
extends ../../layouts/component

// Obrigatório!
// Use este bloco para criar variáveis usadas na view
// A variável title aplica o title da view
append vars
  - let title = "nome do componente";

// Obrigatório!
// O conteúdo da view deve ser escrito neste bloco
append content
  // titulo do bloco de exemplo
  p.h5 Titulo componente
  +mixinComponente
  // opcional - outro exemplo da mesma categoria
  .mt-3
    +mixinComponente

  // exemplo de outros blocos lado a lado em tablet
  .row
    .col-sm-6.col-lg-4.col-xl-3
      // titulo com espaçamento vertical de 5. Ele é zerado a partir de tablet
      p.h5.mt-5.mt-sm-0 Outro titulo componente
      +mixinComponente
      // opcional - outro exemplo da mesma categoria
      .mt-3
        +mixinComponente
    .col-sm-6.col-lg-4.col-xl-3
      p.h5.mt-5.mt-sm-0 Outro titulo componente
      +mixinComponente

// Opcional
// Use somente caso precise criar CSS para a view
append style
  style.
    .classe {
      propriedade: valor;
    }

```
