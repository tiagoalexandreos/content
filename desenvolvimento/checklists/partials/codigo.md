---
title: "Requisitos de implementação"
hidden: true
draft: true
---

```markdown
[ ] Construído de forma otimizada com separação de responsabilidades
[ ] Uso correto dos mixins
[ ] Uso correto das classes utilitárias
[ ] Componentes já criados foram reutilizados (sem redundância)
[ ] Os tokens foram consumidos de forma correta e otimizada
[ ] Código tem comentários explicando o funcionamento das partes mais complexas

Padrões
    [ ] As regras de estilos seguem o padrão definido no GOVBR-DS na página construção de artefatos
    [ ] Javascript segue a arquitetura/padrões definidos na página construção de artefatos
    [ ] O código pug está seguindo o padrão definido na página construção de artefatos?

Design

    [ ] Os tipos, variações e comportamentos definidos na documentação de design foram todos implementados
        - Caso contrário, quais exemplos faltam ser implementados?
    [ ] Atende as especificações visuais de Design
    [ ] Atende as especificações comportamentais de Design
    [ ] A anatomia do artefato segue exatamente o que está previsto na diretriz de design
    [ ] Os exemplos foram implementados seguindo todas os aspectos de usabilidade definidos pela equipe de design
```
