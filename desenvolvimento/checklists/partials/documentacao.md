---
title: "Requisitos de documentação"
hidden: true
draft: true
---

```markdown
[ ] Segue o padrão de documentação
[ ] Condiz com o código implementado
[ ] Descreve todas as classes, atributos, opções e variações implementadas
    - O componente deve ser totalmente documentado conforme foi implementado.
    - Se por exemplo um botão tem 4 tipos possíveis, todos os tipos devem estar descritos na documentação.
[ ] Funcionalidades estão documentadas e com explicações de funcionamento
    - Caso a explicação textual não seja suficiente, é possível que exemplos (imagem, código, etc...) sejam incluídos
[ ] O changelog foi atualizado para refletir as alterações feitas
```
