---
title: "Requisitos de qualidade"
hidden: true
draft: true
---

```markdown
[ ] Nenhum impacto foi identificado em outra parte do projeto
[ ] Caso o projeto seja utilizado em outro (um quickstart por exemplo) a alteração foi testada nesse contexto
[ ] Passou por uma revisão por parte dos designers
[ ] Problemas encontrados pelo designer foram corrigidos

Testes/Lints
    [ ] Passou nos testes de responsividade (múltiplas resoluções)
    [ ] Passou nos testes de acessibilidade
    [ ] Passou nos testes de unitários
    [ ] Passou nos testes de regressão visual (cypress e backstop)
    [ ] Passou nos testes de integração
    [ ] O artefato funciona bem com conteúdos de diversos tamanhos
    [ ] Nenhum erro é apontado pelos lints

Acessibilidade

    [ ] Regras de acessibilidade indicadas na documentação de design
    [ ] Segue as regras de semântica indicadas pela W3C
    [ ] Segue as regras de acessibilidade quanto ao uso do wai-aria
    [ ] Possível utilizar o artefato apenas com o teclado
    [ ] Possível utilizar o artefato em dispositivos móveis
    [ ] Funciona em diferentes browsers
    [ ] Funciona corretamente com leitores de tela

Build

    [ ] Nenhum arquivo de build ou instalação foi commitado (build, node_modules, public...)

Commit

    [ ] Os commits estão seguindo o padrão do projeto
```
