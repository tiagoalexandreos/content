---
title: Publicação
description: Checklist de publicação
date: 17/09/2021
keywords: checklist, publicação
---


```markdown
- [ ] Temos uma versão estável para ser publicada?
- [ ] Os componente não apresentam nenhum problema de responsividade?
- [ ] Os componente não apresentam nenhum problema nas funcionalidades?
- [ ] Os componentes não apresentam nenhum problema de integração com outro componente?
- [ ] Os testes visuais estão refletindo as mudanças esperadas?
- [ ] Changelog de desenvolvimento foi gerado corretamente?
- [ ] Changelog de design foi gerado?
- [ ] As releases de design publicadas correspondem aos componentes implementados?
- [ ] Os uikits foram publicados?
- [ ] A página de novidades foi atualizada?
- [ ] As versões dos itens de download foram atualizadas?
```

## Publicação no ambiente de Produção

```markdown
- [ ] Publicação foi autorizada pelo Product Owner
```

## Após a implantação

```markdown
- [ ] Fazer merge dos *hot-fix* com a branch *main*
```
