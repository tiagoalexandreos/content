---
title: Implementação
description: Requisitos usados durante a implementação
keywords: checklist, implementação
---

{{% notice note %}}
Antes de iniciar uma atividade, avalie os itens abaixo e tenha certeza de que você tem todas as informações necessárias para iniciar os trabalhos. Caso precise de mais informações, solicite ao time/responsável!

- Faça um planejamento do que precisa ser feito antes de iniciar a tarefa;
- Observe os guias e requisitos antes de começar a codificação;
- Sempre que tiver uma dúvida comunique ao time nas reuniões diárias ou mesmo solicite ajuda diretamente a alguém.
{{% /notice %}}

## Implementação

{{% include "/desenvolvimento/checklists/partials/codigo.md" %}}

{{% notice tip %}}
Links de apoio:

- [Padrão de Views de Componentes]({{% relref "/desenvolvimento/guias/views-de-componentes" %}} 'Padrão de Views de Componentes')
{{% /notice %}}

## Qualidade

{{% include "/desenvolvimento/checklists/partials/qualidade.md" %}}

## Documentação

{{% include "/desenvolvimento/checklists/partials/documentacao.md" %}}

{{% notice tip %}}
Links de apoio:

- [Padrão de Documentação]({{% relref "/content/desenvolvimento/guias/documentacao-de-desenvolvedor.md" %}} 'Padrão de documentação') para mais informações.
{{% /notice %}}
