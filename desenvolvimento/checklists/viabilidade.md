---
title: Viabilidade
description: Checklist de viabilidade
date: 26/05/2021
keywords: checklist, viabilidade
---

```markdown
- [ ] Componente enviado pelo designer para análise
- [ ] Tem designer "responsável" designado
- [ ] Foram definidos:
    - [ ] Anatomia
    - [ ] Estados
    - [ ] Densidade alta
    - [ ] Densidade média
    - [ ] Densidade baixa
    - [ ] Tipos/Variações
    - [ ] Elementos obrigatórios
    - [ ] Elementos opcionais
    - [ ] Comportamentos
    - [ ] Tokens (tokens não são variáveis)
    - [ ] Foi realizado Teste de conteúdo?
    - [ ] Foi tratado nas múltiplas resoluções?
- [ ] Contém exemplos
- [ ] Foi atualizado o release notes?
- [ ] A diretriz de Design está clara o suficiente para implementação?
- [ ] Levantamento de impactos
    - [ ] Elencar impactos
    - [ ] Tratar impactos
```

{{% notice tip %}}
Links de apoio:

- [Levantamento de Impactos]({{% relref "/design/checklists/levantamento-impactos" %}} 'Levantamento de Impactos')
{{% /notice %}}

Checklists desejáveis (não travam a análise, mas é interessante avisar aos designers para observá-los nas próximas tarefas)

```markdown
- [ ] Regras de acessibilidade foram observadas
- [ ] Asset está preparado para aceitar temas
- [ ] Exemplos de interação com outros assets
- [ ] Animações/motion
    - [ ] Comportamento detalhado
    - [ ] GIF de exemplo
- [ ] Interações via teclado
```
