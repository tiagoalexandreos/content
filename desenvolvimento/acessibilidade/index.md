---
title: Acessibilidade
description: Um tutorial básico para desenvolvimento de componentes mais acessíveis
pre: '<i class="fab fa-accessible-icon"></i> '
date: "23/03/2020"
keywords: acessibilidade, tela, validação, acessíveis, pessoas, deficiência, w3c, semântica, acessível, emag, accessibility
accessibilty: true
---

## Introdução

O Padrão Digital de Governo foi pensado como um Design System para atender as necessidades do Governo. Sendo assim é imprescindível que os componentes que o compõem sejam acessíveis e atendam as normas do [eMAG](http://emag.governoeletronico.gov.br/) e [WCAG](https://www.w3.org/WAI/standards-guidelines/wcag/)visando o desenvolvimento de sites e portais cada vez mais acessíveis objetivando promover uma maior inclusão digital e acesso à informação para a população brasileira.

Esse documento visa orientar os Desenvolvedores do Design System a desenvolverem componentes acessíveis e de fácil utilização pela maioria dos meios de acesso - Browsers, Leitores de Tela, dispositivos móveis, etc.

Primeiramente vamos falar um pouco sobre algumas deficiências, dificuldades físicas e/ou intelectuais que impactam diretamente no acesso às informações. Isso é importante para conscientizar e motivar os desenvolvedores a entender e pensar cada vez mais nessas dificuldades e avaliar se os componentes estão realmente acessíveis.

Será apresentado também algumas ferramentas que facilitam os testes e depuração dos problemas apresentados, inclusive, documentações que esclarecem o que cada erro impacta e como deve ser solucionado.

Essa é uma versão inicial e deverá ser atualizada constantemente visando promover novas técnicas e ferramentas que facilitem a implementação de acessibilidade.

## Principais deficiências e dificuldades

A acessibilidade tem como objetivo diminuir e até mesmo dirimir as dificuldades encontradas para acessar, navegar, fazer compras, obter serviços, aproveitar entretenimentos, através da internet(seja acesso por computador, celular e tablets). Dirigidas principalmente às pessoas que possuem algum tipo de necessidade e/ou deficiência, podendo essas serem momentâneas ou permanentes. Podendo ser listado como pessoas com dificuldade de locomoção, paralisias, baixa visão, cegueira, surdez, daltonismo, dislexia, deficiência intelectual e autismo.

As principais dificuldades de cadas grupo variam de acordo com suas necessidades:

### Dificuldades no uso de mouse e/ou teclado

Quem possui dificuldade de locomoção e/ou paralisia, necessitará de áreas clicáveis maiores, precisará também que a navegação não dependa só do mouse, tudo que o mouse fizer, deverá ser feito via teclado também ou software de voz para executar os comandos [(https://www.essentialaccessibility.com/](https://www.essentialaccessibility.com/)).

### Dificuldades de visão e/ou auditivas

As pessoas com baixa visão, cegueira e daltônicos, necessitarão de descrições alternativas nas imagens(usar o atributo alt, legends em tabelas, tag figure, Figcaption e atributos arias também), ter um contraste mínimo entre as cores segundo o [WCAG 2.1](http://www.w3.org/TR/WCAG21/), ter uma audiodescrição(recurso que traduz as imagens em palavras para pessoas entenderem o vídeo ou o que acontece na peça de teatro) de vídeo visual e as informações dos sites não depender apenas de cores para se diferenciar das demais partes do site.

Já as pessoas com deficiência auditiva ou surdas precisam de legendas e libras, visto que o deficiente auditivo normalmente é oralizado e alfabetizado na língua portuguesa, já 80% dos surdos do mundo não compreendem a língua escrita, sendo que estes aprendem a língua de sinais e no Brasil usa-se a LIBRA(Língua Brasileira de Sinais).

### Dificuldades intelectuais

Quanto a deficiência intelectual e dislexia(transtorno específico de aprendizagem de origem neurobiológica, caracterizada por dificuldade no reconhecimento preciso e/ou fluente da palavra, na habilidade de decodificação e em soletração) é preciso que as informações do site sejam feitas de forma concisa(frase mais curtas e na ordem direta Sujeito → Predicado → Complemento), evitar usar termos técnicos(se for necessário, explique o seu significado), alinhar texto à esquerda, evitar texto centralizado ou justificado, usar os títulos(H1...H6) de forma semântica e se possível não utilizar textos e títulos em caixa alta.

O indivíduo com autismo, não é considerado como deficiente, mas sim alguém que tem síndrome comportamental ([https://docs.wixstatic.com/ugd/85fd89_a9baa902e9c94ce5b8b19e4072baf46a.pdf](https://docs.wixstatic.com/ugd/85fd89_a9baa902e9c94ce5b8b19e4072baf46a.pdf)) nesta cartilha de parceria do governo e microsoft está a definição de autismo.

Classificados pelos estudiosos em dois grupos, como de alto e baixo rendimento. Eles têm os sentidos aguçados, então é recomendado evitar cores fortes e contrastante nos sites, prefira cores suaves e tons mais agradáveis a visão. Este vídeo demonstra a experiência de um autista passeando no shopping([https://www.youtube.com/watch?v=F96H1xE1s4s](https://www.youtube.com/watch?v=F96H1xE1s4s)

## Atributo Alt

O atributo **alt** é vinculado a *imagens* e é usado por leitores de tela e também se a imagem não for carregada na página por algum motivo.

**Deve** {{< emoji ":heavy_check_mark:" >}}:

1. Ser preenchido apenas se a imagem fizer sentido no contexto do site (explica algo)
    - Para fazer com que os leitores ignorem imagem podemos usar o atributo vazio **alt** ou **alt=""**
1. O texto deve explicar com detalhes suficientes para que a pessoa entenda o conteúdo da imagem.

**Não deve** {{< emoji ":x:" >}}:

1. Conter a palavra **imagem**, **gráfico**, **foto** ou qualquer outra palavra similar

- Ex: "Imagem de um celular", "Foto de montanha", "Gráfico X"

[Vídeo para mais informações sobre Alt e Lang](https://www.youtube.com/watch?v=5FJJuEVt5sA)

## Atributo Lang

Para que leitor de tela consiga ler os textos usando o idioma correto é necessário colocar no topo da página o atributo **lang="pt-br"**.

Caso algum termo ou parte esteja em outro idioma é necessário colocar o **lang** específico para aquele texto.

[Vídeo para mais informações sobre Alt e Lang](https://www.youtube.com/watch?v=5FJJuEVt5sA)

## Atributo role

O atributo role serve para definir um papel aos elementos no nosso código. Exemplo: Para que uma *div* seja interpretada como um botão pelo leitor de tela:

```html
<div role="button"></div>
```

[Link com uma lista de roles WAI-ARIA](https://www.w3.org/TR/using-aria/#aria-roles)

## Escondendo Elementos

Propriedades/atributos que escondem o conteúdo na tela também vão esconder das tecnologias assistivas.

Exemplo:

1. visibility: hidden
1. display: none
1. hidden do HTML5

## Ferramentas e sites para testar e melhorar a acessibilidade em sites

Entre as ferramentas que podem ser usadas para testar e melhorar a acessibilidade de um componente, temos as seguintes:

### Softwares leitores de Tela

- [NVDA](https://www.nvaccess.org/) (Gratuito, código aberto, Windows)
- [Orca](https://help.gnome.org/users/orca/stable/introduction.html.pt_BR) (Gratuito, código aberto, Linux)
- [Jaws](https://support.freedomscientific.com/Downloads/JAWS) (Shareware, Windows)

Para maiores informações a respeito dos leitores de tela acessar o [documento do eMAG](https://www.gov.br/governodigital/pt-br/acessibilidade-digital/emag-descricao-dos-leitores-de-tela.pdf) que detalha essas e outras ferramentas.

### Extensões para Browsers

- [axe - Web Accessibility Testing](https://chrome.google.com/webstore/detail/axe-web-accessibility-tes/lhdoppojpmngadmnindnejefpokejbdd?hl=pt-BR)
- [WAVE Evaluation Tool](https://chrome.google.com/webstore/detail/wave-evaluation-tool/jbbplnpkjmmeebjpijfedlgcdilocofh/related?hl=pt-BR)
- [NoCoffee Vision Simulator](https://addons.mozilla.org/pt-BR/firefox/addon/nocoffee/)
- [Accessibility Insights for Web](https://chrome.google.com/webstore/detail/accessibility-insights-fo/pbjjkligggfmakdaogkfomddhfmpjeni/related?hl=pt-BR)

### Sites e Ferramentas Online

- [https://www.w3.org/WAI/](https://www.w3.org/WAI/)
- [https://developer.mozilla.org/en-US/docs/Web/Accessibility](https://developer.mozilla.org/en-US/docs/Web/Accessibility)
- [https://dequeuniversity.com/rules/axe/3.5/](https://dequeuniversity.com/rules/axe/3.5/)
- [https://blog.elo7.dev/wai-aria-apanhado-geral/](https://blog.elo7.dev/wai-aria-apanhado-geral/)
- [https://a11y-style-guide.com/style-guide/](https://a11y-style-guide.com/style-guide/)
- [https://webaim.org/projects/screenreadersurvey8/](https://webaim.org/projects/screenreadersurvey8/) - Site que mostra o uso da ferramenta e quais navegadores mais usados

## Etapas para testar a acessibilidade de um componente

Para garantir que um componente esteja acessível é necessário seguir algumas etapas de teste nas quais iremos listar abaixo.

### Testar os Padrões WEB W3C

Para criar um componente acessível é necessário inicialmente que o código escrito esteja seguindo os padrões Web Internacionais da W3C. É necessário que a sintaxe do código esteja escrita corretamente e além disso é imprescindível que o código esteja organizado de forma semântica - cada tag HTML deve ser utilizada de acordo com o seu significado, valor e propósito.

Escrever um código correto é muito importante para que os diversos dispositivos como navegadores, leitores de tela, dispositivos móveis (celulares, tablets, etc.) ou agentes de software (mecanismos de busca ou ferramentas de captura de conteúdo) possa interpretar o código da maneira correta.

Códigos que não estão de acordo com os padrões W3C podem apresentar comportamentos inesperados e em sua maioria impedem ou dificultam a acessibilidade do conteúdo.

Para garantir que nosso componente está seguindo os padrões da W3C vamos testar o nosso código HTML e CSS seguindo os passos a seguir.

#### Validar o código HTML

Gerar o código HTML do componente e utilizar o [W3C Validator](https://validator.w3.org/) para realizar os testes de escrita de código a fim de identificar e resolver os problemas de sintaxe e semântica do código.

#### Validar o código CSS

Gera o código CSS compilado do componente e utilizar o [W3C CSS Validator](https://jigsaw.w3.org/css-validator/#validate_by_input) para realizar os testes e resolver possíveis problemas de sintaxe do código.

### Testar os padrões de Acessibilidade WCAG 2.1

Nesse passo iremos testar o nosso componente utilizando algumas ferramentas de testes automatizados que já foram apresentadas na seção 3.

Essas ferramentas, em sua maioria, realizam testes de acessibilidade seguindo os padrões [WCAG 2.1](https://www.w3.org/TR/WCAG21/) e ajudam a determinar se um componente atende ou não as recomendações de acessibilidade, gerando um relatório de erros com um link informado as diretrizes de cada teste e como corrigí-los.

#### Exemplo de teste utilizando a ferramenta de validação automática [axe - Web Accessibility Testing](https://chrome.google.com/webstore/detail/axe-web-accessibility-tes/lhdoppojpmngadmnindnejefpokejbdd?hl=pt-BR)

Neste exemplo vamos testar o Componente Select:

![Componente select](imagens/componente-select.png)

1. Clicar com o botão direito do mouse em uma área branca do componente e escolher a opção &quot;inspecionar&quot;, a janela do Chrome Dev Tools será aberta.
1. Clicar na aba &quot;axe&quot;.
1. Clicar no botão &quot;Anayze&quot; para realizar o teste automatizado.

![Inspecionar o componente select axe](imagens/inspecionar-componente-select-axe.png)

1. Ao clicar em &quot;Analyze&quot; a tela abaixo será exibida e os erros apresentados. Nesse exemplo removemos o texto da tag label do Select para simular um problema na categoria &quot;Form elements must have labels&quot;.
1. Você pode clicar em &quot;Highlight&quot; para destacar na tela qual o elemento que está apresentando o problema. É possível também clicar em &quot;Inspect Node&quot; para exibir o código html que apresenta o problema.
1. Clicando em &quot;see more details&quot; serão apresentados as tags de problemas encontrados.
1. Aqui temos uma breve descrição do problema informando que um elemento de formulário precisa ter um &quot;Label&quot;.
1. Nesse ponto é informado o elemento que infringiu a acessibilidade.
1. Essa seção dar dicas rápidas de como o problema poderá ser resolvido.
1. Se quiser ver detalhes mais completos do problema clique no link [More Info](https://dequeuniversity.com/rules/axe/4.0/label?application=AxeChrome).

![Inspecionar o componente select axe-anayze](imagens/inspecionar-componente-select-axe-anayze.png)

Nesse problema que criamos para simular um erro de acessibilidade removemos o texto da tag &quot;Label&quot; do código. Vamos inspecionar o código para conferir o problema clicando no link inspect (2):

![Inspecionar o componente select axe-anayse-inspect-code](imagens/inspecionar-componente-select-axe-anayze-inspect-code.png)

Observe que o elemento input sem label é destacado em cinza. Nesse caso precisamos colocar o texto no campo label para que o problema seja resolvido:

![Inspecionar o componente select axe-anayze-inspect-code-corrigido](imagens/inspecionar-componente-select-axe-anayze-inspect-code-corrigido.png)

O Axe informa várias soluções que podem ser implementadas para resolver o problema (6). Precisamos analisar qual a que melhor se encaixa no contexto do componente desenvolvido.

Ao analisar novamente clicando no link &quot;Run Again&quot; (1) verificamos que o problema desapareceu da lista. Os demais erros destacados na imagem abaixo podem ser ignorados no contexto desse componente isolado, já em um contexto de uma página html completa ou de um template eles devem ser analisados e resolvidos.

![Inpecionar o componente select axe-anayze-inspect-code-issue](imagens/inspecionar-componente-select-axe-anayze-inspect-code-issues.png)

#### Validação Manual

É preciso salientar que, apesar de tornarem a avaliação de acessibilidade mais rápida e menos trabalhosa, as ferramentas de validação automática como o Axe por si só não determinam se um componente está ou não acessível. Para uma melhor avaliação, é necessária uma posterior validação manual.

A validação manual é necessária porque nem todos os problemas de acessibilidade de um componente são detectados mecanicamente pelos validadores. Para a validação manual, são utilizados checklists de validação humana.

Validadores automáticos conseguem detectar se o atributo para descrever imagens foi utilizado em todas as imagens do sítio, mas somente uma pessoa poderá verificar se a descrição da imagem está adequada ao seu conteúdo.

Para realizar uma validação manual efetiva, o desenvolvedor deverá ter conhecimento sobre as diferentes tecnologias, as barreiras de acessibilidade enfrentadas por pessoas com deficiência e as técnicas ou recomendações de acessibilidade.

A validação manual deve ser feita preferencialmente com dispositivos de tecnologia assistiva como leitores de tela (Ver seção 3.1). Deve-se percorrer toda página apenas utilizando teclado, verificando comportamentos, atalhos, folhas alternativas de contraste, se os textos alternativos estão descritos de acordo com a imagem e seu contexto, entre outros.

Listas com os itens a serem testados (checklists) na validação humana podem ser encontradas em [https://www.gov.br/governodigital/pt-br/acessibilidade-digital/material-de-apoio](https://www.gov.br/governodigital/pt-br/acessibilidade-digital/material-de-apoio).

Deve-se lembrar que após cada teste, os ajustes devidos devem ser feitos e novamente testados.

Assim, os pontos sugeridos para a avaliação de acessibilidade em componentes e templates são os seguintes:

| **Pontos de Checagem**                                                                                                                                   | **PVA\*** | **Esperado** |
| -------------------------------------------------------------------------------------------------------------------------------------------------------- | --------- | ------------ |
| Foram respeitados os Padrões Web HTML?                                                                                                                   | Sim       | Sim          |
| Foram respeitados os Padrões Web CSS?                                                                                                                    | Sim       | Sim          |
| Foi utilizado CSS(s) in-line?                                                                                                                            | Sim       | Não          |
| Foi utilizado CSS(s) interno?                                                                                                                            | Sim       | Não          |
| Foi utilizado JavaScript(s) in-line?                                                                                                                     | Sim       | Não          |
| Foi utilizado JavaScript(s) interno?                                                                                                                     | Sim       | Não          |
| Os elementos html são utilizados de forma semântica?                                                                                                     | Não       | Sim          |
| Os níveis de cabeçalhos foram utilizados?                                                                                                                | Sim       | Sim          |
| A hierarquia dos níveis de título está correta?                                                                                                          | Sim       | Sim          |
| Os níveis de títulos foram utilizados para elementos que não são títulos reais no conteúdo?                                                              | Não       | Não          |
| A navegação por teclado utilizando a tecla TAB segue a ordem lógica do conteúdo da página?                                                               | Não       | Sim          |
| A página pode ser lida corretamente sem o uso das marcações de estilo?                                                                                   | Não       | Sim          |
| As teclas de atalhos funcionam corretamente?                                                                                                             | Não       | Sim          |
| O foco do teclado fica travado/bloqueado em algum elemento?                                                                                              | Não       | Não          |
| Há funcionalidade que só funciona pelo mouse                                                                                                             | Sim       | Não          |
| As janelas modais são acessíveis por teclado e/ou leitor de tela?                                                                                        | Não       | Sim          |
| O itens de menu e submenus são acessíveis por teclado?                                                                                                   | Não       | Sim          |
| O itens de menu e submenus são acessíveis por leitor de tela?                                                                                            | Não       | Sim          |
| As imagens possuem declaração do atributo alt?                                                                                                           | Sim       | Sim          |
| As imagens que representam informações possuem descrição adequada do seu conteúdo?                                                                       | Não       | Sim          |
| As imagens possuem descrições comuns (&quot;figura&quot;, &quot;imagem&quot;, &quot;alt&quot;, &quot;descrição&quot;, &quot;nome do arquivo&quot;, etc)? | Sim       | Não          |
| As imagens decorativas são ignoradas pelo leitor de tela?                                                                                                | Não       | Sim          |
| Imagens diferentes possuem a mesma descrição?                                                                                                            | Sim       | Não          |
| Há imagens que são lidas duas ou mais vezes, devido a sua descrição e o title serem iguais?                                                              | Sim       | Não          |
| As tabelas possuem título e resumo ?                                                                                                                     | Sim       | Sim          |
| As tabelas possuem células associadas?                                                                                                                   | Sim       | Sim          |
| Há informações confusas, erros ortográficos, palavras em outro idioma sem tradução e uso da propriedade lang?                                            | Não       | Não          |
| Há uso de texto com alinhamento justificado?                                                                                                             | Sim       | Não          |
| O espaçamento é igual ou superior a 1.5?                                                                                                                 | Não       | Sim          |
| Quando ocorre o redimensionamento, ocorre perda de funcionalidade?                                                                                       | Não       | Não          |
| Quando ocorre o redimensionamento, apenas uma parte dos elementos é ampliada?                                                                            | Não       | Não          |
| Quando ocorre o redimensionamento, há inserção de barras de rolagem?                                                                                     | Não       | Não          |
| O recurso de redimensionamento de texto permite que o texto chegue a ser 200% do tamanho original?                                                       | Não       | Sim          |
| Há destaque do foco do elemento ativo?                                                                                                                   | Não       | Sim          |
| O destaque dado ao elemento com foco possui contraste suficiente de acordo com o [eMAG](http://emag.governoeletronico.gov.br/)?                          | Não       | Sim          |
| Os botões de imagem possui descrição adequada?                                                                                                           | Não       | Sim          |
| Os campos de formulário possuem label associado?                                                                                                         | Sim       | Sim          |
| O formulário está com ordem de navegação adequada?                                                                                                       | Não       | Sim          |
| Ocorre alteração automática de contexto sem que o usuário tenha conhecimento?                                                                            | Não       | Não          |

\* Passível de Verificação Automática

## Conclusão

Com esse conhecimento podemos construir um componente mais acessível a todos. É importante que todos os desenvolvedores se conscientizem que estamos desenvolvendo um projeto para atender a toda a população Brasileira e quanto mais pessoas possam ser contempladas melhor esse objetivo será alcançado.

Após esses testes seria importante ainda um teste real com pessoas com deficiência e/ou as dificuldades apresentadas nesse documento. Sabemos que isso é bem difícil de se conseguir mas, usando as ferramentas citadas podemos chegar a um ambiente de testes mais próximo da realidade.

Essa é uma versão inicial e esse documento deverá ser atualizado e melhorado constantemente.

Fique a vontade para submeter propostas de melhorias de acordo com suas experiências de testes de acessibilidade.
