---
title: "Acessibilidade"
chapter: true
weight: 1
pre: '<i class="fab fa-accessible-icon"></i> '
noCollapse: true
---

<!--
A listagem da página é gerada dinamicamente.
 - Para incluir insira o metadado `accessibilty: true` no frontmatter do documento.
 - Para remover basta apagar o metadado.
 - Veja exemplo no arquivo <content/desenvolvimento/acessibilidade/index.md>
-->

Um dos princípios que norteiam e fundamentam o Design System é o princípio da Acessibilidade na WEB.

A Acessibilidade é um requisito essencial de qualidade de software que é imprescindível para a construção
de um produto digital de Governo a ser consumido por todas as pessoas na WEB, em particular os cidadãos e cidadãs, seja por meio de um ambiente desktop tradicional ou seja em um ambiente mobile em suas diversas opções.

A presença de Acessibilidade na WEB beneficia positivamente a todas as pessoas, sejam elas pessoas não deficientes, pessoas com deficiência temporária ou permanente, idosos, leigos no uso de recursos digitais ou até mesmo os analfabetos funcionais (pessoas com baixo letramento que apresentam incapacidade de interpretar textos, por exemplo).

A ausência de Acessibilidade, leva a perda de autonomia das pessoas que apresentam estas limitações de uso, e que precisam contar com o apoio e da boa vontade alheia para executarem tarefas simples e essenciais para elas.

Um site ou Portal acessível beneficia o uso e a independência de pessoas com baixa visão, cegas, daltônicas, surdas, mobilidade reduzida, deficientes intelectuais entre outros tipos de limitações de acesso.

Para que os designers e os desenvolvedores do Design System possam ter em mente em suas construções de soluções digitais a Acessibilidade, esta WIKI disponibiliza diversas fontes de informações que servem de guias, referências e orientações para a elaboração de produtos digitais acessíveis, são elas:
